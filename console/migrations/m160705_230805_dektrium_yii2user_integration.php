<?php

use dektrium\user\helpers\Password;
use yii\db\Migration;
use yii\db\Schema;

class m160705_230805_dektrium_yii2user_integration extends Migration
{
    private $migrationDir = '@vendor/dektrium/yii2-user/migrations';

    private function exists($table){
        return Yii::$app->db->schema->getTableSchema($table) !== null;
    }

    private function migrate($migrate, $printCommand = true){
        $php = PHP_BINARY;
        $yii = Yii::getAlias('@app').'/../yii';
        $command = "$php $yii $migrate --migrationPath={$this->migrationDir} --interactive=0";
        if ($printCommand) echo "\n$:php yii $migrate --migrationPath={$this->migrationDir}"."\n\n please wait...\n\n";
        echo `$command`;

        /* if you want to display the output at run time as the program goes, you can do this:

        while (@ ob_end_flush()); // end all output buffers if any
        $proc = popen($cmd, 'r');
        while (!feof($proc)){
            echo fread($proc, 4096);
            @ flush();
        } * */
    }

    private function dropTables($tables){
        foreach ($tables as $table)
            if($this->exists($table))
                $this->dropTable("{{%$table}}");
    }

    private function migrationExists($migration){
        return Yii::$app->db
            ->createCommand("select count(*) from migration where version='$migration'")
            ->queryScalar() > 0;
    }

    public function up()
    {
        $this->dropTables(['profile', 'user_auth', 'user_token', 'user', 'role']);
        $this->execute("delete from migration where version='m150214_044831_init_user' or version like '%_apply_amnah_user'");

        $this->migrate('migrate/up');
        $this->addColumn('{{%user}}', 'rel_table', Schema::TYPE_STRING . ' after auth_key');
        $this->addColumn('{{%user}}', 'rel_table_id', Schema::TYPE_INTEGER . ' after rel_table');
        $this->importUsers();
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'rel_table');
        $this->dropColumn('{{%user}}', 'rel_table_id');

        $files = scandir(Yii::getAlias($this->migrationDir), 1);//scan in descending order
        foreach ($files as $file){
            $migration = str_replace('.php', '', $file);
            if($this->migrationExists($migration)){
                $version = substr($migration, 1, 13);
                $this->migrate("migrate/mark $version");
                $this->migrate("migrate/down 1");
            }
        }
    }

    private function importUsers()
    {
       $this->importInstitutionUsers();
       $this->importInnovatorUsers();
        echo "\n";
    }

    private function importInstitutionUsers()
    {
        echo "\nimporting institution users\n";
        /** @var \common\models\LoginInstitution $institutionUser */
        foreach (\common\models\LoginInstitution::find()->all() as $institutionUser){
            echo "\t".$institutionUser->email;
            $user = new \backend\models\InstitutionLogin();
            $user->username = $institutionUser->email;
            $user->email = $institutionUser->email;
            $user->confirmed_at = $institutionUser->created_at;
            $user->rel_table = $institutionUser::tableName();
            $user->rel_table_id = $institutionUser->institution;
            $user->setAttribute('password_hash', Password::hash('password'));
            $user->setAttribute('auth_key', Yii::$app->security->generateRandomString());
            if($user->save()){
                echo " <- ". $institutionUser->name . "\n";
                $user->profile->user_id = $user->id;
                $user->profile->name = $institutionUser->name;
                $user->profile->save();
            }
        }
    }

    private function importInnovatorUsers()
    {
        echo "\nimporting innovator users\n";
        /** @var \frontend\models\LoginInnovator $innovatorUser */
        foreach (\frontend\models\LoginInnovator::find()->all() as $innovatorUser){
            echo "\t".$innovatorUser->email;
            $user = new \frontend\models\InnovatorLogin();
            $user->username = $innovatorUser->email;
            $user->email = $innovatorUser->email;
            $user->confirmed_at = $innovatorUser->created_at;
            $user->rel_table = $innovatorUser::tableName();
            $user->rel_table_id = $innovatorUser->innovator;
            $user->setAttribute('password_hash', Password::hash('password'));
            $user->setAttribute('auth_key', Yii::$app->security->generateRandomString());
            if($user->save()){
                echo " <- ". $innovatorUser->name . "\n";
                $user->profile->user_id = $user->id;
                $user->profile->name = $innovatorUser->name;
                $user->profile->bio = $innovatorUser->gender;
                $user->profile->save();
            }
        }
    }

}
