<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    file_exists(__DIR__ . '/../../common/config/params-local.php') 
        ? require(__DIR__ . '/../../common/config/params-local.php') : [],
    require(__DIR__ . '/params.php'),
    file_exists(__DIR__ . '/params-local.php')
        ? require(__DIR__ . '/params-local.php') : []
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
       'notifications' => [
               'class' => 'machour\yii2\notifications\NotificationsModule',
               // Point this to your own Notification class
               // See the "Declaring your notifications" section below
               'notificationClass' => 'app\models\Notification',
               // This callable should return your logged in user Id
               'userId' => function() {
                   return \Yii::$app->user->identity->rel_table_id;
               }
           ],
		 
        'user' => [
            // following line will restrict access to profile, recovery, registration and settings controllers from backend
            'as backend' => 'dektrium\user\filters\BackendFilter',
            'controllerMap' => [
                'security' => 'backend\controllers\user\SecurityController'
            ],
        ],
    ],
/*	 
	 'as beforeRequest' => [
	     'class' => 'yii\filters\AccessControl',
	     'rules' => [
	         [
	             'actions' => ['login', 'error','user/recovery/request'],
	             'allow' => true,
	         ],
	         [

	             'allow' => true,
	             'roles' => ['@'],
	         ],
	     ],
		  'denyCallback' => function () {
		                  return Yii::$app->response->redirect(['user/login']);
		          },
	 ],
*/	 
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\InstitutionLogin',
            //'enableAutoLogin' => true,
            'identityCookie' => [
                'name'     => '_dpttAdminIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'DPTTADMINSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@common/themes/AdminLTE/views/user'
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        /** Admin LTE: see @url https://github.com/dmstr/yii2-adminlte-asset for details */
        'view' => [
            'theme' => [
                'pathMap' => [
                    //'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                    '@app/views' => '@backend/views'
                ],
            ],
        ],
    ],
    'params' => $params,
];
