<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use backend\models\MilestoneStage;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\MilestoneStageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Milestone Stages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-stage-index">
<div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Milestone Stage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
             'attribute' => 'Milestone',
             'value' => 'milestone'
             ],
             [
             'attribute' => 'Weight',
             'value' => 'milestone_weight'
             ],

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action}{new_action1}',
            'buttons' => [
                'new_action' => function ($url, $dataProvider) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span> ', $url, [
                                'title' => Yii::t('app', 'Delete'),
                    ]);
                },
                'new_action1' => function ($url, $dataProvider) {
                    $weight = MilestoneStage::findOne($dataProvider['milestone_stage_id'])->weight;
                    if(!$weight)
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span> ', $url, [
                                'title' => Yii::t('app', 'Set Milestone Weight'),
                    ]);
                },
            ],
            'urlCreator' => function ($action, $dataProvider, $key, $index) {
                if ($action === 'new_action') {
                    $url = Url::to(['milestone-stage/delete','id'=>$dataProvider['milestone_stage_id'], 'stage'=>$dataProvider['stage_id'],'pipeline'=>$dataProvider['pipeline_id']]);
                    return $url;
                }
                if ($action === 'new_action1') {
                    $url = Url::to(['milestone-stage/update','id'=>$dataProvider['milestone_stage_id']]);
                    return $url;
                }
              }
            ],
        ],
    ]); ?>
        </div>
    </div>
    </div>
</div>
