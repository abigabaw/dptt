<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MilestoneStage */

$this->title = 'Set Milestone Stage Weight: ';
$this->params['breadcrumbs'][] = ['label' => 'Milestone Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="milestone-stage-update">
	<div class="box box-success">
				<div class="box-header bg-success with-border">
	                  <h3 class="box-title"> Set Milestone stage weight for</h3>
	            </div>
	     <div class="box-body">
	     	  <?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
    			<?= $form->field($model, 'weight',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Project Name',]) ?>
                <?php ActiveForm::end(); ?>

	     </div>
	</div>
</div>
