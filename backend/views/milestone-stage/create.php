<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\Milestone;
use backend\models\Stage;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\MilestoneStage */

$this->title = 'Add Milestone to Stage';
$this->params['breadcrumbs'][] = ['label' => 'Milestone Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-stage-create">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title">

                  <?php if(!$stage){
                  echo '';
                  }else{
                    echo 'Add a milestone to '.Stage::findOne(['id'=>$stage])->stage;
                }
                    ?>
                  </h3>
            </div>
            <div class="box-body">
	<?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>

    <?php
    if(!$stage)
    echo $form->field($model, 'stage',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(Stage::find()->all(),'id','stage'),
    ['prompt'=>'Select Stage'] )

    ?>

    <?= $form->field($model, 'milestone')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Milestone::find()->orderBy('milestone ASC')->all(),'id','milestone'),
                        'options' => ['placeholder' => 'Select Milestone (s)'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => true
                        ],
                ]);
    ?>
      <?= Html::submitButton('Add Milestone to Stage', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    
    </div>
</div>
  <?= $this->registerJs(
      ' 
$("#milestonestage-milestone").select2({
    placeholder: \'Select or type a new milestone if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

')?>
