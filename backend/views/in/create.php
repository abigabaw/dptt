<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LoginInnovator */

$this->title = 'Create Login Innovator';
$this->params['breadcrumbs'][] = ['label' => 'Login Innovators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-innovator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
