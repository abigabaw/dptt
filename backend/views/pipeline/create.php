<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\PipelineType;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\Pipeline */

$this->title = 'Create Pipeline';
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pipeline-create">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">Fill in the fields below to create a new Pipeline</h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['id' => 'pipeline-form']); ?>
    <?= $form->field($model, 'name',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter the name of the pipeline',]) ?>
                <?= $form->field($model, 'type',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(PipelineType::find()->all(),'id','type'),
                    ['prompt'=>'Please the Type of Pipeline'] )?>


    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a description of this pipeline',]) ?>

	<div class="form-group">

      <?= Html::submitButton('Create Pipeline', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>