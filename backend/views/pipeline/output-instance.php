<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

$this->title = 'Select Output Instances for Pipeline: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Logical Pipeline'];
$this->params['breadcrumbs'][] = $this->title;
/*
$intervals=$checked=$outcomes=[];
$outcomes['Outcome 1']=[1=>'out1',3=>'out 3'];
$outcomes['Outcome 2']=[4=>'out21',23=>'out223'];

$check[1]=[1,34];
$check[23]=[41,7,1];

$intervals[1]='H1 Y1';
$intervals[2]='H2 Y1';
$intervals[3]='H1 Y2';
$intervals[4]='H2 Y2';
$intervals[111]='H1 Y3';
$intervals[212]='H2 Y3';
$intervals[314]='H1 Y4';
$intervals[411]='H2 Y4';
$intervals[111]='H1 Y5';
$intervals[7]='H2 Y5';
$intervals[73]='H1 Y6';
$intervals[74]='H2 Y6';
$intervals[171]='H1 Y7';
$intervals[272]='H2 Y7';
$intervals[34]='H1 Y8';
$intervals[41]='H2 Y8';
*/

$form = ActiveForm::begin(['id' => 'milestone-form']);
?>
<table class="table table-striped table-bordered" style='width:95%'>
	<thead><tr>
		<th>&nbsp;</th>
<?php $i=1;foreach($intervals as $intervalid=>$intervalname) { $i++;?>
		<th style='font-size:110%'><?= $intervalname?></th>
<?php } ?>
	</tr></thead>

<?php foreach($outcomes as $outcomename=>$outputs) { ?>
	<tr style="background:#00a65a"><td colspan=<?=$i?>><strong style="color:#fff;"><?=$outcomename?></strong></td></tr>
	<?php foreach($outputs as $outputid=>$outputname) { ?>
		<tr><td><?=$outputname?></td>
		<?php foreach($intervals as $intervalid=>$intervalname) { 
			$checked = is_array($check[$outputid]) && in_array($intervalid,$check[$outputid]) ? "checked=checked" : ""; ?>
		<td><input type='checkbox' name=c[<?=$outputid?>][<?=$intervalid?>]  value=<?=$intervalid?> <?=$checked?> /></td>
		<?php }  ?>
	</tr>
	<?php } ?>
	
		
<?php } ?>
</table>
<input type='submit' value='Submit' class="btn btn-primary">
<?php  ActiveForm::end();?>