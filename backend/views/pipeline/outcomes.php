<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;
use yii\helpers\ArrayHelper;
use backend\models\Pipeline;


$this->title = $pipeline_model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pipeline-stages">
<div class="box box-success">
<div class="box-header bg-success with-border">
            <h3 class="box-title">Rearrange Outcomes</h3>
            </div>
            <div class="box-body">
    <?php
            $items = array();
              if( is_array( $dataProvider ) && count( $dataProvider ) > 0 ) {
              $count = 1;
                foreach($dataProvider as $v)
                {
                    $items[$v['id']]['content'] = $count.' - '.$v['outcome'];
                    
                    ++$count;
                }
            $form = ActiveForm::begin();
             
            // Kartiks widget
                echo SortableInput::widget([
                'name'=> 'sort_list',
                'items' => $items,
                'hideInput' => true,
                ]);
            echo Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']);

            ActiveForm::end();
          }else{
            echo 'There are no outcomes in this pipeline. '; 
          }
			 echo  Html::a('Create an Outcome', ['/outcome/create', 'pipeline' => $pipeline_model->id]);
?>

</div>
</div></div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>