<?php
use kartik\growl\Growl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use backend\models\VStage;
use backend\models\VMilestone;
use backend\models\VIndicator;
//use common\models\PipelineIndicatorweight;
use backend\models\Project;
use kartik\select2\Select2;
//use kartik\alert\Alert;
/* @var $this yii\web\View */
/* @var $model backend\models\Pipeline */

$this->title = $model->name. ' Logical Pipeline';
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['pipeline']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  .select2-selection__rendered li{
    width:100%;
  }
  .select2-selection__rendered li input{
    width:100%!important;
  }
</style>
<div class="pipeline-view">
<div class="box box-success">

            <div class="box-body">

    <p>
        <?= Html::a('Update Pipeline Details', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Re-Arrange Outcomes', ['outcomes', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Choose Milestones', ['outputinstance', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Add Project', ['/project/create', 'pipeline' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('View Pipeline', ['view', 'id'=>$model->id,'pipeline' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete Pipeline', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this logical pipeline?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
</div></div>  

          <div class="row">
            <div class="col-md-12">
            <?php 

	/*  foreach (Yii::$app->session->getAllFlashes() as $key => $flash) {	
		  if($key=='success')
					echo Alert::widget([
					    'type' => Alert::TYPE_SUCCESS,
					    'title' => $flash['title'],
					    'icon' => 'glyphicon glyphicon-ok-sign',
					    'body' => $flash['message'],
					    'showSeparator' => true,
					    'delay' => 22000
					]);
            } */
            ?>
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <?= $pipleinetablist?>
                   <li>
                   <?php 
                   Modal::begin([
                        'id' => 'createCompany',
                        // ... any other yii2 bootstrap modal option you need
                        'header' => '<h4 class="modal-title">Add Outcome</h4>',
                        'toggleButton' => ['label' => '<i class="fa  fa-plus"></i> Add Outcome','class'=>'btn btn-primary btn-sm','style'=>'margin-top:3px;'],
                    ]);


                   $form = ActiveForm::begin(['id' => 'cohort-form']);
                        echo $form->field($m_outcome, 'outcome')->textInput();

                         echo '<div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>';
                        ActiveForm::end();
                    Modal::end(); ?>
                   </li>
                  <li class="pull-right header"> Outcomes <i class="fa fa-dedent "></i></li>
                </ul>
                <div class="tab-content">
                  <?= $pipleinetabcontentlist ?>
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
<!-- Large modal -->

<?php foreach($outcomes as $outcome=>$outcomevalue) {?>
<div class="modal fade outcome-model<?=$outcome?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add a New Output to <strong><?=$outcomevalue?></strong></h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'milestone-form']);
                         echo $form->field($m_output, 'output')->textInput();
			echo $form->field($m_output, 'outcome')->hiddenInput(['value'=>$outcome])->label(false);
                        echo '<div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Add New Output</button>
                    </div>';
                        ActiveForm::end();
?>

    </div>
  </div>
</div></div>
<?php } ?>
<div class="modal fade bs-indicator-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add New Indicator </h4>
      </div>
      <div class="modal-body">
      <table class="table table-striped table table-bordered">
                          <tr>
                      <th style="width: 10px">#</th>
                      <th>Indicator</th>
                      <th >Old Weight</th>
                      <th >New Weight</th>
                    </tr>
                    <tbody id="indictor-weights">
                    </tbody>
                  </table>
                  <div class="row" id="milestone-weight-holder" style="border:1px solid green; margin:5px; padding:5px">
                  <div class="col-md-8" id="milestone_text"></div>
                  <div class="col-md-4">
                      <div class="form-group required">
                            <input type="text" id="set-milestone-weight" class="form-control" placeholder="Weight"><p class="help-block help-block-error"></p>
                      </div>
                  </div>
                  <div class="form-group col-md-12">
                  <div class="col-md-8" style="color:#f00" id="error_message">First Set the milestone weight before adding an indicator</div>
                    <div class="col-md-4">
                        <span type="submit" class="btn btn-primary pull-right" id="set_milestone_weight">Set Milestone Weight</span>
                    </div>
                  </div>
                  </div>

<table class="table table-striped table table-bordered">
</tbody>
                    <tr>
                      <td>#</td>
                      <td>      <?php 
                         $form = ActiveForm::begin(['id' => 'indicator-form','action' =>['/pipeline/indicators']]);
                          echo $form->field($m_indicator, 'name')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VIndicator::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Indicators (s)','template' => '{label}New Indicator{input}'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                        ]);?></td>
                      <td>

                      </td>
                      <td><span class="badge bg-yellow">..</span></td>
                    </tr>
                  </tbody>
                </table>

<?php 
              echo '<div class="modal-footer">
                        <input  type="hidden" name="milestone" value="" id="milestone_value"/>
                        <input  type="hidden" name="milestone_weight" value="" id="milestone_weight"/>
                        <input  type="hidden" name="milestone_stage" value="" id="milestonestage"/>
                        <input  type="hidden" name="pipeline" value="'.$model->id.'"/>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <span  class="btn btn-primary" id="calculate_weight">Preview New Weights</span>
                        <button type="submit" class="btn btn-primary">Add New Indicator</button>
                    </div>';
                        ActiveForm::end();?>



    </div>
  </div>
</div></div>


<?php
yii\bootstrap\Modal::begin([
//		'header' => 'Add Indicator',
		'id'=>'editModalId',
		'class' =>'modal',
		'size' => 'modal-lg',
]);
echo "<div class='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(Url::to(['/js/modal.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);

?>

  <?= $this->registerJs(
      " 
    $('.alert').fadeIn('slow').delay(3000).hide(0);
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>
  <?= $this->registerJs(
      ' 
$("#pipelinestage-stage").select2({
    placeholder: \'Select or type a new stage if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

  $("#milestonestage-milestone").select2({
    placeholder: \'Select or type a new milestone if not in list\',
    width: \'100%\' ,
    tags: true,
    maximumSelectionLength: 1,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

$("#staticindicatormilestone-indicator").select2({
    placeholder: \'Select or type a new indicator if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});
$(".milestone_button").click(function(){
    $("#stage_value").val($(this).attr("id"));
});
$(".indicator_button").click(function(){
    $("#milestone_value").val($(this).attr("id"));
    var milestone = $(this).attr("id");
    $.ajax({
        url: "/pipeline/get-indicator-weight",
        type: "post",
        data: {milestone:milestone,pipeline:'.$model->id.'},
        dataType:"json",
        success: function (response) {
          $(".indictor-weights").remove();
          $.each(response, function (i, value) {
            $("#indictor-weights").prepend("<tr class=\"indictor-weights\"><td>"+parseInt((i*1)+1)+"</td><td>"+response[i].name+"</td><td><span class=\"badge bg-red\">"+response[i].weight+"</span></td><td><span class=\"badge bg-red\">...</span></td></tr>");

          });
           // you will get response from your php page (what you echo or print)                 

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
    //alert($("#milestone_value").val());
});
$("#calculate_weight").click(function(){
  var milestone = $("#milestone_value").val();
  var milestoneweight = $("#milestone_weight").val();
  var new_weight = $("#pipelineindicatorweight-weight").val();
  var indicator = $("#staticindicatormilestone-indicator").val();
      $.ajax({
        url: "/pipeline/calculate-weight",
        type: "post",
        data: {milestone:milestone,pipeline:'.$model->id.',indicator:indicator,weight:new_weight,milestoneweight:milestoneweight},
        dataType:"json",
        success: function (response) {
          if(response=="error"){
            $(".indictor-weights").remove();
             $("#indictor-weights").append("<tr class=\"indictor-weights\"><td></td><td><span class=\"badge bg-red\">Error!!!</span></td><td><span class=\"badge bg-red\">Milestone weight has not been set. Make sure it is set before you can add an indicator</span></td><td></td></tr>");
          }else{
              $(".indictor-weights").remove();
              $.each(response, function (i, value) {
                $("#indictor-weights").append("<tr class=\"indictor-weights\"><td>"+parseInt((i*1)+1)+"</td><td>"+response[i].name+"</td><td><span class=\"badge bg-red\">"+response[i].old_weight+"</span></td><td><span class=\"badge bg-red\">"+response[i].weight+"</span></td></tr>");

              });
            }
           // you will get response from your php page (what you echo or print)                 

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }

    });
})
$(".milestone-accordion").click(function(e){
  var milestoneweight = $(this).attr("milestoneweight");
  $("#milestone_weight").val($(this).attr("milestoneweight"));
  $("#milestonestage").val($(this).attr("milestonestage"));
  if(!milestoneweight){
    $("#milestone_text").html($(this).attr("milestonetext"));
    $("#milestone-weight-holder").fadeIn(600);
  }else{
    $("#milestone-weight-holder").fadeOut(600);
  }
})
$("#set_milestone_weight").click(function(){
  $("#error_message").html("Wait!!! Setting milestone weight");
  var miletstonestage = $("#milestonestage").val();
  var weight = $("#set-milestone-weight").val();
  if(!weight){
   $("#error_message").html("Please enter the miletsone weight"); 
  }else{
  $.ajax({
        url: "/milestone-stage/set-milestone-weight",
        type: "post",
        data: {milestone_weight:weight,milestonestage:miletstonestage},
        dataType:"json",
        success: function (response) {
          if(response.status=="success"){
            $("#error_message").html(response.message);
            $("#milestone_weight").val(response.milestone_weight);
          }else{
            $("#error_message").html(response.message);  
          }              
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
           $("#error_message").html("Ooops. Something went wrong."); 
        }

    });
  }
})
');
?>
