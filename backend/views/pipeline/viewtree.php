<?php
	
use yii\widgets\Pjax;
use yii\web\JsExpression;
use execut\widget\TreeView;
use yii\helpers\Url;
use yii\helpers\Html;


$this->title = 'Viewing Pipeline: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">
   <div class="box-body">
    <p>
        <?= Html::a('Update Name', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Rearrange Stages', ['stages', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Table View', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete Pipeline', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>
</div></div>

<?=$addStage?>

<?php 
Pjax::begin([
    'id' => 'pjax-container',
]);

echo \yii::$app->request->get('page');

Pjax::end();

$onSelect = new JsExpression(<<<JS
function (undefined, item) {
    if (item.href !== location.pathname) {
        $.pjax({
            container: '#pjax-container',
            url: item.href,
            timeout: null
        });
    }

    var otherTreeWidgetEl = $('.treeview.small').not($(this)),
        otherTreeWidget = otherTreeWidgetEl.data('treeview'),
        selectedEl = otherTreeWidgetEl.find('.node-selected');
    if (selectedEl.length) {
        otherTreeWidget.unselectNode(Number(selectedEl.attr('data-nodeid')));
    }
}
JS
);

echo TreeView::widget([
    'data' => $data,//$items,
    'size' => TreeView::SIZE_NORMAL,
	 'header' => '',
	    'searchOptions' => [
	        'inputOptions' => [
	            'placeholder' => 'Search Stages/Milestones/Indicators'
	        ],
		  ],
    'clientOptions' => [
        'onNodeSelected' => $onSelect,
		  
    ],
]);

?>
<?=$milestoneModal?>
<?=$indicatorModal?>

<?= $this->registerJs(
      " 
    $('.alert').fadeIn('slow').delay(3000).hide(0);
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>
  <?= $this->registerJs('
$("#pipelinestage-stage").select2({
    placeholder: \'Select or type a new stage if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

  $("#milestonestage-milestone").select2({
    placeholder: \'Select or type a new milestone if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

$("#staticindicatormilestone-indicator").select2({
    placeholder: \'Select or type a new indicator if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});
$(".milestone_button").click(function(){
    $("#stage_value").val($(this).attr("id"));
});
$(".indicator_button").on("click", function (e) {
    alert($(this).attr("id"));
    $("#milestone_value").val($(this).attr("id"));
    //alert($("#milestone_value").val());
});


')?>
