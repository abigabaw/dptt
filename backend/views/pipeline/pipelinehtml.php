<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use backend\models\VStage;
use backend\models\VMilestone;
use backend\models\VIndicator;
		
switch ($ht) {
	case 'AddIndicator':
?>
	<button class="btn btn-primary btn-sm indicator_button" id="<?=$milestone?>" data-toggle="modal" data-target=".bs-indicator-modal-lg" style="z-index:6666"><i class="fa  fa-plus"></i>  Add a new indicator</button>
<?php		
		break;
case 'indicatorlinks':
?>
<a href="<?=Url::to(['static-indicator-milestone/view', 'id' => $milestone])?>"><button class="btn btn-success btn-sm" data-toggle="modal" style="float:right;margin-top:-5px; margin-right:10px;"><i class="fa fa-pencil-square-o"></i>  Edit Indicators</button></a>

<button class="btn btn-primary btn-sm indicator_button" id="<?=$milestone?>" data-toggle="modal" data-target=".bs-indicator-modal-lg" style="float:right; margin-right:5px;margin-top:-4px;"><i class="fa  fa-plus"></i>  Add Indicator</button>
	
<?php	
break;	
case 'outputlinks':
?>
	 <a class="modalButton" href="<?=Url::to(['/output-indicator/indicator', 'output'=>$milestone])?>"><button class="modalButton btn btn-success btn-sm" data-toggle="modal" style="float:right;"><i class="fa fa-pencil-square-o"></i>  Add/Edit Indicators</button></a>
<?php		

		break;
case 'milestonelinks':
?>
 <a href="<?=Url::to(['milestone-stage/view', 'id' => $stage,'pipeline'=>$pipeline])?>"><button class="btn btn-success btn-sm" data-toggle="modal" style=""><i class="fa fa-pencil-square-o"></i>  Edit Milestone</button></a>

 <button class="btn btn-primary btn-sm milestone_button" id="<?=$stage?>" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right;margin-top:-35px;"><i class="fa  fa-plus"></i>  Add Milestone</button>
	
<?php		
		break;
		
		case 'outcomelinks':
		?>
		 <a href="<?=Url::to(['milestone-stage/view', 'id' => $stage,'pipeline'=>$pipeline])?>"><button class="btn btn-success btn-sm" data-toggle="modal" style="float:right;margin-top:-35px; margin-right:120px;"><i class="fa fa-pencil-square-o"></i>  Edit Output</button></a>

		 <button class="btn btn-primary btn-sm milestone_button" id="<?=$stage?>" data-toggle="modal" data-target=".outcome-model<?=$stage?>" style="float:right;margin-top:-35px;"><i class="fa  fa-plus"></i>  Add Output</button>
	
		<?php		
				break;

	case 'AddMilestone':
?>
	<button class="btn btn-primary btn-sm milestone_button" id="<?=$stage?>" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa  fa-plus"></i>  Add a new milestone</button>
<?php		
		break;
	case 'IndicatorModal':
?>
<div class="modal fade bs-indicator-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add New Indicator </h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'indicator-form','action' =>['pipeline/indicators']]);
                          echo $form->field($indicatormilestone, 'indicator')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VIndicator::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Indicators (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                        ]);

              echo '<div class="modal-footer">
                        <input  type="hidden" name="milestone" value="" id="milestone_value"/>
                        <input  type="hidden" name="pipeline" value="'.$model->id.'"/>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add New Indicator</button>
                    </div>';
                        ActiveForm::end();
                                        ?>


    </div>
  </div>
</div>
</div>
<?php		
		break;
case 'milestoneModal':
?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add New Milestone</h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'milestone-form','action' =>['pipeline/milestones']]);
                         echo $form->field($milestonestage, 'milestone')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VMilestone::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','milestone'),
                                                'options' => ['placeholder' => 'Select Milestones (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true
                                                ],
                                        ]);
                                   echo '<div class="modal-footer">
                      <input type ="hidden" name="stage" value="" id="stage_value"/>
                      <input  type="hidden" name="pipeline" value="'.$model->id.'"/>
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Add New Milestone</button>
                    </div>';
                        ActiveForm::end();
                                        ?>



    </div>
  </div>
</div></div>
<?php		
		break;
case 'addStage':
                   Modal::begin([
                        'id' => 'createCompany',
                        // ... any other yii2 bootstrap modal option you need
                        'header' => '<h4 class="modal-title">Add Stage</h4>',
                        'toggleButton' => ['label' => '<i class="fa  fa-plus"></i> Add Stage','class'=>'btn btn-primary btn-sm','style'=>'margin-top:3px;'],
                    ]);


                   $form = ActiveForm::begin(['id' => 'cohort-form']);
                         echo $form->field($pipelinestage, 'stage')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VStage::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','stage'),
                                                'options' => ['placeholder' => 'Select Stage (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true
                                                ],
                                        ]);
                         echo '<div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>';
                        ActiveForm::end();
                    Modal::end();	
		break;
	default:
		# code...
		break;
}
?>