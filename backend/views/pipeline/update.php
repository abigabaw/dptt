<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\PipelineType;
use backend\models\DurationUnit;
use backend\models\DurationInterval;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Pipeline */

$this->title = 'Update Pipeline: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pipeline-update">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['id' => 'pipeline-form']); ?>
    <?= $form->field($model, 'name',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter the name of the portfolio',]) ?>
                <?= $form->field($model, 'type',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(PipelineType::find()->all(),'id','type'),
                    ['prompt'=>'Please the Type of Pipeline'] )?>
    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a description of this portfolio',]) ?>

	<?php if($model->type==2) { ?>
	
		<fieldset>
			<legend> Logical Attributes</legend>
	      <?= $form->field($lmodel, 'duration')->textInput(['type'=>'number']) ?>

         <?= $form->field($lmodel, 'duration_unit',
                         ['options'=>[
                         'tag'=>'div',
                         'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                         'template'=>'{label}{input}{error}{hint}'
                         ])->dropDownList(
             ArrayHelper::map(DurationUnit::find()->all(),'id','descr'),
             ['prompt'=>'- Select -'] )?>

         <?= $form->field($lmodel, 'duration_interval',
                         ['options'=>[
                         'tag'=>'div',
                         'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                         'template'=>'{label}{input}{error}{hint}'
                         ])->dropDownList(
             ArrayHelper::map(DurationInterval::find()->all(),'id','duration_interval'),
             ['prompt'=>'- Select -'] )?>
		</fieldset>
	
	<?php } ?>

	<div class="form-group">
      <?= Html::submitButton('Update Pipeline', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>