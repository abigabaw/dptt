<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LoginInstitution */

$this->title = 'Update Details: ';
$this->params['breadcrumbs'][] = ['label' => $usermodel->name, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="login-institution-update">
<div class="box box-success">
            <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['id' => 'milestone-form']); ?>
                <?= $form->field($model, 'oldpass',
                    ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group fieldcohort-form-name has-feedback required'],
                        'template'=>'{input}{error}{hint}'
                    ])->passwordInput(['placeholder'=>'Enter your old Password',]) ?>
                <?= $form->field($model, 'newpass',
                    ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group fieldcohort-form-name has-feedback required'],
                        'template'=>'{input}{error}{hint}'
                    ])->passwordInput(['placeholder'=>'Enter your new password',]) ?>
                <?= $form->field($model, 'repeatnewpass',
                    ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group fieldcohort-form-name has-feedback required'],
                        'template'=>'{input}{error}{hint}'
                    ])->passwordInput(['placeholder'=>'Repeat new password',]) ?>
		<div class="form-group">
			<?= Yii::$app->session->getFlash('message')?>
		</div>
				<div class="form-group">
			      <?= Html::submitButton('Change Password', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
			    </div>
                <?php ActiveForm::end(); ?>
            </div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#account_nav').addClass('active');")
  ?>