<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LoginInstitution */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-institution-view">
<div class="box box-success">
            <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">

    <p>
        <?= Html::a('Update Your Details', ['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Change Password', ['change-password'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email:email',
            [   'label' =>'Institution',
                 'value' =>$model->institution0->names,
             ],
        ],
    ]) ?>

</div></div></div>
  <?= $this->registerJs(
    " 
    $('#account_nav').addClass('active');")
  ?>