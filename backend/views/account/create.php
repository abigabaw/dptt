<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LoginInstitution */

$this->title = 'Create Login Institution';
$this->params['breadcrumbs'][] = ['label' => 'Login Institutions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-institution-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
