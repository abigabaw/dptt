<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\VAgency;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Grant */

$this->title = 'Create Grant';
$this->params['breadcrumbs'][] = ['label' => 'Grants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grant-create">
<div class="box box-success">
            <div class="box-body">
			<?php $form = ActiveForm::begin(['id' => 'grant-form']); ?>
					 <?= $form->field($model, 'name',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Grant Name',]) ?>

			 <?= $form->field($model, 'agency')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VAgency::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Agency'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => false
                                                ],
                                        ]);?>
		                <?= $form->field($model, 'description',
							                ['options'=>[
							                			'tag'			=>'div',
							                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
							                			'template'		=>'{input}{error}{hint}'
							                ])->textArea(['placeholder'=>'A brief description of the grant',]) ?>
							<div class="form-group">
						      <?= Html::submitButton('Create Grant', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
						    </div>
					<?php ActiveForm::end(); ?>
			</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#grant_nav').addClass('active');

$('#grant-agency').select2({
    placeholder: 'Select or type a new agency if not in list',
    width: '100%' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

    ")
  ?>