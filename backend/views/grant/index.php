<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grant-index">
<div class="box box-success">
    <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Grant', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
            'attribute'=>'Name',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider['name'], ['view', 'id' => $dataProvider['id']]);
                  
            }
            ],
            [
            'attribute'=>'Agency',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider->agency0->name, ['/agency/view', 'id' => $dataProvider['agency']]);
                  
            }
            ],
            'description:ntext',
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
    </div>
</div>
</div>

  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#grant_nav').addClass('active');")
  ?>