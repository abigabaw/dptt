<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model backend\models\Timeline */

$this->title = 'Timeline';
$this->params['breadcrumbs'][] = ['label' => 'Timelines', 'url' => ['/timeline','innovation' => $model->innovation]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeline-view">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> Details</h3>
            </div>
            <div class="box-body">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add Iteration', ['iteration', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [   'label' =>'Innovation',
                 'value' =>$model->innovation0->innovation,
             ],
            [   'label' =>'Milestone',
                 'value' =>$model->milestone0->milestone,
             ],
            [   'label' =>'Created By',
                 'value' =>$model->createdby0->name,
             ]
        ],
        
    ]) ?>

</div>
</div>

<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title">Iterations</h3>
            </div>
            <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'comment',
            'start_date',
            'duration',
             [
             'attribute' => 'Created By',
             'value' => 'createdBy.name'
             ]
        ],
    ]); ?>

</div>
</div>
</div>
