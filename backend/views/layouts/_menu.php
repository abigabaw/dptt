<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Program;
use backend\models\Pipeline;
use backend\models\Queries;

$user_id                  =  Yii::$app->user->identity->rel_table_id;
$indicator_requests       =  count(Yii::$app->db->createCommand(Queries::getIndicatorRequests($user_id))->queryAll());
$repeat_requests          =  count(Yii::$app->db->createCommand(Queries::getIterationRequests($user_id))->queryAll());
$extension_requests       =  count(Yii::$app->db->createCommand(Queries::getExtensionRequests($user_id))->queryAll()); 
?>

<?php
    return [
        'options' => ['class' => 'sidebar-menu'],
	'encodeLabels' => false,
        'items' => [
            ['label' => 'MAIN NAVIGATION', 'options' => ['class' => 'header', 'style'=>'color:#fff']],
            ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/']],
            ['label' => 'MODULES', 'options' => ['class' => 'header', 'style'=>'color:#fff']],
            [
                'label' => 'Project Management',
                'icon' => 'fa fa-laptop',
                'url' => '#',
                'options' => ['class' => 'treeview', 'id'=>'project_tree'],
                'items' => [
                    [
                        'label' => 'Projects', 'icon' => 'fa fa-file-text-o',
                        'url' => ['/project'], 'options' => ['id'=>'project_nav'],
                    ],
                    [
                        'label' => 'Project Teams', 'icon' => 'fa fa-circle-o',
                        'url' => ['/innovator'], 'options' => ['id'=>'project_teams'],
                    ],
                    [
                        'label' => 'Project Admins', 'icon' => 'fa fa-circle-o',
                        'url' => ['/innovator/admin'], 'options' => ['id'=>'project_admins'],
                    ],
                ],
            ],
            [
                'label' => 'Requests',
                'icon' => 'fa fa-lightbulb-o',
                'url' => '#',
                'options' => ['class' => 'treeview', 'id'=>'request_tree'],
                'items' => [
                    [
                        'label' => 'Iteration Requests <small class="label pull-right bg-red">'.$repeat_requests.'</small>','icon' => 'fa fa-circle-o',
                        'url' => ['/milestone/requests'], 'options' => ['id'=>'iteration_requests','encodeLabels'=>false],
                    ],
                    [
                        'label' => 'Indicator Requests <small class="label pull-right bg-red">'.$indicator_requests.'</small>', 'icon' => 'fa fa-circle-o',
                        'url' => ['/indicator/requests'], 'options' => ['id'=>'indicator_requests'],
                    ],
                    [
                        'label' => 'Extension Requests <small class="label pull-right bg-red">'.$extension_requests.'</small>', 'icon' => 'fa fa-circle-o',
                        'url' => ['/project/extension-requests'], 'options' => ['id'=>'extension_requests'],
                    ],
                ],
            ],
            [
                'label' => 'Administration',
                'icon' => 'fa fa-tasks',
                'url' => '#',
                'options' => ['class' => 'treeview', 'id'=>'adminitrator_tree'], //administrator
                'items' => [
                    [
                        'label' => 'Agencies', 'icon' => 'fa fa-dollar',
                        'url' => ['/agency'], 'options' => ['id'=>'agency_nav'],
                    ],
                    [
                        'label' => 'Grants', 'icon' => 'fa fa-dollar',
                        'url' => ['/grant'], 'options' => ['id'=>'grant_nav'],
                    ],
                    [
                        'label' => 'Portfolios', 'icon' => 'fa fa-map-o',
                        'url' => ['/program'], 'options' => ['id'=>'grant_nav'],
                    ],
                    [
                        'label' => 'Pipelines', 'icon' => 'fa fa-object-group',
                        'url' => ['/pipeline'], 'options' => ['id'=>'grant_nav'],
                    ],
                    [
                        'label' => 'Project Stages', 'icon' => 'fa fa-tasks',
                        'url' => ['/stage'], 'options' => ['id'=>'stage_nav'],
                    ],
                    [
                        'label' => 'Milestones', 'icon' => 'fa fa-tasks',
                        'url' => ['/milestone'], 'options' => ['id'=>'milestone_nav'],
                    ],
                    [
                        'label' => 'Indicators', 'icon' => 'fa fa-tasks',
                        'url' => ['#'], 'options' => ['id'=>'indicator_tree'],
                        'items'=>[
                            [
                                'label' => 'Indicators', 'icon' => 'fa fa-circle-o',
                                'url' => ['/indicator'], 'options' => ['id'=>'indicator_nav'],
                            ],
                            [
                                'label' => 'Indicator Data Types', 'icon' => 'fa fa-circle-o',
                                'url' => ['/indicator/types'], 'options' => ['id'=>'indicatordatatype_nav'],
                            ],
                        ]
                    ],
                ],
            ],
            ['label' => 'ACCOUNT', 'options' => ['class' => 'header', 'style'=>'color:#fff']],
            ['label' => 'Settings', 'icon' => 'fa fa-cog', 'url' => ['/account']],
            ['label' => 'Logout', 'icon' => 'fa fa-sign-out',
                'url' => ['/site/logout'], 'options'=>['data-method'=>'post']],
        ],
    ];


