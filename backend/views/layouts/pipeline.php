<?php
use backend\models\VPipeline;
use yii\helpers\Url;

$pipelines = VPipeline::find()->where([
    'institution_id' => Yii::$app->user->identity->institution
])->all();

if (!empty($pipelines)) {

    $this->title = Yii::t('app', 'Pipeline Selection');
    $this->params['breadcrumbs'] = [];?>

    <div class="milestone-create">
        <div class="box box-success">
            <div class="box-header bg-success with-border">
                <h3 class="box-title"><?= Yii::t('app', 'Click on a Pipeline to Select it') ?></h3>
            </div>

            <div class="box-body">
                <h4 class="box-title">Welcome to the Dynamic Project Trajectory Tracker</h4>

                Please click on one of the following pipelines or
                <a href="<?= Url::to(['/pipeline/create']) ?>">Create a New Pipeline</a><br/>
                <?php foreach ($pipelines as $pipeline) : ?>
                    <p style="font-size:120%"><a href="<?= Url::to(['/site/index', 'pipeline' => $pipeline->id]) ?>"><i
                                class="fa fa-sign-in"></i> <?= $pipeline->name ?></a></br></p>
                <?php endforeach?>
            </div>
        </div>
    </div> <?php
} else {?>
    No Pipelines in the system<br/>

    Please click on one of the following pipelines or
    <a href="<?=Url::to(['/pipeline/create'])?>">Create a NewPipeline</a>
    <br/>
<?php } ?>