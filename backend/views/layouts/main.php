<?php
use yii\helpers\Html;
use machour\yii2\notifications\widgets\NotificationsAsset;
use machour\yii2\notifications\widgets\NotificationsWidget;
/* @var $this \yii\web\View */
/* @var $content string */


$session=Yii::$app->session;
if(!$session['pipeline'] && Yii::$app->controller->id!='pipeline')  {
	$content=$this->render('pipeline'); 
}
	
$theme = \common\assets\ThemeAsset::register($this);
common\assets\DpptAsset::register($this);

//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition <?=$theme->skin?> sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <?= $theme->render('header') ?>
            <?= $theme->render('left', [
                'sidebarTopView'=>'@common/themes/AdminLTE/views/pipeline/_current_pipeline.php',
                'menuConfig' => include_once __DIR__ .'/_menu.php'
            ]) ?>
            <?= $theme->render('content', ['content' => $content]) ?>
            <?= $theme->render('footer') ?>
            <?= $theme->render('sidebar') ?>
        </div>
       <?= NotificationsWidget::widget([
                 'theme' => NotificationsWidget::THEME_GROWL,
                 'clientOptions' => [
                     'location' => 'br',
                 ],
                 'counters' => [
                     '.notifications-header-count',
                     '.notifications-icon-count'
                 ],
                 'listSelector' => '#notifications',
             ]);

       ?>
		 
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
