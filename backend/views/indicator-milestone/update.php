<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\IndicatorMilestone */

$this->title = 'Update Indicator Milestone: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Indicator Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="indicator-milestone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
