<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\Agency */

$this->title = Yii::t('app', 'Add a new Agency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agency-create">
<div class="box box-success">
            <div class="box-body">

    <?php $form = ActiveForm::begin(['id' => 'grant-form']); ?>
					 <?= $form->field($model, 'name',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Agency Name',]) ?>
					 <?= $form->field($model, 'email',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Email Address of the agency',]) ?>
					 <?= $form->field($model, 'contact',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Contact Number of the agency',]) ?>
					<?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'A brief description of the agency',]) ?>

<div class="form-group">
				      <?= Html::submitButton('Add Agency', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

			</div>
</div>
</div>
    <?= $this->registerJs(
     " 
    $('#adminitrator_tree').addClass('active');
    $('#agency_nav').addClass('active');")
  	?>