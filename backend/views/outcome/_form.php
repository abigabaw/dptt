<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Pipeline;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Outcome */
/* @var $form yii\widgets\ActiveForm */

$selected=[];
if($_REQUEST['pipeline'])
	$selected[]=$pipeline;
?>

<div class="outcome-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'outcome')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'pipeline',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Pipeline::find()->where(['type'=>2])->all(),'id','name'),$selected,
                    ['prompt'=>'Please select the pipeline'] )?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
