<?php

use yii\helpers\Html;
use yii\grid\GridView;
Use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IndicatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Indicator Type List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-index">
<div class="box box-success">

            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Assign Datatype to indicator(s)', ['values'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			 [
             'attribute' => 'Indicator',
             'value' => 'indicator0.name'
             ],
             [
             'attribute' => 'Data Type',
             'value' => 'indicatorType.name'
             ],

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}{new_action2}{new_action3}',
            'buttons' => [
                'new_action1' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'View'),
                    ]);
                },
                'new_action2' => function ($url, $model) {
                    return Html::a(' <span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Edit'),
                    ]);
                },
                'new_action3' => function ($url, $model) {
                    return Html::a(' <span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                    ]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'new_action1') {
                    $url = Url::to(['indicator/value/'.$model->id.'']);
                    return $url;
                }
                if ($action === 'new_action2') {
                    $url =Url::to(['indicator/edit-value/'.$model->id.'']);
                    return $url;
                }
                if ($action === 'new_action3') {
                    $url =Url::to(['indicator/delete-value/'.$model->id.'']);
                    return $url;
                }
              }  
            ],
        ],
    ]); ?>
</div>
    </div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#adminitrator_tree').addClass('active');
    $('#indicator_tree').addClass('active');
    $('#indicatordatatype_nav').addClass('active');")
  ?>;