<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\IndicatorType;
use backend\models\Indicator;
/* @var $this yii\web\View */
/* @var $model backend\models\Indicator */

$this->title = 'Update Indicator Data Type';
$this->params['breadcrumbs'][] = ['label' => 'Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->indicator0->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="indicator-update">
<div class="box box-success">
            <div class="box-body">
            <?php $form = ActiveForm::begin(['id' => 'indicator-type-map-form']); ?>
            <?= $form->field($model, 'indicator',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(Indicator::find()->all(),'id','name'),
    ['prompt'=>'Select Indicator'] )?>

			    <?= $form->field($model, 'indicator_type',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(IndicatorType::find()->all(),'id','name'),
    ['prompt'=>'Select Indicator Value'] )?>
					<div class="form-group">
				      <?= Html::submitButton('Update Indicator', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>
</div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#adminitrator_tree').addClass('active');
    $('#indicator_tree').addClass('active');
    $('#indicatordatatype_nav').addClass('active');")
  ?>