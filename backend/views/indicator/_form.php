<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Indicator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="indicator-form">

			<?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
					 <?= $form->field($model, 'name',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Indicator Name',]) ?>
					  <?= $form->field($model, 'weight',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Indicator Weight',]) ?>
					  <?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'Indicator Description',]) ?>
					<div class="form-group">
				      <?= Html::submitButton('Create Indicator', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

</div>
