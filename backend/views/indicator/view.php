<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Indicator */

$this->title = 'Indicator';
$this->params['breadcrumbs'][] = ['label' => 'Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="indicator-view">
<div class="box box-success">

            <div class="box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'weight',
            'description:ntext'
        ],
    ]) ?>
</div>
</div></div>
</div>
  <?= $this->registerJs(
    "$('#adminitrator_tree').addClass('active');
    $('#indicator_tree').addClass('active');
    $('#indicator_nav').addClass('active');")
  ?>