<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model backend\models\Indicator */

$this->title = 'Create Indicator';
$this->params['breadcrumbs'][] = ['label' => 'Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grant-create">
<div class="box box-success">
            <div class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#adminitrator_tree').addClass('active');
    $('#indicator_tree').addClass('active');
    $('#indicator_nav').addClass('active');")
  ?>