<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use common\models\ProjectIndicatorweight;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IndicatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Indicator Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-index">
<div class="box box-success">

            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'milestone0.milestone',
            [
            'attribute'=>'Project Name',
            'value'=>'project0.name'
            ],
            'indicator_name:ntext',

           [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}',
            'buttons' => [
                'new_action1' => function ($url, $dataProvider) {
                    if($dataProvider['status']==0)
                    return '<button class="btn btn-success btn-sm approve_button" project="'.$dataProvider['project'].'" indicator="'.$dataProvider['indicator_name'].'" id="'.$dataProvider['milestone'].'" indicatorid="'.$dataProvider['indicator_id'].'" reqid="'.$dataProvider['id'].'" data-toggle="modal" data-target=".bs-example-approve-lg" style="z-index:6666"><i class="fa  fa-hand-o-right"></i>  Approve</button>
                    <br/>
                    <br/> 
                    <button class="btn btn-danger btn-sm request_button" id="'.$dataProvider['id'].'" data-toggle="modal" data-target=".bs-example-modal-lg" style="z-index:6666"><i class="fa  fa-hand-o-right"></i>  Reject</button>';
                    else if($dataProvider['status']==1)
                    return 'Approved';
                    else if($dataProvider['status']==2)
                    return 'Rejected';
                },
            ],
            ],
        ],
    ]); ?>
</div>
    </div>
</div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Reject Indicator Request</h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'milestone-form','action' =>['/indicator/reject']]);
                         echo $form->field($model, 'supervisor_comment',
                          ['options'=>[
                                'tag'     =>'div',
                                'class'     =>'form-group fieldcohort-form-name has-feedback required'],
                                'template'    =>'{input}{error}{hint}'
                          ])->textArea(['placeholder'=>'Reason for Rejecting',]);

                                   echo '<div class="modal-footer">
                                   <input type="hidden" name="request_id"  id="request_id" value=""/>
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Reject Indicator Request</button>
                    </div>';
                        ActiveForm::end();
                                        ?>



    </div>
  </div>
</div></div>

<div class="modal fade bs-example-approve-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add New Indicator </h4>
      </div>
      <div class="modal-body">
      <table class="table table-striped table table-bordered">
                          <tr>
                      <th style="width: 10px">#</th>
                      <th>Indicator</th>
                      <th >Old Weight</th>
                      <th >New Weight</th>
                    </tr>
                    <tbody id="indictor-weights">
                    </tbody>
                  </table>
 <?php 
                         $form = ActiveForm::begin(['id' => 'milestone-form','action' =>['/indicator/approve']]);
     
   ?>
<table class="table table-striped table table-bordered">
</tbody>
                    <tr>
                      <td>#</td>
                      <td id="new_indicator"></td>
                        <td>
                        <?= $form->field($projectindicatorweight, 'weight',
                            ['options'=>[
                            'tag'=>'div',
                            'class'=>'form-group fieldcohort-form-name has-feedback required'],
                            'template'=>'{input}{error}{hint}'
                            ])->textInput(['placeholder'=>'Weight',]) ?>

                      </td>
                      <td><span class="badge bg-yellow">..</span></td>
                    </tr>
                  </tbody>
                </table>

<?php
              echo '<div class="modal-footer">
                        <input  type="hidden" name="milestone" value="" id="milestone_value"/>
                        <input type="hidden" name="request_id"  id="req_id" value=""/>
                        <input  type="hidden" name="indicator" value="" id="indicator_id"/>
                        <input  type="hidden" name="project" id="project_id" value=""/>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <span  class="btn btn-primary" id="calculate_weight">Preview New Weights</span>
                        <button type="submit" class="btn btn-primary">Add New Indicator</button>
                    </div>';
                        ActiveForm::end();
      
                                        ?>


    </div>
  </div>
</div></div>
   <?= $this->registerJs(' 
    $(\'#request_tree\').addClass(\'active\');
    $(\'#indicator_requests\').addClass(\'active\');
    $(\'.request_button\').click(function(){
        $(\'#request_id\').val($(this).attr(\'id\'));
    });

   $(".approve_button").click(function(){
    $("#milestone_value").val($(this).attr("id"));
    $("#new_indicator").html($(this).attr("indicator"));
    $("#project_id").val($(this).attr("project"));
    $("#indicator_id").val($(this).attr("indicatorid"));
    $("#req_id").val($(this).attr("reqid"));
    var milestone = $(this).attr("id");
    var project = $(this).attr("project");
    $.ajax({
        url: "/pipeline/get-project-indicator-weight",
        type: "post",
        data: {milestone:milestone,project:project},
        dataType:"json",
        success: function (response) {
          $(".indictor-weights").remove();
          $.each(response, function (i, value) {
            $("#indictor-weights").prepend("<tr class=\"indictor-weights\"><td>"+parseInt((i*1)+1)+"</td><td>"+response[i].name+"</td><td><span class=\"badge bg-red\">"+response[i].weight+"</span></td><td><span class=\"badge bg-red\">...</span></td></tr>");

          });
           // you will get response from your php page (what you echo or print)                 

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
    //alert($("#milestone_value").val());
});

$("#calculate_weight").click(function(){
var reqid = $("#req_id").val();
  var new_weight = $("#projectindicatorweight-weight").val();
  var indicator = $("#indicator_id").val();

      $.ajax({
        url: "/pipeline/calculate-project-indicator-weight",
        type: "post",
        data: {reqid:reqid,indicator:indicator,weight:new_weight},
        dataType:"json",
        success: function (response) {
          $(".indictor-weights").remove();
          $.each(response, function (i, value) {
            $("#indictor-weights").append("<tr class=\"indictor-weights\"><td>"+parseInt((i*1)+1)+"</td><td>"+response[i].name+"</td><td><span class=\"badge bg-red\">"+response[i].old_weight+"</span></td><td><span class=\"badge bg-red\">"+response[i].weight+"</span></td></tr>");

          });              
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }

    });
})

    ')
  ?>