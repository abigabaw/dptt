<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

$asset          = common\assets\HighStockAsset::register($this);
$baseUrl        = $asset->baseUrl;

/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */

$this->title = $model->name. ' Portfolio';
$this->params['breadcrumbs'][] = ['label' => 'Portfolios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cohort-view">
<div class="cohort-create">
<div class="box box-success">

            <div class="box-body">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /* Html::a('Add Project To Portfolio', ['assign', 'id' => $model->id], ['class' => 'btn btn-primary'])*/ ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [   'label' =>'Pipeline',
                'value' =>$model->pipeline0->name,
            ],
            [   'label' =>'Grant',
                'value' =>$model->grant0->name,
            ],
            [   'label' =>'Created By',
                'value' =>$model->createdBy->name,
            ],
            'description'
        ],
    ]) ?>

    </div>
</div>


    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-body ">
<a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestonesAchieved','location'=>'backend','portfolio'=>$model->id])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a> <button id="button">Hide Projects</button> <?php  $s= '\backend\components\MilestonesAchieved';
		echo $s::widget(['element_id'=>'milestones_achieved','portfolio'=>$model->id]) ?> 

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'CummulativeScore','location'=>'common'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                 <?= \common\components\CummulativeScore::widget() ?>   
                </div>
            </div>
        </div>
    </div>
    <div class="row">
                <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestoneSubmission','location'=>'backend'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
						 
                    <?= \backend\components\MilestoneSubmission::widget(['portfolio'=>$model->id]) ?>  
                </div>
            </div>
        </div>
                <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestoneSubmission2','location'=>'backend','portfolio'=>$model->id])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                    <?= \backend\components\MilestoneSubmission2::widget(['portfolio'=>$model->id]) ?>  
                </div>
            </div>
        </div>
    </div>
    <div class="row">
                <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestoneDelays','location'=>'backend','params'=>['portfolio'=>$model->id]])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
						 
                    <?= \backend\components\MilestoneDelays::widget(['portfolio'=>$model->id]) ?>  
                </div>
            </div>
        </div>
                <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body ">
  		<select id=cummind>
			<?php
		 //  $s=$this->portfolio ? "portfolio_id=".$this->portfolio : "pipeline_id=".Yii::$app->session['pipeline'];
		 	$qry_cummulative = Yii::$app->db->createCommand("SELECT DISTINCT indicator_id,indicator_name FROM `v_indicator_milestone_submission` WHERE portfolio_id=".$model->id)->queryAll();
			foreach($qry_cummulative as $s)
				echo "<option value=".$s[indicator_id].">".$s[indicator_name]."</option>";
			?>
		</select>
		<a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'CummulativeScoreIndicators','location'=>'common','portfolio'=>$model->id])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                   <?= \common\components\CummulativeScoreIndicators::widget(['portfolio'=>$model->id]) ?>
                </div>
            </div>
        </div>
    </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Projects in this Portfolio</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <?= $projects?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
</div>
</div>

<style>
#editModalId 
{
    width: 96%; 
} 

#editModalId .modal-body {
    max-height: 100%;
	 max-width: 1200px;
}
</style>
<?php
yii\bootstrap\Modal::begin([
		'header' => '',
		'id'=>'editModalId',
		'class' =>'modal',
		'size' => 'modal-lg',
]);
echo "<div class='modalContent' ></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(Url::to(['/js/modal.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);
?>

  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#program_nav').addClass('active');")
  ?>