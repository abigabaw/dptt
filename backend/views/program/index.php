<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CohortSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Portfolios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cohort-index">
<div class="box box-success">

    <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create a New Portfolio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
            'attribute'=>'Name',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider['name'], ['view', 'id' => $dataProvider['id']]);
                  
            }
            ],
            [
            'attribute'=>'Pipeline',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider->pipeline0->name, ['/pipeline/view', 'id' => $dataProvider['pipeline']]);
                  
            }
            ],
            [
            'attribute'=>'Grant',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider->grant0->name, ['/grant/view', 'id' => $dataProvider['grant']]);
                  
            }
            ],

          /*  [   'attribute' =>'Created By',
                'value' =>'createdBy.name',
            ],
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],*/
        ],
    ]); ?></div>
    </div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#program_nav').addClass('active');")
  ?>