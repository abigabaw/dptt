<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\VGrant;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */

$this->title = 'New Portfolio';
$this->params['breadcrumbs'][] = ['label' => 'Cohorts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cohort-create">
<div class="box box-success">
			<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
    <?= $form->field($model, 'name',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter the name of the portfolio',]) ?>

        <?php /* echo $form->field($model, 'pipeline')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Pipeline::find()->orderBy('name ASC')->all(),'id','name'),
                        'options' => ['placeholder' => 'Select or type new Pipeline (s)'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => false
                        ],
                ]);*/
             ?>
    
  <?= $form->field($model, 'grant')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VGrant::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Grant'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => false
                                                ],
                                        ]);?>

    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a description of this portfolio',]) ?>

	<div class="form-group">

      <?= Html::submitButton('Create Portfolio', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#program_nav').addClass('active');

$('#program-pipeline').select2({
    placeholder: 'Select or type a new pipeline if not in list',
    width: '100%' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

$('#program-grant').select2({
    placeholder: 'Select or type a new grant if not in list',
    width: '100%' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});
    ")
  ?>