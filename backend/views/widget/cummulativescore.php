<?php $w=md5(time().rand(1,23342)); ?>
<div class='graphs <?php if (Yii::$app->request->isAjax) echo "xlarge"?>' id="<?=$w?>"></div>
    <?= $this->registerJs(
    " 
      $(function () {
      $('#".$w."').highcharts({
	credits:false,
        chart: {
            type: 'spline',
            inverted: false
        },
        title: {
            text: 'Cumulative Milestone Indicator Scores by Project, over the Milestone Spread'
        },
        xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: 'Milestones Achieved'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Cummulative Scores'
            },
            lineWidth: 2
        },

tooltip: {
     formatter: function() {
               return 'Project: <b>'+ this.point.project +'</b><br/><b>Milestone</b>: '+this.point.milestone+'<br/><b>Score: </b>'+this.point.score+'<br/><b>Cummulative Score: </b>'+this.point.y+'';
     }
},
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },
        series: [".$dataset_cummulative."]
    });
});
    ")
?>
