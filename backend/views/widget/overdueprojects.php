
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Overdue Projects</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ul class="todo-list ui-sortable">
                        <?php
                        foreach ($overdue_milestones as $delayed) {
                            echo '<li class="dropdown notifications-menu">
                                                                <!-- drag handle -->
                                <span class="handle ui-sortable-handle">
                                  <i class="fa fa-ellipsis-v"></i>
                                  <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <!-- todo text -->
                                <span class="text"><th>'.$delayed['project'].'</th></span>
                                <a href="#" class="dropdown-toggle load_milestone" id="'.$delayed['project_id'].'" data-toggle="dropdown" aria-expanded="false">
                                  <span class="label label-warning">'.$delayed['number_of_milestones'].' Overdue milestones</span>
                                </a>
                                <ul class="dropdown-menu col-md-12">
                                  <li class="header col-md-12" id="milestone_header"></li>
                                   <li class="overlay">
                                      <i class="fa fa-refresh fa-spin"></i>
                                  </li>
                                  <li class="col-md-12">
                                    <!-- inner menu: contains the actual data -->
                                    <table class="table table-bordered">
                                    <tbody class="menu menu-milestone">
                                  </tbody>
                                  </table>
                                  </li>
                                </ul>
                              </li>';
                        }
                        ?>
                    </ul>
                </div><!-- /.box-body -->
            </div>
        </div>