  <?php
   $series=json_encode($series,JSON_NUMERIC_CHECK); 
  ?>
     <div class='graphs  <?php if (Yii::$app->request->isAjax) echo "xlarge"?>' id="<?=$elementid?>"></div>

     <?= $this->registerJs(
      ' 
      $(function () {
      $(\'#'.$elementid.'\').highcharts(\'StockChart\', {

            rangeSelector: {
                selected: 1
            },

            title: {
                text: \'Comparision of Timelines\'
            },

            series: '.$series.'
        });

     });')?>

