        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title" >Project Progress</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- Progress bars -->
                    <?php
                    foreach ($projects as $project) {
                        $percentage = round(($project['progress'])*100,1);
            
                        if($percentage>80){
                            $style='progress-bar-green';
                        }else if($percentage>50){
                            $style='progress-bar-yellow';
                        }else{
                            $style='progress-bar-red';
                        }
                        echo '
                      <div class="clearfix">
                        <span class="pull-left">'.$project['project'].'</span>
                        <small class="pull-right">'.$percentage.'%</small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar '.$style.'" style="width: '.$percentage.'%;"></div>
                      </div>';

                    }
                    ?></div>
            </div>
        </div>
    </div>