<?php 
use yii\grid\GridView;
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header bg-success with-border">
                <h3 class="box-title">Milestones Submitted Late</h3>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //   'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'milestone',
                        'project',
                        [
                            'attribute' => 'Delay',
                            'format'=>'raw',
                            'value' => function($dataProvider){
                                $expected_finish_date = $dataProvider['expected_finish_date'];    
                                $expected_finish_date = new DateTime($expected_finish_date);
                                $finish_date = $dataProvider['finish_date'];    
                                $finish_date = new DateTime($finish_date);
                                return $expected_finish_date->diff($finish_date)->format("%d days, %h hours and %i minutes");
                            }
                        ],

                        [
                            'label'=>'Expected Finish Date',
                            'format'=>'raw',
                            'value' => function($dataProvider){
                                return date('F d, Y',strtotime($dataProvider['expected_finish_date']));
                            }
                        ],
                        [
                            'label'=>'Actual Finish Date',
                            'format'=>'raw',
                            'value' => function($dataProvider){
                                if(isset($dataProvider['finish_date']))
                                return date('F d, Y',strtotime($dataProvider['finish_date']));
                                else 
                                    return 'Mileston not yet submitted';
                            }
                        ]
                    ],
                ]); ?>
            </div>
            </div>
    </div>
</div>