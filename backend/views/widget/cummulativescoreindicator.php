<?php $w=md5(time().rand(1,23342)); $q='ww;'?>
<div class='graphs <?php if (Yii::$app->request->isAjax) echo "xlarge"?>' id="<?=$w?>"></div>
    <?= $this->registerJs(
    " 
      $(function () {
      $('#".$w."').highcharts({
	credits:false,
        chart: {
            type: 'spline',
            inverted: false
        },
        title: {
            text: 'Cumulative Indicator Values for all Projects:".$indicator."'
        },
        xAxis: {
            reversed: false,
	 type: 'datetime',
            title: {
                enabled: false,
                text: 'Milestones Achieved'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Cummulative Value'
            },
            lineWidth: 2
        },

tooltip: {
     formatter: function() {
               return 'Project: <b>'+ this.point.project +'</b><br/><b>Milestone</b>: '+this.point.milestone+'<br/><b>Indicator: </b>'+this.point.indicator+'<br/><b>Value: </b>'+this.point.score+'<br/><b>Cummulative Value: </b>'+this.point.y+'';
     }
},
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },
        series: [".$dataset_cummulative."]
    });
});
    ")
?>
