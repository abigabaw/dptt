     <div id="<?=$elementid?>"></div>

     <?= $this->registerJs(
      ' 
      $(function () {
  	  $(\'#'.$elementid.'\').highcharts(\'StockChart\', {

            rangeSelector: {
                selected: 1
            },
            colors: [\'#562F1E\', \'#AF7F24\', \'#263249\', \'#5F7F90\', \'#D9CDB6\'],

            title: {
                text: \'Actual Timeline 2\'
            },

            series: [{
                name: \'Normal Progess\',
                data: ['.$dataset_normal.'],
                color:\''.$normal_color.'\',
             //   step: true,
                tooltip: {
			      formatter: function() { return this.x }
                }
            },{
                name: \'Extension of Normal Progess\',
                data: ['.$dataset_normal_extension.'],
                dashStyle: \'dash\',
                color:\''.$normal_color.'\',
                step: true,
                tooltip: {
                    valueDecimals: 2
                }
            },
            {
                name: \'Iteration\',
                data: ['.$dataset_iteration.'],
                color:\''.$iteration_color.'\',
                step: true,
                tooltip: {
                    valueDecimals: 2
                }
            },
            {
                name: \'Extension of Iteration\',
                data: ['.$dataset_iteration_extension.'],
                dashStyle: \'dash\',
                color:\''.$iteration_color.'\',
                step: true,
                tooltip: {
                    valueDecimals: 2
                }
            },
            {
                name: \'Repeat\',
                data: ['.$dataset_repeat.'],
                color:\''.$repeat_color.'\',
                step: true,
                tooltip: {
                    valueDecimals: 2
                }
            },
            {
                name: \'Extension of Repeat\',
                data: ['.$dataset_repeat_extension.'],
                dashStyle: \'dash\',
                color:\''.$repeat_color.'\',
                step: true,
                tooltip: {
                    valueDecimals: 2
                }
            },

            {
                    type: \'flags\',
                    name: \'Normal Submission\',
                    color: \'#333333\',
                    fillColor: \'rgba(255,255,255,0.8)\',
                    data: [
                        '.$dataset_data_normal.'
                              ],
                    onSeries: \'revenue\',
                    showInLegend: false
                }, {
                    type: \'flags\',
                    name: \'Extension of normal submission\',
                    color:\''.$normal_color.'\',
                    fillColor: \'rgba(255,255,255,0.8)\',
                    data: [
                        '.$dataset_data_normal_extension.'
                              ],
                    onSeries: \'revenue\',
                    showInLegend: false
                },
                {
                    type: \'flags\',
                    name: \'Repeats\',
                    color:\''.$repeat_color.'\',
                    fillColor: \'rgba(255,255,255,0.8)\',
                    data: [
                        '.$dataset_data_repeat.'
                              ],
                    onSeries: \'revenue\',
                    showInLegend: false
                }, {
                    type: \'flags\',
                    name: \'Extension of Repeats\',
                    color:\''.$repeat_color.'\',
                    fillColor: \'rgba(255,255,255,0.8)\',
                    data: [
                        '.$dataset_data_repeat_extension.'
                              ],
                    onSeries: \'revenue\',
                    showInLegend: false
                },{
                    type: \'flags\',
                    name: \'Iterations\',
                    color:\''.$iteration_color.'\',
                    fillColor: \'rgba(255,255,255,0.8)\',
                    data: [
                        '.$dataset_data_iteration.'
                              ],
                    onSeries: \'revenue\',
                    showInLegend: false
                }, {
                    type: \'flags\',
                    name: \'Extension of iteration\',
                    color:\''.$iteration_color.'\',
                    fillColor: \'rgba(255,255,255,0.8)\',
                    data: [
                        '.$dataset_data_iteration_extension.'
                              ],
                    onSeries: \'revenue\',
                    showInLegend: false
                }]
        });

     });')?>