<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Iteration */

$this->title = 'Create Iteration';
$this->params['breadcrumbs'][] = ['label' => 'Iterations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iteration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
