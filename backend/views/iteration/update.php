<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Milestone;

/* @var $this yii\web\View */
/* @var $model backend\models\Iteration */

$this->title = 'Date Extension';
$this->params['breadcrumbs'][] = ['label' => Milestone::findOne($milestone)->milestone, 'url' => ['timeline-entry', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Date Extension';
?>
<div class="iteration-update">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> for <?=Milestone::findOne($milestone)->milestone?></h3>
            </div>
   <div class="box-body">

    <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
    <?= $form->field($model, 'comment',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textarea(['placeholder'=>'Comment/Reason as to why you are extending the date','value'=>'']) ?>
    <?= $form->field($model, 'duration',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Duration You are extending by (Days)','value'=>'']) ?>
		<div class="form-group">
      <?= Html::submitButton('Add Date Extension', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
 				<?php ActiveForm::end(); ?>
</div>
</div>
</div>
