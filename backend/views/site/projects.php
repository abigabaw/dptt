<?php
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = "Welcome to DPTT";
//print_r(Yii::$app->session['pipeline_a']);
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">This Pipeline has no projects. You can create a project by using the link below</h3>
            </div>
            <div class="box-body">
            <div style="text-align:center ">
            	 <?= Html::a("<i class=\"fa fa-map-o\"></i> Create a Project", ['/project/create'],['class'=>'btn btn-lg btn-success']) ?> 
            	 </div>
            </div>
 </div>
 </div>