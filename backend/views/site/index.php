<?php
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */

$asset          = common\assets\HighStockAsset::register($this);
$baseUrl        = $asset->baseUrl;

$this->title = Yii::$app->session['pipeline_a']['name']. " Dashboard";
//print_r(Yii::$app->session['pipeline_a']);
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?=$programs?></h3>
                    <p>No. of Portfolios</p>
                </div>
                <div class="icon">
                    <i class="fa fa-object-group"></i>
                </div>
                <a href="<?= Url::to(['/program']);?>" class="small-box-footer">Portfolios <span class="fa fa-arrow-circle-right"></span></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?=$delayed_milestone_count?></h3>
                    <p>No. of Overdue Projects</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file-text-o"></i>
                </div>
                <a href="#overdue_projects" class="small-box-footer">View Overdue Projects <span class="fa fa-arrow-circle-right"></span></a>
            </div>
        </div>

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?=$innovators?></h3>
                    <p>No. of Project Teams</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?= Url::to(['/innovator']);?>" class="small-box-footer">Project Teams <span class="fa fa-arrow-circle-right"></span></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?=$projects?></h3>
                    <p>No. of Projects</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file-text-o"></i>
                </div>
                <a href="<?= Url::to(['/project']);?>" class="small-box-footer">Projects <span class="fa fa-arrow-circle-right"></span></a>
            </div>
        </div>
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-body ">
<a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestonesAchieved','location'=>'backend'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>  <button id="button">Hide Projects</button>                  <?php  $s= '\backend\components\MilestonesAchieved';
						 echo $s::widget(['element_id'=>'milestones_achieved']) ?> 

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'CummulativeScore2','location'=>'common'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                 <?= \common\components\CummulativeScore2::widget() ?>   
                </div>
            </div>
        </div>
    </div>
    <div class="row">
                <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestoneSubmission','location'=>'backend'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                    <?= \backend\components\MilestoneSubmission::widget() ?>  
                </div>
            </div>
        </div>
                <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-body ">
						 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestoneSubmission2','location'=>'backend'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                    <?= \backend\components\MilestoneSubmission2::widget() ?>  
                </div>
            </div>
        </div>
		  
    </div>
   <div class="row">
               <div class="col-lg-6">
           <div class="box box-success">
               <div class="box-body ">
					 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'MilestoneDelays','location'=>'backend'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
					 
                   <?= \backend\components\MilestoneDelays::widget() ?>  
               </div>
           </div>
       </div>
               <div class="col-lg-6">
           <div class="box box-success">
               <div class="box-body ">
			  		<select id=cummind>
						<?php
					 //  $s=$this->portfolio ? "portfolio_id=".$this->portfolio : "pipeline_id=".Yii::$app->session['pipeline'];
					 	$qry_cummulative = Yii::$app->db->createCommand("SELECT DISTINCT indicator_id,indicator_name FROM `v_indicator_milestone_submission` WHERE pipeline_id=".Yii::$app->session['pipeline'])->queryAll();
						foreach($qry_cummulative as $s)
							echo "<option value=".$s[indicator_id].">".$s[indicator_name]."</option>";
						?>
					</select>
					<a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'CummulativeScoreIndicators','location'=>'common'])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
			                   <?= \common\components\CummulativeScoreIndicators::widget() ?>
					
               </div>
           </div>
       </div>
   </div>
	
      <div class="row" id="overdue_projects">
<?php // \backend\components\OverdueProject::widget() ?>
<?php // \backend\components\ProjectProgress::widget() ?>
    </div>
<?php // \backend\components\LateSubmission::widget() ?>


</div>
<style>
#editModalId 
{
    width: 96%; 
} 

#editModalId .modal-body {
    max-height: 100%;
	 max-width: 1200px;
}
</style>
<?php
yii\bootstrap\Modal::begin([
		'header' => '',
		'id'=>'editModalId',
		'class' =>'modal',
		'size' => 'modal-lg',
]);
echo "<div class='modalContent' ></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(Url::to(['/js/modal.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);
?>

<!-- Page specific script -->
<?= $this->registerJs(
    " 
    function dateToYMD(date) {
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
    }

  $('#dashboard_nav').addClass('active');
  $('.load_milestone').click(function(){
  $('#milestone_header').html('Loading Milestones');
  $('.menu-milestone').html('');
  $('.overlay').show();
  var project_id = $(this).attr('id');
  $.ajax({
          method: 'POST',
          url: 'site/milestones/',
          data: { project_id: project_id },
          dataType:'JSON'
        })
    .done(function(response) {
    $('#milestone_header').html('Overdue Milestones');
    $('.overlay').hide();
    $.each(response, function( index, value ) {
       $('.menu-milestone').append('<tr><th class=\"col-md-8\"><a href=\"project/timeline-entry/'+value.timeline+'\">'+value.milestone+'</a></th><th class=\"col-md-4\"><span>Due on '+dateToYMD(new Date(value.expected_finish_date))+' </span></th>');    });
    });
})

      /**
 * This is an advanced demo of setting up Highcharts with the flags feature borrowed from Highstock.
 * It also shows custom graphics drawn in the chart area on chart load.
 */

")
?>

