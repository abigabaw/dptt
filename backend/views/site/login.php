<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
Use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<link href="<?=Url::base()?>/css/style.css" rel="stylesheet" type="text/css" media="all"/>

<div class="login-main">
    <div class="login"> <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="social">
            <ul>
                <li class="last-mar"><a class="loginicon" href="#">Admin Login</a></li>
            </ul>
        </div>
        <?= $form->field($model, 'email',
            ['options'=>[
                'tag'=>'div',
                'class'=>'form-group field-loginform-usernasme has-feedback required'],
                'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback" style="color:#00a65a"></span>{error}{hint}'
            ])->textInput(['placeholder'=>'Email',]) ?>
        <?= $form->field($model, 'password',['options'=>[
            'tag'=>'div',
            'class'=>'form-group field-loginform-password has-feedback required'],
            'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback" style="color:#00a65a"></span>{error}{hint}'
        ])->passwordInput(['placeholder'=>'Password']) ?>
        <ul class="button">
            <li><input type="submit" value="Submit" ></li>
            <li><input class="clear" type="Reset" value="Clear"></li>
        </ul>
        <!--
		<div class="forgot">
           <?= $form->field($model, 'rememberMe',['options'=>[
            'tag'=>'div',
            'class'=>'form-group field-loginform-rememberme'],
        ])->checkbox() ?>
	   	  <div class="forgot-para">
	   	  	<a href="#"> <p>Forgot Your Password?</p></a>
	   	  </div>
	   	<div class="clear"> </div>
	   </div>-->
    </div><?php ActiveForm::end(); ?>
    <div class="login-password">
        <p>Dynamic Project Trajectory Tracking Toolkit</p>
    </div>
</div>
<div class="copyright">
    <p>&copy; 2016 <a href="#" target="_blank">DPTTT</a></p>
</div>