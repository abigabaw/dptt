<?php 
use backend\models\Grading;
use yii\helpers\Html;

$model = Grading::findOne(['milestone_submission'=>$milestone_submission]);
        if($model)
        echo '<a class="btn btn-success btn-xs"><i class="fa fa-check-circle"></i> ALREADY ASSESSED</a>';
        else
        echo Html::a('<i class="fa fa-dot-circle-o"></i>  ASSESS', ['grade', 'id' =>$milestone,'project'=>$project,'iteration'=>$milestone_submission ], ['class' => 'btn btn-primary btn-xs']);

?>