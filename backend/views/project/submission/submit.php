<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\Milestone;
use backend\models\LoginInnovator;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = 'Submit Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">
<div class="box box-success">
<div class="box-header bg-success with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">

            <?php $form = ActiveForm::begin(['options' => ['id' => 'indicator-form','enctype' => 'multipart/form-data']]) ?>

                  <?= $form->field($model, 'narration',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Please enter the description of this submission',]) ?>
                  <?= $form->field($model, 'file',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->fileInput() ?>
               
              <?=$indicators?>

        <div class="form-group">
      <?= Html::submitButton('Submit Milestone', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
</div>

<?php ActiveForm::end(); ?>


</div>
</div>
</div>    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>;