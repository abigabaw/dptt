    <?php
    use backend\models\Grading;
    use common\models\Functions;
    
    $model = Grading::findOne(['milestone_submission'=>$milestone_submission]);
    $thumb='';
    if($model){
            if($model->status==2)
            $thumb.='<i class="fa  fa-thumbs-o-down bg-red"></i>';
            else
            $thumb.='<i class="fa  fa-thumbs-o-up bg-green"></i>';   
            echo'<li>
                    <!-- timeline icon -->
                    '.$thumb.'
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i>About  '.Functions::humanTiming(strtotime($model->created_at)).' ago  ('.date_format(date_create(date($model->created_at)),"l jS F Y g:ia").') </span>
                        
                        <h3 class="timeline-header"><a href="#">Supervisior Remarks</a></h3>

                        <div class="timeline-body">
                            <p>Grade: '.$model->grade.' </p><p><b>Comment</b><br/>'.$model->comment.'<p>
                        </div>
                    </div>
            </li>';
        }else{
           echo '<li>
                    <!-- timeline icon -->
                    <i class="fa  fa-ticket"></i>
                    <div class="timeline-item">

                        <h3 class="timeline-header"><a href="#">Supervisior Remarks</a></h3>

                        <div class="timeline-body">
                            This Iteration has not yet been graded
                        </div>
                    </div>
            </li>';
        }
?>