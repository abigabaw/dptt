<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\models\Functions;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = 'Assess Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => substr($model->milestone,0,80).'...', 'url' => ['milestones','id'=>$model->milestone_id]];

?>
<div class="project-view">

<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> : <?= Html::encode($model->milestone)?></h3>
            </div>
            

   <div class="box-body">

    <p>
        
    </p>
</div>
</div>
<ul class="timeline">
  <?php
foreach ($submissions as $submission) {
  echo '<!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-green">
                    '.date('d-M-Y',strtotime($submission['submissions']['submission_date'])).'
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-dot-circle-o bg-green"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i>'.strtotime($submission['submissions']['submission_date'])

.'|     date  |'. $submission['submissions']['submission_date'].'
                     About '.Functions::humanTiming(1470135000).' ago ('.date_format(date_create(date($submission['submissions']['submission_date'])),"l jS F Y g:ia").')</span>
                    
                    <div class="timeline-body">
                        <h5><b>Description</b></h5>
                    <p>';


                    foreach ($submission['submissions']['attachments'] as $attachment) {
                       echo '<a href="'.Yii::$app->urlManagerFrontend->createUrl('').''.$attachment['attachment'].'" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download Attachment</a>';
                     }

                      echo '</p> <h5><b>Indicators</b></h5>
                         <div class="box box-success" >
                            <div class="box-body no-padding">
                              <table id="layout-skins-list" class="table table-striped bring-up nth-2-center">
                                <tbody>';
                        $counter = 1;
                        foreach ($submission['submissions']['indicators'] as $indicator) {
                         echo '<tr>
                                    <td><code>'.$counter.'</code> <a href="#" data-skin="skin-blue" class="btn btn-primary btn-xs" style="margin-left:5px;">'.$indicator['indicator'].'</a></td>
                                  </tr>';
                          $counter++;
                        }

                      echo'</tbody>
                              </table>
                            </div><!-- /.box-body -->
                          </div><!-- /.box -->
                    </div>
                    <div class="timeline-footer">
                    '.$this->render('buttons',['milestone_submission'=>$submission['submissions']['id'],'milestone'=>$submission['submissions']['milestone_id'],'project'=>$submission['submissions']['project']]).'
                     </div>
                  </div>
                </li>

                <!-- END timeline item -->
                    <!-- timeline item -->

    <!-- END timeline item -->
'.$this->render('indicators',['milestone_submission'=>$submission['submissions']['id']]).'
                   
    ';

}
?>
     <li>
        <i class="fa fa-clock-o bg-gray"></i>
    </li>

</ul>

</div>    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>