<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\VPipelineStage;
use backend\models\VMilestoneStage;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */
/* @var $form yii\widgets\ActiveForm */
/* @var $durations array */
/* @var $stages VPipelineStage[]*/
/* @var $duration_id integer*/
/* @var $project_id integer*/

$this->title = 'Set Project Timelines';
$duration = $durations[$duration_id];
$dayFactorDuration = !in_array($duration, ['Hour', 'Minute']);
$startDateJsFormat = $dayFactorDuration ? 'yyyy-mm-dd' : 'yyyy-mm-dd HH:MM:ss';
$pluginOptions = [
    'format' => $dayFactorDuration ? 'yyyy-mm-dd' : 'yyyy-MM-dd HH:mm:ss' ,
    //'startDate' => '01-Mar-2014 12:00 AM',
    'todayHighlight' => true
]?>

<script>
    var project_timeline_duration_unit = '<?=strtolower($duration)?>';
    var project_timeline_datetime_format = '<?=$startDateJsFormat?>';
</script>
<div class="project-form">
<div class="box box-success">

    <?php $form = ActiveForm::begin(); ?>
    <table id="project-timelines" class="table table-striped table-bordered detail-view">
                <div class="box-body">
<div class="box-header with-border">
            <div class="col-md-2">
                              <label>Duration Units</label>
            <?= Html::dropDownList(Html::getInputName($model, 'duration'),
                $duration_id, $durations, ['id'=>'timeline-duration', 'class'=>'form-control ']) ?>
            </div>
            </div>


        <?php  $ctr = 0;
        foreach ($stages as $stage) :?>
            <tr style="background:#00a65a">
                <th align=left colspan=5><h3 style="color:#fff;"><?= $stage->stage_name ?></h3></th>
            </tr>
            <tr>
                <th>MileStone</th>
                 <th>Weight</th>
                <th style='width:200px'>Start Date</th>
                <th>Duration</th>
                <th>Comments</th>
            </tr>
            <?php $milestones = VMilestoneStage::getMilestones($stage->stage, $stage->pipeline);
            foreach ($milestones as $milestone): ?>
                <?php $ctr++;
                $isNewRecord = true;
                $object = "Timeline[{$stage->stage}][milestone][{$milestone->milestone_id}]";
                $data = \backend\models\Timeline::find()->where([
                    'project'=>$project_id, 'milestone'=>$milestone->milestone_id,
                    'stage'=>$stage->stage
                ])->one();

                if($data)
                    $isNewRecord = false;

                $startDateInputName = "{$object}[start_date]";
                $startDateInputValue = $isNewRecord ? null : $data->start_date;
                $durationInputValue = $isNewRecord ? null : $data->duration;
                $commentInputValue = $isNewRecord ? null : $data->comment;
				$weightInputValue = $isNewRecord ? null : $data->weight;
                $options = [
                    'placeholder' => '', 'style' => 'width:200px',
                    'id' => 'date_' . $ctr, ' class' => 'milestone_date'
                ]?>
                <tr>
                    <td align=left><?=Html::a( '<i class="glyphicon glyphicon-plus"></i>',
			['saveindicators', 'id'=>$model['id'],'milestone'=>$milestone->milestone_id],
			['class'=>'btn btn-xs btn-default modalButton', 'title'=>'Edit Indicators', ]
		);?>
&nbsp;&nbsp;<?= $milestone->milestone ?></td>
                    <td><div class="form-control"><?= Html::textInput("{$object}[weight]", $weightInputValue,['style'=>'border:0px;']); ?></div></td>
                    <td>

                        <?= (!$dayFactorDuration)
                            ? DateTimePicker::widget([
                                'name' => $startDateInputName,
                                'options' => $options,
                                'convertFormat' => true,
                                'pluginOptions' => $pluginOptions
                            ])
                            : DatePicker::widget([
                                'name' => $startDateInputName,
                                'value' => $startDateInputValue,
                                'options' => $options,
                                'pluginOptions' => $pluginOptions
                            ]);
                        ?>
                    </td>
                    <td><div class="form-control">
                        <?= Html::textInput("{$object}[duration]", $durationInputValue, [
                            'type' => 'number', 'id' => 'duration_' . $ctr, 'class' => 'milestone_duration','style'=>'border:0px;'
                        ]); ?>
                        </div>
                    </td>
                    <td><div class="form-control"><?= Html::textInput("{$object}[comment]", $commentInputValue,['style'=>'border:0px;']); ?></div></td>
                </tr>
            <?php endforeach;?>
        <?php endforeach?>
        <tr>
            <td colspan=4>
                <?= Html::submitButton('Save Timeline', [
                    'class' => $model->isNewRecord
                    ? 'btn btn-success' : 'btn btn-primary']) ?>
            </td>
        </tr>
    </table>
    <?php ActiveForm::end(); ?>
</div>
</div>

<?php
yii\bootstrap\Modal::begin([
		'header' => 'Add Indicator',
		'id'=>'editModalId',
		'class' =>'modal',
		'size' => 'modal-lg',
]);
echo "<div class='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(Url::to(['/js/modal.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);

?>

<?php 

$this->registerJsFile(Url::to(['/js/dateFormat.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);
$this->registerJsFile(Url::to(['/js/project/timeline.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);?>
