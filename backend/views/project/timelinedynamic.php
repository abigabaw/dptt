<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\VPipelineStage;
use yii\helpers\ArrayHelper;
use backend\models\VMilestoneStage;
use backend\models\VIndicator;
use backend\models\IndicatorType;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
$asset          = common\assets\DatatablesAsset::register($this);
$baseUrl        = $asset->baseUrl;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */
/* @var $form yii\widgets\ActiveForm */
/* @var $durations array */
/* @var $stages VPipelineStage[]*/
/* @var $duration_id integer*/
/* @var $project_id integer*/

$this->title = 'Set Project Timelines';
$duration = $durations[$duration_id];
$dayFactorDuration = !in_array($duration, ['Hour', 'Minute']);
$startDateJsFormat = $dayFactorDuration ? 'yyyy-mm-dd' : 'yyyy-mm-dd HH:MM:ss';
$pluginOptions = [
    'format' => $dayFactorDuration ? 'yyyy-mm-dd' : 'yyyy-MM-dd HH:mm:ss' ,
    //'startDate' => '01-Mar-2014 12:00 AM',
    'todayHighlight' => true
]?>

<script>
    var project_timeline_duration_unit = '<?=strtolower($duration)?>';
    var project_timeline_datetime_format = '<?=$startDateJsFormat?>';
</script>

<div class="project-form">
<div class="box box-success">

    <?php $form = ActiveForm::begin(); ?>
    <table id="project-timelines" class="table table-striped table-bordered detail-view">
                <div class="box-body">
<div class="box-header with-border">
            <div class="col-md-2">
            <label>Duration Units</label>
            <?= Html::dropDownList(Html::getInputName($model, 'duration'),
                $duration_id, $durations, ['id'=>'timeline-duration', 'class'=>'form-control ']) ?>
            </div>
            </div>



            <thead>
            <th>Actions</th>
                <th>MileStone</th>
                 <th>Weight</th>
                <th style='width:200px'>Start Date</th>
                <th>Duration</th>
                <th>Comments</th>
            </thead>
                    <?php  $ctr = 0;
        foreach ($stages as $stage) :?>
            <tr style="background:#00a65a">
                <td align=left colspan=6><h3 style="color:#fff;"><?= $stage->stage_name ?></h3></td>
            </tr>
            <?php $milestones = VMilestoneStage::getMilestones($stage->stage, $stage->pipeline);
            foreach ($milestones as $milestone): ?>
                <?php $ctr++;
                $isNewRecord = true;
                $object = "Timeline[{$stage->stage}][milestone][{$milestone->milestone_id}]";
                $data = \backend\models\Timeline::find()->where([
                    'project'=>$project_id, 'milestone'=>$milestone->milestone_id,
                    'stage'=>$stage->stage
                ])->one();

                if($data)
                    $isNewRecord = false;

                $startDateInputName = "{$object}[start_date]";
                $startDateInputValue = $isNewRecord ? null : $data->start_date;
                $durationInputValue = $isNewRecord ? null : $data->duration;
                $commentInputValue = $isNewRecord ? null : $data->comment;
				$weightInputValue = $isNewRecord ? null : $data->weight;
                $options = [
                    'placeholder' => '', 'style' => 'width:200px',
                    'id' => 'date_' . $ctr, ' class' => 'milestone_date'
                ]?>
                <tr>
            <td id="<?=$milestone->milestone_id?>">
                 <button type="button" class ="btn btn-success add_indicator_button" milestone="<?=$milestone->milestone_id?>" data-toggle="modal" data-target="#indicatormodal"><i class ="fa fa-plus-circle"></i></button>            
                </td>
                    <td align=left><?= $milestone->milestone ?></td>
                    <td><div class="form-control"><?= Html::textInput("{$object}[weight]", $weightInputValue,['style'=>'border:0px;']); ?></div></td>
                    <td>

                        <?= (!$dayFactorDuration)
                            ? DateTimePicker::widget([
                                'name' => $startDateInputName,
                                'options' => $options,
                                'convertFormat' => true,
                                'pluginOptions' => $pluginOptions
                            ])
                            : DatePicker::widget([
                                'name' => $startDateInputName,
                                'value' => $startDateInputValue,
                                'options' => $options,
                                'pluginOptions' => $pluginOptions
                            ]);
                        ?>
                    </td>
                    <td>
                    <div class="form-control">
                        <?= Html::textInput("{$object}[duration]", $durationInputValue, [
                            'type' => 'number', 'id' => 'duration_' . $ctr, 'class' => 'milestone_duration','style'=>'border:0px;'
                        ]); 
                        ?>
                     </div>
                    </td>
                    <td>
                    <div class="form-control"><?= Html::textInput("{$object}[comment]", $commentInputValue,['style'=>'border:0px;']); ?></div></td>
                
                </tr>
            <?php endforeach;?>
        <?php endforeach?>
        <tr>
            <td colspan=6>
                <?= Html::submitButton('Save Timeline', [
                    'class' => $model->isNewRecord
                    ? 'btn btn-success' : 'btn btn-primary']) ?>
            </td>
        </tr>
    </table>
    <?php ActiveForm::end(); ?>
</div>
</div>

<div class="modal fade bs-example-modal-lg" id="indicatormodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add Indicator to milestone</h4>
      </div>
      <div class="modal-body" style="max-height:500px; overflow-y:scroll;">
                    <?php 
        $form = ActiveForm::begin(['method' => 'post','action' => Url::to(['indicator/ind']),]);?>
<table class="table table-bordered">
        <tbody id="milestone_indicator">

                  </tbody>
        </table>

        <table class="table table-striped table-bordered">
        <tbody>
                    <tr>
                      <th style="width: 200px"> <?php echo $form->field($indicator, 'id')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VIndicator::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Milestones (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true
                                                ],
                                        ]);?>
                        </th>
                      <th><?php echo $form->field($indicator, 'indicator_type')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(IndicatorType::find()->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Milestones (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => false
                                                ],
                                        ]);?></th>
                       <th><?php echo $form->field($indicatorparams, 'weight',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Weight',]);?></th>
                       <th></th>
                       <th><?php echo $form->field($indicatorparams, 'target_value',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Target Value',]);?></th>
                      <th style="width: 40px"><input type="hidden" value="" id="'.$milestone->milestone_id.'"/>
                    <button type="submit" class="btn btn-primary" style="margin-top:25px;">Add Indicator</button></th>
                    </tr>
        </table>



                    <?php echo '<div class="modal-footer">
                    <input type="hidden" name="milestone" value="" id="milestone_value"/>
                    <input type="hidden" name="project" value="'.$model->id.'"/>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                  </div>';
                        ActiveForm::end();
                                        ?>



    </div>
  </div>
</div></div>

<div class="modal fade bs-example-modal-lg" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Delete Indicator from milestone</h4>
      </div>
      <div class="modal-body" >
       
<p id="delete-message">
    Do you really want to remove this indicator from this milestone?
</p>

<div class="modal-footer">
                    <input type="hidden" name="milestone" value="" id="milestone_delete"/>
                    <input type="hidden" name="indicator" value="" id="indicator_delete"/>
                    <input type="hidden" name="project" value="<?=$model->id?>"/>
                    <button id="delete-btn" class="btn btn-primary pull-right" style="margin-top:25px;">Delete Indicator</button></th>
                   
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  </div>



    </div>
  </div>
</div></div>
<?php 
$this->registerJsFile(Url::to(['/js/dateFormat.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);
$this->registerJsFile(Url::to(['/js/project/timeline.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);?>
<?= $this->registerJs(
/* Formatting function for row details - modify as you need */
"
$(document).ready(function() {
    // Add event listener for opening and closing details

	$('#indicator-id').on('change',function(){
		if($.isNumeric(this.value)){
			$.ajax({
            url: '".Url::to(['indicator/indicator'])."',
            type: \"post\",
            data: {indicator:this.value} ,
            success: function (response) {
                data = $.parseJSON(response);
                if(data){
                    $('#indicator-weight').val(data.weight);
                    $('#indicator-target_value').val(data.target_value);
                }             

            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
    });
		}

	})

$('.add_indicator_button').on('click',function(){
var milestone = $(this).attr('milestone');
$('#milestone_value').val(milestone);
$('#milestone_indicator').html('<tr>\
                        <th>Actions</th>\
                      <th width=\"200px\">Indicator</th>\
                      <th>Datatype</th>\
                      <th>Weight</th>\
                      <th>New Weight</th>\
                      <th>Target Value</th>\
                    </tr>');
//make ajax request to populate the table with indicators
$.ajax({
            url: '".Url::to(['indicator/milestone'])."',
            type: \"post\",
            data: {milestone:milestone,project:".$model->id."} ,
            success: function (response) {
                data = $.parseJSON(response);
   
                $.each(data, function (i, value) {
                if(!data[i].weight)data[i].weight='';
                if(!data[i].target_value)data[i].target_value='';
                if(!data[i].indicator_type_name)data[i].indicator_type_name='--';
                if(data[i].editable==0)
                $('#milestone_indicator').append('<tr>\
                                    <td width=\"10px\"><input type=\"hidden\" value=\"'+data[i].id+'\" name=\"p_indicator_id[]\"></td>\
                                    <td width=\"200px\">'+data[i].indicator+'</td>\
                                    <td>'+data[i].indicator_type_name+'</td>\
                                    <td><span><input type=\"text\" class=\"form-control\" value=\"'+data[i].weight+'\" name=\"p_weight[]\" placeholder=\"Weight\"></span></td>\
                                    <td><span>'+data[i].weight+'</span></td>\
                                    <td><span><input type=\"text\" class=\"form-control\" value=\"'+data[i].target_value+'\" name=\"p_target_value[]\" placeholder=\"Target Value\"></span></td>\
                                </tr>');
                else
                $('#milestone_indicator').append('<tr>\
                                    <td width=\"10px\"><input type=\"hidden\" value=\"'+data[i].id+'\" name=\"pr_indicator_id[]\">\
                                    <a class=\"fa fa-times-circle indicatormilestone\" data-toggle=\"modal\" data-target=\"#deletemodal\"  id=\"'+data[i].id+'\" milestone=\"'+$('#milestone_value').val()+'\" style=\"font-size:30px;cursor:pointer;\"></a></td>\
                                    <td width=\"200px\">'+data[i].indicator+'</td>\
                                    <td>'+data[i].indicator_type_name+'</td>\
                                    <td><span><input type=\"text\" class=\"form-control\" value=\"'+data[i].weight+'\" name=\"pr_weight[]\" placeholder=\"Weight\"></span></td>\
                                    <td><span>'+data[i].weight+'</span></td>\
                                    <td><span><input type=\"text\" class=\"form-control\" value=\"'+data[i].target_value+'\" name=\"pr_target_value[]\" placeholder=\"Target Value\"></span></td>\
                                </tr>'); 
                });           
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
});
});

$('body').on('click', '.indicatormilestone', function (){
$('#milestone_delete').val($(this).attr('milestone'));
   $('#indicator_delete').val($(this).attr('id'));
    });
$('#delete-btn').on('click',function(){
    var milestone = $('#milestone_delete').val();
    var indicator = $('#indicator_delete').val();
     $('#delete-message').html('Wait deleting message');
     $(this).attr('disabled',true);
    $.ajax({
            url: '".Url::to(['indicator/indicatordelete'])."',
            type: \"post\",
            data: {indicator:indicator,milestone:milestone,project:".$model->id."} ,
            success: function (response) {
                    $('#delete-message').html('Indicator Removed Successfully')       
                    $('#delete-btn').attr('disabled',false);
                    $('#deletemodal').modal('hide');
                    $('#indicatormodal').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
    });

})

$(\"#indicator-id\").on('change',function(){
    $.ajax({
            url: '".Url::to(['indicator/indicatordetails'])."',
            type: \"post\",
            data: {indicator:$(this).val()} ,
            success: function (response) {
                    $('#indicator-indicator_type').select2('val',response); 
                    $('#indicator-indicator_type').val(response).trigger(\"change\");     
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
    });
})

$(\"#indicator-id\").select2({
    placeholder: 'Select or type a new indicator if not in list',
    width: '100%' ,
    tags: true,
    maximumSelectionLength: 1,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

} );

")
?>