<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\VPipelineStage;
use backend\models\VMilestoneStage;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\DurationUnit;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */
/* @var $form yii\widgets\ActiveForm */
$this->title='Set Project Timelines';
?>

<div class="project-form">


    <?php $form = ActiveForm::begin(); ?>
<table id="w0" class="table table-striped table-bordered detail-view">
	 	Duration Units<?= Html::activeDropDownList($model, 'id',
      ArrayHelper::map(DurationUnit::find()->all(), 'id', 'unit')) ?>
	 <?php
	 $stages=VPipelineStage::find()->where(['pipeline'=>$model->pipeline_id])->orderBy(['ordering'=>SORT_ASC])->all();
	 $ctr=0;
	 foreach ($stages as $stage) {
		 ?>
		 <tr><th align=left colspan=4><h3><?=$stage->stage_name?></h3></th></tr>
		 <tr>
			 <th>MileStone</th>
			 <th style='width:200px'>Start Date</th>
			 <th>Duration</th>
			 <th>Comments</th>
		</tr>
		 <?php
		 
		 $milestones=VMilestoneStage::find()->where([
			 'stage_id'=>$stage->stage,
			 'pipeline_id'=>$stage->pipeline
			])->orderBy(['milestone_ordering'=>SORT_ASC])->all();
			 foreach($milestones as $milestone) {
				 $ctr++;
			?>
				<tr>
					<td align=left><?=$milestone->milestone?></td>
					<td>                    <?=  DatePicker::widget([
   					'name' => 'date['.$milestone->id.']', 
  //  'value' => date('Y-m-d', strtotime('+2 days')),
    'options' => ['placeholder' => '','style'=>'width:200px','id'=>'date_'.$ctr,'class'=>'milestone_date'],
    	  	'pluginOptions' => [
        	 'format' => 'yyyy-mm-dd',
        	'todayHighlight' => true
    		  ]
]);?></td>
					<td><?= Html::textInput('duration['.$milestone->id.']', '',['type' => 'number','id'=>'duration_'.$ctr,'class'=>'milestone_duration']); ?></td>
					<td><?= Html::textInput('comment['.$milestone->id.']', ''); ?></td>
				</tr>
			<?php
			 }
	 }
	 ?>
	 <tr><td colspan=4>
        <?= Html::submitButton( 'Save Timeline', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</td></tr>
</table>
    <?php ActiveForm::end(); ?>
</div>


<?= $this->registerJS("
(function($, window, document, undefined){
  $('.milestone_duration').change(function() {
    var myid = new $(this).attr('id').split('_');
	 var mid='date_'+ myid[1];
    var days = parseInt($(this).val());
	var olddate=parseDate($('#' + mid).val());

  //date = parseDate(date);
    if(!isNaN(olddate.getTime())){
            olddate.setDate(olddate.getDate() + days);
		var e=parseInt(myid[1]) + 1;
            $('#date_' + e).val(olddate.toInputFormat());
    } else {
            alert(\"Invalid Date\");  
    }
  });

	function parseDate(input) {
	  	var parts = input.match(/(\d+)/g);
	  	return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
	}

  Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return yyyy + \"-\" + (mm[1]?mm:\"0\"+mm[0]) + \"-\" + (dd[1]?dd:\"0\"+dd[0]); // padding
  };
})(jQuery, this, document);


    "); 
?>
