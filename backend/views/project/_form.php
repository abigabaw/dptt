<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'innovator')->textInput() ?>

    <?= $form->field($model, 'portfolio')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'institution')->textInput() ?>

    <?= $form->field($model, 'project_group')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'country')->textInput() ?>

    <?= $form->field($model, 'teamleader')->textInput() ?>

    <?= $form->field($model, 'award_date')->textInput() ?>

    <?= $form->field($model, 'signing_date')->textInput() ?>

    <?= $form->field($model, 'startstage')->textInput() ?>

    <?= $form->field($model, 'startmilestone')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
