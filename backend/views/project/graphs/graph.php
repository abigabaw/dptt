<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Milestone;
use yii\widgets\DetailView;
use backend\models\Queries;
use yii\bootstrap\ActiveForm;

$asset          = common\assets\HighStockAsset::register($this);
$baseUrl        = $asset->baseUrl;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
$graphs 			= Yii::$app->db->createCommand(Queries::getIndicatorSubmissionsGraphs($model->id))->queryAll();

?>
        <style type="text/css">
            table th:first-child {
                width: 150px;
            }
            /* Bootstrap 3.x re-reset */
            .fn-gantt *,
            .fn-gantt *:after,
            .fn-gantt *:before {
              -webkit-box-sizing: content-box;
                 -moz-box-sizing: content-box;
                      box-sizing: content-box;
            }
        </style>

<div class="project-view">
<div class="box box-success">
<?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
<div class="box-header bg-success with-border">
                  <h3 class="box-title">Select Indicators to plot against</h3>
                  <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-area-chart"></i> Plot Graph</button>
 </div>
    <div class="box-body ">
         <div class="row col-md-12">

         <?php
foreach ($graphs as $graph) {

	echo '<div class="col-md-6"><div class="checkbox">
                        <label>
                          <input type="checkbox" name="indicator_ids[]" value =\''.$graph['indicator_id'].'\'>
                          '.$graph['indicator_name'].'
                        </label>
         </div>
        </div>';
	# code...
}
         ?>


          </div>
	</div>
<?php ActiveForm::end(); ?>
</div>
<div class="box box-success">
<div class="box-header bg-success with-border">
 </div>
    <div class="box-body ">
         <h3 class="box-title">Indicator Graph</h3>
          	<div class="chart">
            	<?= \common\components\IndicatorSubmission::widget(['id'=>$model->id,'elementid'=>'container','indicator_ids'=>$indicator_ids]) ?> 
			</div>
	</div>

</div>
</div>
