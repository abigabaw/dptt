<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */

$this->title = 'View Grading';
$this->params['breadcrumbs'][] = ['label' => 'Project', 'url' => ['project/'.$submisson['project_id']]];
$this->params['breadcrumbs'][] = $submisson['milestone'];
?>
<div class="grading-view">
<div class="box box-success">
            <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [   'label' =>'Grade',
                'value' =>$model->grade,
            ],
            [   'label' =>'Comment',
                'value' =>$model->comment,
            ],
            [   'label' =>'Status',
                'value' =>$model->status0->name,
            ],
        ],
    ]) ?>

    </div>
</div>
<div class="box box-success">
            <div class="box-body">
                        <div class="box-header with-border">
                  <h3 class="box-title">Indicator Grades</h3>
            </div>
            <div class="box-body">
                       <table class="table">
                    <tbody><tr>
                      <th>Indicator</th>
                      <th>Score</th>
                    </tr>
 <?php
        foreach ($indicatorgrades as $indicator) {
                    echo'<tr>
                      <td>'.$indicator['indicator'].'</td>
                      <td>'.$indicator['value'].'</td>
                    </tr>';
        }
  ?>
                    </tbody></table>

    </div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")
  ?>