<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\GradingOutcome;
use backend\models\Milestone;
/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */
$grading_title = substr($model->milestone,0,80).'...';
$this->title = 'New Grading';
$this->params['breadcrumbs'][] = ['label' => 'Cohorts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $grading_title
?>
<div class="cohort-create">
<div class="box box-success">
			<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($model->milestone) ?></h3>
            </div>
            <div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>

    <?= $form->field($gradingmodel, 'comment',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a comment',])?>
    <?= $form->field($gradingmodel, 'grade',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter a the overal score',])?>

    <?= $form->field($gradingmodel, 'status',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
    ArrayHelper::map(GradingOutcome::find()->all(),'id','name'),

    ['prompt'=>'Please Select the Outcome of this Grading'] )?>

    <b>Indicators</b>
    <?php 
    if($indicators){

      echo '<table id="layout-skins-list" class="table table-striped bring-up nth-2-center">
              <tbody>
              <tr>
                <th>#</th>
                <th>Indicator</th>
                <th>Value Submitted</th>
                <th>Grade</th>
              <tr/>
              ';
                         $counter = 1;
    foreach ($indicators as $indicator) {
           
                         echo '<tr>
                                <td>'.$counter.'</td>
                                <td>'.$indicator['name'].'</td>
                                <td>'.$indicator['value'].'</td>
                                <td>
                                  <input type="text" class="form-control" name="indicator[]" >
                                  <input type="hidden" class="form-control" name="indicator_ids[]" value="'.$indicator['id'].'">
                                </td>
                              </tr>';
                          $counter++;
    }
                          echo'</tbody>
                              </table>';
  }else{
      echo '<div class="form-group"><div class="col-sm-12">
                      No Indicators where submitted
                      </div>
                      </div>';
    }?><br/>
      <div class="col-md-12"><b>Attachments</b></div>
    <?php 
    if($attachments){
    foreach ($attachments as $attachment) {
          echo'<div class="form-group"><div class="col-sm-12"><a href="'.Yii::$app->urlManagerFrontend->createUrl('').''.$attachment['attachment'].'" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download Attachment</a></div></div><br/><br/>';
              }
    }else{
           echo '<div class="form-group"><div class="col-sm-12">
                      No attachments where uploaded
                      </div>
                      </div>';
    }
    ?>

    <?= '<input type="hidden" name="iteration" value="'.$iteration.'">'?>

	<div class="form-group">

      <?= Html::submitButton('Submit Grading', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
    <?= $this->registerJs(
    " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")
    ?>