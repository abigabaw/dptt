<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Milestone;
use yii\widgets\DetailView;

$asset          = common\assets\DatatablesAsset::register($this);
$baseUrl        = $asset->baseUrl;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
        <style type="text/css">
            table th:first-child {
                width: 150px;
            }
            /* Bootstrap 3.x re-reset */
            .fn-gantt *,
            .fn-gantt *:after,
            .fn-gantt *:before {
              -webkit-box-sizing: content-box;
                 -moz-box-sizing: content-box;
                      box-sizing: content-box;
            }
        </style>

<div class="project-view">
<div class="box box-success">
            <div class="box-body ">
            <h3 class="box-title">Project Summary</h3>
            <div class="chart">
            <?= \common\components\ProjectReport::widget(['project'=>$model->id,]) ?> 
</div>
</div>

</div>
