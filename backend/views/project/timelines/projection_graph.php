<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\models\Milestone;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use backend\models\Queries;
use yii\helpers\Url;

$asset          = common\assets\HighStockAsset::register($this);
$baseUrl        = $asset->baseUrl;
$graphs       = Yii::$app->db->createCommand(Queries::getIndicatorSubmissionsGraphs($model->id))->queryAll();

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
            table th:first-child {
                width: 150px;
            }
            /* Bootstrap 3.x re-reset */
            .fn-gantt *,
            .fn-gantt *:after,
            .fn-gantt *:before {
              -webkit-box-sizing: content-box;
                 -moz-box-sizing: content-box;
                      box-sizing: content-box;
            }
	.graphs {
		height:650px;
	}
</style>
<div class="project-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">
                        <div class="box-tools pull-left">
                       <h2 class="box-title">SELECT THE GRAPHS TO DISPLAY</h2>
                       </div>
                  </h3>
              
            </div>
            <div class="box-body">
            <div class="col-md-4">
                <div class="checkbox">
                            <label>
                              <input class="dppt-graphs"  checked type="checkbox" id ="indicator_comparision">
                             Indicator Comparison 
                            </label>
                </div>
            </div>
            <div class="col-md-4">
              <div class="checkbox">
                        <label>
                          <input class="dppt-graphs" checked type="checkbox" id ="actual_timeline">
                         Actual timeline
                        </label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="checkbox">
                        <label>
                          <input class="dppt-graphs" checked type="checkbox" id ="actual_expected">
                         Actual timeline Vs Expected timeline
                        </label>
              </div>
            </div>
             <div class="col-md-4">
              <div class="checkbox">
                        <label>
                          <input class="dppt-graphs" checked type="checkbox" id ="start_end_dates">
                         Start and End Date of timelines
                        </label>
              </div>
            </div>
             <div class="col-md-4">
              <div class="checkbox">
                        <label>
                          <input class="dppt-graphs" checked type="checkbox" id ="actual_cummulative">
                         Actual timeline Vs Cummulative Scores
                        </label>
              </div>
            </div>
                         <div class="col-md-4">
              <div class="checkbox">
                        <label>
                          <input class="dppt-graphs" checked type="checkbox" id ="cummulative_scores">
                         Cummulative Scores
                        </label>
              </div>
            </div>
</div>
</div>
</div>

<div class="project-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">
                        <div class="box-tools pull-left">
                        <span class="label label-danger timeline_display" style="padding:5px; cursor:pointer;">Show Line of best fit</span>
                      </div>
                  </h3>
            </div>
            <div class="box-body" style="display:none" id="timeline_content">
            <h3 class="box-title">Expected Timeline</h3>
            <?= \common\components\ChartJSTimeline::widget(['id'=>$model->id]) ?> 
</div>
</div>
</div>
<div class="project-view">
<div class="box box-success">
            <div class="box-body ">
            <div class="box box-success">
<?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
<div class="box-header bg-success with-border">
                  <h3 class="box-title">Select Indicators to plot against</h3>
                  <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-area-chart"></i> Plot Graph</button>
 </div>
    <div class="box-body ">
         <div class="row col-md-12">
         <?php
foreach ($graphs as $graph) {

  echo '<div class="col-md-6"><div class="checkbox">
                        <label>
                          <input type="checkbox" name="indicator_ids[]" value =\''.$graph['indicator_id'].'\'>
                          '.$graph['indicator_name'].'
                        </label>
         </div>
        </div>';
}
         ?>


          </div>
  </div>
<?php ActiveForm::end(); ?>
</div>
            <div class="chart">
					 <a class="btn btn-xs btn-default modalButton"
			<?php $params=['id'=>$model->id,'indicator_ids'=>$indicator_ids];?>		 
			 href="<?=Url::to(['/site/widget','widget'=>'IndicatorSubmission','location'=>'common','params'=>$params])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
            <?php echo \common\components\IndicatorSubmission::widget($params) ?> 

					 <a class="btn btn-xs btn-default modalButton"
			<?php $params=['id'=>$model->id,'indicator_ids'=>$indicator_ids];?>		 
			 href="<?=Url::to(['/site/widget','widget'=>'IndicatorSubmission2','location'=>'common','params'=>$params])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
            <?php echo \common\components\IndicatorSubmission2::widget($params) ?> 
				<?php $param=['id'=>$model->id,'elementid'=>'contauiner3'];?>		 
				
				<a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'ActualTimeline','location'=>'common','params'=>['id'=>$model->id]])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                  <?php echo \common\components\ActualTimeline::widget($param) ?> 
						<a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'StartEndDateGraph','location'=>'common','params'=>['id'=>$model->id]])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                  <?php echo\common\components\StartEndDateGraph::widget(['id'=>$model->id,'elementid'=>'container4']) ?> 
                  <br/>
                <?php /* \common\components\ActualVsCumm::widget(['id'=>$model->id,
                                                                        'elementid'=>'actualvscumm',
                                                                        'scale_type'=>'indicator',
                                                                        'y_scale'=>'pipeline']) 
                                                              ?> 
                                <?php // \common\components\ActualVsCummStep::widget(['id'=>$model->id,
                                                                        'elementid'=>'actualvscummstep',
                                                                        'scale_type'=>'indicator',
                                                                        'y_scale'=>'pipeline']) 
                                                              ?> 
                <?= \common\components\ActualVsCummDates::widget(['id'=>$model->id,
                                                                        'elementid'=>'actualvscummdates']) 
                                                             */ ?>
					 <a class="btn btn-xs btn-default modalButton" href="<?=Url::to(['/site/widget','widget'=>'ComparisonTimeline','location'=>'common','params'=>['id'=>$model->id]])?>" title="Open in a New Window"><i class="glyphicon glyphicon-new-window"></i></a>
                 <?= \common\components\ComparisonTimeline::widget(['id'=>$model->id,'elementid'=>'container']) ?>
              <br/>
                  <?php /* \common\components\CummulativeScoreSingle::widget([
                                                                        'id'=>$model->id,
                                                                        'elementid'=>'container_cumulative',
                                                                        'scale_type'=>'milestone',
                                                                        'y_scale'=>'milestones_achieved'
                                                                        ]) ?>
                <?php \common\components\CummulativeScoreSingleMulti::widget([
                                                                        'id'=>$model->id,
                                                                        'elementid'=>'container_cumulative_multi',
                                                                        'scale_type'=>'milestone',
                                                                        'y_scale'=>'milestones_achieved'
                                                                        ]) */ ?>
                  

    

</div>
</div>

</div>
<?php
yii\bootstrap\Modal::begin([
		'header' => '',
		'id'=>'editModalId',
		'class' =>'modal',
		'size' => 'modal-lg',
]);
echo "<div class='modalContent' style='width:1000px'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(Url::to(['/js/modal.js']), [
    'depends'=>\yii\web\JqueryAsset::className()
]);
?>

  <?= $this->registerJs(
      " 
    $('.dppt-graphs').click(function(){
      if ($(this).prop('checked')==false) {
      if($(this).attr('id')=='indicator_comparision'){
        $('#container5').find('.highcharts-container').hide();
      }
      if($(this).attr('id')=='actual_timeline'){
        $('#container3').find('.highcharts-container').hide();
      }
      if($(this).attr('id')=='actual_expected'){
        $('#container').find('.highcharts-container').hide();
      }
      if($(this).attr('id')=='start_end_dates'){
        $('#container4').find('.highcharts-container').hide();
      }
      if($(this).attr('id')=='actual_cummulative'){
        $('#actualvscumm').find('.highcharts-container').hide();
        $('#actualvscummstep').find('.highcharts-container').hide();
        $('#actualvscummdates').find('.highcharts-container').hide();
      }
      if($(this).attr('id')=='cummulative_scores'){
        $('#container_cumulative').find('.highcharts-container').hide();
        $('#container_cumulative_multi').find('.highcharts-container').hide();
      }
      }else{
      if($(this).attr('id')=='indicator_comparision'){
        $('#container5').find('.highcharts-container').show();
      }
      if($(this).attr('id')=='actual_timeline'){
        $('#container3').find('.highcharts-container').show();
      }
      if($(this).attr('id')=='actual_expected'){
        $('#container').find('.highcharts-container').show();
      }
      if($(this).attr('id')=='start_end_dates'){
        $('#container4').find('.highcharts-container').show();
      }
      if($(this).attr('id')=='actual_cummulative'){
        $('#actualvscumm').find('.highcharts-container').show();
        $('#actualvscummstep').find('.highcharts-container').show();
        $('#actualvscummdates').find('.highcharts-container').show();
      }
      if($(this).attr('id')=='cummulative_scores'){
        $('#container_cumulative').find('.highcharts-container').show();
        $('#container_cumulative_multi').find('.highcharts-container').show();
      }
      }
    });")

  ?>