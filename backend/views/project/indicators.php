<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\Innovator;
use backend\models\Timeline;
use backend\models\Portfolio;
use common\models\Institution;
use backend\models\Country;
use backend\models\Stage;
use backend\models\Milestone;
use backend\models\LoginInnovator;
use backend\models\Program;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = 'Add Indicators to Milestone <strong>'.$milestone.'</strong> in Project: <strong>'.$model->name.'</strong>';
?>
<div class="project-update">
<div class="box box-success">
<div class="box-header bg-success with-border"><h3 class="box-title"><?= $this->title ?></h3></div>
<div class="box-body">
	<?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
	<table id="project-timelines" class="table table-striped table-bordered detail-view">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>#</th>
			<th>Indicator</th>
			<th>Data Type</th>
			<th>Weight</th>
			<th>Target Value</th>
		</tr>
	</thead>	
	<tbody>
	<tr><td colspan=6><h4>Pipeline Indicators</h4></td></tr>
	<?php foreach($pipeline as $p) { $c++?>
		<tr>
		<td>&nbsp;</td>
		<td><?=$c?></td>
		<td><?=$p['name']?></td>
		<td><?=$p['indicatortype']?></td>
		<td><input type='number' name=weight[<?=$p['indicator']?>] value='<?=$p['weight']?>' /></td>
		<td><input type='number' name=target[<?=$p['indicator']?>] value='<?=$p['target_value']?>' /></td>
	</tr>
	<?php } ?>
	<tr><td colspan=6><h4>Project Indicators</h4></td></tr>
	<?php foreach($project as $p) { $c++?>
		<tr>
		<td><?=Html::a( '<i class="glyphicon glyphicon-remove-sign"></i>',
			['removeindicators', 'id'=>$model['id'],'milestone'=>$p['milestone']],
			['style'=>'color:red', 'title'=>'Remove this Indicator', ]
		);?>                   
		</td>
		<td><?=$c?></td>
		<td><?=$p['name']?></td>
		<td><?=$p['indicatortype']?></td>
		<td><input type='number' name=weight[<?=$p['indicator']?>] value='<?=$p['weight']?>' /></td>
		<td><input type='number' name=target[<?=$p['indicator']?>] value='<?=$p['target_value']?>' /></td>
	</tr>
	<?php } ?>
	</tbody></table>
	<div class="form-group"><?= Html::submitButton('Update Indicators', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?></div>
	<?php ActiveForm::end(); ?>
</div>
</div></div>