<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\models\Innovator;
use backend\models\Portfolio;
use common\models\Institution;
use backend\models\Country;
use backend\models\VStage;
use backend\models\VMilestone;
use backend\models\LoginInnovator;
use backend\models\VProgram;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = 'Creating a New Project';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">Fill in the form below to create a project</h3>
            </div>
            <div class="box-body">
            <?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
    <?= $form->field($model, 'name',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Project Name',]) ?>
                <?= $form->field($model, 'program',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(VProgram::find()->where(['pipeline_id'=>Yii::$app->session['pipeline'],'institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                    ['prompt'=>'Please Select the progam under which this project belongs'] )?>

                <?= $form->field($model, 'amount',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Project Amount',]) ?>
                <?= $form->field($model, 'institution')->hiddenInput(['value'=> Yii::$app->user->identity->institution])->label(false) ?>

                    <?= $form->field($model, 'teamleader')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(LoginInnovator::find()->orderBy('name ASC')->all(),'id','name'),
                        'options' => ['placeholder' => 'Select the project team leader (s)'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => false
                        ],
                ]);
    ?>
                <?= $form->field($model, 'description',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Project Description',]) ?>

                <?= $form->field($model, 'country',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Country::find()->all(),'id','name'),
                    ['prompt'=>'Please Select Country'] )?>
                    <?= $form->field($model, 'award_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter Award Date'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-d',
                            ]
                        ]);?>
                    <?= $form->field($model, 'signing_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter Signing Date'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-d',
                            ]
                        ]);?>

                    <?= $form->field($model, 'startstage',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(ArrayHelper::map(VStage::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','stage'),
                    ['prompt'=>'Please Select Start stage'] )?>

                    <?= $form->field($model, 'startmilestone',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList( ArrayHelper::map(VMilestone::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->orderBy('milestone')->all(),'id','milestone'),
                    ['prompt'=>'Please Select Starting Milestone'] )?>
        <div class="form-group">
      <?= Html::submitButton('Create Project', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>

    <?= $this->registerJs(
      ' 
$("#project-teamleader").select2({
    placeholder: \'Select or type a new team leader if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

$("#project-startstage").on("change",function(){
    var options = $("#project-startmilestone");
    $("#project-startmilestone").html("");
    var stage_id = $(this).val();
    $.ajax({
        url: "'.Url::to(['milestone/milestones']).'",
        type: "post",
        data: {stage_id:stage_id} ,
        success: function (response) {
            data = $.parseJSON(response);
            $.each(data, function(i, item) {
            options.append(new Option(item.milestone, item.id));
            });               

        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
});

')?>