<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IndicatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Milestone Iteration Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-index">
<div class="box box-success">
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'milestone_name',
            'project_name',
            'reason:ntext',
           [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}',
            'buttons' => [
                'new_action1' => function ($url, $dataProvider) {
                    if($dataProvider['status']==0)
                    return Html::a('Approve <span class="fa  fa-hand-o-right"></span>', ['approve', 'id' =>$dataProvider['id']], ['class' => 'btn btn-primary btn-sm']).'<br/>
                    <br/> 
                    <button class="btn btn-danger btn-sm request_button" id="'.$dataProvider['id'].'" data-toggle="modal" data-target=".bs-example-modal-lg" style="z-index:6666"><i class="fa  fa-hand-o-right"></i>  Reject</button>';
                    else if($dataProvider['status']==1)
                    return 'Approved';
                    else if($dataProvider['status']==2)
                    return 'Rejected';
                },
            ],
            ],
        ],
    ]); ?>
</div>
    </div>
</div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Reject Iteration Request</h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'milestone-form','action' =>['/milestone/reject']]);
                         echo $form->field($model, 'supervisor_comment',
                          ['options'=>[
                                'tag'     =>'div',
                                'class'     =>'form-group fieldcohort-form-name has-feedback required'],
                                'template'    =>'{input}{error}{hint}'
                          ])->textArea(['placeholder'=>'Reason for Rejecting',]);

                                   echo '<div class="modal-footer">
                                   <input type="hidden" name="request_id"  id="request_id" value=""/>
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Reject Iteration Request</button>
                    </div>';
                        ActiveForm::end();
                                        ?>



    </div>
  </div>
</div></div>
   <?= $this->registerJs(
      " 
    $('#request_tree').addClass('active');
    $('#repeat_requests').addClass('active');
    $('.request_button').click(function(){
        $('#request_id').val($(this).attr('id'));
    });
    ")

  ?>