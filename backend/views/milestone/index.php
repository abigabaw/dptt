<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MilestoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Milestones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-index">
<div class="box box-success">

                <div class="box-body">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Milestone', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'num',
            [
            'attribute'=>'Name',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider['milestone'], ['view', 'id' => $dataProvider['id']]);
                  
            }
            ],
            'description:ntext',

          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div></div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#milestone_nav').addClass('active');")
  ?>