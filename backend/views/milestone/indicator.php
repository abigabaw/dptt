<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;


$this->title = 'Rearrange Indicators';
$this->params['breadcrumbs'][] = ['label' => 'Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = substr($model->milestone,0,20).'...';
?>

<div class="stage-indicator">
<div class="box box-success">
<div class="box-header bg-success with-border">
            <h3 class="box-title"><?=$model->milestone?></h3>
            </div>
            <div class="box-body">
    <?php
        if( is_array( $dataProvider ) && count( $dataProvider ) > 0 ) {
            $count = 1;
        foreach($dataProvider as $v)
        {
            $items[$v['indicator_id']]['content'] = $count.' - '.$v['indicator'];
            $count++;
        }
         
        $form = ActiveForm::begin();
         
        // Kartiks widget
        echo SortableInput::widget([
        'name'=> 'sort_list',
        'items' => $items,
        'hideInput' => true,
        ]);
         
        echo Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']);

         
        ActiveForm::end();
        }else{
            echo 'There are no indicators in this milestone. You can add indicators by clicking  
                <a href="'.Url::to(['/indicator-milestone']).'">
                <i class="fa fa fa-map-o"></i> <span>Here</span>
              </a>';
        }
?>

</div>
</div></div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#milestone_nav').addClass('active');")
  ?>