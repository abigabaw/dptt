<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Milestone */

$this->title = 'Milestone Details';
$this->params['breadcrumbs'][] = ['label' => 'Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = substr($model->milestone,0,20).'...';
?>
<div class="milestone-view">
<div class="box box-success">
            <div class="box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('View Indicators', ['indicators', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Assign Indicators', ['indicator-milestone/indicators','id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'num',
            'milestone',
            'description:ntext',
        ],
    ]) ?>
</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#milestone_nav').addClass('active');")
  ?>