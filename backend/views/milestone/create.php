<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\Milestone */

$this->title = 'Create Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-create">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
 <?php $form = ActiveForm::begin(['id' => 'milestone-form']); ?>
					 <?= $form->field($model, 'milestone',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Milestone Name',]) ?>
					<?= $form->field($model, 'num',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Milestone Num',]) ?>
					<?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'Brief description of Milestone',]) ?>
					<div class="form-group">
				      <?= Html::submitButton('Create Milestone', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#milestone_nav').addClass('active');")
  ?>