<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\OutputIndicator */

$this->title = Yii::t('app', 'Create Output Indicator');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Output Indicators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="output-indicator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
