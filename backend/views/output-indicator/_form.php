<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OutputIndicator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="output-indicator-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'indicator')->textInput() ?>

    <?= $form->field($model, 'output')->textInput() ?>

    <?= $form->field($model, 'target_value')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'proxy')->dropDownList([ 'Y' => 'Y', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
