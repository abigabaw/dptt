<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OutputIndicator */

$this->title = 'Edit Add/Edit Indicators';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Output Indicators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="output-indicator-view">

    <h1><?= Html::encode($this->title) ?></h1>

   <?=$indicator?>
	New Indicator
	<table>
		<tr><td>Indicator</td><td><input /></td></tr>
		<tr><td>Data Type</td><td><select><option>Number</option><option>Percentage</option><option>Boolean</option><option>Text</option></select></td></tr>
		<tr><td>Default Weight</td><input /></tr>
		<tr><td>Default Target Value</td><input /></tr>
		<tr><td colspan=2><input type='submit' value='Submit'/>
	</table>
</div>
