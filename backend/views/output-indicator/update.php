<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OutputIndicator */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Output Indicator',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Output Indicators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="output-indicator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
