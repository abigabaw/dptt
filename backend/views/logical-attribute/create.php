<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LogicalAttribute */

$this->title = Yii::t('app', 'Create Logical Attribute');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Logical Attributes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logical-attribute-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
