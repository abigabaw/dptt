<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\PipelineStageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pipeline Stages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pipeline-stage-index">
<div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pipeline Stage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
             'attribute' => 'Pipeline',
             'value' => 'pipeline0.name'
             ],
            [
             'attribute' => 'Stage',
             'value' => 'stage0.stage'
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
    </div>
</div>
