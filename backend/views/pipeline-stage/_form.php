<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PipelineStage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pipeline-stage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pipeline')->textInput() ?>

    <?= $form->field($model, 'stage')->textInput() ?>

    <?= $form->field($model, 'ordering')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
