<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PipelineStage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pipeline Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pipeline-stage-view">
 <div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
     </div>
       <div class="box-body">

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [   'label' =>'Pipeline',
                'value' =>$model->pipeline0->name,
            ],
            [   'label' =>'Grant',
                'value' =>$model->stage0->stage,
            ],
        ],
    ]) ?>

</div>
</div>
</div>
