<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Institution;
use backend\models\Country;

/* @var $this yii\web\View */

/* @var $model backend\models\Innovator */

$this->title = 'Create Project Team';
$this->params['breadcrumbs'][] = ['label' => 'Project Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-create">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
 
<?php $form = ActiveForm::begin(['id' => 'innovator-form']); ?>
	<?= $form->field($model, 'names',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Full Name of the Project Team',]) ?>
	<?= $form->field($model, 'instituition',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Institution::find()->all(),'id','names'),
	['prompt'=>'Select Insititution'] )?>
    <?= $form->field($model, 'email',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Email Address of the Project Team',]) ?>
    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textArea(['placeholder'=>'A brief description of the Project Team','rows'=>6,]) ?>

     <?= $form->field($model, 'country',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Country::find()->all(),'id','name'),

	['prompt'=>'Select Country'] )?>
	<div class="form-group">
      <?= Html::submitButton('Create Project Team', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
</div>
</div>