<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InnovatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-index">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                       
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'email:email',
            'name',
            'gender',
            // 'gender',
            // 'country',
            // 'status',
            // 'created_at',
            // 'updated_at',

           [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}{new_action2}{new_action3}',
            'buttons' => [
                'new_action1' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> ', $url, [
                                'title' => Yii::t('app', 'View'),
                    ]);
                },
                'new_action2' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span> ', $url, [
                                'title' => Yii::t('app', 'Edit'),
                    ]);
                },
                'new_action3' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span> ', $url, [
                                'title' => Yii::t('app', 'Delete'),
                    ]);
                }
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'new_action1') {
                    $url = Url::to(['innovator/user/'.$model['id'].'']);
                    return $url;
                }
                if ($action === 'new_action2') {
                    $url = Url::to(['innovator/user/'.$model['id'].'']);
                    return $url;
                }
                if ($action === 'new_action2') {
                    $url = Url::to(['innovator/user/'.$model['id'].'']);
                    return $url;
                }
              }
            ],
        ],
    ]); ?>
</div>
</div>
</div>
 <?= $this->registerJs(
      " 
      $('#project_tree').addClass('active');
    $('#project_admins').addClass('active');")

  ?>