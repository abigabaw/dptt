<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Institution;
use common\models\Innovator;
use backend\models\Country;
use backend\models\Gender;

/* @var $this yii\web\View */

/* @var $model backend\models\Innovator */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Innovators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-create">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
 
<?php $form = ActiveForm::begin(['id' => 'innovator-form']); ?>
	<?= $form->field($model, 'name',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Name of User',]) ?>
    <?= $form->field($model, 'email',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Email Address of the Innovator',]) ?>
    <?= $form->field($model, 'gender',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Gender::find()->all(),'gender','gender'),
	['prompt'=>'Select Gender'] )?>
	<div><?=Yii::$app->session->getFlash('success')?></div>
	<div class="form-group">
      <?= Html::submitButton('Create Project Admin', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_teams').addClass('active');")

  ?>