<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Innovation */

$this->title = 'Create Innovation';
$this->params['breadcrumbs'][] = ['label' => 'Innovations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovation-create">
<?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
<?= $form->field($model, 'name',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Project Name',]) ?>
<?php ActiveForm::end(); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
