<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovation */

$this->title = $model->innovation;
$this->params['breadcrumbs'][] = ['label' => 'Innovations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovation-view">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add Timelines', ['timeline','id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('View Timelines', ['/timeline','innovation' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'innovation',
             [   'label' =>'Innovator',
                 'value' =>$model->innovator0->names,
             ],
            'amount',
            [   'label' =>'Innovator',
                 'value' =>$model->innovator0->names,
             ],
             [   'label' =>'Project',
                 'value' =>$model->project0->name,
             ],
            'description:ntext',
             [   'label' =>'Country',
                 'value' =>$model->country0->name,
             ],
            [   'label' =>'Team Leader',
                 'value' =>$model->teamleader0->name,
             ],
            'award_date',
            'signing_date',
            [   'label' =>'Start Stage',
                 'value' =>$model->startstage0->stage,
             ],
            [   'label' =>'Start Milestone',
                 'value' =>$model->startmilestone0->milestone,
             ],
        ],
    ]) ?>

</div>
</div>
</div>
