<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;
use backend\models\VProject;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class MilestoneSubmission2 extends Widget{
	public $dataProvider;
	public $threshold;
    	public $element_id;
	public $dataset_cummulative;
	public $data;
    	public $flags;
    	public $max;
 	public $portfolio;
	public function init(){
    $ct=0;
    //Scale 2: Milestones of pipeline
    $mstone2[0]= 1; 
    $mstone2[1]= 2;
    $st=PipelineStage::find()->where(['pipeline'=>Yii::$app->session['pipeline']])->orderBy(['ordering'=>SORT_ASC])->All();
    foreach($st as $stage) {
        $ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc,milestone asc")->queryAll();
        foreach ($ms as $mst) {
            $mstone2[$mst['milestone']]=++$ct;
        }
    }
    $scale=$mstone2;
    //print_r($mstone2);


        $qry = Yii::$app->db->createCommand(Queries::MilestoneSubmissionsAll($this->portfolio))->queryAll();
		//  echo Queries::MilestoneSubmissionsAll();
        foreach ($qry as $qry) {
			 // echo $qry['project']."<br>";
            $this->data .='{
                        type: \'spline\',
                        id: \'project-'.$qry['project_id'].'\',
                        dashStyle: \'line\',
                        name: \''.$qry['project'].'</em>\',
			lineWidth : 0,
                        step:true,
                        data: ['.$this->getSubmissions($qry['project_id'],$scale).'],
                        tooltip: {
                            xDateFormat: \'%B %Y\',
                            valueSuffix: \' '.$qry['project'].'\'
                        }
                    },';
           /*  $flags .='{
                         type: \'flags\',
                         name: \'Proposed Timeline\',
                         color: \'#333333\',
                         shape: \'circlepin\',
                         data: [
                             '.$this->getSubmissions($qry['project_id'],'flags').'
                         ],
                         showInLegend: true
                      },';*/

        }  
            
        $this->data = rtrim($this->data, ',');
        $this->flags = rtrim($this->flags, ',');
        $this->max = @max($this->max);
	}

	private function getSubmissions($project,$scale){
        $dataset ='';
        $sql = 'SELECT * FROM `v_milestone_submission`
                WHERE project_id ='.$project.' order by created_at asc';  
        $qry =  Yii::$app->db->createCommand($sql)->queryAll();
        //append award and signing date
        $project = VProject::find()->where(['id'=>$project])->one();
        $dataset .= '{ x: '.strtotime($project->award_date).'000,
                       y: 1,
                       text:\'<b>Award Date</b>\'
                     },
                     {x: '.strtotime($project->signing_date).'000,
                       y: 2,
                       text:\'<b>Signing Date</b>\'
                     },';
       
        foreach ($qry as $qry) {
		$ddate=date_format(date_add(date_create($qry['start_date']),date_interval_create_from_date_string($qry['duration']." days")),"Y-m-d");
		$delay=(strtotime($qry['created_at'])-strtotime($ddate));
		$this->threshold=14*86400;
		if($delay>$this->threshold) {
        $dataset .= '{ x: '.strtotime($qry['created_at']).'000, y: '.$scale[$qry['milestone_id']].',text:\'<b>'.$scale[$qry['milestone_id']].$qry['project'].': Achieved milestone ('.date('F d, Y',strtotime($qry['created_at'])).' that was supposed to be on '.$ddate.' - '.($delay/86400).'):</b> <br/>'.str_replace('\'', '', $qry['milestone']).'<br/>\' } ,';
	  }
        }
        $dataset = rtrim($dataset, ',');
        return $dataset;
    }
	private function getMaxYaxis($project){
        $sql = 'SELECT max(milestone_no) as milestone_no FROM  v_milestone_submission
                    WHERE project_id ='.$project.'';
        $qry =  Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($qry as $qry) {
        $max =  $qry['milestone_no'];       
        }
        return $max; 

    }
	public function run(){
	return $this->render('/widget/milestonesubmission',
						['data' 			=> $this->data,
						'flags'=>$this->flags,
						'max_y'=>$this->max,
                       'element_id'=>$this->element_id ? $this->element_id : md5(rand(1, 10005)),
                        'title'=>'Instances of all Milestone Delays  > '.($this->threshold/86400).' Days (Non-ARVs)',
						]);
	}
}
?>