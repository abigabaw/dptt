<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class CummulativeScoreSingle extends Widget{
	public $id;
	public $dataset_cummulative;
	public $tooltip;
	public $elementid;
	public function init(){
	$qry_cummulative     = Yii::$app->db->createCommand(Queries::qryCummulative($this->id))->queryAll();
	$score = 0;
	foreach ($qry_cummulative as $qry) {
            $score +=$qry['grade']; 
            $this->dataset_cummulative .= '['.$qry['id'].','.$score.' ],';
            $this->tooltip =  'tooltip: {
                                    headerFormat: \'<b>{series.name}</b><br/>\',
                                    pointFormat: \'<b>Indicator:</b>'.$qry['indicator'].' <br/> <b>Cummulative Score:</b> '.$score.' <br/><b>Actual Score:</b>'.$qry['grade'].'\'
                                  },';
            //$dataset_data2 .='{ x: '.$qry2['created_at'].'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $qry2['milestone']).'<br/>\' },';
       
        }
    $this->dataset_cummulative = rtrim($this->dataset_cummulative);
    $this->tooltip 			   = rtrim($this->tooltip);
	}
	public function run(){
	return $this->render('/widget/cummulativescoresingle',
						['dataset_cummulative' 			=> $this->dataset_cummulative,
						 'tooltip' 						=> $this->tooltip,
						 'elementid'					=> $this->elementid,
						]);
	}
}
?>