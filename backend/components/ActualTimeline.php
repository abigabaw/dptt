<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;

class ActualTimeline extends Widget{
	public $id;
	public $elementid;
	public $series;

	public function init(){
	$series=[];
	//Scale 1: Distinct Milestones Achieved
	$m=Yii::$app->db->createCommand("SELECT distinct milestone_id from v_milestone_submission where project_id=".$this->id." order by start_date asc")->queryAll();
	foreach($m as $ms)
		$mstone[$ms['milestone_id']]=++$ct;
	
	//Scale 2: Milestones of pipeline
	$st=PipelineStage::find()->where(['pipeline'=>Yii::$app->session['pipeline']])->orderBy(['ordering'=>SORT_ASC])->All();
	foreach($st as $stage) {
		$ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
		foreach ($ms as $mst) {
			$mstone2[$mst['milestone']]=++$ctr;
		}
	}
	$scale=$mstone2;
	//print_r($mstone2);
	
	$iteration_types=IterationType::find()->All();
	foreach ($iteration_types as $it ) {
		$iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id=".$this->id)->queryAll();
		if($iterations) {
			//$data_points=$data_flags='';
			$data_points=$data_flags=[];
			foreach ($iterations as $ar) {
				$data_points[]=[
						'x'=>strtotime($ar['start_date']).'000',
						'y'=>$scale[$ar['milestone_id']]
				];
             	  	$data_flags[]=[
						'x' =>strtotime($ar['start_date']).'000', 
						'text' => '<b>'.$it['name'].' (Start):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
				];
				if($ar['end_date']) {
					$data_points[]=[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>$scale[$ar['milestone_id']]
					];
	             	  	$data_flags[]=[
							'x' =>strtotime($ar['end_date']).'000', 
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
				}
			}
			$series[]=[
            		'type'		=> 'flags',
            		//'name'	=> $it['name'],
            		'color'		=> $it['color_code'],
            		//fillColor: \'rgba(255,255,255,0.8)\',
            		'data'=>$data_flags,
 				//'onSeries' => 'revenue',
            		//'showInLegend'=> false					
			];
			$series[]=[
      			'name'	=>$it['name'],
				'data' =>$data_points,
				//fillColor' => '#123457',
				'dashStyle' => $it['dash_style'],
				'color'  =>$it['color_code'],
				'step' => true,
			];
			
		}
	}
 $this->series =$series;
    }

	public function run(){
	
	return $this->render('/widget/actualtimeline',
						[
						'series'=>$this->series,
						'elementid'	=> $this->elementid,
						]);
	}
}
?>