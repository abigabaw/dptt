<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;
use backend\models\VProject;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class MilestoneDelays extends Widget{
	public $dataProvider;
	public $threshold;
    	public $element_id;
	public $dataset_cummulative;
	public $data;
    	public $flags;
    	public $max;
 	public $portfolio;
	public function init(){
    $ct=0;
    //Scale 2: Milestones of pipeline
    $mstone2[0]= 1; 
    $mstone2[1]= 2;
    $st=PipelineStage::find()->where(['pipeline'=>Yii::$app->session['pipeline']])->orderBy(['ordering'=>SORT_ASC])->All();
    foreach($st as $stage) {
        $ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc,milestone asc")->queryAll();
        foreach ($ms as $mst) {
            $mstone2[$mst['milestone']]=++$ct;
        }
    }
    $scale=$mstone2;
    //print_r($mstone2);

	 $dset=$this->getSubmissions($scale);
//	echo "<pre>"; print_r($dset['delayed']);exit;
     $dat.='{
                 type: \'spline\',
                 id: \'Submissions\',
                 dashStyle: \'line\',
                 name: \'Timely Attainments\',
lineWidth : 0,
                 step:true,
		lineColor: \'green\',
		color: \'black\',
                 data: ['.$dset['ontime'].'],
                 tooltip: {
                     xDateFormat: \'%B %Y\',
                     valueSuffix: \'D\'
                 }
             },';
		              $dat2.='{
		                          type: \'spline\',
		                          id: \'Delays\',
		                          dashStyle: \'line\',
								  		
								  		
					
		                          name: \'Delayed Attainments\',
		  			lineWidth : 0,
					color:\'red\',
		                          step:true,
		                          data: ['.$dset['delayed'].'],
		                          tooltip: {
		                              xDateFormat: \'%B %Y\',
		                              valueSuffix: \'D\'
		                          }
		                      },';
$this->data=$dat.$dat2;
           /*  $flags .='{
                         type: \'flags\',
                         name: \'Proposed Timeline\',
                         color: \'#333333\',
                         shape: \'circlepin\',
                         data: [
                             '.$this->getSubmissions($qry['project_id'],'flags').'
                         ],
                         showInLegend: true
                      },';*/ 
            
        $this->data = rtrim($this->data, ',');
        $this->flags = rtrim($this->flags, ',');
        $this->max = @max($this->max);
	}

	private function getSubmissions($scale){
        $dataset =$dataset2='';
	$s=$this->portfolio ? "portfolio_id=".$this->portfolio : "pipeline=".Yii::$app->session['pipeline'];
        $sql = 'SELECT * FROM `v_milestone_submission`
                WHERE '.$s.' order by created_at asc limit 20000';  
	//	  echo $sql;
        $qry =  Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($qry as $qry) {
		$ddate=date_format(date_add(date_create($qry['start_date']),date_interval_create_from_date_string($qry['duration']." days")),"Y-m-d");
		$delay=(strtotime($qry['created_at'])-strtotime($ddate));
		$this->threshold=14*86400;
		if($delay<$this->threshold) 
$datase2 .= '{ x: '.strtotime($qry['created_at']).'000, y: '.$scale[$qry['milestone_id']].',text:\'On Time\' },';
else 
$datase .= '{ x: '.strtotime($qry['created_at']).'000, y: '.$scale[$qry['milestone_id']].',text:\'Delay\' },';
	  
        }
        $datas = rtrim($datase, ',');
	$datas2 = rtrim($datase2, ',');
        return ['ontime'=>$datas2,'delayed'=>$datas];
    }
	private function APCIteratorYaxis($project){
        $sql = 'SELECT max(milestone_no) as milestone_no FROM  v_milestone_submission
                    WHERE project_id ='.$project.'';
        $qry =  Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($qry as $qry) {
        $max =  $qry['milestone_no'];       
        }
        return $max; 

    }
	public function run(){
	return $this->render('/widget/milestonesubmission',
						['data' 			=> $this->data,
						'flags'=>$this->flags,
						'max_y'=>$this->max,
                       'element_id'=>$this->element_id ? $this->element_id : md5(rand(1, 10005)),
                        'title'=>'Timely Milestone Attainments Vs. Delays  > '.($this->threshold/86400).' Days (Non-ARV Drugs)',
						]);
	}
}
?>