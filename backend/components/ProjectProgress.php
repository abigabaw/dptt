<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\Timeline;
use backend\models\VMilestoneSubmission;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class ProjectProgress extends Widget{
	public $projects;
	public $progress;
	public function init(){
    $this->projects = Yii::$app->db->createCommand(Queries::qryProjects())->queryAll();
	    foreach ($this->projects as $kp => $value) {
	    	$timeline_count 				= count(Timeline::find()->where(['project'=>$value['id']])->groupBy('milestone')->all());
	    	$submissions_count 				= count(VMilestoneSubmission::find()->where(['project_id'=>$value['id'],'grading_status'=>1])->groupBy('milestone_id')->all());
	    	if($timeline_count>0)
	    	$project_progress 				=  $submissions_count/$timeline_count;
	    	else 
	    	$project_progress 				= 0;
	    	$this->progress[$kp]			= [
	    								'progress'	=>	$project_progress,
	    								'project'	=>	$value['name'],
	    								'project_id'=>	$value['id'],
	    					  		  ];
	    	# code...
	    }
	}
	public function run(){
	return $this->render('/widget/projectprogress',
						['projects' 			=>$this->progress,
						]);
	}
}
?>