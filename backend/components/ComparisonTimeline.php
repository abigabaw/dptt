<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class ComparisonTimeline extends Widget{
	public $id;
	public $elementid;
	public $dataset_normal;
	public $dataset_data_normal;

	public $dataset_repeat;
	public $dataset_data_repeat;

	public $dataset_iteration;
	public $dataset_data_iteration;

	public $dataset_extension;
	public $dataset_data_extension;

	public $dataset_expected;
	public $dataset_data_expected;

	public function init(){
	$qry_expected        = Yii::$app->db->createCommand(Queries::qry($this->id))->queryAll();
	$qry_actual          = Yii::$app->db->createCommand(Queries::qryMilestoneSubmission($this->id))->queryAll();
	
    foreach($qry_actual as $key => $ar){
    	switch($ar['iteration_type']){
    	case 'Normal':
            $this->dataset_normal .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Started milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_normal .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
        	//set other values to null


        break;
        case 'Repeat':
        	$this->dataset_repeat .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_repeat .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Started repeat of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_repeat .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted repeat of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
      	break;
      	case 'Iteration':
        	$this->dataset_iteration .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_iteration .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Started iteration of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_iteration .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_iteration .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted iteration of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
      	break;
      	case 'Extension':
        	$this->dataset_extension .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_extension .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Extended milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_extension .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_extension .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
      	
        	$this->dataset_normal .= '{ x: '.strtotime($ar['start_date']).'000, y: null },';
      	break;
        }	
        }


        foreach($qry_expected as $key => $ar){
 			$this->dataset_expected .='{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_expected .='{ x: '.strtotime($ar['start_date']).'000, text:\'<b>Supposed to submit milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'\' },';
        }

        $this->dataset_normal 			= rtrim($this->dataset_normal);
        $this->dataset_data_normal 		= rtrim($this->dataset_data_normal);

        $this->dataset_repeat 			= rtrim($this->dataset_repeat);
        $this->dataset_data_repeat 		= rtrim($this->dataset_data_repeat);

        $this->dataset_iteration 		= rtrim($this->dataset_iteration);
        $this->dataset_data_iteration 	= rtrim($this->dataset_data_iteration);

        $this->dataset_extension 		= rtrim($this->dataset_extension);
        $this->dataset_data_extension 	= rtrim($this->dataset_data_extension);

		$this->dataset_expected 		= rtrim($this->dataset_expected);
        $this->dataset_data_expected 	= rtrim($this->dataset_data_expected);
    }

	public function run(){
	return $this->render('/widget/comparisontimeline',
						[
						'dataset_normal' 			=> $this->dataset_normal,
						'dataset_data_normal' 		=> $this->dataset_data_normal,
						'dataset_repeat' 			=> $this->dataset_repeat,
						'dataset_data_repeat' 		=> $this->dataset_data_repeat,
						'dataset_iteration' 		=> $this->dataset_iteration,
						'dataset_data_iteration' 	=> $this->dataset_data_iteration,
						'dataset_extension' 		=> $this->dataset_extension,
						'dataset_data_extension' 	=> $this->dataset_data_extension,
						'dataset_expected' 			=> $this->dataset_expected,
						'dataset_data_expected' 	=> $this->dataset_data_expected,
						'elementid'					=> $this->elementid,
						]);
	}
}
?>