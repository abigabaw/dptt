/* @var project_timeline_duration_unit ∈ [minute, hour, day, week, month, year] from php code*/

/*** Modify string prototype to support C#/Java style String.format() function ***/
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match;
        });
    };
}

/*** Modify Date prototype to support C#/Java style Date.format() function ***/
if(!Date.prototype.format){
    Date.prototype.format = function (mask, utc) {
        return dateFormat(this, mask, utc);
    };
}

var Dptt = {
        timeline : {
            current_unit: project_timeline_duration_unit,
            current_format: project_timeline_datetime_format,
            durations: {
                second : 1000,
                minute: 60000,
                hour: 3600000,
                day: 86400000,
                week : 604800000,
                month: 2592000000,
                year: 31556952000
            },

            increment: function(date, duration){
                var time = date.getTime();
                var newTime = time + (parseInt(duration) * this.durations[this.current_unit]);
                return new Date(newTime);
            }
        },
        milestone: {
            getId : function(element){
                var element_id = element.getAttribute('id');
                var id = element_id.match(/(\d+$)/);
                return id.length > 0 ? id[0] : null;
            },

            getStartDate : function (milestone_id) {
                var dateInput = $('#date_{0}'.format(milestone_id));
                if(dateInput.val() == ''){
                    alert('Start date is required');
                    this.setDuration(milestone_id, null);
                    dateInput.focus();
                    return null
                }
                return new Date(dateInput.val());
            },

            setDuration: function(milestone_id, duration){
                $('#duration_{0}'.format(milestone_id)).val(duration);
            },

            setEndDate: function (milestone_id, date) {
                var next_milestone_id = (parseInt(milestone_id) + 1);
                var endDate = date.format(Dptt.timeline.current_format);
                $('#date_{0}'.format(next_milestone_id)).val(endDate);
            }
        }

};

$(document).ready(function() {
    $('#timeline-duration').on('change', function(){
        window.location =
            location.protocol + '//' + location.host + location.pathname + '?duration='+ this.value;
    });

    $('input.milestone_duration').on('change', function () {
        var milestone_id = Dptt.milestone.getId(this);
        var duration = this.value;

        if(isNaN(duration)){
            Dptt.milestone.setDuration(milestone_id, null);
            return;
        }

        var startDate = Dptt.milestone.getStartDate(milestone_id);
        if(startDate == null)
            return;

        var endDate = Dptt.timeline.increment(startDate, duration);
        Dptt.milestone.setEndDate(milestone_id, endDate);
    })
});