(function($, window, document, undefined){
    $('.milestone_duration').change(function() {
        var myid = new $(this).attr('id').split('_');
        var mid='date_'+ myid[1];
        var days = parseInt($(this).val());
        var olddate=parseDate($('#' + mid).val());

        //date = parseDate(date);
        if(!isNaN(olddate.getTime())){
            olddate.setDate(olddate.getDate() + days);
            var e=parseInt(myid[1]) + 1;
            $('#date_' + e).val(olddate.toInputFormat());
        } else {
            alert("Invalid Date");
        }
    });

    function parseDate(input) {
        var parts = input.match(/(\d+)/g);
        return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
    }

    Date.prototype.toInputFormat = function() {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = this.getDate().toString();
        return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    };
})(jQuery, this, document);