<?php

namespace backend\controllers;

use Yii;
use backend\models\Indicator;
use backend\models\IndicatorSearch;
use backend\models\Project;
use backend\models\LoginInnovator;
use backend\models\IndicatorTypeMap;
use backend\models\IndicatorTypeMapSearch;
use backend\models\IndicatorMilestone;
use backend\models\IndicatorRequests;
use backend\models\IndicatorRequestsSearch;
use backend\models\IndicatorType;
use common\models\ProjectIndicatorweight;
use common\models\Functions;
use app\models\Notification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * IndicatorController implements the CRUD actions for Indicator model.
 */
class IndicatorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionRequests()
    {
        $model = new IndicatorRequests();
        $searchModel = new IndicatorRequestsSearch();
        $projectindicatorweight = new ProjectIndicatorweight;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('requests', [
            'searchModel'               => $searchModel,
            'dataProvider'              => $dataProvider,
            'projectindicatorweight'    => $projectindicatorweight,
            'model'                     => $model
        ]);
    }

    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndicatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

        /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionIndicator()
    {

        $data = Yii::$app->request->post();
        if($data){
        $sql        = 'SELECT * FROM `indicator` WHERE id='.$data['indicator'].'';
        $data       = Yii::$app->db->createCommand($sql )->queryOne();
        return json_encode($data);
        }
        return null;
    }

            /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionMilestone()
    {

        $data = Yii::$app->request->post();
        if($data){
        $indicators = [];
        $sql = 'SELECT indicator as id FROM static_indicator_milestone where milestone='.$data['milestone'].'
                union 
                SELECT indicator as id FROM indicator_milestone where milestone='.$data['milestone'].' and project='.$data['project'].'';
        
        $qry       = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($qry as $key => $value) {
            $sql = 'select * from project_indicatorweight where indicator='.$value['id'].' and milestone='.$data['milestone'].' and project='.$data['project'].'';
            $qry       = Yii::$app->db->createCommand($sql)->queryOne();
            if($qry){
            $indicators[]=[
                            'id'=>$value['id'],
                            'indicator'=>Indicator::findOne($value['id'])->name,
                            'indicator_type_name'=> $this->findIndicatorType($value['id']),
                            'weight'=>$qry['weight'],
                            'target_value'=>$qry['target_value'],
                            'editable'=>1
                            ];
            }else{
            $sql = 'select * from pipeline_indicatorweight where indicator='.$value['id'].' and milestone='.$data['milestone'].'';
            $qry       = Yii::$app->db->createCommand($sql)->queryOne();
            $indicators[]=[
                            'id'=>$value['id'],
                            'indicator'=>Indicator::findOne($value['id'])->name,
                            'indicator_type_name'=> $this->findIndicatorType($value['id']),
                            'weight'=>$qry['weight'],
                            'target_value'=>$qry['target_value'],
                            'editable'=>0
                            ];  
            } 
        }
        return json_encode($indicators);
        }
        return null;
    }

    private function findIndicatorType($indicator)
    {
        $indicator = Indicator::findOne($indicator);
        if($indicator){
            if($indicator->indicator_type){
                $indicatortype = IndicatorType::findOne($indicator->indicator_type);
                return $indicator ? $indicatortype->name : null;
            }
        }
        return null;
    }
                /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionInd()
    {

        $data = Yii::$app->request->post();
        if($data){
            $milestone        = $data['milestone'];
            $project          = $data['project'];
            $milestone_weight = Functions::getWeight($project,$milestone);
            if(!$milestone_weight)
            throw new ForbiddenHttpException('The action could not be completed because the milestone this indicator is attached to has no weight assigned to it'); 
            $indicator = new Indicator;
            if( filter_var($data['Indicator']['id'][0], FILTER_VALIDATE_INT) !== false ){
                $indicatormilestone = new IndicatorMilestone;
                $indicatormilestone->indicator  = $data['Indicator']['id'][0];
                $indicatormilestone->milestone  = $data['milestone'];
                $indicatormilestone->project    = $data['project'];
                $indicatormilestone->save();
                $indicators=[
                                'indicator'=>[$data['Indicator']['id'][0]],
                                'weight'=>[$data['ProjectIndicatorweight']['weight']],
                                'target_value'=>[$data['ProjectIndicatorweight']['target_value']]
                            ];
            }else{
                $indicator_model            = new Indicator();
                $indicator_model->name      = $data['Indicator']['id'][0];
                $indicator_model->indicator_type = $data['Indicator']['indicator_type'];
                $indicator_model->save();

                $indicatormilestone = new IndicatorMilestone;
                $indicatormilestone->indicator  = $indicator_model->id;
                $indicatormilestone->milestone  = $data['milestone'];
                $indicatormilestone->project    = $data['project'];
                $indicatormilestone->save();
                $indicators = [
                              'indicator'=>[$indicator_model->id],
                              'weight'=>[$data['ProjectIndicatorweight']['weight']],
                              'target_value'=>[$data['ProjectIndicatorweight']['target_value']]
                            ];
            }

            Functions::setIndicatorWeight($project,$milestone,$milestone_weight,$indicators);
            //recalculate the weights
            foreach ($data['p_indicator_id'] as $key => $value) {
                $indicators[] = [
                              'indicator'=>$value,
                              'weight'=>$data['p_weight'][$key],
                              'target_value'=>$data['p_target_value'][$key]
                            ];
            }

            Functions::setPipelineIndicatorWeight(Yii::$app->session['pipeline'],$milestone,$milestone_weight,$indicators);
            //
             return $this->redirect(['/project/settimelines','id'=>$project]);
        }
    }
       /**
     * Displays a single Indicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndicatordetails()
    {
        $data = $data = Yii::$app->request->post();
        if($data){
        if(Indicator::findOne($data['indicator']))
            return Indicator::findOne($data['indicator'])->id;  
            return null;  
        }
        return null;
    }

        /**
     * Displays a single Indicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndicatordelete()
    {
        $data = $data = Yii::$app->request->post();
        if($data){
        $indicator = IndicatorMilestone::findOne(['milestone'=>$data['milestone'],'project'=>$data['project'],'indicator'=>$data['indicator']]);
        $indicator->delete();
        $indicator = ProjectIndicatorweight::findOne(['milestone'=>$data['milestone'],'project'=>$data['project'],'indicator'=>$data['indicator']]);
        $indicator->delete();
        return json_encode("Indicator has been removed from the milestone");       
        }
        return json_encode("Could not remove indicator from milestone");
    }

    /**
     * Displays a single Indicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Indicator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApprove()
    {
        $data = Yii::$app->request->post();
        if($data){
        $id             = $data['request_id'];
        $weight         = $data['ProjectIndicatorweight']['weight'];
        $milestone      = $data['milestone'];
        $project        = $data['project'];
        $modelapprove   = $this->findModelRequests($id);

        $milestone_weight = Functions::getWeight($project,$milestone);
        if(!$milestone_weight)
        throw new ForbiddenHttpException('The action could not be completed because the milestone this indicator is attached to has no weight assigned to it');

        $sql = 'UPDATE indicator_requests SET status = 1 WHERE id='.$id.'';
        Yii::$app->db->createCommand($sql)->execute();

        $model = new Indicator();
        
        if(!$modelapprove->indicator_id){
        $model->name = $modelapprove->indicator_name;
        $model->save();
        $indicator_id = $model->id;
        }else{
        $indicator_id = $modelapprove->id;
        }


        $indicatortype = new IndicatorTypeMap();
        $indicatortype->indicator = $indicator_id;
        $indicatortype->indicator_type = $modelapprove->indicator_type;
        $indicatortype->save();


        $modelindicator = new IndicatorMilestone();
        $modelindicator->indicator = $indicator_id;
        $modelindicator->milestone = $modelapprove->milestone;
        $modelindicator->project = $modelapprove->project;
        $modelindicator->save();
        $innovator = Project::findOne($modelapprove->project)->innovator;
        
               //indicator created now add the weight
        $indicators=[
                        'indicator'=>[$indicator_id],
                        'weight'=>[$weight]
                    ];

        Functions::setIndicatorWeight($project,$milestone,$milestone_weight,$indicators);
        Notification::notify(Notification::INDICATOR_REQUEST_APPROVAL,$innovator,$modelapprove->id);
        return $this->redirect(['requests']);
        }
        return $this->redirect(['requests']);
    }

    public function actionReject()
    {

        $post = Yii::$app->request->post();
        if($post){
        $supervisor_comment         = $post['IndicatorRequests']['supervisor_comment'];
        $request_id                 = $post['request_id'];
        $modelapprove = $this->findModelRequests($request_id);



        $sql = 'UPDATE indicator_requests SET status = 2,supervisor_comment="'.$supervisor_comment.'", supervisor_id='.Yii::$app->user->identity->id.' WHERE id='.$request_id.'';
        Yii::$app->db->createCommand($sql)->execute();

        $innovator = Project::findOne($modelapprove->project)->innovator;
        Notification::notify(Notification::INDICATOR_REQUEST_REJECT,$innovator,$modelapprove->id);
        return $this->redirect(['requests']);
        }else{
         return $this->redirect(['requests']);
        }
    }

    /**
     * Creates a new Indicator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSetIndicatorWeight()
    {
        //get 
       // $indicators = [3=>0.8, 4=>0.5];
        $indicators=[
                        'indicator'=>[2,1],
                        'weight'=>[0.8,0.5]
                    ];

        return json_encode(Functions::setPipelineIndicatorWeight(40,35,1,$indicators));
    }

    /**
     * Creates a new Indicator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Indicator();
        $data = Yii::$app->request->post();
        if ($model->load($data)) {
            $model->weight = $data['Indicator']['weight'];
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Indicator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //return json_encode(Yii::$app->request->post());
        $data = Yii::$app->request->post();
        if ($model->load($data)) {
            $model->weight = $data['Indicator']['weight'];
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Indicator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this indicator has values dependant on it. Please contact your administrator');
            }
    }

    /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Indicator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelRequests($id)
    {
        if (($model = IndicatorRequests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    //CONTROLLER FUNCTIONS FOR DATA TYPE OF INNOVATOR
    public function actionValues(){

        $model = new IndicatorTypeMap();

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $indicators = $data['IndicatorTypeMap']['indicator'];
                $request = Yii::$app->getRequest();
                if ($request->isPost && $request->post('ajax') !== null) {
                        $data = Yii::$app->request->post('Item', []);
                        foreach (array_keys($data) as $index) {
                            $models[$index] = new MilestoneStage();
                        }
                        Model::loadMultiple($models, Yii::$app->request->post());
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = ActiveForm::validateMultiple($models);
                        return $result;
                }
                $record = array();
                foreach($indicators as $indicator) {
                    $record[] = [
                                'indicator'             =>$indicator,
                                'indicator_type'        =>$data['IndicatorTypeMap']['indicator_type'],
                            ];
                }

                if(count($record)>0){
                        $columnNameArray=['indicator','indicator_type'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'indicator_type_map', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['types']);
                }
        } else {
            return $this->render('assign', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionTypes()
    {
        $searchModel = new IndicatorTypeMapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('typelist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     /**
         * Displays a single Indicator model.
         * @param integer $id
         * @return mixed
         */
    public function actionValue($id)
        {
            return $this->render('value', [
                'model' => $this->findModelType($id),
            ]);
        }

        /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelType($id)
    {
        if (($model = IndicatorTypeMap::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Updates an existing IndicatorType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditValue($id)
    {
        $model = $this->findModelType($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('edit_value', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Indicator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteValue($id)
    {
        try {
              $this->findModelType($id)->delete();
              return $this->redirect(['values']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Please contact your administrator');
            }
    }
}