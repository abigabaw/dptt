<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MilestoneStage;
use backend\models\VMilestoneStage;

/**
 * MilestoneStageSearch represents the model behind the search form about `backend\models\MilestoneStage`.
 */
class MilestoneStageSearch extends MilestoneStage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'milestone', 'stage', 'ordering'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$stage,$pipeline)
    {
        $query = VMilestoneStage::find()->where(['stage_id'=>$stage,'pipeline_id'=>$pipeline]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'milestone' => $this->milestone,
            'stage' => $this->stage,
            'ordering' => $this->ordering,
        ]);

        return $dataProvider;
    }
}
