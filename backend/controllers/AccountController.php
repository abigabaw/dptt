<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\AccountForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountController implements the CRUD actions for LoginInstitution model.
 */
class AccountController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LoginInstitution models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->identity->id),
        ]);
    }


    /**
     * Updates an existing LoginInstitution model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->user->identity->id;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing LoginInstitution model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionChangePassword(){
        $id = Yii::$app->user->identity->id;
        $usermodel = $this->findModel($id);
        $model = new AccountForm;
        $modeluser = User::find()->where([
            'id'=>$id
        ])->one();
      
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['AccountForm']['newpass']);
                    if($modeluser->save()){
                        Yii::$app->getSession()->setFlash(
                            'message','Password changed'
                        );
                        return $this->redirect(['change-password']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'message','Password not changed'
                        );
                        return $this->redirect(['changepassword']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('change_password',[
                        'model'=>$model,
                        'usermodel'=>$usermodel
                    ]);
                }
            }else{
                return $this->render('change_password',[
                    'model'=>$model,
                    'usermodel'=>$usermodel
                ]);
            }
        }else{
            return $this->render('change_password',[
                'model'=>$model,
                'usermodel'=>$usermodel
            ]);
        }
    }



    /**
     * Finds the LoginInstitution model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LoginInstitution the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Finds the LoginInstitution model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LoginInstitution the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPass($id)
    {
        if (($model = AccountForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
