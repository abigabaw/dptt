<?php
namespace backend\controllers;

use Yii;
use backend\models\VProgram;
use backend\models\Pipeline;
use backend\models\VProject;
use backend\models\Queries;
use backend\models\DashboardSearch;
use backend\models\Innovator;
use backend\models\VDelayedMilestone;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','milestones','choosepipeline','widget'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'milestones' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	 public function actionWidget($widget,$location) {
		 return $this->renderAjax('widget',['location'=>$location,'widget'=>$widget]);
	 }
    public function actionMilestones(){
        $data = Yii::$app->request->post();
        $sql_delayed ='SELECT a.timeline,a.milestone,a.expected_finish_date
                    FROM v_delayed_milestone a
                    WHERE finish_date is null
                    AND a.project_id='.$data['project_id'].'
				    AND date(a.expected_finish_date)	<= date\''.date('Y-m-d').'\'	  
						  ';
        $delayed_milestones = Yii::$app->db->createCommand($sql_delayed)->queryAll();
        return json_encode($delayed_milestones);
    }

    public function actionIndex()
    { 

		 $session = Yii::$app->session;
		 if(isset($_REQUEST['pipeline'])) {
			 $o=Pipeline::findOne(['id'=>$_REQUEST['pipeline']]);
			 if($o) {
				 $session->set('pipeline',$_REQUEST['pipeline']);
				 $session->set('pipeline_a',$o);
			 }
			 else
				$session->remove('pipeline'); 
		 }

		 if($this->getPrograms() > 0) {
         if($this->getProjects()==0){
         return $this->render('projects');
          }

        return $this->render('index',
                    ['programs'                => $this->getPrograms(),
                    'innovators'               => $this->getInnovators(),
                    'projects'                 => $this->getProjects(),
                    'pipelines'                => $this->getPipelines(),
                    'delayed_milestone_count'  => count(Yii::$app->db->createCommand(Queries::qryOverDue())->queryAll())
                    ]);
		} else {  //No portfolios. render Welcome Page

			return $this->render('welcome');
		}
    }    
   
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = '@common/themes/AdminLTE/views/layouts/login.php';
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('@common/themes/AdminLTE/views/site/login.php', [
                'model' => $model,
                'form_title'=>'Admin Login'
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout(); 
        return $this->goHome();
    }

    //HELPER FUNCTIONS FOR DASHBOARD

    private function getPrograms(){
        return VProgram::find()->where(['pipeline_id'=>Yii::$app->session['pipeline'],'institution_id' => Yii::$app->user->identity->institution])->count();
    }

    private function getProjects(){
        return VProject::find()->where(['pipeline_id'=>Yii::$app->session['pipeline'],'institution' => Yii::$app->user->identity->institution])->count();
    }
    private function getPipelines(){
        return Pipeline::find()->count();
    }
    private function getInnovators(){
        return VProject::find()->select('innovator')->where(['pipeline_id'=>Yii::$app->session['pipeline'],'institution' => Yii::$app->user->identity->institution])->distinct()->count();
    }
}
