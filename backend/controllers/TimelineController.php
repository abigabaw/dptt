<?php

namespace backend\controllers;

use Yii;
use backend\models\Timeline;
use backend\models\TimelineForm;
use backend\models\Milestone;
use backend\models\Item;
use backend\models\Iteration;
use backend\models\IterationSearch;
use backend\models\TimelineSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\base\Action;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\web\Response;

/**
 * TimelineController implements the CRUD actions for Timeline model.
 */
class TimelineController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Timeline models.
     * @return mixed
     */
    public function actionIndex($innovation)
    {
        $searchModel = new TimelineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'innovation'=>$innovation,
        ]);
    }

    /**
     * Displays a single Timeline model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new IterationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function getIterations($timeline){

        $data = Iteration::find()
                ->where(['timeline'=>id])
                ->orderBy('id')
                ->limit(2)
                ->all();
    }

    /**
     * Creates a new Timeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $models = $this->getItems();
        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $data = Yii::$app->request->post('Item', []);
            foreach (array_keys($data) as $index) {
                $models[$index] = new TimelineForm();
            }
            Model::loadMultiple($models, Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validateMultiple($models);
            return $result;
        }

        if (Model::loadMultiple($models, Yii::$app->request->post())) {
            return json_encode($models);
                $model = new TimelineForm();
                if($model->addTimeline($models,Yii::$app->user->identity->id))
                return json_encode("Timelines Added");
        }
        return $this->render('create', [
                'models' => $models,
            ]);
    }

    private function getItems()
    {
        $data = Milestone::find()
                ->orderBy('id')
                ->limit(2)
                ->all();

        $items = [];
        foreach ($data as $row) {
            $item = new TimelineForm();
            $item->setAttributes($row);
            $items[] = $item;
        }
        return $items;
    }

    public function actionIteration($id){
        $model = $this->findModelIteration($id);
        if ($model->load(Yii::$app->request->post())) {
            $sql = 'INSERT INTO iteration (`timeline`,`innovation`,`comment`,`start_date`,`duration`,`created_by`)
                    VALUES ('.$id.','.$model->innovation.',"'.$model->comment.'","'.$model->start_date.'",'.$model->duration.','.Yii::$app->user->identity->id.')';
            if(Yii::$app->db->createCommand($sql)->execute())
            return $this->redirect(['view', 'id' => $id]);
            else
            return $this->render('/iteration/update', [
                'model' => $model,
            ]);
        } else {
            return $this->render('/iteration/update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Timeline model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('updatesingle', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Timeline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCalendar($id){
        $data = Iteration::find()
                ->where(['project'=>$id])
                ->orderBy('id')
                ->all();
        $calendardata=array();
        foreach ($data as $data) {
        $calendardata[] =array(
              'title'=> Milestone::findOne(Timeline::findOne($data->timeline)->milestone)->milestone,
              'start'=> $data['start_date'],
              'end' =>date('Y-m-d', strtotime($data['start_date']. ' + '.$data['duration'].' days')),
              'backgroundColor'=> "#f39c12", //yellow
              'borderColor'=> "#f39c12" //yellow
            );
        }
        return $this->render('calendar', [
            'model' => $this->findModel($id),
            'events'=>json_encode($calendardata),
        ]);
    }



    /**
     * Finds the Timeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Timeline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

     /**
     * Finds the Timeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelIteration($id)
    {
        if (($model = Iteration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
