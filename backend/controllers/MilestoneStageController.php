<?php

namespace backend\controllers;

use Yii;
use backend\models\MilestoneStage;
use backend\models\Milestone;
use backend\controllers\MilestoneStageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Action;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\web\Response;
/**
 * MilestoneStageController implements the CRUD actions for MilestoneStage model.
 */
class MilestoneStageController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all MilestoneStage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MilestoneStageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MilestoneStage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$pipeline)
    {
        $searchModel = new MilestoneStageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id,$pipeline);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MilestoneStage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MilestoneStage();

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $milestones = $data['MilestoneStage']['milestone'];
                $request = Yii::$app->getRequest();
                if ($request->isPost && $request->post('ajax') !== null) {
                        $data = Yii::$app->request->post('Item', []);
                        foreach (array_keys($data) as $index) {
                            $models[$index] = new MilestoneStage();
                        }
                        Model::loadMultiple($models, Yii::$app->request->post());
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = ActiveForm::validateMultiple($models);
                        return $result;
                }
                $record = array();
                foreach($milestones as $milestone) {
                    if( filter_var($milestone, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'stage'         =>$data['MilestoneStage']['stage'],
                                'milestone'     =>$milestone
                            ];
                    }else{
                        //first add the milestone to the db
                        $milestone_model = new Milestone;
                        $milestone_model->milestone = $milestone;
                        $milestone_model->num = 000;
                        $milestone_model->save();
                        //then add the mapping
                        $record[] = [
                                'stage'         =>$data['MilestoneStage']['stage'],
                                'milestone'     =>$milestone_model->id
                        ];

                    }

                }

                if(count($record)>0){
                        $columnNameArray=['stage','milestone'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'milestone_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['index']);
                }
        } else {
            return $this->render('create', [
                'model' => $model,
                'stage'=>false
            ]);
        }
    }

    /**
     * Creates a new MilestoneStage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionMilestones($id)
    {
        $model = new MilestoneStage();

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $milestones = $data['MilestoneStage']['milestone'];
                $request = Yii::$app->getRequest();
                if ($request->isPost && $request->post('ajax') !== null) {
                        $data = Yii::$app->request->post('Item', []);
                        foreach (array_keys($data) as $index) {
                            $models[$index] = new MilestoneStage();
                        }
                        Model::loadMultiple($models, Yii::$app->request->post());
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = ActiveForm::validateMultiple($models);
                        return $result;
                }
                $record = array();
                foreach($milestones as $milestone) {
                    if( filter_var($milestone, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'stage'         =>$id,
                                'milestone'     =>$milestone
                            ];
                    }else{
                        //first add the milestone to the db
                        $milestone_model = new Milestone;
                        $milestone_model->milestone = $milestone;
                        $milestone_model->num = 000;
                        $milestone_model->save();
                        //then add the mapping
                        $record[] = [
                                'stage'         =>$id,
                                'milestone'     =>$milestone_model->id
                        ];

                    }

                }

                if(count($record)>0){
                        $columnNameArray=['stage','milestone'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'milestone_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['index']);
                }
        } else {
            return $this->render('create', [
                'model' => $model,
                'stage'=>$id
            ]);
        }
    }

    public function actionSetMilestoneWeight(){
        $data = Yii::$app->request->post();
        if($data){
            $model = MilestoneStage::findOne($data['milestonestage']);
            if($model){
                $model->weight = $data['milestone_weight'];
                $model->save();
                $response = [
                                'status'=>'success',
                                'message'=>'Milestone weight set successfully',
                                'milestone_weight'=>$model->weight
                            ];
                return json_encode($response);
            }else{
                $response = [
                                'status'=>'error',
                                'message'=>'Could not find requested data'
                            ];
                return json_encode($response);
            }
        }
                $response = [
                                'status'=>'error',
                                'message'=>'No data posted'
                            ];
        return json_encode($response);
    }

    /**
     * Updates an existing MilestoneStage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MilestoneStage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$stage,$pipeline)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['view', 'id' => $stage, 'pipeline'=>$pipeline]);
    }

    /**
     * Finds the MilestoneStage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MilestoneStage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MilestoneStage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
