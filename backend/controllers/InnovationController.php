<?php

namespace backend\controllers;

use Yii;
use backend\models\Innovation;
use backend\models\InnovationSearch;
use backend\models\Timeline;
use backend\models\TimelineSearch;
use backend\models\TimelineForm;
use backend\models\Milestone;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Action;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\web\Response;

/**
 * InnovationController implements the CRUD actions for Innovation model.
 */
class InnovationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Lists all Timeline models.
     * @return mixed
     */
    public function actionViewtimelines($id)
    {
        $searchModel = new TimelineSearch();
        $dataProvider = $searchModel->searchTimeline(Yii::$app->request->queryParams,$id);

        return $this->render('/timeline/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Timeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionTimeline($id)
    {

        $models = $this->getItems($id);
        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $data = Yii::$app->request->post('Item', []);
            foreach (array_keys($data) as $index) {
                $models[$index] = new TimelineForm();
            }
            Model::loadMultiple($models, Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validateMultiple($models);
        }

        if (Model::loadMultiple($models, Yii::$app->request->post())) {
                $model = new TimelineForm();
                if($model->addTimeline($id,$models,Yii::$app->user->identity->id))
                return json_encode("Timelines Added");
        }
        return $this->render('/timeline/create', [
                'models' => $models,
                'innovation'=>$id
            ]);
    }

    /**
     * Creates a new Timeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionUpdatetimeline($id)
    // {

    //     $models = $this->getItemsUpate($id);
    //     $request = Yii::$app->getRequest();
    //     if ($request->isPost && $request->post('ajax') !== null) {
    //         $data = Yii::$app->request->post('Item', []);
    //         foreach (array_keys($data) as $index) {
    //             $models[$index] = new TimelineForm();
    //         }
    //         Model::loadMultiple($models, Yii::$app->request->post());
    //         Yii::$app->response->format = Response::FORMAT_JSON;
    //         $result = ActiveForm::validateMultiple($models);
    //         return $result;
    //     }

    //     if (Model::loadMultiple($models, Yii::$app->request->post())) {
    //         return json_encode($models);
    //             $model = new TimelineForm();
    //             if($model->addTimeline($models,Yii::$app->user->identity->id))
    //             return json_encode("Timelines Added");
    //     }
    //     return $this->render('/timeline/create', [
    //             'models' => $models,
    //         ]);
    // }

    private function getItems($id)
    {
        $data = Milestone::find()
                ->orderBy('id')
                ->all();
        $record="";
        foreach ($data as $data) {
            $record[] = array(
                'milestone'=>$data['id'],
                'duration'=>'',
                'iteration'=>'',
                'start_date'=>''
                );
        }
        $items = [];
        foreach ($record as $row) {
            $item = new TimelineForm();
            $item->setAttributes($row);
            $items[] = $item;
        }
        return $items;
    }
    // private function getItemsUpate($id)
    // {
    //     $data = Timeline::find()
    //             ->where(['innovation'=>$id])
    //             ->orderBy('id')
    //             ->all();
    //     $record="";
    //     foreach ($data as $data) {
    //         $record[] = array(
    //             'milestone'=>$data['milestone'],
    //             'duration'=>$data['duration'],
    //             'iteration'=>$data['iteration'],
    //             'start_date'=>$data['start_date']
    //             );
    //     }

    //     $items = [];
    //     foreach ($record as $row) {
    //         $item = new TimelineForm();
    //         $item->setAttributes($row);
    //         $items[] = $item;
    //     }
    //     return $items;
    // }
    /**
     * Lists all Innovation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InnovationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Innovation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Innovation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Innovation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Innovation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Innovation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Innovation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Innovation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Innovation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
