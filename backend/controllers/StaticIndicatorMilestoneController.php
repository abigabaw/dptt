<?php

namespace backend\controllers;

use Yii;
use backend\models\StaticIndicatorMilestone;
use backend\models\StaticIndicatorMilestoneSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StaticIndicatorMilestoneController implements the CRUD actions for StaticIndicatorMilestone model.
 */
class StaticIndicatorMilestoneController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all StaticIndicatorMilestone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StaticIndicatorMilestoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StaticIndicatorMilestone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new StaticIndicatorMilestoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new StaticIndicatorMilestone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StaticIndicatorMilestone();

        if(Yii::$app->request->post()){
                $data = Yii::$app->request->post();
                $indicators = $data['StaticIndicatorMilestone']['indicator'];
                $record = array();
                foreach($indicators as $indicator) {
                    $record[] = [
                                'milestone'     =>$data['StaticIndicatorMilestone']['milestone'],
                                'indicator'     =>$indicator
                            ];
                }
                if(count($record)>0){
                        $columnNameArray=['milestone','indicator'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'indicator_milestone', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['index']);
                }
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StaticIndicatorMilestone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StaticIndicatorMilestone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$milestone)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['view', 'id' => $milestone]);
    }

    /**
     * Finds the StaticIndicatorMilestone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StaticIndicatorMilestone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StaticIndicatorMilestone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
