<?php

namespace backend\controllers;

use Yii;
use backend\models\Stage;
use backend\models\StageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * StageController implements the CRUD actions for Stage model.
 */
class StageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Displays and sorts milestones in a single stage model.
     * @return mixed
     */
    public function actionMilestones($id)
    {

            $dataProvider = Yii::$app->db->createCommand('SELECT a.id, a.milestone as milestone_id, b.milestone as milestone, a.ordering FROM milestone_stage a INNER JOIN milestone b on a.milestone=b.id WHERE a.stage='.$id.' group by a.milestone order by ordering')->queryAll();
            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE milestone_stage SET ordering = :ordering WHERE milestone = :milestone";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':milestone', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 return $this->redirect(['milestones','id' => $id]);
            } else {
                 return $this->render('milestone', ['dataProvider' => $dataProvider, 'model' => $this->findModel($id)]);
            }
    }

    /**
     * Updates an existing Stage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // update the milstones too
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Stage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this pipeline has values dependant on it. Please contact your administrator');
            }
    }

    /**
     * Finds the Stage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
