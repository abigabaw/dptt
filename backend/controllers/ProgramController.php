<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use backend\models\Program;
use backend\models\Pipeline;
use backend\models\ProgramSearch;
use backend\models\IndicatorMilestone;
use backend\models\AssignForm;
use backend\models\Project;
use backend\models\Grant;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

/**
 * ProgramController implements the CRUD actions for Program model.
 */
class ProgramController extends Controller
{
    public function behaviors()
    {
        return [
        'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','create','view','assign','delete','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Program models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProgramSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Program model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),'projects'=>$this->getProjects($id),
        ]);
    }

    /**
     * Creates a new Program model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Program();

        $data = Yii::$app->request->post();
        if ($data) {
            $grant = $data['Program']['grant'];
                if(filter_var($grant, FILTER_VALIDATE_INT) !== false ){
                //save data
                $model->name            =  $data['Program']['name'];  
                $model->grant           =  $data['Program']['grant']; 
                $model->description     =  $data['Program']['description']; 
                $model->save();

                }else{
                //create new grant
                    $grant                =  new Grant();
                    $grant->name          =  $data['Program']['grant'];
                    $grant->save();
                //save portfolio
                    $model->name           =  $data['Program']['name'];  
                    $model->grant          =  $grant->id; 
                    $model->description    =  $data['Program']['description']; 
                    $model->save();
                }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        
    }

    /**
     * Updates an existing Program model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionAssign($id){

        $modelForm = new AssignForm;
        if (($model = Program::findOne($id)) !== null) {
 //           $project = $model->id;
   //         $sql      = 'SELECT  * from project where program != '.$id;//a.program='.$id.' GROUP BY a.id';
    //    $projects = Yii::$app->db->createCommand($sql)->queryAll();
		  $projects=Project::find()->where(['<>','program',$id])->all(); //Yii
        } else {
           return $this->redirect(['error', 'id' => $model->id]);
        }

        $indicators = IndicatorMilestone::find()->where(['project'=>$project])->all();
        $data = Yii::$app->request->post();
        if($data){
            $record = array();
            foreach($indicators as $indicator) {
                                $record[] = [
                                            'milestone'     =>$indicator->milestone,
                                            'indicator'     =>$indicator->indicator,
                                            'project'       =>$modelForm->project
                                        ];
            }
            if(count($record)>0){
            $columnNameArray=['milestone','indicator','project'];
                                    // below line insert all your record and return number of rows inserted
            $insertCount = Yii::$app->db->createCommand()
                            ->batchInsert(
                                           'indicator_milestone', $columnNameArray, $record
                                         )
                            ->execute();
            }

         return $this->redirect(['index']);
        }
        return $this->render('assign', [
                'model' => $modelForm,
                'projects'=>$projects
            ]);
    }

    /**
     * Deletes an existing Program model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this portfolio has values dependant on it. Please contact your administrator');
            }
    }

    /**
     * Finds the Program model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Program the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Program::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getProjects($id){
        $projects = Project::find()
        ->where(['program' => $id])
        ->orderBy('id')
        ->all();
        $projectlist = 'There are no Projects in this Portfolio. You can add projects to this portfolio by <a href="'.Url::to(['/project/create']).'"> creating a new project</a>.';
        if($projects){
            $projectlist = '';
            foreach ($projects as $project) {
                $projectlist.='<tr>
                              <td><a href="'.Url::to(['/project/', 'id'=>$project['id'].'']).'">'.$project['name'].'</a></td>
                           </tr>';
            }
        }
        return $projectlist;
    }
}
