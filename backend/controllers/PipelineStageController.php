<?php

namespace backend\controllers;

use Yii;
use backend\models\PipelineStage;
use backend\models\Stage;
use backend\controllers\PipelineStageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PipelineStageController implements the CRUD actions for PipelineStage model.
 */
class PipelineStageController extends Controller
{
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['post'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * Lists all PipelineStage models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new PipelineStageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PipelineStage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new PipelineStageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new PipelineStage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PipelineStage();

         if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $stages = $data['PipelineStage']['stage'];
                $record = array();
                foreach($stages as $stage) {
                    if( filter_var($stage, FILTER_VALIDATE_INT) !== false ){
                          $record[] = [
                                'pipeline'      =>$data['PipelineStage']['pipeline'],
                                'stage'         =>$stage
                            ];
                    }else{
                    $stage_model = new Stage();
                    $stage_model->stage = $stage;
                    $stage_model->description = $stage;
                    $stage_model->save();
                    $record[] = [
                                'pipeline'      =>$data['PipelineStage']['pipeline'],
                                'stage'         =>$stage_model->id
                            ];
                    }
                }
                if(count($record)>0){
                        $columnNameArray=['pipeline','stage'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'pipeline_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['index']);
                }
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PipelineStage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PipelineStage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$pipeline)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['view', 'id' => $pipeline]);
    }

    /**
     * Finds the PipelineStage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PipelineStage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PipelineStage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
