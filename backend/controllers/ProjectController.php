<?php

namespace backend\controllers;

use backend\models\DurationUnit;
use backend\models\VPipeline;
use backend\models\Rawdata;
use Yii;
use backend\models\Project;
use backend\models\VProject;
use backend\models\Indicator;
use backend\models\ProjectSearch;
use backend\models\Timeline;
use backend\models\TimelineSearch;
use backend\models\TimelineForm;
use backend\models\Iteration;
use backend\models\IterationSearch;
use backend\models\Milestone;
use common\models\Functions;
use backend\models\MilestoneSubmission;
use backend\models\ExtensionRequests;
use backend\models\ExtensionRequestsSearch;
use backend\models\Attachments;
use backend\models\MilestoneSub;
use backend\models\Pipeline;
use backend\models\Program;
use backend\models\VMilestoneStage;
use backend\models\VPipelineStage;
use backend\models\VIndicatorSubmission;
use backend\models\VMilestoneSubmission;
use backend\models\IndicatorMilestone;
use backend\models\VStaticIndicatorMilestone;
use backend\models\Innovator;
use backend\models\Queries;
use app\models\Notification;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Action;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\web\Response;
use yii\grid\GridView;
use yii\helpers\Html;
use backend\models\IndicatorMilestoneSubmission;
use backend\models\IndicatorMilestoneSubmissionSearch;
use backend\models\IndicatorMilestoneSubSearch;
use backend\models\Grading;
use backend\models\LoginInnovator;
use yii\web\ForbiddenHttpException;
use backend\models\UserForm;
use backend\models\Outcome;
use backend\models\Output;
use backend\models\Stage;
use backend\models\PipelineStage;
use backend\models\MilestoneStage;
use common\models\ProjectIndicatorWeight;
use backend\models\LogicalAttribute;
use backend\models\IntervalUnitMap;
use backend\models\IndicatorGrades;
use backend\models\PipelineIndicatorweight;
/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
 
	 public function actionGradet() {
		 echo "<table border=1>";
		 for($i=59;$i<101;$i++) {
		 $grade=0;
		 echo $i." ";
		 $d=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where  project_id =".$i." order by created_at asc")->queryAll();
		 foreach($d as $dd) {
			 $ddate=date_format(date_add(date_create($dd['start_date']),date_interval_create_from_date_string($dd['duration']." days")),"Y-m-d");
			 $grade = round(@((strtotime($ddate) - strtotime($dd['start_date']))/(strtotime($dd['end_date']) - strtotime($dd['start_date']))),5);
			 Yii::$app->db->createCommand("update grading set grade='".$grade."' where milestone_submission=".$dd['id'])->execute();
			 echo "<tr><td>".$dd['start_date']."</td><td>".$dd['duration']."</td><td>".$ddate."</td><td>".$dd['end_date']."</td><td>".$grade."</td></tr>";
		 }
		 		 echo "</table>";
		}
	 }

	 public function actionCreatemilestones() {
		 $dates=Yii::$app->db->createCommand("select distinct podate from rawdata order by podate asc")->queryAll();
		 $indicators=[104,105,106,107];
		 $stage=46;
		 for($i=0;$i<count($dates);$i++) {
			 $name=$dates[$i]['podate'];
			 $ms=Milestone::find()->where(['milestone'=>$name])->one();
			 if(!$ms) {
				 $ms=new Milestone;
				 $ms->milestone=$name;
				 $ms->num=$i;
				 $ms->description="Milestone for orders on the date: ".$dates[$i]['podate'];
				 $ms->save() or die(print_r($ms->getErrors()));
			 }
			 $mst=MilestoneStage::find()->where(['milestone'=>$ms->id,'stage'=>$stage])->one();
			 if(!$mst) {
			 	  $mst=new MilestoneStage;
				  $mst->milestone=$ms->id;
				  $mst->stage=$stage;
				  $mst->ordering=$i;
				  $mst->weight=1;
				  $mst->save() or die(print_r($mst->getErrors()));
			 }
			 foreach($indicators as $in) {
				 $ii=PipelineIndicatorweight::find()->where(['indicator'=>$in,'milestone'=>$ms->id,'stage'=>$stage,'pipeline'=>30])->one();
				 if(!$ii) {
					 $ii=new PipelineIndicatorweight;
					 $ii->indicator=$in;
					 $ii->milestone=$ms->id;
					 $ii->stage=$stag;
					 $ii->pipeline=30;
					 $ii->weight=1;
					 $ii->save() or die(print_r($ii->getErrors()));;
				 }
			 }
		 }
	 	
	 }
	 
	 public function actionLogical() {
		 
		 $in[108]=[957=>41,958=>82,959=>86,960=>76,961=>87,962=>78,963=>77,964=>86,965=>87,966=>85,967=>86,968=>90];
		 $in[109]=[969=>3,970=>9,971=>5];
		 $in[110]=[969=>25,970=>20,971=>23];
		 $in[111]=[972=>8,973=>30.3,974=>45.2,975=>56.1,976=>57.1,977=>23,978=>67,979=>69,980=>58,981=>52,982=>56,983=>65];
		 $in[112]=[972=>99.45,973=>98.9,974=>99.9,975=>99.9,976=>99.9,977=>99.6,978=>99.8,979=>100,980=>100,981=>99.8,982=>100,983=>99.9];
		 $in[113]=[972=>98.86,973=>99.9,974=>99.9,975=>99.9,976=>98.7,977=>99.4,978=>99.3,979=>99.9,980=>100,981=>99.3,982=>99.1,983=>99.6];
	
  		$sta[47]=[108=>$in[108]];
  		$sta[48]=[109=>$in[109],110=>$in[110]];
  		$sta[49]=[111=>$in[111],112=>$in[112],113=>$in[113]];
		 
		 $tmln=Timeline::find()->where(['project'=>101])->all();
		 foreach($tmln as $tm) {
		$iter=Iteration::find()->where(['timeline'=>$tm->id,'iteration_type'=>1,'start_date'=>$tm->start_date])->one();
		$ddate=date_format(date_add(date_create($tm->start_date),date_interval_create_from_date_string($tm->duration." days")),"Y-m-d");
		if(!$iter) {
			
			$iter=new Iteration;
			$iter->timeline=$tm->id;
			$iter->iteration_type=1;
			$iter->start_date=$tm->start_date;
			$iter->duration=$tm->duration;
			$iter->end_date=$ddate;
			$iter->save() or die(print_r($iter->gerErrors()));
		}
		
		//Proceed to Submit the Milestone
		$msub=MilestoneSubmission::find()->where(['iteration'=>$iter->id])->one();
		if(!$msub){
			$insert="insert into milestone_submission 
				(`iteration`, `narration`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`)
				values (
					".$iter->id.",'Submission ',1,'".$ddate."','".$ddate."',2,2
					)";
			Yii::$app->db->createCommand($insert)->execute();
		}
		$msub=MilestoneSubmission::find()->where(['iteration'=>$iter->id])->one();
		if(!$msub) die("failed to insert into mileston submission ".$insert);
	
		// now the indicators
		// get the attached indicators
		
		foreach($sta[$tm->stage] as $ind=>$vs) {
			echo ++$ctr."inserting ".$vs[$tm->milestone]." for indictaor ".$ind." in milestone ".$tm->milestone." stage ".$tm->stage."<br/>";
			$indsub=IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$msub->id,'indicator'=>$ind])->one();
			if(!$indsub) {
				$insert="insert into indicator_milestone_submission (milestone_submission,indicator,value,created_at,updated_at,created_by,updated_by) values (
				".$msub->id.",".$ind.",'".$vs[$tm->milestone]."','".$ddate."','".$ddate."',2,2)";
				Yii::$app->db->createCommand($insert)->execute();
			}
			$indsub=IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$msub->id,'indicator'=>$ind])->one();
			if(!$indsub) die("failed to insert indicator sub ".$insert );
			
			//and grade it
			$img=IndicatorGrades::find()->where(['indicator_milestone_submission'=>$indsub->id])->one();
			if(!$img) {
				$img=new IndicatorGrades;
				$img->indicator_milestone_submission=$indsub->id;
				$img->value=$indsub->value;
				$img->save() or die(print_r($img).print_r($img->getErrors())); 
			}	
		}
		//Finally, Grade the milestones
		$mgrd=Grading::find()->where(['milestone_submission'=>$msub->id])->one();
		if(!$mgrd) {
			$insert="insert into grading (milestone_submission,grade,comment,status,created_at,updated_at,created_by,updated_by) values (
			".$msub->id.",1,'System Grade',1,'".$ddate."','".$ddate."',2,2)";
			Yii::$app->db->createCommand($insert)->execute();
			/*$mgrd=new Grading;
			$mgrd->milestone_submission=$msub->id;
			$mgrd->grade=1;
			$mgrd->comment='System Grade';
			$mgrd->status=1;
			$mgrd->save() or die(print_r($mgrd->getErrors()));*/
		}
		$mgrd=Grading::find()->where(['milestone_submission'=>$msub->id])->one();
		if(!$mgrd)
			die("failed to insert grade".$insert);
	}
	 echo "done..";
 }


	 public function actionImport() {
		 
		 	echo "<pre>";	
			
		 //get Milestones Array
		 $ctr=0;
		 $mstones=Yii::$app->db->createCommand("select * from milestone where milestone like 'M00%' order by id asc")->queryAll();
		 //print_r($mstones);
		 //exit;
		 $pri=Yii::$app->db->createCommand("SELECT distinct productname,category from rawdata")->queryAll();
		 $stag=46;
		 $rs=0;
		 foreach($pri as $prod) {
			 echo "action on ".$prod['productname']."<br>";
			 // find Portfolio
			 $po=Program::find()->where(['name'=>$prod['category']])->one();
			 if(!$po) die("po not yet created".$prod['category']);
			 // find or create project
			$pr=Project::find()->where(['name'=>$prod['productname']])->one();
			if(!$pr) {
				$pr=new Project;
				$pr->name = $prod['productname'];
				$pr->innovator=6;
				$pr->program=$po->id;
				$pr->amount=1;
				$pr->institution=2;
				$pr->description=$prod['productname']." for SCMS evaluataion";
				$pr->country=231;
				$pr->teamleader=7;
				$pr->award_date='2011-09-01';
				$pr->signing_date='2011-09-01';
				$pr->startstage=$stag;
				$pr->startmilestone=132;
				$pr->save() or die(print_r($pr->getErrors()));
			}
	
			//find or create timelines
			$ctr=0;
			$data=Rawdata::find()->where(['productname'=>$pr->name])->orderBy(['podate'=>SORT_ASC])->all();
			$rs+=count($data);
			echo count($data)." |".$rs." of ".$pr->name."<br>";
			foreach($data as $r) {
				$projected=$r->projecteddeliverydate ? $r->projecteddeliverydate : $r->deliverydate;
				$duration= round((strtotime($projected) - strtotime($r->podate))/86400);
				$mss=Milestone::find()->where(['milestone'=>$r->podate])->one();
				$mstone=$mss->id;
				$ts=$r->podate;
				//echo "ts.".$ts."<br>";
				$tm=Timeline::find()->where(['project'=>$pr->id,'stage'=>$stag,'milestone'=>$mstone,'start_date'=>$ts])->one();
				while($tm != false) {
					$ts = date('Y-m-d H:i:s',strtotime($ts.'  + 1 second'));
					echo $pr->name." new TS:".$ts."<br>";
					$tm=Timeline::find()->where(['project'=>$pr->id,'stage'=>$stag,'milestone'=>$mstone,'start_date'=>$ts])->one();
				}
//				if(!$tm) {
					$tm=new Timeline;
					$tm->project=$pr->id;
					$tm->stage=$stag;
					$tm->milestone=$mstone;
					$tm->start_date=$ts;
					$tm->duration=$duration;
					$tm->duration_unit=1;
					$tm->weight=1;
					$tm->comment='Default Timeline Allocation from Excel Sheet';
					$tm->save() or die(print_r($tm->getErrors()));
					//}
				
				// Now, create the 1st (and only) Iteration
				$iter=Iteration::find()->where(['timeline'=>$tm->id,'iteration_type'=>1,'start_date'=>$r->podate])->one();
				if(!$iter) {
					$iter=new Iteration;
					$iter->timeline=$tm->id;
					$iter->iteration_type=1;
					$iter->start_date=$r->podate;
					$iter->duration=$duration;
					$iter->end_date=$r->deliverydate;
					$iter->save() or die(print_r($iter->gerErrors()));
				}
				
				//Proceed to Submit the Milestone
				$msub=MilestoneSubmission::find()->where(['iteration'=>$iter->id])->one();
				if(!$msub){
					$insert="insert into milestone_submission 
						(`iteration`, `narration`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`)
						values (
							".$iter->id.",'Submission for PO #".$r->ponumber."',1,'".$r->deliverydate."','".$r->deliverydate."',2,2
							)";
					Yii::$app->db->createCommand($insert)->execute();
				}
				$msub=MilestoneSubmission::find()->where(['iteration'=>$iter->id])->one();
				if(!$msub) die("failed to insert into mileston submission ".$insert);
			
				// now the indicators
				$inds=[104=>'quantity',105=>'unitprice',106=>'totallandedcost',107=>'ponumber']; //hard coded to avoid so many queries, since we know them
				foreach($inds as $ind=>$colname) {
					$indsub=IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$msub->id,'indicator'=>$ind])->one();
					if(!$indsub) {
						$insert="insert into indicator_milestone_submission (milestone_submission,indicator,value,created_at,updated_at,created_by,updated_by) values (
						".$msub->id.",".$ind.",'".$r->$colname."','".$r->deliverydate."','".$r->deliverydate."',2,2)";
						Yii::$app->db->createCommand($insert)->execute();
					}
					$indsub=IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$msub->id,'indicator'=>$ind])->one();
					if(!$indsub) die("failed to insert indicator sub ".$insert );
					
					//and grade it
					$img=IndicatorGrades::find()->where(['indicator_milestone_submission'=>$indsub->id])->one();
					if(!$img) {
						$img=new IndicatorGrades;
						$img->indicator_milestone_submission=$indsub->id;
						$img->value=$indsub->value;
						$img->save() or die(print_r($img).print_r($img->getErrors())); 
					}
					
				}
				//Finally, Grade the milestones
				$mgrd=Grading::find()->where(['milestone_submission'=>$msub->id])->one();
				if(!$mgrd) {
					$insert="insert into grading (milestone_submission,grade,comment,status,created_at,updated_at,created_by,updated_by) values (
					".$msub->id.",1,'System Grade',1,'".$r->deliverydate."','".$r->deliverydate."',2,2)";
					Yii::$app->db->createCommand($insert)->execute();
					/*$mgrd=new Grading;
					$mgrd->milestone_submission=$msub->id;
					$mgrd->grade=1;
					$mgrd->comment='System Grade';
					$mgrd->status=1;
					$mgrd->save() or die(print_r($mgrd->getErrors()));*/
				}
				$mgrd=Grading::find()->where(['milestone_submission'=>$msub->id])->one();
				if(!$mgrd)
					die("failed to insert grade".$insert);
			}
			
		 }
			 
		 
		 echo "done..";
	 }


	 public function actionSaveindicators($id,$milestone) {

	if($_POST['weight'] && $_POST['target']) {
		foreach($_POST['target'] as $in=>$value) {
			$i=ProjectIndicatorWeight::find()->where(['milestone'=>$milestone,'indicator'=>$in,'project'=>$id])->one();
			if(!$i) $i=new ProjectIndicatorWeight;
			$i->milestone=$milestone;
			$i->indicator=$in;
			$i->project=$id;
			$i->weight=$_POST['weight'][$in];
			$i->target_value=$value;
			$i->save() or die(print_r($i->getErrors()));
		}
	 return $this->redirect(['settimeline', 'id' => $id]);
	}
     	$portfolio=Program::find()->where(['id'=>VProject::find()->where(['id'=>$id])->one()->portfolio_id])->one();
 	$pipeline=Pipeline::find()->where(['id'=>$portfolio->pipeline])->one();
	$ins=[];$ins[]=0;
		$pri=Yii::$app->db->createCommand("SELECT a.indicator, a.milestone, a.id, a.weight, a.target_value, b.name, c.name indicatortype FROM  `project_indicatorweight` a join indicator b on a.indicator=b.id join indicator_type c on b.indicator_type=c.id  where  project='".$id."' and milestone=".$milestone)->queryAll();
		foreach($pri as $p)  $ins[]=$p['indicator'];
		$ppi=Yii::$app->db->createCommand("SELECT a.indicator, a.id, a.weight, a.target_value, b.name, c.name indicatortype FROM  `pipeline_indicatorweight` a join indicator b on a.indicator=b.id join indicator_type c on b.indicator_type=c.id  where  a.milestone='".$milestone."' and a.pipeline=".$pipeline['id']." and indicator not in (".implode($ins,',').")")->queryAll();
	         $model = $this->findModel($id);
		if (Yii::$app->request->isAjax) {
	             return $this->renderAjax('indicators', [
	                         'model' => $model,
				 'pipeline'=>$ppi,
				 'project' =>$pri,
				 'milestone'=>Milestone::findOne($milestone)->milestone
	             ]);
	         } else {
	             return $this->render('indicators', [
                   	'model' => $model,
			'pipeline'=>$ppi,
	 		  'project' =>$pri,
			  'milestone'=>Milestone::findOne($milestone)->milestone
	             ]);
	         }
	     }
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$timeline = Timeline::findOne(['project'=>$id]);
		  $this->initialiseMilestones ($id);
        $timeline = $this->getDifference($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'timeline'=>$timeline,
        ]);
    }
    public function actionSettimeline($id)
    {
        if(isset($_POST['Timeline']) && !empty($_POST['Timeline'])){
            $data = [];
            $duration_unit = $_POST['VProject']['duration'];
            foreach ($_POST['Timeline'] as $stage_id => $stage){
                foreach ($stage['milestone'] as $milestone_id => $timelineAttributes){
                if($timelineAttributes['start_date'] && $timelineAttributes['duration']) {
                        $timeline = Timeline::getTimeline([
                            'project'=>$id,
                            'stage'=>$stage_id,
                            'milestone'=>$milestone_id,
                        ]);
                        $timeline->duration = $timelineAttributes['duration'];
                        $timeline->comment = $timelineAttributes['comment'];
                        $timeline->weight = $timelineAttributes['weight'];
                        $timeline->start_date = $timelineAttributes['start_date'];
                        $timeline->duration_unit = $duration_unit;                      
                        if(!$timeline->save())
                            throw new UserException(json_encode($timeline->errors));
                    }
                }
            }
           // return $this->render(["timeline",'id'=>$id]);
        }
        $project = VProject::find()->where(['id'=>$id])->one();
        $stage=VPipelineStage::find()->where(['stage'=>$project->startstage,'pipeline'=>$project->pipeline_id])->one();
	//	  if(!$stage) die ('couldnt find start stage'); else echo 'stage'.$stage."<br>";
        $ordering= isset($stage->ordering) &&  $stage->ordering >=1 ? 'and ordering >= '.$stage->ordering : null;
          $stages = VPipelineStage::find()
                    ->where("pipeline={$project->pipeline_id} $ordering")
                    ->orderBy(['ordering' => SORT_ASC])->all();       
        $duration = Yii::$app->request->get('duration');

        return $this->render('timeline', [
            'model' => $project,
            'durations'=> ArrayHelper::map(DurationUnit::find()->all(), 'id', 'unit'),
            'stages'=>$stages,
            'duration_id' => $duration ? $duration : 1,
            'project_id'=>$id
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionGraph($id)
    {
        $sql ='SELECT a.milestone as milestone_id, b.timeline, b.comment, b.start_date, b.duration, c.milestone, c.num FROM timeline a INNER JOIN iteration b ON b.timeline=a.id INNER JOIN milestone c ON a.milestone=c.id WHERE b.project='.$id.' ORDER BY b.timeline ASC';
        $qry = Yii::$app->db->createCommand($sql)->queryAll();
        $timeline = '';
        foreach ($qry as $qry) {
            $timeline.='{desc   :"'.$qry['num'].'",
                         values :[{ 
                                   from:"'.$qry['start_date'].'",
                                   to: "'.date('Y-m-d', strtotime($qry['start_date']. ' + '.$qry['duration'].' days')).'",
                                   label:"'.$qry['num'].'",
                                   customClass:"ganttRed"
                                 }]},'; 
        }

        return $this->render('timelines/graph_stage', [
            'model' => $this->findModel($id),
            'timeline'=>'['.rtrim($timeline,',').']'
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndicatorGraph($id)
    {
        $data               = Yii::$app->request->post();
        if($data)
        $indicator_ids      = $data['indicator_ids'];
        else
        $indicator_ids      = null;   

        $model = $this->findModel($id);
        return $this->render('graphs/graph', [
            'model'         =>  $model,
            'indicator_ids' =>$indicator_ids
        ]);
        //return json_encode($labels).'<br/>'.json_encode($data);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionReport($id)
    {
        $model = $this->findModel($id);
        return $this->render('timelines/report', ['model' =>  $model]);
        //return json_encode($labels).'<br/>'.json_encode($data);
    }

        /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndicatorProjectGraph($id)
    {
        $model = $this->findModelIndicator($id);
        return $this->render('graphs/indicatorgraph', [
            'model' =>  $model,
        ]);
        //return json_encode($labels).'<br/>'.json_encode($data);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionTimelineGraph($id)
    {
        $data               = Yii::$app->request->post();
        if($data)
        $indicator_ids      = $data['indicator_ids'];
        else
        $indicator_ids      = null;   
        $model = $this->findModel($id);
        return $this->render('timelines/projection_graph', [
            'model' =>  $model,
            'indicator_ids'=>$indicator_ids
        ]);
        //return json_encode($labels).'<br/>'.json_encode($data);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        if(Yii::$app->request->post()){		
	$data = Yii::$app->request->post();		
	$model->load($data);
            if( filter_var($data['Project']['teamleader'], FILTER_VALIDATE_INT) == false ){
                //create new team
                $innovatormodel                 = new Innovator();
                $innovatormodel->names          = $data['Project']['teamleader'];
                $innovatormodel->instituition   = Yii::$app->user->identity->institution;
                $innovatormodel->save();

                $usermodel = new LoginInnovator();
                $usermodel->name = $data['Project']['teamleader'];
                $email = md5(time());
                $usermodel->email = substr($email,6).'@example.com';
                $usermodel->status = 10;
                $usermodel->innovator = $innovatormodel->id;
                $usermodel->setPassword('123456789xyz');
                $usermodel->generateAuthKey();
                $usermodel->save();
                $model->innovator = $innovatormodel->id;
                $model->teamleader = $usermodel->id;
		} else
			$model->innovator = LoginInnovator::findOne($data['Project']['teamleader'])->innovator;
                if ($model->save()) {
                    Notification::notify(Notification::PROJECT_CREATION,$model->innovator, $model->id);
						  
		//if pipeline is logical, initialise
		$this->initialiseMilestones ($model->id);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
 
        }
        return $this->render('create', ['model' => $model]);
    }
    protected function initialiseMilestones($project) {
	$proj=VProject::find()->where(['id'=>$project])->one();
    	$portfolio=Program::find()->where(['id'=>$proj->portfolio_id])->one();
	$pipeline=Pipeline::find()->where(['id'=>$portfolio->pipeline])->one();
	$attr=LogicalAttribute::find()->where(['pipeline'=>$pipeline->id])->one();
	$attributes=IntervalUnitMap::find()->where(['duration_interval'=>$attr->duration_interval,'duration_unit'=>$attr->duration_unit])->one();
	$label=DurationUnit::find()->where(['id'=>$attributes->label])->one();
	$ctr=0;
	if($pipeline->type==2) {
		
		$outs=Outcome::find()->where(['pipeline'=>$pipeline->id])->all();
		
		foreach($outs as $outcome) {
			$st=\backend\models\Stage::find()->where(['stage'=>$outcome->outcome])->one();
			//echo "<pre>";print_r($st);
			if(!$st) {
				$st=new Stage;
				$st->stage=$outcome->outcome;	
				$st->save() or die(print_r($st->getErrors())); 
			}
			
			$stp=PipelineStage::find()->where(['stage'=>$st->id,'pipeline'=>$pipeline->id])->one();
			if(!$stp) {
				$stp=new PipelineStage;
				$stp->pipeline=$pipeline->id;
				$stp->stage=$st->id;
				$stp->save() or die(print_r($stp->getErrors()));
			}
			$s=Yii::$app->db->createCommand("select * from v_output_instance where outcome_id='".$outcome->id."' and pipeline=".$pipeline->id)->queryAll();
			foreach($s as $mstone) {
				$ms=$mstone['milestone'].' - '.$mstone['output'];
				
				$m=Milestone::find()->where(['milestone'=>$ms])->one();
				if(!$m) {
					$m=new Milestone;
					$m->milestone=$ms;
					$m->save() or die(print_r($m->getErrors()));
				}
				
				$mst=MilestoneStage::find()->where(['milestone'=>$m->id,'stage'=>$st->id])->one();

				if(!$mst) {
					$mst=new MilestoneStage;
					$mst->milestone=$m->id;
					$mst->stage=$st->id;
					$mst->weight=1;
					$mst->save() or die(print_r($mst->getErrors()));
				}
				//Now Timelines
				//echo "Now on times<br>";
				$tm=Timeline::find()->where(['stage'=>$st->id,'milestone'=>$m->id,'project'=>$project])->one();
				if(!$tm) {
					$d=date_format(date_add(date_create($proj->signing_date),date_interval_create_from_date_string(($ctr*$attributes->units)." ".$label->descr)),"Y-m-d");
					$tm=New Timeline;
					$tm->stage=$st->id;
					$tm->milestone=$m->id;
					$tm->project=$project;
					$tm->start_date=$d;
					$tm->duration=$attributes->units;
					$tm->duration_unit=$attributes->label;
					$tm->comment='Default allocation from initialisation';
					$tm->save() or die(print_r($tm->getErrors()));
				}
				$ctr++;
			}
		}
	}	 
 	}
   public function actionSettimelines($id)
   {
       if(isset($_POST['Timeline']) && !empty($_POST['Timeline'])){
           $data = [];
           $duration_unit = $_POST['VProject']['duration'];
           foreach ($_POST['Timeline'] as $stage_id => $stage){
               foreach ($stage['milestone'] as $milestone_id => $timelineAttributes){
               if($timelineAttributes['start_date'] && $timelineAttributes['duration']) {
                       $timeline = Timeline::getTimeline([
                           'project'=>$id,
                           'stage'=>$stage_id,
                           'milestone'=>$milestone_id,
                       ]);
                       $timeline->duration = $timelineAttributes['duration'];
                       $timeline->comment = $timelineAttributes['comment'];
                       $timeline->weight = $timelineAttributes['weight'];
                       $timeline->start_date = $timelineAttributes['start_date'];
                       $timeline->duration_unit = $duration_unit;                      
                       if(!$timeline->save())
                           throw new UserException(json_encode($timeline->errors));
                   }
               }
           }
          // return $this->render(["timeline",'id'=>$id]);
       }
       $project = VProject::find()->where(['id'=>$id])->one();
       $stage=VPipelineStage::find()->where(['stage'=>$project->startstage,'pipeline'=>$project->pipeline_id])->one();
       $ordering= isset($stage->ordering) &&  $stage->ordering >=1 ? 'and ordering >= '.$stage->ordering : null;
         $stages = VPipelineStage::find()
                   ->where("pipeline={$project->pipeline_id} $ordering")
                   ->orderBy(['ordering' => SORT_ASC])->all();       
       $duration = Yii::$app->request->get('duration');

       return $this->render('timelinedynamic', [
           'model' => $project,
           'durations'=> ArrayHelper::map(DurationUnit::find()->all(), 'id', 'unit'),
           'stages'=>$stages,
           'indicator'=>new Indicator,
           'indicatorparams'=> new ProjectIndicatorWeight,
           'duration_id' => $duration ? $duration : 1,
           'project_id'=>$id
       ]);
   }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	  $this->initialiseMilestones ($model->id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this project has values dependant on it. Please contact your administrator');
            }
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


   /**
     * Lists all Timeline models.
     * @return mixed
     */
    public function actionTimelines($id)
    {
        $searchModel = new TimelineSearch();
        $dataProvider = $searchModel->searchTimeline(Yii::$app->request->queryParams,$id);
	  $model=$this->findModel($id);
        return $this->render('timelines/project_timeline_view', [
            	'searchModel' => $searchModel,
            	'dataProvider' => $dataProvider,
            	'project'=>$id,
			'model'=>$model,
        ]);
    }

        /**
     * Creates a new Timeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionTimeline($id)
    {
        $models = $this->getItems($id);
        if(!$models){
         return $this->redirect(['view', 'id' => $id]);
        }
        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $data = Yii::$app->request->post('Item', []);

            foreach (array_keys($data) as $index) {
                $models[$index] = new TimelineForm();
            }
            Model::loadMultiple($models, Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validateMultiple($models);
            return $result;
        }

        if (Model::loadMultiple($models, Yii::$app->request->post())) {
                $model = new TimelineForm();
                if($model->addTimeline($id,$models,Yii::$app->user->identity->id))
                return $this->redirect(['view', 'id' => $id]);
                
        }
        return $this->render('timelines/project_timeline', [
                'models' => $models,
                'project'=>$id
            ]);
    }
    private function getDifference($id){
        $pipeline = Program::findOne(Project::findOne($id)->program)->pipeline;
        $data = VMilestoneStage::find()
                ->where(['pipeline_id'=>$pipeline])
                ->groupBy('milestone_id')
                ->orderBy('milestone_id')
                ->all();

        $data2 = Timeline::find()->where(['project'=>$id])->orderBy('milestone')->all();
        $record1=array();
        $record2=array();
        foreach ($data as $data) {
            $record2[] = $data['milestone_id'];
        }
        foreach ($data2 as $data2) {
            $record1[] = $data2['milestone'];
        }
        $milestones = array_diff($record2, $record1);
        return count($milestones);
    }

    private function getItems($id)
    {
        $pipeline = Program::findOne(Project::findOne($id)->program)->pipeline;
        $data = VMilestoneStage::find()
                ->where(['pipeline_id'=>$pipeline])
                ->groupBy('milestone_id')
                ->orderBy('milestone_id')
                ->all();

        $data2 = Timeline::find()->where(['project'=>$id])->orderBy('milestone')->all();
        $record1=array();
        $record2=array();
        foreach ($data as $data) {
            $record2[] = $data['milestone_id'];
        }
        foreach ($data2 as $data2) {
            $record1[] = $data2['milestone'];
        }
        $milestones = array_diff($record2, $record1);

        $record="";
        $count = 1;
        foreach ($milestones as $d=>$val) {
            $record[] = array(
                'num'=>$count,
                'milestone'=>$val,
                'milestone_hidden'=>$val,
                'duration'=>'',
                'iteration'=>'',
                'start_date'=>''
                );
            $count ++;
        }

        $items = [];
        foreach ($milestones as $row=>$val) {
            $item = new TimelineForm();
            $item->setAttributes($row);
            $item->milestone = $val;
            $items[] = $item;
        }
        return $items;
    }

    private function getTimelineValue($project,$milestone,$case){
        $timeline = Timeline::findOne(['project'=>$project,'milestone'=>$milestone]);
        if($timeline){

        }else{
            return '';
        }
    }

     /**
     * Displays a single Timeline model.
     * @param integer $id
     * @return mixed
     */
    public function actionTimelineEntry($id)
    {
        $model = $this->findModelTimeline($id);

        $searchModel = new IterationSearch();
        $searchModelExt = new ExtensionRequestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProviderExtension = $searchModelExt->search($model->milestone,$model->project);
        
        return $this->render('timelines/timeline_entry_view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderExtension'=>$dataProviderExtension,
            'project'=>$id,
            'timeline_detail'=>$this->findSearchTimelineDetails($id)
        ]);
    }
    public function actionExtensionRequests(){
        $model = new ExtensionRequests();
        $searchModel = new ExtensionRequestsSearch();
        $dataProvider = $searchModel->searchMain(Yii::$app->request->queryParams);
        return $this->render('requests', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model
        ]);
    }
    private function findSearchTimelineDetails($id){
        $iteration = Iteration::find()->where(['timeline'=>$id])->all();
        $initial_duration = '';
        $start_date = '';
        $sum = '';
        if($iteration){
            $iteration              = Iteration::findOne(['timeline'=>$id]);
            $initial_duration       = $iteration->duration;
            $start_date             = $iteration->start_date;
            $command = Yii::$app->db->createCommand("SELECT sum(duration) FROM iteration WHERE timeline=$id");
            $sum = $command->queryScalar();
        }
        return array(
                        'initial_duration'=>$initial_duration,
                        'start_date'=>$start_date,
                        'sum'=>(int) $sum
                    );

    }

     /**
     * Finds the Timeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelTimeline($id)
    {
        if (($model = Timeline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIteration($id,$milestone){
        $model = $this->findModelIteration($id);
        $start_date = $model->start_date;
        $project = $model->project;
        if ($model->load(Yii::$app->request->post())) {
            $sql = 'INSERT INTO iteration (`timeline`,`project`,`comment`,`start_date`,`duration`,`created_by`)
                    VALUES ('.$id.','.$project.',"'.$model->comment.'","'.$start_date.'",'.$model->duration.','.Yii::$app->user->identity->id.')';
            if(Yii::$app->db->createCommand($sql)->execute())
            return $this->redirect(['timeline-entry', 'id' => $id]);
            else
            return $this->render('/iteration/update', [
                'model' => $model,
                'milestone'=>$milestone
            ]);
        } else {
            return $this->render('/iteration/update', [
                'model' => $model,
                'milestone'=>$milestone
            ]);
        }
    }

    /**
     * Updates an existing Timeline model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditTimelineEntry($id)
    {
        $model = $this->findModelTimeline($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('timelines/edit_timeline_entry', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Timeline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTimelineEntry($id)
    {
        $this->findModelTimeline($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Iteration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelIteration($id)
    {
        if (($model = Iteration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //FUNCTIONS FOR VIEWING SUBMISSION
    /**
     * Lists all Project milestones.
     * @return mixed
     */
    public function actionMilestones($id)
    {
        //return Project::findOne($id)->program;

        $pipeline   = Yii::$app->session['pipeline'];


        $stages=VPipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->all();
//       print_r($stages);
        //     print_r($stages);
        $items=[];
        foreach($stages as $k=>$stage) {
        $milestones=VMilestoneStage::find()->where(['stage_id'=>$stage['stage'],'pipeline_id'=>$pipeline])->distinct()->orderBy(['milestone_ordering'=>SORT_ASC])->all();
            $nodes=[];
            foreach($milestones as $km=>$milestone) {
                $indicators = VStaticIndicatorMilestone::find()->where(['milestone_id'=>$milestone['milestone_id']])->distinct()->all();
                $secondarynodes=[];
                foreach ($indicators as $ki => $indicator) {
                  $secondarynodes[$ki]=[
                      'id'          =>$indicator['indicator_id'],
                      'indicator'   =>$indicator['indicator']
                  ];
                }
                $nodes[$km]=[
                                            'id'            =>$milestone['milestone_id'],
                                            'num'           =>$milestone['milestone_ordering'],
                                            'milestone'     =>$milestone['milestone'],
                                            'status'        =>$this->getMStatus($milestone['milestone_id'],$id),
                                            'indicators'    =>$secondarynodes
                            ];
              }
                $items[$k]=[ 'stages'=>[
                  'id'          =>$stage['stage'],
                  'stage'       =>$stage['stage_name'],
                  'percentage'  =>$this->calculatePercentage($stage['stage'],$id),
                  'milestones'  =>$nodes]
              ];
            }

        return $this->render('submission/milestone', [
            'data' => $items,
            'project'=>$id,
        ]);
    }
    private function getMStatus($milestone,$project){
        $status = VMilestoneSubmission::findOne(['milestone_id'=>$milestone,'project_id'=>$project]);
        if(count($status)>0){  
        return Html::a('Assess <span class="fa  fa-hand-o-right"></span>', ['assess', 'id' =>$milestone,'project'=>$project ], ['class' => 'btn btn-primary']);                                                   
        }else {
        return '<a href="#" class="btn btn-warning"><span class="fa fa-check-square-o"></span> Nothing to Assess</a>';
        }
    }

    public function calculatePercentage($stage,$project){
        $stages_sql        = 'SELECT COUNT(*) FROM `v_milestone_stage` WHERE stage_id ='.$stage.'';
        $stages            =  Yii::$app->db->createCommand($stages_sql)->queryScalar();

        $submissions_sql        = 'SELECT COUNT(a.milestone_id) FROM v_milestone_submission a LEFT JOIN milestone_stage b
                                  ON a.milestone_id = b.milestone WHERE a.project_id='.$project.' AND b.stage='.$stage.'';
        return $submissions      =  Yii::$app->db->createCommand($submissions_sql)->queryScalar();
        return ($submissions/$stages)*100;
    }

    /**
     * Performs submisson of a milestone
     * If deletion is successful, the browser will be redirected to the 'submit' page.
     * @param integer $id (milestone id)
     * @return mixed
     */

    public function actionAssess($id,$project){
        $model = $this->findModelSub($id,$project);
        $submissons = VMilestoneSubmission::find()
                                            ->where(['milestone_id'=>$id,'project_id'=>$project])
                                            ->orderBy('id DESC')
                                            ->all();
        $milestones=[];
        foreach ($submissons as $km => $submisson) {
            $attachments    = Attachments::find()->where(['milestone_submission'=>$submisson['id']])->all();
            $indicators     = IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$submisson['id']])->all();
            $nodes_a=[];
            $nodes_i=[];
            foreach ($attachments as $ka => $attachment) {
                $nodes_a[$ka]=[
                                    'id'        =>$attachment['id'],
                                    'attachment'=>$attachment['attachment']
                                 ];

            }
            foreach ($indicators as $ki => $indicator) {
                   $nodes_i[$ki]=[
                                    'id'        =>$indicator['indicator'],
                                    'indicator' =>Indicator::findOne($indicator['indicator'])->name
                                 ];
            }

            $milestones[$km]=[
                                'submissions'=>[
                                                'id'=>$submisson['id'],
                                                'milestone_id'=>$submisson['milestone_id'],
                                                'milestone'=>$submisson['milestone'],
                                                'project'=>$submisson['project_id'],
                                                'comment'=>$submisson['comment'],
                                                'submission_date'=>$submisson['created_at'],
                                                'attachments'=>$nodes_a,
                                                'indicators'=>$nodes_i,
                                             ]
                            ];
        }
        return $this->render('submission/assess', [
            'model' => $model,
            'submissions'=>$milestones,
            'status'=>$this->getStatus($model->status)
        ]);
    }

    private function getAttacthment($attachment){
        if($attachment)
        return '<a href="http://trajectory.ranlab.org/projects/'.$attachment.'" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download Attachment</a>';
                   
        else 
        return '<a class="btn btn-warning btn-xs"> No attachment Uploaded</a>';
    }


    public function actionGrade($id,$project,$iteration){
           $model = $this->findModelSub($id,$project);
           $indicators = VIndicatorSubmission::find()->where(['milestone_submission'=>$iteration])->all();
           $attachments = Attachments::find()->where(['milestone_submission'=>$iteration])->all();
           $milestone_weight = Functions::getWeight($project,$id);
           if(!$milestone_weight)
            throw new ForbiddenHttpException('The action could not be completed because the milestone this indicator is attached to has no weight assigned to it');
           //return $innovator->innovator;
           $gradingmodel= new Grading();
           $data = Yii::$app->request->post();
           if($data){
            $indicators=$data['indicator'];
            $gradingmodel->comment              =   $data['Grading']['comment'];
            $gradingmodel->status               =   $data['Grading']['status'];
            $gradingmodel->grade                =   Functions::calculateWeight($data['Grading']['grade'],$milestone_weight);
            $gradingmodel->milestone_submission =   $iteration;
            foreach ($indicators as $key => $indicator) {
                        $indicator_weight = Functions::getIndicatorWeight($data['indicator'][$key],$project);
                        if(!$indicator_weight)
                        throw new ForbiddenHttpException('The action could not be completed because this indicator ('.Indicator::findOne($data['indicator'][$key])->name.') has no weight assigned to it');
                        $record[] = [
                                            'indicator_milestone_submission'            =>$data['indicator_ids'][$key],
                                            'value'                                     =>Functions::calculateWeight($data['indicator'][$key],$indicator_weight),
                                            'created_at'                                =>date('Y-m-d : h:i:s'),
                                            'created_by'                                =>Yii::$app->user->identity->id,
                                            'updated_at'                                =>date('Y-m-d : h:i:s'),
                                            'updated_by'                                =>Yii::$app->user->identity->id,
                                    ];
                                # code...
            }
            $gradingmodel->save();
            if(count($record)>0){
                $columnNameArray=['indicator_milestone_submission','value','created_at','created_by','updated_at','updated_by'];
                        // below line insert all your record and return number of rows inserted
                $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'indicator_grades', $columnNameArray, $record
                                         )
                                       ->execute();
                $grading_id = Yii::$app->db->getLastInsertID();
                $innovator = $this->findModel($project)->innovator;


                //send notification of assessment
                Notification::notify(Notification::PROJECT_ASSESMENT,$innovator,$gradingmodel->id);
                if($data['Grading']['status']==2){
                    //failed the milestone. Add new iteration for repeat
                    $milestone_submission = MilestoneSubmission::findOne($iteration);
                    $iteration_model = Iteration::findOne($milestone_submission->iteration);
                    $timeline_model  = Timeline::findOne($iteration_model->timeline);
                    $iteration = new Iteration();
                    $iteration->timeline = $timeline_model->id;
                    $iteration->start_date = date('Y-m-d : h:i:s');
                    $iteration->duration = $timeline_model->duration;
                    $iteration->iteration_type = 3;
                    $iteration->save();
                    //send notification of new iteration 
                 Notification::notify(Notification::MILESTONE_REPEAT,$innovator,$iteration->id);
                }
                return $this->redirect(['grade-view', 'id' => $gradingmodel->id]);
            }else {
                return $this->render('grade/grade', [
                    'model' => $model,
                    'gradingmodel'=>$gradingmodel,
                    'milestone'=>$id,
                    'indicators'=>$indicators,
                    'project'=>$project,
                    'iteration'=>$iteration,
                ]);
            }

           }else{
            return $this->render('grade/grade', [
                    'model' => $model,
                    'gradingmodel'=>$gradingmodel,
                    'milestone'=>$id,
                    'indicators'=>$indicators,
                    'project'=>$project,
                    'iteration'=>$iteration,
                    'attachments'=>$attachments
                ]);
           }
    }

    public function actionGradeView($id){
        $model = $this->findModelGrade($id);
        $iteration = MilestoneSubmission::findOne($model->milestone_submission)->iteration;
        $submisson = VMilestoneSubmission::find()->where(['iteration'=>$iteration])->one();
        $indicatorgrades = Yii::$app->db->createCommand(Queries::getIndicatorGrades($model->milestone_submission))->queryAll();
        
        return $this->render('grade/grade_view', [
            'model'                 => $this->findModelGrade($id),
            'submisson'             =>$submisson,
            'indicatorgrades'       =>$indicatorgrades
        ]);

    }

    public function actionScale(){
        $scale = Functions::getScaleMilestone(42,'pipeline');
        return json_encode(Functions::getScaleM($scale,40));
    }

    public function actionUpdateGrading($id){
        $model = $this->findModelGrade($id);

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['grade-view', 'id' => $model->id]);
        } else {
            return $this->render('grade/grade_update', [
                'model' => $model,
            ]);
        }
    }
    public function actionApprove($id,$ext){
        $milestone = ExtensionRequests::findOne([$ext]);
        $sql = 'UPDATE extension_requests SET status = 1 WHERE id='.$ext.'';
        $iteration = Iteration::findOne(['timeline'=>$id]);
        Yii::$app->db->createCommand($sql)->execute();
        $sql = 'INSERT INTO iteration (`timeline`,`project`,`comment`,`start_date`,`duration`,`created_by`)
                VALUES ('.$id.','.$milestone->project.',"'.$milestone->reason.'","'.$iteration->start_date.'",'.$milestone->duration.','.Yii::$app->user->identity->id.')';
        if(Yii::$app->db->createCommand($sql)->execute()){
        Notification::notify(Notification::EXTENSION_APPROVAL,$milestone->innovator,$milestone->id);
        }  
        return $this->redirect(['timeline-entry', 'id' =>$id]);   
    }
    
    public function actionApproveExtension($id){
        //get extension request and update the value to 1;
        $request = ExtensionRequests::findOne([$id]);
        $sql = 'UPDATE extension_requests SET status = 1 WHERE id='.$id.'';
        Yii::$app->db->createCommand($sql)->execute();
        //get details of the timeline being extended
        $timeline = Timeline::findOne(['project'=>$request->project,'milestone'=>$request->milestone]);
        $innovator = Project::findOne($request->project)->innovator;

           //add first new iteration value of the timeline with iteration_type of normal.
        //this iteration takes the values of the timeline
        $iteration = Iteration::findOne($request->iteration);
        $iteration->end_date                    = date('Y-m-d : h:i:s', strtotime($iteration->start_date. ' + '.$iteration['duration'].' days'));
        $iteration->save();
        
        //add second iteration with values of the request
        $iteration_extension                    = new Iteration();
        $iteration_extension->timeline          = $timeline->id;
        $iteration_extension->start_date        = $iteration->end_date;
        $iteration_extension->duration          = $request->duration;
        $iteration_extension->iteration_type    = $iteration->iteration_type;
        $iteration_extension->end_date          = NULL;
        $iteration_extension->save();

        // send notification to innovator
        Notification::notify(Notification::EXTENSION_APPROVAL,$innovator,$request->id);
        return $this->redirect(['extension-requests']);   
    }


    
     public function actionRejectExtension(){
        $post = Yii::$app->request->post();
        if($post){
        $supervisor_comment         = $post['ExtensionRequests']['supervisor_comment'];
        $request_id                 = $post['request_id'];
        $request = ExtensionRequests::findOne([$request_id]);

        $sql = 'UPDATE extension_requests SET status = 2, supervisor_comment="'.$supervisor_comment.'", supervisor_id='.Yii::$app->user->identity->id.' WHERE id='.$request_id.'';
        Yii::$app->db->createCommand($sql)->execute();
        $timeline  = Timeline::findOne(['project'=>$request->project,'milestone'=>$request->milestone]);
        $innovator = Project::findOne($request->project)->innovator;
        Notification::notify(Notification::EXTENSION_REQUEST_REJECT,$innovator,$request_id);
        return $this->redirect(['extension-requests']);
        }else{
         return $this->redirect(['extension-requests']);
        } 
    }

    private function findIndicatorSubmissions($id){
        $sql = 'SELECT a.id, a.indicator as indicator_id, a.value, b.name
                FROM indicator_milestone_submission a LEFT JOIN indicator b ON a.indicator=b.id
                WHERE a.milestone_submission ='.$id.'';
        $indicators = Yii::$app->db->createCommand($sql)->queryAll();
        $indicator_list ='';
        foreach ($indicators as $indicator) {
            $indicator_list.='<tr>
                                    <td class="col-md-10"><code>'.$indicator['name'].'</code></td>
                                    <td class="col-md-2"><a href="#" data-skin="skin-blue" class="btn btn-primary btn-xs">'.$indicator['value'].'</a></td>
                                  </tr>';
        }
        return $indicator_list;
    }

    private function getStatus($status){
        switch ($status) {
                    case 0:
                        return 'Not Reviewed';
                    case 5:
                        return 'Rejected';
                    case 10:
                        return 'Approved';
                    default:
                    return null;
                }
    }

     /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelSub($milestone,$project)
    {
        if (($model = VMilestoneSubmission::findOne(['milestone_id'=>$milestone,'project_id'=>$project])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelIndicator($id)
    {
        if (($model = Indicator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelGrade($id)
    {

        if (($model = Grading::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
