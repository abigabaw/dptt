<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\Pipeline;
use backend\models\PipelineSearch;
use backend\models\PipelineStage;
use backend\models\MilestoneStage;
use backend\models\VPipelineStage;
use backend\models\VMilestoneStage;
use backend\models\Indicator;
use backend\models\Milestone;
use backend\models\Stage;
use backend\models\Queries;
use backend\models\PipelineIndicatorweight;
use backend\models\IndicatorRequests;
use common\models\Functions;
use backend\models\IndicatorMilestone;
use backend\models\VStaticIndicatorMilestone;
use backend\models\StaticIndicatorMilestone;
use backend\models\VPipelineStageMilestoneStaticindicator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use backend\models\LogicalAttribute;
use backend\models\IntervalUnitMap;
use backend\models\PipelineInterval;
use backend\models\DurationUnit;
use backend\models\DurationInterval;
use backend\models\Outcome;
use backend\models\Output;
use backend\models\OutputInstance;
/**
 * PipelineController implements the CRUD actions for Pipeline model.
 */
class PipelineController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
			   'access' => [
		           'class' => \yii\filters\AccessControl::className(),
		           'rules' => [
		               [
		                   'allow' => true,
		                   'roles' => ['@'],
		               ],
		           ],
		       ],            
		
        ];
    }

  /**
     * Displays a single Pipeline model.
     * @param integer $id
     * @return mixed
  */
  public function actionViewtree($id){
  
     $model = $this->findModel($id);
     $pipelinestage = new PipelineStage;
     $milestonestage = new MilestoneStage;
     $indicatormilestone = new StaticIndicatorMilestone;
     $stages=VPipelineStage::find()->where(['pipeline'=>$id])->orderBy(['ordering'=>SORT_ASC])->all();
//     print_r($stages);
    $items=[];
    foreach($stages as $k=>$stage) {
      $milestones=VMilestoneStage::find()->where(['stage_id'=>$stage['stage'],'pipeline_id'=>$id])->distinct()->orderBy(['milestone_ordering'=>SORT_ASC])->all();
      $nodes=[];
      foreach($milestones as $km=>$milestone) {
        $indicators = VStaticIndicatorMilestone::find()->where(['milestone_id'=>$milestone['milestone_id']])->distinct()->all();
        $secondarynodes=[];
        foreach ($indicators as $ki => $indicator) {
          $secondarynodes[$ki]=[
              'text'=>($ki+1).'. '.$indicator['indicator']
          ];
        }
        $secondarynodes[]=['text'=> $this->renderPartial('pipelinehtml',['ht'=>'AddIndicator','milestone'=>$milestone['milestone_id']])];
        $nodes[$km]=[
                                    'text'=>($km+1).'. '.$milestone['milestone'],
                                    'nodes'=>$secondarynodes
                                 ];
      }
      $nodes[]=['text'=>$this->renderPartial('pipelinehtml',['ht'=>'AddMilestone','stage'=>$stage['stage']])];
      $items[$k]=[
          'text'=>($k+1).'. '.$stage['stage_name'],
            'state'=>['expanded'=>false],
          'nodes'=>$nodes
      ];
    }
    if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $stages = $data['PipelineStage']['stage'];
                $record = array();
                foreach($stages as $stage) {
                   if( filter_var($stage, FILTER_VALIDATE_INT) !== false ){
                          $record[] = [
                                'pipeline'      =>$model->id,
                                'stage'         =>$stage
                            ];
                    }else{
                    $stage_model = new Stage();
                    $stage_model->stage = $stage;
                    $stage_model->description = $stage;
                    $stage_model->save();
                    $record[] = [
                                'pipeline'      =>$model->id,
                                'stage'         =>$stage_model->id
                            ];
                    }
                }
                if(count($record)>0){
                        $columnNameArray=['pipeline','stage'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'pipeline_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        Yii::$app->getSession()->setFlash('success', ['title'=>'Changes Saved','message'=>'Changes have been made. You can link a project to this pipeline clicking on the porfolio link']);           
                         return $this->render('viewtree',
                            ['model'             => $model,
                            'data'               => $items,           
                            'pipelinestage'      => $pipelinestage,
                            'milestonestage'     => $milestonestage,
                            'indicatormilestone' => $indicatormilestone,
                            'addStage'           => $this->renderPartial('pipelinehtml',['ht'=>'addStage','pipelinestage'=>$pipelinestage,'model'=>$model]),
                            'indicatorModal'     => $this->renderPartial('pipelinehtml',['ht'=>'IndicatorModal','indicatormilestone'=>$indicatormilestone,'model'=>$model]),
                            'milestoneModal'     => $this->renderPartial('pipelinehtml',['ht'=>'milestoneModal','milestonestage'=>$milestonestage,'model'=>$model])]);
 
                }
      }else{
         return $this->render('viewtree',
                            ['model'             => $model,
                            'data'               => $items,           
                            'pipelinestage'      => $pipelinestage,
                            'milestonestage'     => $milestonestage,
                            'indicatormilestone' => $indicatormilestone,
                            'addStage'           => $this->renderPartial('pipelinehtml',['ht'=>'addStage','pipelinestage'=>$pipelinestage,'model'=>$model]),
                            'indicatorModal'     => $this->renderPartial('pipelinehtml',['ht'=>'IndicatorModal','indicatormilestone'=>$indicatormilestone,'model'=>$model]),
                            'milestoneModal'     => $this->renderPartial('pipelinehtml',['ht'=>'milestoneModal','milestonestage'=>$milestonestage,'model'=>$model])]);
      }
   }


    /**
     * Lists all Pipeline models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PipelineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pipeline model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$pipeline='ok') 
 	{
		$model = $this->findModel($id); 
		return $model->type==2  && $pipeline=='ok' ? $this->viewLogical($id) : $this->viewPipeline($id);
	}
   public function actionOutputinstance($id) 
	{
		if($_POST['c']) {
			foreach($_POST['c'] as $output=>$instances) {
				$todel=[];;
				foreach($instances as $instance) {
					$todel[]=$instance;
					$m=OutputInstance::find()->where(['output'=>$output,'output_instance'=>$instance])->one();
					if(!$m) $m=new OutputInstance;
					$m->output=$output;
					$m->output_instance=$instance;
					$m->save() or die(print_r($m->getErrors()));
				}
				Yii::$app->getDb()->createCommand("delete from output_instance where output='".$output."' and output_instance not in (".implode($todel,",").")")->execute();
			}
		}
		
		$model = $this->findModel($id); 
		$intervals=$checked=$outcomes=[];
		
		$d=PipelineInterval::find()->where(['pipeline'=>$id])->all();
		foreach($d as $interval)
			$intervals[$interval['id']]=$interval['milestone'];
		
		$outc=Outcome::find()->where(['pipeline'=>$id])->all();
		foreach($outc as $i) {	
			$outp=Output::find()->where(['outcome'=>$i['id']])->all();
			$outputs=[];
			foreach($outp as $o)  {
				$outputs[$o['id']]=$o['output'];
				$oi=OutputInstance::find()->where(['output'=>$o['id']])->all();
				foreach($oi as $oii)
					$check[$o['id']][]=$oii['output_instance'];
				
			}
			$outcomes[$i['outcome']]=$outputs;
		}
		
		return $this->render('output-instance',[
			'model'=>$model,
			'outcomes'=>$outcomes,
			'intervals'	=>$intervals,
			'check'	=>$check
		]);
	}

   	protected function viewLogical($id)
	{
	$model = $this->findModel($id); 
	$m_outcome=new Outcome;
	$m_output=new Output;
	if ($m_outcome->load(Yii::$app->request->post()))  {
		$m_outcome->pipeline=$model->id;
		if(!$m_outcome->save()) die(print_r($m_outcome->getErrors())); 
	}
	if ($m_output->load(Yii::$app->request->post()))  {
		if(!$m_output->save()) die(print_r($m_output->getErrors())); 
	}

      $pipelines = Yii::$app->db->createCommand('select * from outcome where pipeline='.$id.' order by ordering asc')->queryAll();

      $pipleinetablist = '';
      $pipleinetabcontentlist = '';
      $count = 0;
		$outcomes=[];
      foreach ($pipelines as $pipeline) {
			$outcomes[$pipeline['id']]=$pipeline['outcome'];
         $pipleinetablist.='<li class="'.($count==0 ? "active" : "").'"><a href="#tab_'.$pipeline['id'].'" data-toggle="tab">'.ucwords($pipeline['outcome']).'</a></li>';
         $pipleinetabcontentlist .= '<div class="tab-pane  '.($count==0 ? "active" : "").' " id="tab_'.$pipeline['id'].'">
                                      <div class="box-group" id="accordion">
                                          <h4>Outputs</h4>
                                          '.$this->renderPartial('pipelinehtml',['ht'=>'outcomelinks','stage'=>$pipeline['id'],'pipeline'=>$id]).'
                               
                                       '.$this->getOutcomes($pipeline['id']).'
                                      </div>
                                    </div><!-- /.tab-pane -->';
      $count++;
      }
		
		return $this->render('viewlogical', [
			'outcomes'=>$outcomes,
			'model' => $model,
			'pipleinetablist'=>$pipleinetablist,
			'pipleinetabcontentlist'=>$pipleinetabcontentlist,
			'm_outcome'=>$m_outcome,
			'm_output'=>$m_output,
			'm_indicator'=>new Indicator,
		]);
          /*  'pipelinestage'             => $pipelinestage,
            'milestonestage'            => $milestonestage,
            'indicatormilestone'        => $indicatormilestone,
            'pipleinetablist'           		=> $pipleinetablist,
            'pipleinetabcontentlist'    => $pipleinetabcontentlist,
            'pipelineindicatorweight'   => $pipelineindicatorweight,
            'addStage'                  => $this->renderPartial('pipelinehtml',['ht'=>'addStage','pipelinestage'=>$pipelinestage,'model'=>$pmodel]),
            'indicatorModal'            => $this->renderPartial('pipelinehtml',['ht'=>'IndicatorModal','indicatormilestone'=>$indicatormilestone,'model'=>$pmodel]),
            'milestoneModal'            => $this->renderPartial('pipelinehtml',['ht'=>'milestoneModal','milestonestage'=>$milestonestage,'model'=>$pmodel])]);*/
	}
    protected function viewPipeline($id)
    {
        $model = new Pipeline();
        $pmodel = $this->findModel($id); 
        $pipelinestage = new PipelineStage;
        $milestonestage = new MilestoneStage;
        $indicatormilestone = new StaticIndicatorMilestone;
        $pipelineindicatorweight = new PipelineIndicatorweight;
        $pipelines = Yii::$app->db->createCommand('SELECT a.id, a.stage as stage_id, b.stage as stage, a.ordering FROM pipeline_stage a LEFT JOIN stage b on a.stage=b.id WHERE a.pipeline='.$id.' order by ordering')->queryAll();

        $pipleinetablist = '';
        $pipleinetabcontentlist = '';
        $count = 0;
        foreach ($pipelines as $pipeline) {
        if($count==0){
           $pipleinetablist.='<li class="active"><a href="#tab_'.$pipeline['stage_id'].'" data-toggle="tab">'.ucwords($pipeline['stage']).'</a></li>';
           $pipleinetabcontentlist .= '<div class="tab-pane active" id="tab_'.$pipeline['stage_id'].'">
                                        <div class="box-group" id="accordion">
                                            <h4>Milestones</h4>
                                           '.$this->renderPartial('pipelinehtml',['ht'=>'milestonelinks','stage'=>$pipeline['stage_id'],'pipeline'=>$id]).'
                                
                                        '.$this->getMilestones($id,$pipeline['stage_id']).'
                                        </div>
                                      </div><!-- /.tab-pane -->';
        }else{
           $pipleinetablist.='<li ><a href="#tab_'.$pipeline['stage_id'].'" data-toggle="tab">'.ucwords($pipeline['stage']).'</a></li>';
           $pipleinetabcontentlist .= '<div class="tab-pane " id="tab_'.$pipeline['stage_id'].'">
                                            <h4>Milestones</h4>
                                      '.$this->renderPartial('pipelinehtml',['ht'=>'milestonelinks','stage'=>$pipeline['stage_id'],'pipeline'=>$id]).'
                                
                                        <div class="box-group" id="accordion">'.$this->getMilestones($id,$pipeline['stage_id']).'

                                        </div>
                                      </div><!-- /.tab-pane -->';
        }
        $count++;
        }

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $stages = $data['PipelineStage']['stage'];
                $record = array();
                foreach($stages as $stage) {
			 $e=PipelineStage::find()->where(['pipeline'=>$pmodel->id,'stage'=>$stage])->one();
 			if(!$e) {
 				$e=new PipelineStage;
 				$e->pipeline=$pmodel->id;
                   if( filter_var($stage, FILTER_VALIDATE_INT) !== false ){
			 $e->stage=$stage;
                    }else{
                    $stage_model = Stage::find()->where(['stage'=>$stage])->one();
			if(!$stage_model) { $stage_model=new Stage;
                    $stage_model->stage = $stage;
                    $stage_model->description = $stage;
                    $stage_model->save() or die(print_r($stage_model->getErrors()));;
			}
			$e->stage=$stage_model->id;
                    }
		  $e->save() or die(print_r($e->getErrors()));
		}
                }
                if(count($record)>0){
                        $columnNameArray=['pipeline','stage'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'pipeline_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        Yii::$app->getSession()->setFlash('success', ['title'=>'Changes Saved','message'=>'Changes have been made. You can link a project to this pipeline clicking on the porfolio link']);           
                        return $this->redirect(['view','id' => $pmodel->id]);
                }
        } else {
        return $this->render('view', [
            'model' => $pmodel,
            'pipelinestage'             => $pipelinestage,
            'milestonestage'            => $milestonestage,
            'indicatormilestone'        => $indicatormilestone,
            'pipleinetablist'           		=> $pipleinetablist,
            'pipleinetabcontentlist'    => $pipleinetabcontentlist,
            'pipelineindicatorweight'   => $pipelineindicatorweight,
            'addStage'                  => $this->renderPartial('pipelinehtml',['ht'=>'addStage','pipelinestage'=>$pipelinestage,'model'=>$pmodel]),
            'indicatorModal'            => $this->renderPartial('pipelinehtml',['ht'=>'IndicatorModal','indicatormilestone'=>$indicatormilestone,'model'=>$pmodel]),
            'milestoneModal'            => $this->renderPartial('pipelinehtml',['ht'=>'milestoneModal','milestonestage'=>$milestonestage,'model'=>$pmodel])]);

      }
    }
    public function actionGetIndicatorWeight(){
      $data = Yii::$app->request->post();
      //return json_encode($data);
      return json_encode(Yii::$app->db->createCommand(Queries::getIndicatorWeights($data['pipeline'],$data['milestone']))->queryAll());
      //return json_encode(Queries::getIndicatorWeights(['milestone'=>$data['milestone'],'pipeline'=>$data['pipeline']]));

    }
    public function actionGetProjectIndicatorWeight(){
      $data = Yii::$app->request->post();
      //return json_encode($data);
      return json_encode(Yii::$app->db->createCommand(Queries::getProjectIndicatorWeights($data['project'],$data['milestone']))->queryAll());
      //return json_encode(Queries::getIndicatorWeights(['milestone'=>$data['milestone'],'pipeline'=>$data['pipeline']]));

    }

    public function actionCalculateWeight(){
      $data = Yii::$app->request->post();
      if(!$data['milestoneweight'])
        return json_encode("error");
       //get 
       // $indicators = [3=>0.8, 4=>0.5];
      $indicators =  [
                        'indicator'=>[$data['indicator']],
                        'weight'=>[$data['weight']]
                    ];

      return json_encode(Functions::calculateWeights($data['pipeline'],$data['milestone'],$data['milestoneweight'],$indicators));
    }

    public function actionCalculateProjectIndicatorWeight(){
      $data = Yii::$app->request->post();
      $request = IndicatorRequests::findOne($data['reqid']);
      $milestone_weight = Functions::getWeight($request->project,$request->milestone);
      if(!$milestone_weight)
      return json_encode("error");
     // return json_encode($data);
       //get 
       // $indicators = [3=>0.8, 4=>0.5];
      $indicators =  [
                        'indicator'=>[$data['indicator']],
                        'weight'=>[$data['weight']]
                    ];

      return json_encode(Functions::calculateIndicatorWeights($data['reqid'],$milestone_weight,$indicators));
    }

    private function getOutcomes($outcome){
       $milestonelist='';
        $milestones = Output::find()->where(['outcome'=>$outcome])->all();
        foreach ($milestones as $milestone) {
           $milestonelist.= '<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                              <div class="box-header with-border">
                                <h4 class="box-title">
                                  <a class="milestone-accordion" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$milestone['id'].'">
                                     '.$milestone['output'].'
                                  </a>
                                </h4>
                              </div>
                              <div id="collapse'.$milestone['id'].'" class="panel-collapse collapse">
                                <div class="box-body">
                                <h4>Indicators</h4>
                                <ol>'.$this->getLogicalIndicators($milestone['id']).'</ol>
                                '.$this->renderPartial('pipelinehtml',['ht'=>'outputlinks','milestone'=>$milestone['id']]).'
                                  </div>
                              </div>
                            </div>';
        }
        if($milestonelist) return $milestonelist;
       else
       return 'This stage has no milestones added yet.';
    } 
    private function getMilestones($pipeline,$stage){
       $milestonelist='';
       //$milestones = Yii::$app->db->createCommand('SELECT id, milestone as milestone_id, (SELECT milestone FROM milestone WHERE milestone_stage.milestone=milestone.id) as milestone, ordering FROM milestone_stage WHERE stage='.$id.' group by ordering order by ordering')->queryAll();
        $milestones = VPipelineStageMilestoneStaticindicator::find()
                                                                   ->where(['pipeline_id'=>$pipeline])
                                                                   ->where(['stage_id'=>$stage])
                                                                   ->groupBy(['milestone_id'])
                                                                   ->all();
        foreach ($milestones as $milestone) {
           $milestonelist.= '<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                              <div class="box-header with-border">
                                <h4 class="box-title">
                                  <a class="milestone-accordion" data-toggle="collapse" milestonestage="'.$milestone['milestone_stage_id'].'" milestoneweight="'.$milestone['milestone_weight'].'" milestonetext ="'.$milestone['milestone'].'"data-parent="#accordion" href="#collapse'.$milestone['milestone_id'].'">
                                     '.$milestone['milestone'].'
                                  </a>
                                </h4>
                              </div>
                              <div id="collapse'.$milestone['milestone_id'].'" class="panel-collapse collapse">
                                <div class="box-body">
                                <h4>Indicators</h4>
                                <ol>
                                '.$this->getIndicators($pipeline,$stage,$milestone['milestone_id']).'
                                </ol>
                                '.$this->renderPartial('pipelinehtml',['ht'=>'indicatorlinks','milestone'=>$milestone['milestone_id']]).'
                                  </div>
                              </div>
                            </div>';
        }
        if($milestonelist) return $milestonelist;
       else
       return 'This stage has no milestones added yet.';
    }
    private function getLogicalIndicators($output){
      //return $stage.' '.$milestone;
       $indicatorlist='<table class="table table-striped table table-bordered"><thead>
			 <tr>
		 	<th>#</th>
			<th>Indicator</th>
			<th>Data Type</th>
			<th>Default Weight</th>
			<th>Default Target Value</th>
		 </tr>
	
	</thead>';
       $indicators = Yii::$app->getDb()->createCommand("SELECT a.id,a.indicator,a.target_value,a.weight,a.proxy,b.name,c.name indicator_type FROM `output_indicator` a join indicator b on b.id=a.indicator join indicator_type c on c.id=b.indicator_type where output='".$output."'")->queryAll();
		 $ctr=0;
      foreach ($indicators as $indicator) {$ctr++;
          $indicatorlist .='<tr>
				 <td>'.$ctr.'</td>
			 	<td>'.$indicator['name'].'</td>
				<td>'.$indicator['indicator_type'].'</td>
				<td>'.$indicator['weight'].'</td>
				<td>'.$indicator['target_value'].'</td>
			 </tr>';
       }
	$indicatorlist.='</tbody></table>';
       if($indicatorlist) return $indicatorlist;
       return 'This Milestone has no indicators added yet.'; 
    }
    private function getIndicators($pipeline,$stage,$milestone){
      //return $stage.' '.$milestone;
       $indicatorlist='';
       $indicators = Yii::$app->db->createCommand("SELECT b.name indicator, b.id indicator_id FROM `pipeline_indicatorweight` a join indicator b on a.indicator=b.id where a.pipeline='".$pipeline."' and a.stage='".$stage."' and a.milestone='".$milestone."' ")->queryAll();
//			 PipelineIndicatorweight::find()->where(['pipeline'=>$pipeline,'stage'=>$stage,'milestone'=>$milestone])->all();
      foreach ($indicators as $indicator) {
          $indicatorlist .='<li>'.$indicator['indicator'].'</li>';
       }
       if($indicatorlist)
       return $indicatorlist;
       else
       return 'This Milestone has no indicators added yet.'; 
    }

    public function actionSort($id)
    {
             $model = new Pipeline();  
             $dataProvider = Yii::$app->db->createCommand('SELECT id, stage as stage_id, (SELECT stage FROM stage WHERE pipeline_stage.stage=stage.id) as stage, ordering FROM pipeline_stage WHERE pipeline='.$id.' order by ordering')->queryAll();

            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE pipeline_stage SET ordering = :ordering WHERE stage = :stage";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':stage', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 return $this->redirect(['sort','id' => $id]);
            } else {
                 return $this->render('stages', ['dataProvider' => $dataProvider, 'model' => $model]);
            }
    }
    public function actionOutcomes($id)
    {
             $pipeline_model = $this->findModel($id);
             $model = new Pipeline();  

             $dataProvider = Yii::$app->db->createCommand('SELECT * from outcome where pipeline='.$id.' order by ordering')->queryAll();

            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE outcome SET ordering = :ordering WHERE id = :id";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':id', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 Yii::$app->getSession()->setFlash('success', ['title'=>'Outcome Ordering Saved','message'=>'Outcomes have been rearranged']);
                 return $this->redirect(['view','id' => $id]);
            } else {
                 return $this->render('outcomes', ['dataProvider' => $dataProvider, 'model' => $model,'pipeline_model'=>$pipeline_model]);
            }
    }

    public function actionStages($id)
    {
             $pipeline_model = $this->findModel($id);
             $model = new Pipeline();  
             $dataProvider = Yii::$app->db->createCommand('SELECT id, stage as stage_id, (SELECT stage FROM stage WHERE pipeline_stage.stage=stage.id) as stage, ordering FROM pipeline_stage WHERE pipeline='.$id.' order by ordering')->queryAll();

            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE pipeline_stage SET ordering = :ordering WHERE stage = :stage";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':stage', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 Yii::$app->getSession()->setFlash('success', ['title'=>'Stages Saved','message'=>'Stages have been rearranged']);
                 return $this->redirect(['view','id' => $id]);
            } else {
                 return $this->render('stages', ['dataProvider' => $dataProvider, 'model' => $model,'pipeline_model'=>$pipeline_model]);
            }
    }

    /**
     * Creates a new IndicatorMilestone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIndicators()
    {
        $model = new StaticIndicatorMilestone();

        if(Yii::$app->request->post()){
                $data = Yii::$app->request->post();
                if(!$data['milestone_weight'])
                  throw new ForbiddenHttpException('The action could not be completed because the milestone this indicator is attached to has no weight assigned to it');
                $indicators = $data['StaticIndicatorMilestone']['indicator'];
                foreach($indicators as $indicator) {
                    if( filter_var($indicator, FILTER_VALIDATE_INT) !== false ){
                      $model->milestone = $data['milestone'];
                      $model->indicator = $indicator;
                      $model->save();
                    }else{
                      $indicator_model = new Indicator ();
                      $indicator_model->name = $indicator;
                      $indicator_model->save();

                      $model->milestone = $data['milestone'];
                      $model->indicator = $indicator_model->id;
                      $model->save();
                    }
                }

                $indicators=[
                        'indicator'=>[$model->indicator],
                        'weight'=>[$data['PipelineIndicatorweight']['weight']]
                    ];

                Functions::setPipelineIndicatorWeight($data['pipeline'],$data['milestone'],$data['milestone_weight'],$indicators);
                Yii::$app->getSession()->setFlash('success', ['title'=>'Changes Saved','message'=>'Changes have been made. You can link a project to this pipeline clicking on the porfolio link']);
           
                return $this->redirect(['view','id' => $data['pipeline']]);
        }else {
            return $this->redirect(['index']);
        }
    }
/**
     * Creates a new MilestoneStage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionMilestones()
    {
        $model = new MilestoneStage();

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $milestones = $data['MilestoneStage']['milestone'];
                $request = Yii::$app->getRequest();
                if ($request->isPost && $request->post('ajax') !== null) {
                        $data = Yii::$app->request->post('Item', []);
                        foreach (array_keys($data) as $index) {
                            $models[$index] = new MilestoneStage();
                        }
                        Model::loadMultiple($models, Yii::$app->request->post());
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = ActiveForm::validateMultiple($models);
                        return $result;
                }
                $record = array();
                foreach($milestones as $milestone) {
                    if( filter_var($milestone, FILTER_VALIDATE_INT) !== false ){
                      $model->stage                 = $data['stage'];
                      $model->weight                = $data['MilestoneStage']['weight'];
                      $model->milestone             = $milestone;
                      $model->save();
                    }else {
                        //first add the milestone to the db
                        $milestone_model            = new Milestone;
                        $milestone_model->milestone = $milestone;
                        $milestone_model->num = 000;
                        $milestone_model->save();
                        //then add the mapping
                        $model->stage               = $data['stage'];
                        $model->weight              = $data['MilestoneStage']['weight'];
                        $model->milestone           = $milestone_model->id;
                        $model->save();
                    }

                }
              return $this->redirect(['view','id' => $data['pipeline']]);
        } else {
            return $this->redirect(['index']);
        }
    }
    /**
     * Creates a new Pipeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pipeline();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['title'=>'Pipeline Created','message'=>'Pipeline Created, you can now complete editing it by adding stages milestones and indicators']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pipeline model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	$lmodel = LogicalAttribute::find()->where(['pipeline'=>$id])->one();
	if(!$lmodel) $lmodel=new LogicalAttribute;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			 if ($model->type==2 && $lmodel->load(Yii::$app->request->post())) {
				 $lmodel->pipeline=$model->id;
				 $lmodel->save() or die(print_r($lmodel->getErrors()));
//				 die($lmodel->duration_unit);
				 $factor=IntervalUnitMap::find()->where(
				 	['duration_unit'=>$lmodel->duration_unit,'duration_interval'=>$lmodel->duration_interval])->one()->factor;
				$mstones=[];
				for($i=1;$i<=$lmodel->duration;$i++) {
					for($j=1;$j<=$factor;$j++)  {
						$mstone=DurationUnit::find()->where(['id'=>$lmodel->duration_unit])->one()->initial.$i.' '.DurationInterval::find()->where(['id'=>$lmodel->duration_interval])->one()->initial.$j;
						$mstones[]=$mstone;
						//die($mstone);
						$m=PipelineInterval::find()->where(['milestone'=>$mstone,'pipeline'=>$model->id])->one();
						if(!$m) $m=new PipelineInterval;
						$m->milestone=$mstone;
						$m->pipeline=$model->id;
						//Yii::$app->getDb()->createCommand("delete from pipeline_interval where pipeline='' and milestone not in ('".implode($mstones,"','")."')")->execute();
						$m->save() or die(print_r($m->getErrors()));
					}
				}
				 Yii::$app->getDb()->createCommand("delete from pipeline_interval where pipeline='".$model->id."' and milestone not in ('".implode($mstones,"','")."')")->execute();
			 } 
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
		'lmodel' => $lmodel,
            ]);
        }
    }

    /**
     * Deletes an existing Pipeline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
            try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this pipeline has values dependant on it. Please contact your administrator');
            }
        
    }

    /**
     * Finds the Pipeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pipeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pipeline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
