<?php

namespace backend\controllers;

use Yii;
use backend\models\OutputIndicator;
use backend\models\OutputIndicatorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Output;

/**
 * OutputIndicatorController implements the CRUD actions for OutputIndicator model.
 */
class OutputIndicatorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OutputIndicator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutputIndicatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OutputIndicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndicator($output)
    {
//		 die($output);
		 $model=Output::findOne($id);
		 
		 
		 
       $indicatorlist='<table class="table table-striped table table-bordered"><thead>
			 <tr>
		 	<th>#</th>
			<th>Indicator</th>
			<th>Data Type</th>
			<th>Default Weight</th>
			<th>Default Target Value</th>
		 </tr>
	
	</thead>';
       $indicators = Yii::$app->getDb()->createCommand("SELECT a.id,a.indicator,a.target_value,a.weight,a.proxy,b.name,c.name indicator_type FROM `output_indicator` a join indicator b on b.id=a.indicator join indicator_type c on c.id=b.indicator_type where output='".$output."'")->queryAll();
		// echo "SELECT a.id,a.indicator,a.target_value,a.weight,a.proxy,b.name,c.name indicator_type FROM `output_indicator` a join indicator b on b.id=a.indicator join indicator_type c on c.id=b.indicator_type where output='".$output."'";
		 $ctr=0;
      foreach ($indicators as $indicator) {$ctr++;
		if($indicator['proxy']=='Y') {$sth="<strong>"; $stf="</strong>";} else {$sth=""; $stf="";}
          $indicatorlist .='<tr>
				 <td>'.$ctr.'</td>
			 	<td>'.$sth.$indicator['name'].$stf.'</td>
				<td>'.$indicator['indicator_type'].'</td>
				<td>'.$indicator['weight'].'</td>
				<td>'.$indicator['target_value'].'</td>
			 </tr>';
       }
	$indicatorlist.='</tbody></table>';
       if(!$indicatorlist) $indicatorlist='This Output has no indicators added yet.'; 
		 
		 
		 
		 
 		if (Yii::$app->request->isAjax) {
 	             return $this->renderAjax('indicators', [
 	                         'model' => $model,
 				 'indicator'=>$indicatorlist,
 				 'project' =>$pri,
 				 'milestone'=>''
 	             ]);
 	         } else {
 	             return $this->render('indicators', [
                    	'model' => $model,
 			'pipeline'=>$ppi,
 	 		  'project' =>$pri,
			  'indicator'=>$indicatorlist,
 			  'milestone'=>''
 	             ]);
 	         }
        return $this->render('indicators', [
            'model' => Output::findOne($output),
        ]);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OutputIndicator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OutputIndicator();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OutputIndicator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OutputIndicator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OutputIndicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OutputIndicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OutputIndicator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
