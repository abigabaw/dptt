<?php

namespace backend\models;

use Yii;
use common\models\Institution;

/**
 * This is the model class for table "innovation".
 *
 * @property integer $id
 * @property string $innovation
 * @property integer $innovator
 * @property integer $amount
 * @property integer $institution
 * @property integer $project
 * @property string $description
 * @property integer $country
 * @property integer $teamleader
 * @property string $award_date
 * @property string $signing_date
 * @property integer $startstage
 * @property integer $startmilestone
 *
 * @property Milestone $startmilestone0
 * @property Innovator $innovator0
 * @property Institution $institution0
 * @property Project $project0
 * @property Country $country0
 * @property LoginInnovator $teamleader0
 * @property Stage $startstage0
 */
class Innovation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['innovation', 'innovator', 'amount', 'institution', 'project', 'teamleader', 'award_date', 'signing_date', 'startstage', 'startmilestone'], 'required'],
            [['innovator', 'amount', 'institution', 'project', 'country', 'teamleader', 'startstage', 'startmilestone'], 'integer'],
            [['description'], 'string'],
            [['award_date', 'signing_date'], 'safe'],
            [['innovation'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'innovation' => 'Innovation',
            'innovator' => 'Innovator',
            'amount' => 'Amount',
            'institution' => 'Institution',
            'project' => 'Project',
            'description' => 'Description',
            'country' => 'Country',
            'teamleader' => 'Teamleader',
            'award_date' => 'Award Date',
            'signing_date' => 'Signing Date',
            'startstage' => 'Startstage',
            'startmilestone' => 'Startmilestone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartmilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'startmilestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnovator0()
    {
        return $this->hasOne(Innovator::className(), ['id' => 'innovator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution0()
    {
        return $this->hasOne(Institution::className(), ['id' => 'institution']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamleader0()
    {
        return $this->hasOne(LoginInnovator::className(), ['id' => 'teamleader']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartstage0()
    {
        return $this->hasOne(Stage::className(), ['id' => 'startstage']);
    }
}
