<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "duration_unit".
 *
 * @property integer $id
 * @property string $unit
 * @property string $descr
 *
 * @property IntervalUnitMap[] $intervalUnitMaps
 * @property LogicalAttribute[] $logicalAttributes
 * @property Timeline[] $timelines
 */
class DurationUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'duration_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unit', 'descr'], 'required'],
            [['unit'], 'string', 'max' => 100],
            [['descr'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unit' => Yii::t('app', 'Unit'),
            'descr' => Yii::t('app', 'Descr'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntervalUnitMaps()
    {
        return $this->hasMany(IntervalUnitMap::className(), ['duration_unit' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogicalAttributes()
    {
        return $this->hasMany(LogicalAttribute::className(), ['duration_unit' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimelines()
    {
        return $this->hasMany(Timeline::className(), ['duration_unit' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DurationUnitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DurationUnitQuery(get_called_class());
    }
}
