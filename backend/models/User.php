<?php

namespace backend\models;

use Yii;
use common\models\Institution;
use common\models\LoginInstitution;

/**
 * This is the model class for table "login_institution".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $institution
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Grading[] $gradings
 * @property Grading[] $gradings0
 * @property Institution $institution0
 * @property Program[] $programs
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login_institution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['name', 'email'], 'string', 'max' => 50],
            ['email', 'unique', 'targetClass' => '\common\models\LoginInstitution', 'message' => 'This email address has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'institution' => 'Institution',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings()
    {
        return $this->hasMany(Grading::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings0()
    {
        return $this->hasMany(Grading::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution0()
    {
        return $this->hasOne(Institution::className(), ['id' => 'institution']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrograms()
    {
        return $this->hasMany(Program::className(), ['created_by' => 'id']);
    }
}
