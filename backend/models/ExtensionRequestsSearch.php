<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ExtensionRequests;

/**
 * ExtensionRequestsSearch represents the model behind the search form about `backend\models\ExtensionRequests`.
 */
class ExtensionRequestsSearch extends ExtensionRequests
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'milestone', 'project', 'duration', 'status', 'created_at', 'updated_at'], 'integer'],
            [['reason'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($milestone,$project)
    {
        $query = ExtensionRequests::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

  

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'milestone' => $milestone,
            'project' => $project,
            'duration' => $this->duration,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'reason', $this->reason]);

        return $dataProvider;
    }
    public function searchMain($params)
    {
        $query = ExtensionRequests::find($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

  

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'duration' => $this->duration,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'reason', $this->reason]);

        return $dataProvider;
    }
}
