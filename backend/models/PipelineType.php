<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pipeline_type".
 *
 * @property integer $id
 * @property string $type
 *
 * @property Pipeline[] $pipelines
 */
class PipelineType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string', 'max' => 100],
            [['type'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelines()
    {
        return $this->hasMany(Pipeline::className(), ['type' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PipelineTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PipelineTypeQuery(get_called_class());
    }
}
