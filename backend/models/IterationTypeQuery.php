<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[IterationType]].
 *
 * @see IterationType
 */
class IterationTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IterationType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IterationType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
