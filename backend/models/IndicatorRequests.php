<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "indicator_requests".
 *
 * @property integer $id
 * @property integer $project
 * @property integer $milestone
 * @property string $indicator
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Milestone $milestone0
 * @property Project $project0
 */
class IndicatorRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project', 'milestone'], 'required'],
            [['project', 'milestone','indicator_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['indicator_name'], 'string', 'max' => 255],
           // [['project'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project' => Yii::t('app', 'Project'),
            'milestone' => Yii::t('app', 'Milestone'),
            'indicator_id' => Yii::t('app', 'Indicator ID'),
            'indicator_name' => Yii::t('app', 'Indicator Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
                 return [
                    [ 'class'=>TimestampBehavior::className(),
                    'value'=>new Expression('NOW()')
                ],
                     BlameableBehavior::className(),
                 ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }
}
