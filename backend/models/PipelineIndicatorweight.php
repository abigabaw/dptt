<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "pipeline_indicatorweight".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $pipeline
 * @property double $weight
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Milestone $milestone0
 * @property Pipeline $pipeline0
 */
class PipelineIndicatorweight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline_indicatorweight';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
            'value'=>new Expression('NOW()')
        ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone', 'pipeline', 'weight',], 'required'],
            [['milestone', 'pipeline', 'created_by', 'updated_by'], 'integer'],
            [['weight'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['milestone'], 'exist', 'skipOnError' => true, 'targetClass' => Milestone::className(), 'targetAttribute' => ['milestone' => 'id']],
            [['pipeline'], 'exist', 'skipOnError' => true, 'targetClass' => Pipeline::className(), 'targetAttribute' => ['pipeline' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'milestone' => Yii::t('app', 'Milestone'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'weight' => Yii::t('app', 'Weight'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipeline0()
    {
        return $this->hasOne(Pipeline::className(), ['id' => 'pipeline']);
    }
}
