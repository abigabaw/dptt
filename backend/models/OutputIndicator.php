<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "output_indicator".
 *
 * @property integer $id
 * @property integer $indicator
 * @property integer $output
 * @property string $target_value
 * @property double $weight
 * @property string $proxy
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property Indicator $indicator0
 * @property Output $output0
 * @property User $createdBy
 * @property User $updatedBy
 */
class OutputIndicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'output_indicator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator', 'output', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'required'],
            [['indicator', 'output', 'created_by', 'updated_by'], 'integer'],
            [['target_value', 'proxy'], 'string'],
            [['weight'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['indicator', 'output'], 'unique', 'targetAttribute' => ['indicator', 'output'], 'message' => 'The combination of Indicator and Output has already been taken.'],
            [['output', 'proxy'], 'unique', 'targetAttribute' => ['output', 'proxy'], 'message' => 'The combination of Output and Proxy has already been taken.'],
            [['indicator'], 'exist', 'skipOnError' => true, 'targetClass' => Indicator::className(), 'targetAttribute' => ['indicator' => 'id']],
            [['output'], 'exist', 'skipOnError' => true, 'targetClass' => Output::className(), 'targetAttribute' => ['output' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'indicator' => Yii::t('app', 'Indicator'),
            'output' => Yii::t('app', 'Output'),
            'target_value' => Yii::t('app', 'Target Value'),
            'weight' => Yii::t('app', 'Weight'),
            'proxy' => Yii::t('app', 'Proxy'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator0()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutput0()
    {
        return $this->hasOne(Output::className(), ['id' => 'output']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return OutputIndicatorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OutputIndicatorQuery(get_called_class());
    }
}
