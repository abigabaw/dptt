<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "duration_interval".
 *
 * @property integer $id
 * @property string $duration_interval
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property IntervalUnitMap[] $intervalUnitMaps
 * @property LogicalAttribute[] $logicalAttributes
 */
class DurationInterval extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'duration_interval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['duration_interval'], 'string', 'max' => 200],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'duration_interval' => Yii::t('app', 'Duration Interval'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntervalUnitMaps()
    {
        return $this->hasMany(IntervalUnitMap::className(), ['duration_interval' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogicalAttributes()
    {
        return $this->hasMany(LogicalAttribute::className(), ['duration_interval' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DurationIntervalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DurationIntervalQuery(get_called_class());
    }
}
