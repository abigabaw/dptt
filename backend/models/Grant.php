<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use common\models\LoginInstitution;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "grant".
 *
 * @property integer $id
 * @property string $name
 * @property integer $agency
 * @property string $description
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Agency $agency0
 * @property Program[] $programs
 */
class Grant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

           /**
     * @inheritdoc
     */
		     public function behaviors()
		     {
		         return [
		            [ 'class'=>TimestampBehavior::className(),
		 		    'value'=>new Expression('NOW()')
		 		],
		             BlameableBehavior::className(),
		         ];
		     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'agency' => 'Agency',
            'description' => 'Description',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency0()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrograms()
    {
        return $this->hasMany(Program::className(), ['grant' => 'id']);
    }
}
