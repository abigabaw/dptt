<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "indicator_milestone_submission".
 *
 * @property integer $id
 * @property integer $indicator
 * @property integer $milestone
 * @property integer $project
 * @property string $value
 * @property integer $iteration
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 *
 * @property Indicator $indicator0
 * @property Milestone $milestone0
 */
class IndicatorMilestoneSubmission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_milestone_submission';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator', 'milestone_submission', 'value', 'created_by'], 'required'],
            [['indicator', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator' => 'Indicator',
            'milestone' => 'Milestone',
            'project' => 'Project',
            'value' => 'Value',
            'iteration' => 'Iteration',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator0()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
}
