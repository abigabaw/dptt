<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "iteration_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $color_code
 * @property string $dash_style
 *
 * @property Iteration[] $iterations
 */
class IterationType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iteration_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color_code', 'dash_style'], 'required'],
            [['name', 'dash_style'], 'string', 'max' => 20],
            [['color_code'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'color_code' => Yii::t('app', 'Color Code'),
            'dash_style' => Yii::t('app', 'Dash Style'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIterations()
    {
        return $this->hasMany(Iteration::className(), ['iteration_type' => 'id']);
    }

    /**
     * @inheritdoc
     * @return IterationTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IterationTypeQuery(get_called_class());
    }
}
