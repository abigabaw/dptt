<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "indicator_grades".
 *
 * @property integer $id
 * @property integer $indicator_milestone_submission
 * @property double $value
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property IndicatorMilestoneSubmission $indicatorMilestoneSubmission
 */
class IndicatorGrades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_grades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_milestone_submission', 'value'], 'required'],
            [['indicator_milestone_submission', 'created_by', 'updated_by'], 'integer'],
            [['value'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['indicator_milestone_submission'], 'unique'],
            [['indicator_milestone_submission'], 'exist', 'skipOnError' => true, 'targetClass' => IndicatorMilestoneSubmission::className(), 'targetAttribute' => ['indicator_milestone_submission' => 'id']],
        ];
    }
    public function behaviors()
{
    return [
       [ 'class'=>TimestampBehavior::className(),
        'value'=>new Expression('NOW()')
    ],
        BlameableBehavior::className(),
    ];
}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'indicator_milestone_submission' => Yii::t('app', 'Indicator Milestone Submission'),
            'value' => Yii::t('app', 'Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestoneSubmission()
    {
        return $this->hasOne(IndicatorMilestoneSubmission::className(), ['id' => 'indicator_milestone_submission']);
    }

    /**
     * @inheritdoc
     * @return IndicatorGradesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IndicatorGradesQuery(get_called_class());
    }
}
