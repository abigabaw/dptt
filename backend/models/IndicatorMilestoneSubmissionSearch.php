<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\IndicatorMilestoneSubmission;

/**
 * IndicatorMilestoneSubmissionSearch represents the model behind the search form about `backend\models\IndicatorMilestoneSubmission`.
 */
class IndicatorMilestoneSubmissionSearch extends IndicatorMilestoneSubmission
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'milestone_submission', 'indicator', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = IndicatorMilestoneSubmission::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'milestone_submission' => $id,
            'indicator' => $this->indicator,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
