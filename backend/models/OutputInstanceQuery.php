<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[OutputInstance]].
 *
 * @see OutputInstance
 */
class OutputInstanceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OutputInstance[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OutputInstance|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
