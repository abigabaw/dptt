<?php

namespace backend\models;
use common\models\LoginInstitution;
use common\models\Institution;

use Yii;

/**
 * This is the model class for table "v_project".
 *
 * @property integer $id
 * @property string $name
 * @property integer $innovator
 * @property integer $portfolio_id
 * @property integer $amount
 * @property integer $institution
 * @property string $description
 * @property integer $country
 * @property integer $teamleader
 * @property string $award_date
 * @property string $signing_date
 * @property integer $startstage
 * @property integer $startmilestone
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $pipeline_name
 * @property integer $pipeline_id
 * @property string $portfolio_name
 */
class VProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'innovator', 'portfolio_id', 'amount', 'institution', 'country', 'teamleader', 'startstage', 'startmilestone', 'created_by', 'updated_by', 'created_at', 'updated_at', 'pipeline_id','program'], 'integer'],
            [['name', 'innovator', 'portfolio_id', 'amount', 'institution', 'teamleader', 'award_date', 'signing_date', 'startstage', 'startmilestone', 'created_by', 'updated_by', 'created_at', 'updated_at', 'pipeline_name', 'portfolio_name'], 'required'],
            [['description'], 'string'],
            [['award_date', 'signing_date'], 'safe'],
            [['name', 'pipeline_name'], 'string', 'max' => 255],
            [['portfolio_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'innovator' => Yii::t('app', 'Innovator'),
            'portfolio_id' => Yii::t('app', 'Portfolio ID'),
            'amount' => Yii::t('app', 'Amount'),
            'institution' => Yii::t('app', 'Institution'),
            'description' => Yii::t('app', 'Description'),
            'country' => Yii::t('app', 'Country'),
            'teamleader' => Yii::t('app', 'Teamleader'),
            'award_date' => Yii::t('app', 'Award Date'),
            'signing_date' => Yii::t('app', 'Signing Date'),
            'startstage' => Yii::t('app', 'Startstage'),
            'startmilestone' => Yii::t('app', 'Startmilestone'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'pipeline_name' => Yii::t('app', 'Pipeline Name'),
            'pipeline_id' => Yii::t('app', 'Pipeline ID'),
		 'program' => Yii::t('app', 'Portfolio'),
            'portfolio_name' => Yii::t('app', 'Portfolio Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return VProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VProjectQuery(get_called_class());
    }
    public function getDateIterations()
    {
        return $this->hasMany(DateIteration::className(), ['innovation' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(Program::className(), ['id' => 'portfolio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnovator0()
    {
        return $this->hasOne(Innovator::className(), ['id' => 'innovator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution0()
    {
        return $this->hasOne(Institution::className(), ['id' => 'institution']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamleader0()
    {
        return $this->hasOne(LoginInnovator::className(), ['id' => 'teamleader']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartstage0()
    {
        return $this->hasOne(Stage::className(), ['id' => 'startstage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartmilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'startmilestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimelines()
    {
        return $this->hasMany(Timeline::className(), ['project' => 'id']);
    }
 
}
