<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_agency".
 *
 * @property integer $id
 * @property integer $admin
 * @property string $name
 * @property string $email
 * @property string $contact
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $institution_name
 * @property integer $institution_id
 */
class VAgency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'admin', 'created_by', 'updated_by', 'institution_id'], 'integer'],
            [['admin', 'name', 'email', 'contact', 'description'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'email', 'contact', 'description', 'institution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'admin' => Yii::t('app', 'Admin'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'contact' => Yii::t('app', 'Contact'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return VAgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VAgencyQuery(get_called_class());
    }
}
