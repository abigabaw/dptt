<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[VProgram]].
 *
 * @see VProgram
 */
class VProgramQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VProgram[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VProgram|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}