<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "grading".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $project
 * @property integer $iteration
 * @property integer $grade
 * @property string $comment
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property GradingOutcome $status0
 * @property Milestone $milestone0
 * @property ProjectGroup $project0
 * @property LoginInstitution $createdBy
 * @property LoginInstitution $updatedBy
 * @property MilestoneSubmission $iteration0
 */
class Grading extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grading';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone_submission', 'grade', 'comment', 'status',], 'required'],
            [['milestone_submission', 'status', 'created_by', 'updated_by'], 'integer'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }
          /**
     * @inheritdoc
     */
        public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
            'value'=>new Expression('NOW()')
        ],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'milestone' => Yii::t('app', 'Milestone'),
            'project' => Yii::t('app', 'Project'),
            'iteration' => Yii::t('app', 'Iteration'),
            'grade' => Yii::t('app', 'Grade'),
            'comment' => Yii::t('app', 'Comment'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(GradingOutcome::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(LoginInstitution::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(LoginInstitution::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIteration0()
    {
        return $this->hasOne(MilestoneSubmission::className(), ['id' => 'iteration']);
    }
}
