<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Outcome]].
 *
 * @see Outcome
 */
class OutcomeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Outcome[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Outcome|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
