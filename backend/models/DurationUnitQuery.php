<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[DurationUnit]].
 *
 * @see DurationUnit
 */
class DurationUnitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DurationUnit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DurationUnit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
