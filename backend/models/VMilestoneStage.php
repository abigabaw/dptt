<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_milestone_stage".
 *
 * @property integer $milestone_id
 * @property string $milestone
 * @property integer $stage_id
 * @property string $stage
 * @property integer $pipeline_id
 * @property string $pipeline
 */
class VMilestoneStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_milestone_stage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone_id', 'stage_id'], 'required'],
            [['milestone_id', 'stage_id', 'pipeline_id'], 'integer'],
            [['milestone', 'stage', 'pipeline'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'milestone_id' => Yii::t('app', 'Milestone ID'),
            'milestone' => Yii::t('app', 'Milestone'),
            'stage_id' => Yii::t('app', 'Stage ID'),
            'stage' => Yii::t('app', 'Stage'),
            'pipeline_id' => Yii::t('app', 'Pipeline ID'),
            'pipeline' => Yii::t('app', 'Pipeline'),
        ];
    }

    /**
     * @inheritdoc
     * @return VMilestoneStageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VMilestoneStageQuery(get_called_class());
    }

    /**
     * @param $stage_id
     * @param $pipeline_id
     * @return array|VMilestoneStage[]
     */
    public static function getMilestones($stage_id, $pipeline_id){
        return VMilestoneStage::find()->where([
            'stage_id' => $stage_id,
            'pipeline_id' => $pipeline_id,
        ])->orderBy(['milestone_ordering' => SORT_ASC,'milestone_id'=>SORT_ASC])->all();
    }
}
