<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_program".
 *
 * @property integer $id
 * @property string $name
 * @property integer $pipeline
 * @property integer $grant
 * @property string $description
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $institution_name
 * @property integer $institution_id
 * @property integer $pipeline_id
 * @property string $pipeline_name
 * @property integer $grant_id
 * @property string $grant_name
 */
class VProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pipeline', 'grant', 'created_by', 'updated_by', 'created_at', 'updated_at', 'institution_id', 'pipeline_id', 'grant_id'], 'integer'],
            [['name', 'pipeline', 'grant', 'description', 'name', 'grant_name'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['description', 'institution_name', 'name', 'grant_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'grant' => Yii::t('app', 'Grant'),
            'description' => Yii::t('app', 'Description'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
            'pipeline_id' => Yii::t('app', 'Pipeline ID'),
            'name' => Yii::t('app', 'Pipeline Name'),
            'grant_id' => Yii::t('app', 'Grant ID'),
            'grant_name' => Yii::t('app', 'Grant Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return VProgramQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VProgramQuery(get_called_class());
    }
    public function getCreatedBy()
    {
        return $this->hasOne(LoginInstitution::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipeline0()
    {
        return $this->hasOne(Pipeline::className(), ['id' => 'pipeline']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrant0()
    {
        return $this->hasOne(Grant::className(), ['id' => 'grant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['cohort' => 'id']);
    }

}
