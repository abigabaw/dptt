<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "output_instance".
 *
 * @property integer $id
 * @property integer $output
 * @property integer $output_instance
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Output $output0
 * @property PipelineInterval $outputInstance
 * @property User $createdBy
 * @property User $updatedBy
 */
class OutputInstance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'output_instance';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['output', 'output_instance', 'created_by', 'updated_by'], 'integer'],
            [['output', 'output_instance'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['output', 'output_instance'], 'unique', 'targetAttribute' => ['output', 'output_instance'], 'message' => 'The combination of Output and Output Instance has already been taken.'],
            [['output'], 'exist', 'skipOnError' => true, 'targetClass' => Output::className(), 'targetAttribute' => ['output' => 'id']],
            [['output_instance'], 'exist', 'skipOnError' => true, 'targetClass' => PipelineInterval::className(), 'targetAttribute' => ['output_instance' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'output' => Yii::t('app', 'Output'),
            'output_instance' => Yii::t('app', 'Output Instance'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutput0()
    {
        return $this->hasOne(Output::className(), ['id' => 'output']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutputInstance()
    {
        return $this->hasOne(PipelineInterval::className(), ['id' => 'output_instance']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return OutputInstanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OutputInstanceQuery(get_called_class());
    }
}
