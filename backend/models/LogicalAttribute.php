<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "logical_attribute".
 *
 * @property integer $id
 * @property integer $pipeline
 * @property integer $duration
 * @property integer $duration_unit
 * @property integer $duration_interval
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pipeline $pipeline0
 * @property DurationUnit $durationUnit
 * @property DurationInterval $durationInterval
 * @property User $createdBy
 * @property User $updatedBy
 */
class LogicalAttribute extends \yii\db\ActiveRecord
{
   public function behaviors()
   {
       return [
          [ 'class'=>TimestampBehavior::className(),
	    'value'=>new Expression('NOW()')
	],
           BlameableBehavior::className(),
       ];
   }
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logical_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pipeline', 'duration', 'duration_unit', 'duration_interval', 'created_by', 'updated_by'], 'integer'],
            [['pipeline','duration','duration_interval','duration_unit'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['pipeline'], 'exist', 'skipOnError' => true, 'targetClass' => Pipeline::className(), 'targetAttribute' => ['pipeline' => 'id']],
            [['duration_unit'], 'exist', 'skipOnError' => true, 'targetClass' => DurationUnit::className(), 'targetAttribute' => ['duration_unit' => 'id']],
            [['duration_interval'], 'exist', 'skipOnError' => true, 'targetClass' => DurationInterval::className(), 'targetAttribute' => ['duration_interval' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'duration' => Yii::t('app', 'Duration'),
            'duration_unit' => Yii::t('app', 'Duration Unit'),
            'duration_interval' => Yii::t('app', 'Duration Interval'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipeline0()
    {
        return $this->hasOne(Pipeline::className(), ['id' => 'pipeline']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDurationUnit()
    {
        return $this->hasOne(DurationUnit::className(), ['id' => 'duration_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDurationInterval()
    {
        return $this->hasOne(DurationInterval::className(), ['id' => 'duration_interval']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return LogicalAttributeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LogicalAttributeQuery(get_called_class());
    }
}
