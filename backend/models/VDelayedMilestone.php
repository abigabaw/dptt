<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_delayed_milestone".
 *
 * @property integer $id
 * @property integer $timeline
 * @property integer $milestone_id
 * @property string $milestone
 * @property integer $project_id
 * @property string $project
 * @property string $comment
 * @property string $start_date
 * @property string $finish_date
 * @property integer $created_at
 */
class VDelayedMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_delayed_milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'timeline', 'milestone_id', 'project_id', 'created_at'], 'integer'],
            [['timeline', 'project_id', 'comment', 'start_date'], 'required'],
            [['start_date', 'finish_date'], 'safe'],
            [['milestone', 'project', 'comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'timeline' => Yii::t('app', 'Timeline'),
            'milestone_id' => Yii::t('app', 'Milestone ID'),
            'milestone' => Yii::t('app', 'Milestone'),
            'project_id' => Yii::t('app', 'Project ID'),
            'project' => Yii::t('app', 'Project'),
            'comment' => Yii::t('app', 'Comment'),
            'start_date' => Yii::t('app', 'Start Date'),
            'finish_date' => Yii::t('app', 'Finish Date'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
