<?php

namespace backend\models;

use Yii;
use common\models\LoginInstitution;
use yii\base\UserException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "timeline".
 *
 * @property integer $id
 * @property integer $project
 * @property integer $milestone
 * @property integer $duration
 * @property integer $iteration
 * @property string $start_date
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Iteration $iteration0
 * @property Milestone $milestone0
 */
class Timeline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timeline';
    }

    /**
     * @param $params
     * @return Timeline|\yii\db\ActiveRecord
     * @throws UserException
     */
    public static function getTimeline($params)
    {
        $timeline = self::find()->where($params)->one();
        if(!$timeline){
            $timeline = new self;
            $timeline->attributes = $params;
            $timeline->start_date = date('Y-m-d h:i:s');
            $timeline->created_at = date('Y-m-d h:i:s');
            $timeline->updated_at = date('Y-m-d h:i:s');
            $timeline->duration = 1;
            $timeline->comment = '';
            if(!$timeline->save())
                throw new UserException(json_encode($timeline->errors));
        }

        return $timeline;
    }

    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project', 'milestone'], 'required'],
            [['project', 'milestone', 'stage'], 'integer'],
            [['start_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project' => 'Project',
            'milestone' => 'Milestone',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIteration0()
    {
        return $this->hasOne(Iteration::className(), ['id' => 'iteration']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(LoginInstitution::className(), ['id' => 'created_by']);
    }
}
