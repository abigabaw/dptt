<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[IndicatorGrades]].
 *
 * @see IndicatorGrades
 */
class IndicatorGradesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return IndicatorGrades[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return IndicatorGrades|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
