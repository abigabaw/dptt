<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attachments".
 *
 * @property integer $id
 * @property integer $milestone_submission
 * @property string $attachment
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property MilestoneSubmission $milestoneSubmission
 */
class Attachments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone_submission', 'attachment', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['milestone_submission', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['attachment'], 'string', 'max' => 255],
            [['milestone_submission'], 'exist', 'skipOnError' => true, 'targetClass' => MilestoneSubmission::className(), 'targetAttribute' => ['milestone_submission' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'milestone_submission' => Yii::t('backend', 'Milestone Submission'),
            'attachment' => Yii::t('backend', 'Attachment'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestoneSubmission()
    {
        return $this->hasOne(MilestoneSubmission::className(), ['id' => 'milestone_submission']);
    }
}
