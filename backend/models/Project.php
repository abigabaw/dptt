<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use common\models\LoginInstitution;
use common\models\Institution;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property integer $innovator
 * @property integer $program
 * @property integer $amount
 * @property integer $institution
 * @property string $description
 * @property integer $country
 * @property integer $teamleader
 * @property string $award_date
 * @property string $signing_date
 * @property integer $startstage
 * @property integer $startmilestone
 *
 * @property DateIteration[] $dateIterations
 * @property Program $program0
 * @property Innovator $innovator0
 * @property Institution $institution0
 * @property ProjectGroup $projectGroup
 * @property Country $country0
 * @property LoginInnovator $teamleader0
 * @property Stage $startstage0
 * @property Milestone $startmilestone0
 * @property Timeline[] $timelines
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'innovator', 'program', 'amount', 'institution', 'teamleader', 'award_date', 'signing_date', 'startstage', 'startmilestone'], 'required'],
            [['innovator', 'program', 'amount', 'institution', 'country', 'startstage', 'startmilestone'], 'integer'],
            [['description'], 'string'],
            [['award_date', 'signing_date'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
		     public function behaviors()
		     {
		         return [
		            [ 'class'=>TimestampBehavior::className(),
		 		    'value'=>new Expression('NOW()')
		 		],
		             BlameableBehavior::className(),
		         ];
		     }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'innovator' => 'Project Team',
            'program' => 'Portfolio',
            'amount' => 'Amount',
            'institution' => 'Project Manager',
            'description' => 'Description',
            'country' => 'Country',
            'teamleader' => 'Team Leader',
            'award_date' => 'Award Date',
            'signing_date' => 'Signing Date',
            'startstage' => 'Start Stage',
            'startmilestone' => 'Start Milestone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDateIterations()
    {
        return $this->hasMany(DateIteration::className(), ['innovation' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(Program::className(), ['id' => 'program']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInnovator0()
    {
        return $this->hasOne(Innovator::className(), ['id' => 'innovator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution0()
    {
        return $this->hasOne(Institution::className(), ['id' => 'institution']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamleader0()
    {
        return $this->hasOne(LoginInnovator::className(), ['id' => 'teamleader']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartstage0()
    {
        return $this->hasOne(Stage::className(), ['id' => 'startstage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartmilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'startmilestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimelines()
    {
        return $this->hasMany(Timeline::className(), ['project' => 'id']);
    }
}
