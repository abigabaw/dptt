<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "milestone_submission".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $project
 * @property string $narration
 * @property string $attachment
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property IndicatorMilestoneSubmission[] $indicatorMilestoneSubmissions
 * @property Project $project0
 * @property Milestone $milestone0
 */
class MilestoneSub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'milestone_submission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone', 'project', 'narration', 'attachment', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['milestone', 'project', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['attachment'], 'string'],
            [['narration'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'milestone' => 'Milestone',
            'project' => 'Project',
            'narration' => 'Narration',
            'attachment' => 'Attachment',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestoneSubmissions()
    {
        return $this->hasMany(IndicatorMilestoneSubmission::className(), ['milestone_submission' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
}
