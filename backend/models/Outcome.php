<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "outcome".
 *
 * @property integer $id
 * @property string $outcome
 * @property integer $pipeline
 * @property integer $ordering
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Outcome extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
	 
    public static function tableName()
    {
        return 'outcome';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
   				[['pipeline', 'outcome'], 'required'],
	[['pipeline', 'ordering', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['outcome'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'outcome' => Yii::t('app', 'Outcome'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'ordering' => Yii::t('app', 'Ordering'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     * @return OutcomeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OutcomeQuery(get_called_class());
    }
}
