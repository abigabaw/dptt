<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "pipeline".
 *
 * @property integer $id
 * @property integer $name
 *
 * @property Cohort[] $cohorts
 * @property PipelineStage[] $pipelineStages
 */
class Pipeline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    public function rules()
    {
        return [
            [['name','description','type'], 'required'],
            [['name','description'], 'string', 'max' => 255],
	   [['type'], 'exist', 'skipOnError' => true, 'targetClass' => PipelineType::className(), 'targetAttribute' => ['type' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
				'type'=>'Pipeline Type'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCohorts()
    {
        return $this->hasMany(Cohort::className(), ['pipeline' => 'id']);
    }
    
	 public function getLogicalAttributes()
	   { return $this->hasMany(LogicalAttribute::className(), ['pipeline' => 'id']);
   }
	public function getOutcomes()
	   {return $this->hasMany(Outcome::className(), ['pipeline' => 'id']);
   }
	public function getOutputInstances()
	   {
	       return $this->hasMany(OutputInstance::className(), ['pipeline' => 'id']);
	   }
	   /**
	    * @return \yii\db\ActiveQuery
	    */
	   public function getCreatedBy()
	   {
	       return $this->hasOne(User::className(), ['id' => 'created_by']);
	   }
	   /**
	    * @return \yii\db\ActiveQuery
	    */
	   public function getUpdatedBy()
	   {
	       return $this->hasOne(User::className(), ['id' => 'updated_by']);
	   }
	   /**
	    * @return \yii\db\ActiveQuery
	    */
	   public function getType0()
	   {
	       return $this->hasOne(PipelineType::className(), ['id' => 'type']);
	   }
	   /**
	    * @return \yii\db\ActiveQuery
	    */
	   public function getPipelineIndicatorweights()
	   {
	       return $this->hasMany(PipelineIndicatorweight::className(), ['pipeline' => 'id']);
	   }
	   /**
	    * @return \yii\db\ActiveQuery
	    */
	   public function getPipelineIntervals()
	   {
	       return $this->hasMany(PipelineInterval::className(), ['pipeline' => 'id']);
	   }
	   
	  
	   /** 
	    * @return \yii\db\ActiveQuery 
	    */ 
	   public function getStages() 
	   { 
	       return $this->hasMany(Stage::className(), ['id' => 'stage'])->viaTable('pipeline_stage', ['pipeline' => 'id']); 
	   } 
 
	   /** 
	    * @return \yii\db\ActiveQuery 
	    */ 
	   public function getPrograms() 
	   { 
	       return $this->hasMany(Program::className(), ['pipeline' => 'id']); 
	   }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelineStages()
    {
        return $this->hasMany(PipelineStage::className(), ['pipeline' => 'id']);
    }
}
