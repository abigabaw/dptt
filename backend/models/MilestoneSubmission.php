<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "milestone_submission".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $project
 * @property string $narration
 * @property string $attachment
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property IndicatorMilestoneSubmission[] $indicatorMilestoneSubmissions
 * @property Project $project0
 * @property Milestone $milestone0
 */
class MilestoneSubmission extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'milestone_submission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iteration', 'narration'], 'required'],
            [['file'], 'file','extensions' => 'pdf, xls, doc, docx'],
            [['iteration', 'status', 'created_by', 'updated_by'], 'integer'],
            [['narration'], 'string', 'max' => 500]
        ];
    }
          /**
     * @inheritdoc
     */
	     public function behaviors()
	     {
	         return [
	            [ 'class'=>TimestampBehavior::className(),
	 		    'value'=>new Expression('NOW()')
	 		],
	             BlameableBehavior::className(),
	         ];
	     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'innovator' => 'Innovator',
            'milestone' => 'Milestone',
            'project' => 'Project',
            'narration' => 'Narration',
            'attachment' => 'Attachment',
            'status' => 'Status',
            'file' => 'Attachment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestoneSubmissions()
    {
        return $this->hasMany(IndicatorMilestoneSubmission::className(), ['milestone_submission' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
}