<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "extension_requests".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $project
 * @property integer $innovator
 * @property integer $institution
 * @property string $reason
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Institution $institution0
 * @property Milestone $milestone0
 * @property Project $project0
 * @property Innovator $innovator0
 */
class ExtensionRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extension_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iteration','reason', 'status'], 'required'],
            [['iteration','status'], 'integer'],
            [['reason'], 'string', 'max' => 255]
        ];
    }

        public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
            'value'=>new Expression('NOW()')
        ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'iteration' => Yii::t('app', 'Iteration'),
            'reason' => Yii::t('app', 'Reason'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }
}
