<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pipeline_interval".
 *
 * @property integer $id
 * @property integer $pipeline
 * @property string $milestone
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property OutputInstance[] $outputInstances
 * @property Pipeline $pipeline0
 * @property User $createdBy
 * @property User $updatedBy
 */
class PipelineInterval extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline_interval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pipeline', 'created_by', 'updated_by'], 'integer'],
            [['milestone', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['milestone'], 'string', 'max' => 200],
            [['pipeline', 'milestone'], 'unique', 'targetAttribute' => ['pipeline', 'milestone'], 'message' => 'The combination of Pipeline and Milestone has already been taken.'],
            [['pipeline'], 'exist', 'skipOnError' => true, 'targetClass' => Pipeline::className(), 'targetAttribute' => ['pipeline' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'milestone' => Yii::t('app', 'Milestone'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutputInstances()
    {
        return $this->hasMany(OutputInstance::className(), ['output_instance' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipeline0()
    {
        return $this->hasOne(Pipeline::className(), ['id' => 'pipeline']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return PipelineIntervalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PipelineIntervalQuery(get_called_class());
    }
}
