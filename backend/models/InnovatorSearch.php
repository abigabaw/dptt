<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Innovator;

/**
 * InnovatorSearch represents the model behind the search form about `backend\models\Innovator`.
 */
class InnovatorSearch extends Innovator
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'instituition', 'country', 'created_at', 'updated_at'], 'integer'],
            [['email', 'names', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Innovator::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'instituition' => Yii::$app->user->identity->institution,
            'country' => $this->country,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'names', $this->names])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
