<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PipelineStage;

/**
 * PipelineStageSearch represents the model behind the search form about `backend\models\PipelineStage`.
 */
class PipelineStageSearch extends PipelineStage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pipeline', 'stage'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = PipelineStage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pipeline' => $id,
            'stage' => $this->stage,
        ]);

        return $dataProvider;
    }
}
