<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use backend\models\VDelayedMilestone;

/**
 * TimelineSearch represents the model behind the search form about `backend\models\Timeline`.
 */
class DashboardSearch extends Timeline
{

    public function search($params)
    {
         $sql        = 'SELECT * FROM `v_delayed_milestone` WHERE  finish_date IS NOT NULL
                          AND expected_finish_date <= date\''.date('Y-m-d').'\' AND pipeline='.Yii::$app->session['pipeline'].' GROUP BY timeline
                        
                        ';
        $query = VDelayedMilestone::find(['expected_finish_date'=>'< date\''.date('Y-m-d').'\'','pipeline'=>Yii::$app->session['pipeline']])->groupBy('timeline');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        // $count      =   count(Yii::$app->db->createCommand($sql)->queryAll());

        // $dataProvider = new SqlDataProvider([
        //             'sql' => $sql,
        //             'totalCount' => $count,
        //             'sort' => ['attributes' => ['expected_finish_date']],
        //             ]);

 $dataProvider->setSort([
        'attributes' => [
            'milestone',
            'project',
            'duration_unit',
            'delay'
        ]
    ]);    

        return $dataProvider;
    }
}