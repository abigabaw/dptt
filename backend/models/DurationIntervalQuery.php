<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[DurationInterval]].
 *
 * @see DurationInterval
 */
class DurationIntervalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DurationInterval[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DurationInterval|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
