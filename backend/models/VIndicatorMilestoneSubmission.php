<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_indicator_milestone_submission".
 *
 * @property integer $id
 * @property integer $milestone_submission
 * @property integer $indicator_id
 * @property string $indicator_name
 * @property integer $indicator_type_id
 * @property integer $milestone_id
 * @property string $milestone_name
 * @property integer $project_id
 * @property string $project_name
 * @property string $indicator_type_name
 * @property string $value
 * @property string $submission_date
 */
class VIndicatorMilestoneSubmission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_indicator_milestone_submission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'milestone_submission', 'indicator_id', 'indicator_type_id', 'milestone_id', 'project_id'], 'integer'],
            [['milestone_submission', 'indicator_id', 'value'], 'required'],
            [['submission_date'], 'safe'],
            [['indicator_name', 'milestone_name', 'project_name', 'indicator_type_name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'milestone_submission' => Yii::t('app', 'Milestone Submission'),
            'indicator_id' => Yii::t('app', 'Indicator ID'),
            'indicator_name' => Yii::t('app', 'Indicator Name'),
            'indicator_type_id' => Yii::t('app', 'Indicator Type ID'),
            'milestone_id' => Yii::t('app', 'Milestone ID'),
            'milestone_name' => Yii::t('app', 'Milestone Name'),
            'project_id' => Yii::t('app', 'Project ID'),
            'project_name' => Yii::t('app', 'Project Name'),
            'indicator_type_name' => Yii::t('app', 'Indicator Type Name'),
            'value' => Yii::t('app', 'Value'),
            'submission_date' => Yii::t('app', 'Submission Date'),
        ];
    }
}
