<?php
namespace backend\models;

use backend\models\LoginInnovator;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class UserForm extends Model
{
    public $name;
    public $email;
    public $gender;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['gender', 'required'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\backend\models\LoginInnovator', 'message' => 'This email address has already been taken.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($innovator)
    {
    	if ($this->validate()) {
        	$password = LoginInnovator::generatePassword(7);
        	//return 
            $user = new LoginInnovator();
            $user->name 	= $this->name;
            $user->email 	= $this->email;
            $user->gender 	= $this->gender;
            $user->innovator = $innovator;
            $user->setPassword($password);
            $user->generateAuthKey();
            if ($user->save()) {
            	if($this->sendEmail($this->email,$password))
                return $user;
            }
        }
        return null;
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail($email,$password)
    {
    	return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
            ->setSubject('User Registration')
            ->setTextBody('Hello '.$this->name.',<br/> an account has been created for you at MERL.
            			   Please login with the following details. <br/> Email: '.$this->email.'<br/> Password: '.$password.'<br/>
            			   Remember to change this password when you log in')
            ->send();
    }
}