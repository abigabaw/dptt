<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "interval_unit_map".
 *
 * @property integer $id
 * @property integer $duration_interval
 * @property integer $duration_unit
 * @property integer $factor
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property DurationInterval $durationInterval
 * @property DurationUnit $durationUnit
 * @property User $createdBy
 * @property User $updatedBy
 */
class IntervalUnitMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interval_unit_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['duration_interval', 'duration_unit', 'factor', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['duration_interval', 'duration_unit'], 'unique', 'targetAttribute' => ['duration_interval', 'duration_unit'], 'message' => 'The combination of Duration Interval and Duration Unit has already been taken.'],
            [['duration_interval'], 'exist', 'skipOnError' => true, 'targetClass' => DurationInterval::className(), 'targetAttribute' => ['duration_interval' => 'id']],
            [['duration_unit'], 'exist', 'skipOnError' => true, 'targetClass' => DurationUnit::className(), 'targetAttribute' => ['duration_unit' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'duration_interval' => Yii::t('app', 'Duration Interval'),
            'duration_unit' => Yii::t('app', 'Duration Unit'),
            'factor' => Yii::t('app', 'Factor'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDurationInterval()
    {
        return $this->hasOne(DurationInterval::className(), ['id' => 'duration_interval']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDurationUnit()
    {
        return $this->hasOne(DurationUnit::className(), ['id' => 'duration_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return IntervalUnitMapQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IntervalUnitMapQuery(get_called_class());
    }
}
