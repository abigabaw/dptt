<?php
namespace backend\models;

use common\models\LoginInstitution;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class AccountForm extends Model
{
        public $oldpass;
        public $newpass;
        public $repeatnewpass;
        
        public function rules(){
            return [
                [['oldpass','newpass','repeatnewpass'],'required'],
                ['oldpass','findPasswords'],
                ['repeatnewpass','compare','compareAttribute'=>'newpass'],
            ];
        }
        
        public function findPasswords($attribute, $params){
            $user = User::find()->where([
                'id'=>Yii::$app->user->identity->id
            ])->one();
            if (!Yii::$app->getSecurity()->validatePassword($this->oldpass, $user->password_hash))
            $this->addError($attribute,'Old password is incorrect');        }
        
        public function attributeLabels(){
            return [
                'oldpass'=>'Old Password',
                'newpass'=>'New Password',
                'repeatnewpass'=>'Repeat New Password',
            ];
        }
}