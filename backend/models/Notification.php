<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use backend\models\VMilestoneSubmission;
use backend\models\Project;
use backend\models\NotificationInnovator;
use machour\yii2\notifications\NotificationsModule;
use backend\models\ExtensionRequests;
use backend\models\IndicatorRequests;
use common\models\VIterationRequests;
use backend\models\Timeline;
use machour\yii2\notifications\models\Notification as BaseNotification;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "notification".
 *
 * @property integer $idf
 * @property integer $key_id
 * @property string $key
 * @property string $type
 * @property boolean $seen
 * @property string $created_at
 * @property integer $user_id
 */
class Notification extends BaseNotification
{
	/**
     * new creation notification
     */
    const PROJECT_CREATION              = 'project_creation';
    const MILESTONE_SUBMISSION          = 'milestone_submission';
    const ITERATION_REQUEST             = 'iteration_request';
    const ITERATION_REQUEST_APPROVAL    = 'iteration_request_approval';
    const ITERATION_REQUEST_REJECT      = 'iteration_request_reject';
    const DATE_EXTENSION                = 'extension_request';
    const EXTENSION_APPROVAL            = 'extension_approval';
    const EXTENSION_REQUEST_REJECT      = 'extension_request_reject';
    const INDICATOR_REQUEST             = 'indicator_request';
    const INDICATOR_REQUEST_APPROVAL    = 'indicator_request_approval';
    const INDICATOR_REQUEST_REJECT      = 'indicator_request_reject';
    const PROJECT_ASSESMENT             = 'project_assement';
    const MILESTONE_REPEAT              = 'milestone_repeat';
    
    /**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        self::PROJECT_CREATION,
        self::MILESTONE_SUBMISSION,
        self::ITERATION_REQUEST,
        self::DATE_EXTENSION,
        self::EXTENSION_APPROVAL,
        self::ITERATION_REQUEST_APPROVAL,
        self::ITERATION_REQUEST_REJECT,
        self::INDICATOR_REQUEST,
        self::INDICATOR_REQUEST_APPROVAL,
        self::INDICATOR_REQUEST_REJECT,
        self::PROJECT_ASSESMENT,
        self::MILESTONE_REPEAT,
        self::EXTENSION_REQUEST_REJECT
    ];


    /**
     * @var array List of all enabled notification types
     */
    public static $types = [
        self::TYPE_WARNING,
        self::TYPE_DEFAULT,
        self::TYPE_ERROR,
        self::TYPE_SUCCESS,
    ];


    /**
     * Gets the notification title
     *
     * @return string
     */
    public function getTitle(){
   
        switch ($this->key) {

            case 'milestone_submission':
                return Yii::t('app', 'New Milestone Submission');
            case 'date_extension':
                return Yii::t('app', 'Request for date extension');
            case 'iteration_request':
                return Yii::t('app', 'Request to iterate a milestone');
            case 'iteration_request':
                return Yii::t('app', 'Request to add an an indicator');
            case 'indicator_request':
                return Yii::t('app', 'Request to add an an indicator');
        }
    }

    /**
     * Gets the notification description
     *
     * @return string
     */
    public function getDescription(){
        switch ($this->key) {
        case 'milestone_submission':
                $submission = VMilestoneSubmission::findOne(['id'=>$this->key_id]);
                if($submission){
                return Yii::t('app', 'Please review the submission for:<br/> {milestone}', [
                    'milestone' => $submission->milestone
                ]);
                }
                return;
        case 'extension_request':
                $request = ExtensionRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone0->milestone
                ]);
                }
                return;
        case 'iteration_request':
                $request = VIterationRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'Milestone:<br/> {milestone_name}', [
                    'milestone_name' => $request->milestone_name
                ]);
                }
                return;
        case 'indicator_request':
                $request = IndicatorRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'Milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone0->milestone
                ]);
                }
                return;
        }

    }
    
    /**
     * Gets the notification route
     *
     * @return string
     */
    public function getRoute(){
		switch ($this->key) {
            case 'milestone_submission':
                $submission = VMilestoneSubmission::findOne(['id'=>$this->key_id]);
                if($submission){
                return ['/project/assess/'.$submission->milestone_id.'?project='.$submission->project_id];
                }else{
                return ['/project'];
                }
           case 'extension_request':
                $request = ExtensionRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/submit/'.$request->milestone.'?project='.$request->project];
                }else{
                return ['/project'];
                }
            case 'iteration_request':
                $request = VIterationRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/milestone/requests/'];
                }else{
                return ['/project'];
                }
            case 'indicator_request':
                $request = IndicatorRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/indicator/requests'];
                }else{
                return ['/project'];
                }
        };
    }

    /**
     * @inheritdoc
     */
    static function tableName()
    {
        return '{{%notification_institution}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'user_id', 'key', 'created_at'], 'required'],
            [['id', 'key_id', 'created_at'], 'safe'],
            [['key_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * Creates a notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The foreign instance id
     * @param string $type
     * @return bool Returns TRUE on success, FALSE on failure
     * @throws \Exception
     */
    public static function notify($key, $user_id, $key_id = null, $type = self::TYPE_DEFAULT)
    {
        $notification = NotificationInnovator::className();
        //@overide the vendor class function
        //return NotificationsModule::notify(new $class(), $key, $user_id, $key_id, $type);
        /** @var Notification $instance */

        $instance = $notification::findOne(['user_id' => $user_id, 'key' => $key, 'key_id' => $key_id]);
        if (!$instance) {
            $instance = new $notification([
                'key' => $key,
                'type' => $type,
                'seen' => 0,
                'user_id' => $user_id,
                'key_id' => $key_id,
                'created_at' => new Expression('NOW()'),
            ]);
            return $instance->save();
        }

    }

    /**
     * Creates a warning notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function warning($key, $user_id, $key_id = null)
    {
        return static::notify($key, $user_id, $key_id, self::TYPE_WARNING);
    }


    /**
     * Creates an error notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function error($key, $user_id, $key_id = null)
    {
        return static::notify($key, $user_id, $key_id, self::TYPE_ERROR);
    }


    /**
     * Creates a success notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function success($key, $user_id, $key_id = null)
    {
        return static::notify($key, $user_id, $key_id, self::TYPE_SUCCESS);
    }

}