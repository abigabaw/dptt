<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[VStaticIndicatorMilestone]].
 *
 * @see VStaticIndicatorMilestone
 */
class VStaticIndicatorMilestoneQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return VStaticIndicatorMilestone[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VStaticIndicatorMilestone|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
