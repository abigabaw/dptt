<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_grant".
 *
 * @property integer $id
 * @property string $name
 * @property integer $agency
 * @property string $description
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $institution_name
 * @property integer $institution_id
 * @property string $grant_name
 * @property integer $grant_id
 */
class VGrant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_grant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'agency', 'created_by', 'updated_by', 'institution_id', 'grant_id'], 'integer'],
            [['name', 'agency', 'description'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'institution_name', 'grant_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'agency' => Yii::t('app', 'Agency'),
            'description' => Yii::t('app', 'Description'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
            'grant_name' => Yii::t('app', 'Grant Name'),
            'grant_id' => Yii::t('app', 'Grant ID'),
        ];
    }
    public function getAgency0()
    {
        return $this->hasOne(Agency::className(), ['id' => 'agency']);
    }

    /**
     * @inheritdoc
     * @return VGrantQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VGrantQuery(get_called_class());
    }
}
