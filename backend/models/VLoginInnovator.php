<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_login_innovator".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $gender
 * @property integer $innovator
 * @property string $auth_key
 * @property integer $status
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $institution_name
 * @property integer $institution_id
 */
class VLoginInnovator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_login_innovator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'innovator', 'status', 'created_at', 'updated_at', 'institution_id'], 'integer'],
            [['name', 'email', 'gender', 'innovator', 'auth_key', 'status', 'password_hash', 'password_reset_token', 'created_at', 'updated_at'], 'required'],
            [['name', 'email'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 8],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'institution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'gender' => Yii::t('app', 'Gender'),
            'innovator' => Yii::t('app', 'Innovator'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'status' => Yii::t('app', 'Status'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return VLoginInnovatorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VLoginInnovatorQuery(get_called_class());
    }
}
