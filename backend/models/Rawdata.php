<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "rawdata".
 *
 * @property integer $id
 * @property string $category
 * @property string $productname
 * @property integer $ponumber
 * @property string $podate
 * @property string $projecteddeliverydate
 * @property string $deliverydate
 * @property integer $quantity
 * @property integer $unitprice
 * @property double $totallandedcost
 */
class Rawdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rawdata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ponumber', 'quantity', 'unitprice'], 'integer'],
            [['podate', 'projecteddeliverydate', 'deliverydate'], 'safe'],
            [['totallandedcost'], 'number'],
            [['category', 'productname'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'productname' => Yii::t('app', 'Productname'),
            'ponumber' => Yii::t('app', 'Ponumber'),
            'podate' => Yii::t('app', 'Podate'),
            'projecteddeliverydate' => Yii::t('app', 'Projecteddeliverydate'),
            'deliverydate' => Yii::t('app', 'Deliverydate'),
            'quantity' => Yii::t('app', 'Quantity'),
            'unitprice' => Yii::t('app', 'Unitprice'),
            'totallandedcost' => Yii::t('app', 'Totallandedcost'),
        ];
    }

    /**
     * @inheritdoc
     * @return RawdataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RawdataQuery(get_called_class());
    }
}
