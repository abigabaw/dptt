<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[PipelineType]].
 *
 * @see PipelineType
 */
class PipelineTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PipelineType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PipelineType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
