<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "stage".
 *
 * @property integer $id
 * @property string $stage
 *
 * @property MilestoneStage[] $milestoneStages
 * @property PipelineStage[] $pipelineStages
 */
class Stage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stage';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stage'], 'required'],
            [['stage','description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stage' => 'Stage',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestoneStages()
    {
        return $this->hasMany(MilestoneStage::className(), ['stage' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelineStages()
    {
        return $this->hasMany(PipelineStage::className(), ['stage' => 'id']);
    }
}
