<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "v_indicator_submission".
 *
 * @property integer $milestone_submission
 * @property integer $indicator_id
 * @property string $name
 * @property string $value
 */
class VIndicatorSubmission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_indicator_submission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone_submission', 'indicator_id', 'value'], 'required'],
            [['milestone_submission', 'indicator_id'], 'integer'],
            [['name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'milestone_submission' => Yii::t('backend', 'Milestone Submission'),
            'indicator_id' => Yii::t('backend', 'Indicator ID'),
            'name' => Yii::t('backend', 'Name'),
            'value' => Yii::t('backend', 'Value'),
        ];
    }
}
