<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Timeline;
use yii\data\SqlDataProvider;

/**
 * TimelineSearch represents the model behind the search form about `backend\models\Timeline`.
 */
class TimelineSearch extends Timeline
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project', 'milestone', 'created_by', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$project)
    {
        $query = Timeline::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'project'    => $this->project,
            'milestone'  => $this->milestone,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }

    public function searchTimeline($params,$project)
    {
         $sql        = 'SELECT a.id, a.milestone as milestone_id, b.milestone,
                        c.comment, c.start_date, c.project as project_id, c.duration FROM timeline a LEFT join milestone b
                        ON a.milestone=b.id LEFT join iteration c ON a.id=c.timeline WHERE c.project='.$project.' GROUP BY
                        a.milestone';
        $count      =   count(Yii::$app->db->createCommand($sql)->queryAll());

        $dataProvider = new SqlDataProvider([
                    'sql' => $sql,
                    'totalCount' => $count,
                    ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}