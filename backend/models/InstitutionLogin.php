<?php

namespace backend\models;

use common\models\Institution;
use dektrium\user\models\User;

/** 
 * @property string $rel_table
 * @property int $rel_table_id
 *
 *  *** Virtual Properties  ***
 * @property int $institution
 * @property string $name
 *
 * @property Institution $userInstitution
 */
class InstitutionLogin extends User
{
    public function getInstitution(){
        return $this->rel_table_id;
    }

    public function getName(){
        return $this->profile->name;
    }

    public static function find()
    {
        return parent::find()->where(['=','rel_table','institution']);
    }
}