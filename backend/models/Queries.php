<?php 
namespace backend\models;
use Yii;

class Queries{
  public static function ReportQuery($project){
  	return 'SELECT a.milestone as milestone_id, b.timeline, 
  			(SELECT SUM(duration) FROM iteration WHERE timeline=b.timeline) AS total_duration,
  			 b.comment, b.start_date, b.duration, e.created_at, c.milestone, c.num, d.stage as stage_id, f.stage, e.attachment FROM timeline
  			 a LEFT JOIN iteration b ON b.timeline=a.id LEFT JOIN milestone c ON a.milestone=c.id LEFT JOIN
  			 milestone_stage d on a.milestone=d.milestone LEFT JOIN milestone_submission e ON a.milestone=e.milestone
  			  AND e.project='.$project.' LEFT JOIN stage f ON d.stage = f.id WHERE b.project='.$project.' GROUP BY a.milestone ORDER BY b.timeline ASC';
  }

  public static function qry($project){
  	return 'SELECT a.id as timeline, a.milestone as milestone_id,a.comment, a.start_date, a.duration, b.milestone, b.num, a.duration_unit, c.unit FROM timeline a LEFT JOIN milestone b ON a.milestone=b.id left join duration_unit c on a.duration_unit=c.id
         WHERE a.project='.$project.' ORDER BY a.start_date ASC';


  }
  public static function getIndicatorWeights($pipeline,$indicator){
    return 'SELECT a.id, a.pipeline, a.indicator as indicator_id, b.name, a.weight
            from `pipeline_indicatorweight` a left join indicator b on a.indicator=b.id
            WHERE a.milestone='.$indicator.' AND a.pipeline='.$pipeline.'';
  }
  public static function getProjectIndicatorWeights($project,$indicator){
    return 'SELECT a.id, a.project, a.indicator as indicator_id, b.name, a.weight
            from `project_indicatorweight` a left join indicator b on a.indicator=b.id
            WHERE a.milestone='.$indicator.' AND a.project='.$project.'';
  }
  public static function qry2($project){
  	return  'SELECT * from v_milestone_submission
        where pipeline=\''.Yii::$app->session['pipeline'].'\' and project='.$project.'
                GROUP BY project_id
                ORDER BY created_at ASC';
  }
  public static function qryMilestoneSubmission($project){
    return  'SELECT * from v_milestone_submission
        where pipeline=\''.Yii::$app->session['pipeline'].'\' and project_id='.$project.'
                ORDER BY start_date ASC';
  }
  public static function qry3($project){
  	return 'SELECT reason, duration, created_at FROM `extension_requests` WHERE project = '.$project.'';
  }
  public static function qryIteration($project){
  	return 'SELECT * from v_milestone_submission WHERE iteration_type=\'Iteration\'';
  }
  public static function qryRepeat($repeat){
    return 'SELECT * from v_milestone_submission WHERE iteration_type=\'Repeat\'';
  }
  public static function qryExtension($repeat){
    return 'SELECT * from v_milestone_submission WHERE iteration_type=\'Extension\'';
  }
  public static function qryDelayed($project){
  	return 'SELECT a.project_id, a.project, iteration_type,( SELECT COUNT( DISTINCT milestone_id ) 
          FROM v_delayed_milestone WHERE project_id = '.$project.' AND finish_date is null AND
           expected_finish_date is not null
          AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' ) AS 
          number_of_milestones FROM v_delayed_milestone a
          WHERE finish_date is null AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' AND
           a.project = '.$project.'';
  }

  public static function qryOverDue(){
  return 'SELECT a.project_id, a.project, iteration_type,( SELECT COUNT( DISTINCT milestone_id ) 
          FROM v_delayed_milestone WHERE project_id = a.project_id AND finish_date is null AND expected_finish_date is not null
          AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' ) AS 
          number_of_milestones, ( SELECT COUNT( DISTINCT milestone ) 
          FROM timeline WHERE project = a.project_id) AS 
          number_of_assigned_milestones FROM v_delayed_milestone a
          WHERE finish_date is null AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' and pipeline =\''.Yii::$app->session['pipeline'].'\'
                
                    GROUP BY project_id';
  }

public static function qryProjects(){
  return 'SELECT * FROM `v_project` WHERE pipeline_id=\''.Yii::$app->session['pipeline'].'\' and institution='.Yii::$app->user->identity->institution.'';
}


  public static function qryLateSubmission($project){
    return 'SELECT * FROM `v_delayed_milestone` WHERE  finish_date IS NOT NULL
            AND  project = '.$project.' AND expected_finish_date <= \''.date('Y-m-d').'\'';

  }

  public static function qryCummulative($project){
  	return 'SELECT * FROM v_cummulative_scores_indicator WHERE project_id='.$project.'
  	ORDER BY id ASC';
  }
  public static function qryCummulativeAll($pipeline){
    return 'SELECT * FROM v_cummulative_scores_indicator where pipeline_id='.$pipeline.'
            GROUP BY project_id ORDER BY id ASC';
  }

  public static function MilestoneSubmissionsAll($portfolio=''){
	  if($portfolio)
	     return 'SELECT * from v_milestone_submission
	         where  portfolio_id=\''.$portfolio.'\'
	                 GROUP BY project_id
	                 ORDER BY created_at ASC';	  
		  else
    return 'SELECT * from v_milestone_submission
        where project_id !=101 and pipeline=\''.Yii::$app->session['pipeline'].'\'
                GROUP BY project_id
                ORDER BY created_at ASC';
  }


  public static function getIndicatorRequests($institution){
    return 'SELECT a.id,a.project as project_id, b.name as project_name, a.milestone FROM `indicator_requests`
            a LEFT JOIN project b on a.project=b.id left join innovator c on b.innovator = c.id where
            c.instituition = '.$institution.' and status=0';
  }

  public static function getIterationRequests($institution){
    return 'SELECT * FROM `v_iteration_requests` where institution ='.$institution.' 
           and status=0';
  }

  public static function getExtensionRequests($institution){
    return 'SELECT a.id,a.project as project_id, b.name as project_name, a.milestone FROM `extension_requests` a 
            LEFT JOIN project b on a.project=b.id left join innovator c on b.innovator = c.id where c.instituition ='.$institution.'
            and status=0';
  }

  public static function getIndicatorGrades($id){
    return 'SELECT a.id, b.indicator as indicator_id, a.value, c.name as indicator from indicator_grades
            a left join indicator_milestone_submission b on a.indicator_milestone_submission=b.id left join 
            indicator c on b.indicator=c.id where 
            b.milestone_submission= '.$id.'';
  }

  public static function getIndicatorScore($project,$indicator){
    return 'SELECT a.value, f.name from indicator_grades a left join indicator_milestone_submission b on
            b.id=a.indicator_milestone_submission left join milestone_submission c on c.id=b.milestone_submission
            left join iteration d on d.id=c.iteration left join timeline e on d.timeline=e.id
            left join indicator f on b.indicator = f.id
            where e.project='.$project.' and b.indicator='.$indicator.'  LIMIT 1';
  }

  public static function getMilestoneScore($project,$milestone){
    return 'SELECT a.grade, e.milestone as name from grading a left join milestone_submission b on a.milestone_submission=b.id left join
            iteration c on b.iteration=c.id left join timeline d on c.timeline = d.id
            left join milestone e on d.milestone=e.id
            where d.milestone='.$milestone.' and d.project='.$project.'';
  }

  public static function getIndicatorSubmissions($project,$id){
    return 'SELECT indicator_id, target_value, indicator_name, value, milestone_id, milestone_name, submission_date from v_indicator_milestone_submission
            where project_id='.$project.' and indicator_id = '.$id.' order by submission_date asc';
  }

  public static function getIndicatorSubmissionsGraphs($project){
    return 'SELECT DISTINCT indicator_id, indicator_name from v_indicator_milestone_submission
            where project_id='.$project.'';
  }
  public static function getIndicatorSubmissionsGraphsSelected($project,$indicator_ids){
    return 'SELECT DISTINCT indicator_id, indicator_name from v_indicator_milestone_submission
            where project_id='.$project.' and indicator_id in ('.$indicator_ids.')';
  }

  public static function getProjectIndicatorSubmissions($pipeline,$indicator){
    return 'SELECT indicator_id, indicator_name, project_id, project_name, value, score from
            v_indicator_milestone_submission where pipeline_id='.$pipeline.' and indicator_id = '.$indicator.'
            order by project_id';
  }

}
?>