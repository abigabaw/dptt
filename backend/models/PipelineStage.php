<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "pipeline_stage".
 *
 * @property integer $id
 * @property integer $pipeline
 * @property integer $stage
 * @property integer $ordering
 *
 * @property Stage $stage0
 * @property Pipeline $pipeline0
 */
class PipelineStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline_stage';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pipeline', 'stage'], 'required'],
            [['pipeline', 'stage', 'ordering'], 'integer'],
            [['pipeline', 'stage'], 'unique', 'targetAttribute' => ['pipeline', 'stage'], 'message' => 'The combination of Pipeline and Stage has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pipeline' => 'Pipeline',
            'stage' => 'Stage',
            'ordering' => 'Ordering',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStage0()
    {
        return $this->hasOne(Stage::className(), ['id' => 'stage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipeline0()
    {
        return $this->hasOne(Pipeline::className(), ['id' => 'pipeline']);
    }
}
