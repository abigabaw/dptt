<?php

namespace common\assets;

class HighStockAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@webroot/css/highstock/';

    public $css = [];

    public $js = ['js/highstock.js','js/exporting.js'];

    public $depends = [
        'common\assets\ThemeAsset',
    ];
}