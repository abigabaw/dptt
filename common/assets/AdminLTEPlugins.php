<?php

namespace common\assets;

class AdminLTEPlugins extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins/';

    public $css = [];

    public $js = [];

    public $depends = [
        'common\assets\ThemeAsset',
    ];
}