<?php

namespace common\assets;

use dmstr\web\AdminLteAsset;
use yii\helpers\ArrayHelper;

class ThemeAsset extends AdminLteAsset
{

    public $skin = 'skin-green';

    public function render($layout, $params = []){
        
        $params = ArrayHelper::merge([
            'directoryAsset'=>$this->baseUrl,
        ], $params);
        
        return \Yii::$app->controller->renderPartial(
            "@common/themes/AdminLTE/views/layouts/$layout", $params);
    }
}