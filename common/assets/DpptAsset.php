<?php

namespace common\assets;

class DpptAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@webroot/css/dptt/';

    public $css = ['css/style.css',
    			   'css/notifications.css',
                   'css/growl.css'
                  ];

    public $js = ['js/Chart.js',
    			  'js/growl.js',
    			  'js/notifications.js',
    			  'js/jquery.timeago.js'
    			 ];

    public $depends = [
        'common\assets\ThemeAsset',
    ];
}