<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DatatablesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'datatables/jquery.dataTables.css'
    ];
    public $js = [
        'datatables/jquery.dataTables.js'
    ];
    public $depends = [
        'common\assets\ThemeAsset',
    ];
}
