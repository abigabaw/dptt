<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\VIndicatorMilestoneSubmission;
use backend\models\VProject;
class IndicatorSubmission extends Widget{
	public $series;
    public $id;
    public $elementid;
    public $scales;
    public $indicator_ids;
	 private $project;
	public function init(){
		$ids = '';
		$this->project=VProject::find()->where(['id'=>$this->id])->one()->name;
		if($this->indicator_ids){
		foreach ($this->indicator_ids as $id) {
			$ids.=$id.',';
		}
		$ids 				= rtrim($ids,',');
		$graphs 			= Yii::$app->db->createCommand(Queries::getIndicatorSubmissionsGraphsSelected($this->id,$ids))->queryAll();
      	}else{
      	$graphs 			= Yii::$app->db->createCommand(Queries::getIndicatorSubmissionsGraphs($this->id))->queryAll();
        }
		  $colors=['red','blue','green','black'];
		// $colors= ['#562F1E','#AF7F24', '#263249', '#5F7F90', '#D9CDB6'];
        foreach ($graphs as $k=>$graph) {
			$data_points 	= [];
        	$submissions 	= Yii::$app->db->createCommand(Queries::getIndicatorSubmissions($this->id,$graph['indicator_id']))->queryAll();
	        $value = 0;
	        foreach ($submissions as $data) {
	        	$value +=$data['value']; 
	        	$data_points[]=[
							'x'=>strtotime($data['submission_date']).'000',
							'y'=>$value,
							'text' => '<b>'.$data['milestone_name'].'</b> on '.$data['submission_date'].'<br/><b>'.$data['indicator_name'].' : </b> '.$data['value'].' of '.$value.' so far<br/>'
					];
	        }
	        $this->scales[] =[
	        'title'=>['text'=>$graph['indicator_name'].' (in '.ucfirst($colors[$k]).')'],
	        'lineWidth'=>2,
			  'lineColor'=>$colors[$k],
			  'opposite'=>$k%2==0 ? true : false,
	       // 'height'=>200
	        ];  
	       	$this->series[]=[
					'lineColor'=>$colors[$k],
      			'name'	=>$graph['indicator_name'],
      			'id'=>'graph_'.$graph['indicator_id'],
				'data' =>$data_points,
				'marker'=>['enabled'=>true, 'radius'=>3],
				'yAxis'=> $k,
				'step' => true,
			];
        }

        
  
    }

    public function run (){
    	return $this->render('@common/views/indicatorsubmission',
						[ 'series' 			=> $this->series,
						  'scales' 			=> $this->scales,
						  'project'			=>$this->project,
						  'elementid'		=> $this->elementid ? $this->elementid : md5(time())
						]);
    }

}

?>