<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;
use backend\models\VProject;
class StartEndDateGraph extends Widget{
	public $id;
	public $elementid;
	public $series;
	public $data_flags;

	public function init(){
	$series=[];
	$mstone =[];
	$mstone2 =[];
	$ct=$ctr=0;

	//Scale 1: Distinct Milestones Achieved
	$m=Yii::$app->db->createCommand("SELECT distinct milestone_id from v_milestone_submission where project_id=".$this->id." order by start_date asc")->queryAll();
	foreach($m as $ms)
		$mstone[$ms['milestone_id']]=++$ct;
	
	//Scale 2: Milestones of pipeline
	$pipeline=Yii::$app->session['pipeline'] ? Yii::$app->session['pipeline'] : VProject::find()->where(['id'=>$this->id])->one()->pipeline_id;
	//die(print_r($this->id));
	$st=PipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->All();
	//print_r($st);
	foreach($st as $stage) {
		//echo $stage;
		$ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
		foreach ($ms as $mst) {
			$mstone2[$mst['milestone']]=++$ctr;
		}
	}
	$scale=$mstone2;
	//print_r($scale);
	//exit;
	$iteration_types=IterationType::find()->All();
	foreach ($iteration_types as $it ) {
		$iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id=".$this->id." ORDER BY start_date")->queryAll();
		$iterationsx=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id=".$this->id." ORDER BY end_date")->queryAll();
		
		if($iterations) {
			//$data_points=$data_flags='';
			$data_points=$this->data_flags=[];
			$data_pointse = [];
			foreach ($iterations as $ar) {
				$data_points[]=[
						'x'=>strtotime($ar['start_date']).'000',
						'y'=>$scale[$ar['milestone_id']],
						'text' => '<b>'.$scale[$ar['milestone_id']].' - '.$it['name'].' (Start - Milestone on ('.date('F d, Y',strtotime($ar['start_date'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
				];
            }

			$series[]=[
      			'name'	=>'Start Dates',
				'data' =>$data_points,
				'marker'=>['enabled'=>true, 'radius'=>3],
				//fillColor' => '#123457',
				'dashStyle' => 'line',
				'color'  =>$it['color_code'],
				'step' => true,
			];
		}
		if($iterationsx) {
			//$data_points=$data_flags='';
			$data_points=$this->data_flags=[];
			$data_pointse = [];
			foreach ($iterationsx as $ar) {
				if($ar['end_date']) {
					$data_points[] =[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$scale[$ar['milestone_id']].' - '.$it['name'].' (Submitted - Milestone on ('.date('F d, Y',strtotime($ar['end_date'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
				}
            }

			$series[]=[
      			'name'	=>'End Dates',
				'data' =>$data_points,
				'marker'=>['enabled'=>true, 'radius'=>3],
				//fillColor' => '#123457',
				'dashStyle' => 'line',
				'color'  =>'#f00',
				'step' => true,
			];
			
		}
	}
 	$this->series =$series;
    }

	public function run(){
	
	return $this->render('@common/views/startenddategraph',
						[
						'series'=>$this->series,
						'elementid'	=> $this->elementid ? $this->elementid : md5(time()),
						'tooltip'=>$this->data_flags,
						]);
	}
}
?>