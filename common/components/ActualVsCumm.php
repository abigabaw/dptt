<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;
use backend\models\VProject;
use common\models\Functions;
class ActualVsCumm extends Widget{
	public $id;
	public $elementid;
	public $series;
	public $data_flags;
	public $y_scale;
	public $scale_indicator;
	public $scale_type;
	public $scale_milestone;
	public $data_indicator;
	public $data_milestone;

	public function init(){
	$series=[];
	$mstone =[];
	$mstone2 =[];
	$ct=$ctr=2;
	//Scale 1: Distinct Milestones Achieved
	$mstone2[0]= 1; 
	$mstone2[1]= 2;
	  //append siging and start date to serie
     $project = VProject::find()->where(['id'=>$this->id])->one();
	  //$data_pointsx = [];
	  $data_pointsx[]=[
	            'x'=>strtotime($project->award_date).'000',
	            'y'=>0,
	            'text' => '<b>Award Date</b>'
	        ];
	  $data_pointsx[]=[
	            'x'=>strtotime($project->signing_date).'000',
	            'y'=>1,
	            'text' => '<b>Signing Date</b>'
	        ];
	  $series[]=[
	        'data' =>$data_pointsx,
	        'marker'=>['enabled'=>true, 'radius'=>3],
	        //fillColor' => '#123457',
	        'dashStyle' => 'line',
	        'color'  =>'#19335D',
	        'step' => true,
	      ];

		// indicator scale
	$this->scale_indicator 			= Functions::getScaleIndicator($this->id,$this->y_scale);

	$this->scale_indicator 			= Functions::getScale($this->scale_indicator,$this->id);
	//print_r($this->scale_indicator);

		// milestone scale
	$this->scale_milestone 			= Functions::getScaleMilestone($this->id,$this->y_scale);
	$this->scale_milestone 			= Functions::getScaleM($this->scale_milestone,$this->id);

	//indicator data
	foreach ($this->scale_indicator  as $k=>$scale) 
	$this->data_indicator[] = [$scale['indicator_no'],$scale['cummulative_score']];     

	foreach ($this->scale_milestone  as $k=>$scale) 
	$this->data_milestone[] = [$scale['milestone_no'],$scale['cummulative_score']]; 

	$series[]=[
      			'name'	=>'Cummulative Milestone',
				'data' =>$this->data_milestone,
				'xAxis'=>'x2',
				'marker'=>['enabled'=>true, 'radius'=>3],
			];

	//Scale 1: Distinct Milestones Achieved
	$m=Yii::$app->db->createCommand("SELECT distinct milestone_id from v_milestone_submission where project_id=".$this->id." order by start_date asc")->queryAll();
	foreach($m as $ms)
		$mstone[$ms['milestone_id']]=++$ct;
	
	//Scale 2: Milestones of pipeline
	$pipeline=Yii::$app->session['pipeline'] ? Yii::$app->session['pipeline'] : VProject::find()->where(['id'=>$this->id])->one()->pipeline_id;
	//die(print_r($this->id));
	$st=PipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->All();
	//print_r($st);
	foreach($st as $stage) {
		//echo $stage;
		$ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
		foreach ($ms as $mst) {
			$mstone2[$mst['milestone']]=++$ctr;
		}
	}
	$scale=$mstone2;
	//print_r($scale);
	//exit;
	$iteration_types=IterationType::find()->All();
	foreach ($iteration_types as $it ) {
		$iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id=".$this->id." ORDER BY start_date")->queryAll();
		if($iterations) {
			//$data_points=$data_flags='';
			$data_points=$this->data_flags=[];
			$data_pointse = [];
			$data_pointsee=[];
			foreach ($iterations as $k=>$ar) {
				$data_points[]=[
						'x'=>strtotime($ar['start_date']).'000',
						'y'=>$scale[$ar['milestone_id']],
						'text' => '<b>'.$it['name'].' (Start):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
				];
             	  	$this->data_flags[]=[
						'x' =>strtotime($ar['start_date']).'000', 
						'text' => '<b>'.$it['name'].' (Start):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
				];
									if(isset($iterations[$k+1]))
					$eeed 	= strtotime($iterations[$k+1]['start_date']);
				if($ar['end_date']) {
					$eed 	= strtotime($ar['start_date']. ' + '.$ar['duration'].' '.$ar['unit'].'s');
					$ed 	= strtotime($ar['end_date']);
					if($ed>$eed){
					$data_pointse[] =[
							'x'=>$eed.'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (To Submit):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointse[] =[
							'x'=>$ed.'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointse[] =[
							'x'=>$ed.'000',
							'y'=>null,
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointsee[] =[
							'x'=>$ed.'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointsee[] =[
							'x'=>$eeed.'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointsee[] =[
							'x'=>$eeed.'000',
							'y'=>null,
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					}

					$data_points[]=[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_points[]=[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>null,
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					
					$data_pointsee[]=[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>$scale[$ar['milestone_id']],
							//'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointsee[]=[
							'x'=>$eeed.'000',
							'y'=>$scale[$ar['milestone_id']],
							//'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointsee[]=[
							'x'=>$eeed.'000',
							'y'=>null,
							//'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
	             	$this->data_flags[]=[
							'x' =>strtotime($ar['end_date']).'000', 
							'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
				}
			}
			$series[]=[
            		'type'		=> 'flags',
            		//'name'	=> $it['name'],
            		'color'		=> $it['color_code'],
            		//fillColor: \'rgba(255,255,255,0.8)\',
            		//'data'=>$this->data_flags,
            		'onSeries'=> 'dataseries',
 				//'onSeries' => 'revenue',
            		//'showInLegend'=> false					
			];
			$series[]=[
      			'name'	=>$it['name'],
				'data' =>$data_points,
				'xAxis'=>'x1',
				'marker'=>['enabled'=>true, 'radius'=>3],
				//fillColor' => '#123457',
				'dashStyle' => $it['dash_style'],
				'color'  =>$it['color_code'],
				'step' => true,
			];
			$series[]=[
      			'name'	=>$it['name'],
				'data' =>$data_pointse,
				 'xAxis'=>'x1',
				'marker'=>['enabled'=>true, 'radius'=>3],
				//fillColor' => '#123457',
				'dashStyle' => $it['dash_style'],
				'color'  =>'#F00',
				'step' => true,
			];
			$series[]=[
      			'name'	=>$it['name'],
				'data' =>$data_pointsee,
				 'xAxis'=>'x1',
				'marker'=>['enabled'=>true, 'radius'=>3],
				'fillColor' => '#123457',
				'dashStyle' => 'dash',
				'color'  =>'#F00',
				'step' => true,
			];
			
		}
	}
 	$this->series =$series;
    }

	public function run(){
	
	return $this->render('@common/views/actualvscumm',
						[
						'series'=>$this->series,
						'elementid'	=> $this->elementid,
						'tooltip'=>$this->data_flags,
						'scale'	=>$this->scale_milestone
						]);
	}
}
?>