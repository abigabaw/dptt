<?php
namespace backend\components;
use yii\base\Widget;
use yii\helpers\Html;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class DPTTInfo extends Widget{
	public $message;
	public function init(){
                // your logic here
		parent::init();
		if($this->message===null) {
			$this->message= 'Welcome Guest';
		}else{
			$this->message= 'Welcome '.$this->message;
		}
	}
	public function run(){
		//return Html::encode($this->message);
		
		  return $this->render('/site/myview',['message' => $this->message]);
	}
}
?>
