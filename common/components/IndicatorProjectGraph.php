<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\VIndicatorMilestoneSubmission;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class IndicatorProjectGraph extends Widget{
	public $series;
    public $indicator;
    public $elementid;
    public $scale;
    public $data_points;
	public function init(){
	$ct=0;
	$mstone =[];
	$pipeline  = isset(Yii::$app->session['pipeline']) && !empty(Yii::$app->session['pipeline']) ? Yii::$app->session['pipeline']
            : VProject::findOne(['id'=>$project])->pipeline_id;
			//Scale 1: For Projects
	$p=Yii::$app->db->createCommand("SELECT DISTINCT project_id from v_indicator_milestone_submission
									 where pipeline_id='".$pipeline."' and indicator_id = '".$this->indicator."'
            order by project_id")->queryAll();
	foreach($p as $ps)
	$mstone[$ps['project_id']]=++$ct;
	$scale=$mstone;

     $data_points 	= [];
    $submissions 	= Yii::$app->db->createCommand(Queries::getProjectIndicatorSubmissions($pipeline,$this->indicator))->queryAll();
	foreach ($submissions  as $data) {
			$data_points[] = [$scale[$data['project_id']],$data['value']];
	       	
    }
    $this->data_points = $data_points;
    $this->scale = $scale;
	}

    public function run (){
    	return $this->render('@common/views/indicatorprojectgraph',
						[ 'dataset' 		=> json_encode($this->data_points,JSON_NUMERIC_CHECK),
						  'scale' 			=> $this->scale,
						  'elementid'		=> $this->elementid
						]);
    }

}
?>