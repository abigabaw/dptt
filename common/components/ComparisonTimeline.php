<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;
use backend\models\VProject;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class ComparisonTimeline extends Widget{
	public $id;
  public $data_flags;
	public $elementid;
  public $series;

	public function init(){
	$qry_expected        = Yii::$app->db->createCommand(Queries::qry($this->id))->queryAll();
  $series=[];
  $mstone =[];
  $mstone2 =[];
  $ct=$ctr=2;
  //Scale 1: Distinct Milestones Achieved
  $mstone2[0]= 1; 
  $mstone2[1]= 2;
    //append siging and start date to serie
    $project = VProject::find()->where(['id'=>$this->id])->one();
    //$data_pointsx = [];
    $data_pointsx[]=[
              'x'=>strtotime($project->award_date).'000',
              'y'=>0,
              'text' => '<b>Award Date</b>'
          ];
    $data_pointsx[]=[
              'x'=>strtotime($project->signing_date).'000',
              'y'=>1,
              'text' => '<b>Signing Date</b>'
          ];
    $series[]=[
          'data' =>$data_pointsx,
          'marker'=>['enabled'=>true, 'radius'=>3],
          //fillColor' => '#123457',
          'dashStyle' => 'line',
          'color'  =>'#19335D',
          'step' => true,
        ];

  //Scale 1: Distinct Milestones Achieved
  $m=Yii::$app->db->createCommand("SELECT distinct milestone_id from v_milestone_submission where project_id=".$this->id." order by start_date asc")->queryAll();
  foreach($m as $ms)
    $mstone[$ms['milestone_id']]=++$ct;
  
  //Scale 2: Milestones of pipeline
  $pipeline=Yii::$app->session['pipeline'] ? Yii::$app->session['pipeline'] : VProject::find()->where(['id'=>$this->id])->one()->pipeline_id;
  //die(print_r($this->id));
  $st=PipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->All();
  //print_r($st);
  foreach($st as $stage) {
    //echo $stage;
    $ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
    foreach ($ms as $mst) {
      $mstone2[$mst['milestone']]=++$ctr;
    }
  }
  $scale=$mstone2;
    //print_r($qry_expected);
    //exit;

    $data_points=$data_flags=[];
    foreach($qry_expected as $key => $ar){
               //$data_points=$data_flags='';
        $data_points[]=[
            'x'=>strtotime($ar['start_date']).'000',
            'y'=>$scale[$ar['milestone_id']],
            'text'=>'<b>Start</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
        ];
        if(isset($qry_expected[$key+1]))
          $key = $qry_expected[$key+1];

        $data_points[]=[
            'x'=>strtotime($ar['start_date']. ' + '.$ar['duration'].' '.$ar['unit'].'s').'000',
            'y'=>$scale[$ar['milestone_id']],
            'text'=>'<b>Submit</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>
                    <b>Start </b><br/>'.str_replace('\'', '', $key['milestone']).''
        ];
        $data_points[]=[
            'x'=>strtotime($ar['start_date']. ' + '.$ar['duration'].' days').'000',
            'y'=>null,
        ];

  }
      // $series[]=[
      //           'type'    => 'flags',
      //           //'name'  => $it['name'],
      //           'color'   => '#FFF',
      //           //fillColor: \'rgba(255,255,255,0.8)\',
      //           'data'=>$data_flags,
      //   //'onSeries' => 'revenue',
      //           //'showInLegend'=> false          
      // ];
      $series[]=[
            'type'  => 'area', //line
            'id'    => 'expected',
            'fillColor' => '#2291FF',
            'name'  => 'Expected Timeline <em>Graph</em>',
            'step'  =>true,
            'data'  =>$data_points,
          ];
      
            // $this->dataset_expected .='{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            // $this->dataset_data_expected .='{ x: '.strtotime($ar['start_date']).'000, text:\'<b>Supposed to submit milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'\' },';


  $iteration_types=IterationType::find()->All();
  foreach ($iteration_types as $it ) {
    $iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id=".$this->id." ORDER BY start_date")->queryAll();
    if($iterations) {
      //$data_points=$data_flags='';
      $data_points=$this->data_flags=[];
      $data_pointse = [];
      foreach ($iterations as $ar) {
        $data_points[]=[
            'x'=>strtotime($ar['start_date']).'000',
            'y'=>$scale[$ar['milestone_id']],
            'text' => '<b>'.$it['name'].' (Start):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
        ];
                  $this->data_flags[]=[
            'x' =>strtotime($ar['start_date']).'000', 
            'text' => '<b>'.$it['name'].' (Start):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
        ];
        if($ar['end_date']) {
          $eed  = strtotime($ar['start_date']. ' + '.$ar['duration'].' '.$ar['unit'].'s');
          $ed   = strtotime($ar['end_date']);
          if($ed>$eed){
          $data_pointse[] =[
              'x'=>$eed.'000',
              'y'=>$scale[$ar['milestone_id']],
              'text' => '<b>'.$it['name'].' (To Submit):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
          ];
          $data_pointse[] =[
              'x'=>$ed.'000',
              'y'=>$scale[$ar['milestone_id']],
              'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
          ];
              $data_pointse[] =[
              'x'=>$ed.'000',
              'y'=>null,
              'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
          ];
          }

          $data_points[]=[
              'x'=>strtotime($ar['end_date']).'000',
              'y'=>$scale[$ar['milestone_id']],
              'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
          ];
          
          $data_points[]=[
              'x'=>strtotime($ar['end_date']).'000',
              'y'=>null,
              //'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
          ];
                $this->data_flags[]=[
              'x' =>strtotime($ar['end_date']).'000', 
              'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
          ];
        }
      }
      $series[]=[
                'type'    => 'flags',
                //'name'  => $it['name'],
                'color'   => $it['color_code'],
                //fillColor: \'rgba(255,255,255,0.8)\',
                //'data'=>$this->data_flags,
                'onSeries'=> 'dataseries',
        //'onSeries' => 'revenue',
                //'showInLegend'=> false          
      ];
      $series[]=[
            'name'  =>$it['name'],
        'data' =>$data_points,
        'marker'=>['enabled'=>true, 'radius'=>3],
        //fillColor' => '#123457',
        'dashStyle' => $it['dash_style'],
        'color'  =>$it['color_code'],
        'step' => true,
      ];
      $series[]=[
            'name'  =>$it['name'],
        'data' =>$data_pointse,
        'marker'=>['enabled'=>true, 'radius'=>3],
        //fillColor' => '#123457',
        'dashStyle' => $it['dash_style'],
        'color'  =>'#F00',
        'step' => true,
      ];
      
    }
  }
 $this->series =$series;
    }

  public function run(){
	return $this->render('@common/views/comparisontimeline',
						[
            'series'=>$this->series,
						'elementid' => $this->elementid ? $this->elementid : md5(time()),
						]);
	}
}
?>