<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\VMilestoneSubmission;
use common\models\Functions;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class ProjectReport extends Widget{
	public $project;
	public $report;
	public $data;
	public function init(){

	//$report = VMilestoneSubmission::find(['project_id'=>$this->project])->all();
	$report = Yii::$app->db->createCommand("SELECT * from v_milestone_submission where project_id=".$this->project."")->queryAll();
  
	foreach ($report as $pr) {
		$expected_finish_date = date('Y-m-d h:i:s', strtotime($pr['start_date']. ' + '.$pr['duration'].' '.$pr['unit'].'s'));
		$delay = $this->getDateDifference($expected_finish_date,$pr['end_date'],$pr['start_date']);
		$this->data[] = [
				'proxy'					=>$this->getProxy($pr),
				'milestone'				=>$pr['milestone'],
				'date_achieved'			=>date('F d, Y',strtotime($pr['end_date'])),
				'start_date'			=>date('F d, Y',strtotime($pr['start_date'])),
				'expected_finish_date'	=>date('F d, Y',strtotime($expected_finish_date)),
				'otif'					=>$this->getOTIF($pr['grade'],Functions::getWeight($pr['project_id'],$pr['milestone_id'])),
				'delay'					=>$delay['delay'],
				'percentage_delay'		=>$delay['percentage_delay']
				];
	}
	//var_dump($this->data);
	}
	private function getProxy($pr) {
		return Yii::$app->db->createCommand("SELECT * from v_indicator_milestone_submission where project_id='".$pr['project_id']."' and stage_id='".$pr['stage_id']."' and milestone_id='".$pr['milestone_id']."' and proxy='Y' ")->queryOne();
		//print_r($p);exit;
		//die( "SELECT * from v_indicator_milestone_submission where project_id='".$pr['project_id']."' and stage_id='".$pr['stage_id']."' and milestone_id='".$pr['milestone_id']."' and proxy='Y' ");
	}
	private function getDateDifference($d1,$d2,$d3){
		if(!$d2)
			return 'NA';
		//get date difference
		$expected_finish_date 			= new \DateTime($d1);
		$finish_date 					= new \DateTime($d2);
		$start_date 					= new \DateTime($d3);

		if($expected_finish_date>$finish_date)
			return 0;
		$delay = $expected_finish_date->diff($finish_date)->days;
		$delay = $delay ? $delay.' days' : 'NA';
		$a = (int)$start_date->diff($finish_date)->format('%a');;
		$b = (int)$start_date->diff($expected_finish_date)->format('%a');
		//echo $a.' '.$b.' delay '.$delay.''.'<br/>';
		if($b)
		$percentage_delay = ($delay/$b)*100;
		else
		$percentage_delay = 0;	
		return ['delay'=>$delay,'percentage_delay'=>round($percentage_delay,2).'%'];
	}

	private function getOTIF($grade,$weight){
		if(!$weight)
			return 'NA';
		return ($grade/$weight)*100;
	}

	public function run(){
		return $this->render('@common/views/projectreport',['report' =>$this->data]);
	}
}
?>