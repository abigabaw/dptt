<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use common\models\Functions;
use backend\models\Indicator;
use backend\models\Milestone;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class CummulativeScoreSingleMulti extends Widget{
	public $id;
	public $dataset_cummulative;
	public $tooltip;
	public $scale_milestone;
	public $scale_indicator;
	public $y_scale;
	public $scale_type;
	public $elementid;
	public $label;
	public $data_indicator;
	public $data_milestone;
	public $view;
	public function init(){
	
	// indicator scale
	$this->scale_indicator 			= Functions::getScaleIndicator($this->id,$this->y_scale);
	$this->scale_indicator 			= Functions::getScale($this->scale_indicator,$this->id);
	
	// milestone scale
	$this->scale_milestone 			= Functions::getScaleMilestone($this->id,$this->y_scale);
	$this->scale_milestone 			= Functions::getScaleM($this->scale_milestone,$this->id);


	//indicator data
	foreach ($this->scale_indicator  as $k=>$scale) 
	$this->data_indicator[] = [$scale['indicator_no'],$scale['cummulative_score']];     

	foreach ($this->scale_milestone  as $k=>$scale) 
	$this->data_milestone[] = [$scale['milestone_no'],$scale['cummulative_score']]; 
	
}
	public function run(){
	return $this->render('@common/views/cummulativemulti',
						[
						 'elementid'					=> $this->elementid,
						 'label'						=> $this->label,
						 'data_indicator'				=> $this->data_indicator,
						 'data_milestone'				=> $this->data_milestone,
						]);
	}
}
?>