<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use common\models\Functions;
use backend\models\Indicator;
use backend\models\Milestone;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class CummulativeScoreSingle extends Widget{
	public $id;
	public $dataset_cummulative;
	public $tooltip;
	public $scale;
	public $y_scale;
	public $scale_type;
	public $elementid;
	public $label;
	public $data;
	public $view;
	public function init(){
	//get score
	if($this->scale_type=='indicator'){
		$this->scale 				= Functions::getScaleIndicator($this->id,$this->y_scale);
		$this->scale 				= Functions::getScale($this->scale,$this->id);
		$this->label 				= 'Indicator';
		$index 						= 'indicator_no';
		$label_id					= 'indicator_id';
		$this->view 				= 'css_indicator';
	}else{
		$this->scale 				= Functions::getScaleMilestone($this->id,$this->y_scale);
		$this->scale 				= Functions::getScaleM($this->scale,$this->id);
		$this->label 				= 'Milestone';
		$index 						= 'milestone_no';
		$label_id					= 'milestone_id';
		$this->view 				= 'css_milestone';
	}

	foreach ($this->scale  as $k=>$scale) {
			$this->data[] = [$scale[$index],$scale['cummulative_score']];
			$this->tooltip =  'tooltip: {
                                    headerFormat: \'<b>{series.name}</b><br/>\',
                                    pointFormat: \'<b>'.$this->label.':</b>'.$this->getLabelName($scale[$label_id],$index ).' <br/> <b>Cummulative Score:</b> '.$scale['cummulative_score'].' <br/><b>Actual Score:</b>'.$scale['score'].'\'
                                  },';
                 
        }
         $this->tooltip 			   = rtrim($this->tooltip);
	}

	private function getLabelName($id,$type){
		if($type=='milestone_no')
			return Milestone::findOne($id)->milestone;
		else
			return Indicator::findOne($id)->name;
	}
	public function run(){
	return $this->render('@common/views/'.$this->view,
						['dataset' 						=> json_encode($this->data,JSON_NUMERIC_CHECK),
						 'tooltip' 						=> $this->tooltip,
						 'elementid'					=> $this->elementid,
						 'label'						=> $this->label,
						 'scale'						=>$this->scale
						]);
	}
}
?>