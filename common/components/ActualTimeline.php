<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
use backend\models\PipelineStage;
use backend\models\IterationType;
use backend\models\VProject;
class ActualTimeline extends Widget{
	public $id;
	public $elementid;
	public $series;
	public $data_flags;

	public function init(){
	$series=[];
	$mstone =[];
	$mstone2 =[];
	$ct=$ctr=0;

	//Scale 1: Distinct Milestones Achieved
	//$mstone2[0]= 1; 
	//$mstone2[1]= 2;
	$m=Yii::$app->db->createCommand("SELECT distinct milestone_id from v_milestone_submission where project_id=".$this->id." order by start_date asc")->queryAll();
	foreach($m as $ms)
		$mstone[$ms['milestone_id']]=++$ct;
	
	//Scale 2: Milestones of pipeline
	$pipeline=Yii::$app->session['pipeline'] ? Yii::$app->session['pipeline'] : VProject::find()->where(['id'=>$this->id])->one()->pipeline_id;
	//die(print_r($this->id));
	$st=PipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->All();
	//print_r($st);
	foreach($st as $stage) {
		//echo $stage;
		$ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
		foreach ($ms as $mst) {
			$mstone2[$mst['milestone']]=++$ctr;
		}
	}
	$scale=$mstone2;
	//echo "<pre>";print_r($scale);
	//exit;
	$iteration_types=IterationType::find()->all();
	$project = VProject::find()->where(['id'=>$this->id])->one();
	//$data_pointsx = [];
	$data_pointsx[]=[
						'x'=>strtotime($project->award_date).'000',
						'y'=>0,
						'text' => '<b>Award Date</b>'
				];
	$data_pointsx[]=[
						'x'=>strtotime($project->signing_date).'000',
						'y'=>1,
						'text' => '<b>Signing Date</b>'
				];
	$series[]=[
				'data' =>$data_pointsx,
				'marker'=>['enabled'=>true, 'radius'=>10],
				//'fillColor' => '#ff3457',
				'dashStyle' => 'line',
				//'color'  =>'#19335D',
				'step' => true,
			];
	foreach ($iteration_types as $it ) {
		//$iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id=".$this->id." ORDER BY start_date")->queryAll();
		$iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id in (".$this->id.") ORDER BY start_date")->queryAll();
		//$iterations=Yii::$app->db->createCommand("SELECT * from v_milestone_submission where iteration_type_name ='".$it['name']."' and project_id in (60,61,62,63) ORDER BY start_date")->queryAll();
		if($iterations) {
			//$data_points=$data_flags='';
			$data_points=$this->data_flags=[];
			$data_pointse = [];
			foreach ($iterations as $ar) {
				$data_points[]=[
						'x'=>strtotime($ar['start_date']).'000',
						'y'=>$scale[$ar['milestone_id']],
						'text' => '<b>'.$it['name'].' (Start - Milestone on ('.date('F d, Y',strtotime($ar['start_date'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
				];
             	  	$this->data_flags[]=[
						'x' =>strtotime($ar['start_date']).'000', 
						'text' => '<b>'.$it['name'].' (Start - Milestone on ('.date('F d, Y',strtotime($ar['start_date'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
				];
				if($ar['end_date']) {
					$eed 	= strtotime($ar['start_date']. ' + '.$ar['duration'].' '.$ar['unit'].'s');
					$ed 	= strtotime($ar['end_date']);
					$ddd=($ed-$eed)/86400;
					if($ed>$eed){
						$data_pointsee[] =[
								'x'=>$eed.'000',
								'y'=>$scale[$ar['milestone_id']],
								'title'=>$ddd,
								'text' => '<b>'.$it['name'].' (To Submit - Milestone on ('.date('F d, Y',$eed).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
						];
						$data_pointsee[] =[
								'x'=>$eed.'900',
								'y'=>null,
								'title'=>$ddd,
								'text' => '<b>'.$it['name'].' (To Submit - Milestone on ('.date('F d, Y',$eed).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
						];


					$data_pointse[] =[
							'x'=>$eed.'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (To Submit - Milestone on ('.date('F d, Y',$eed).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					$data_pointse[] =[
							'x'=>$ed.'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (Submitted - Milestone on ('.date('F d, Y',$ed).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
							$data_pointse[] =[
							'x'=>$ed.'000',
							'y'=>null,
							//'text' => '<b>'.$it['name'].' (Submitted - Milestone ('.date('F d, Y',strtotime($qry['created_at'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					}

					$data_points[]=[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>$scale[$ar['milestone_id']],
							'text' => '<b>'.$it['name'].' (Submitted - Milestone on ('.date('F d, Y',strtotime($ar['end_date'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
					
					$data_points[]=[
							'x'=>strtotime($ar['end_date']).'000',
							'y'=>null,
							//'text' => '<b>'.$it['name'].' (Submitted):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
	             	$this->data_flags[]=[
							'x' =>strtotime($ar['end_date']).'000', 
							'text' => '<b>'.$it['name'].' (Submitted - Milestone on ('.date('F d, Y',strtotime($ar['end_date'])).')):</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>'
					];
				}
			}
			/*$series[]=[
            		'type'		=> 'flags',
            		//'name'	=> $it['name'],
            		'color'		=> $it['color_code'],
            		//fillColor: \'rgba(255,255,255,0.8)\',
            		//'data'=>$this->data_flags,
            		'onSeries'=> 'dataseries',
 				//'onSeries' => 'revenue',
            		//'showInLegend'=> false					
			];*/
			$series[]=[
				//'type'=>'flags',
				'title'=>'D',
				'fillColor'=>'#dfdad0',
				'lineColor'=>'yellow',
				'shape'=>'squarepin',
      			'name'	=>$it['name'],
				'data' =>$data_pointsee,
				'marker'=>['enabled'=>true, 'radius'=>10,'fillColor'=>'brown'],
				//fillColor' => '#123457',
				'dashStyle' => $it['dash_style'],
				'color'  =>$it['color_code'],
				'step' => true,
			];

			$series[]=[
      			'name'	=>$it['name'],
				'data' =>$data_points,
				'marker'=>['enabled'=>true, 'radius'=>4,'fillColor'=>'green','symbol'=>'diamond'],
				//fillColor' => '#123457',
				'dashStyle' => $it['dash_style'],
				'color'  =>$it['color_code'],
				'step' => true,
			];
			$series[]=[
      			'name'	=>$it['name'],
				'data' =>$data_pointse,
				'marker'=>['enabled'=>true, 'radius'=>4,'fillColor'=>'red','symbol'=>'diamond'],
				//fillColor' => '#123457',
				'dashStyle' => $it['dash_style'],
				'color'  =>'#F00',
				'step' => true,
			];
			
		}
	}
 	$this->series =$series;
    }

	public function run(){
	
	return $this->render('/widget/actualtimeline',
						[
						'series'=>$this->series,
						'elementid'	=> $this->elementid ? $this->elementid : md5(time()),
						'tooltip'=>$this->data_flags
						]);
	}
}
?>