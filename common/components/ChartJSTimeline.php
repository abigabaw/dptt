<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class ChartJSTimeline extends Widget{
	public $id;
	public $labels;
	public $data;
	public $milestones;
	public $labels2;
	public $data2;
	public $milestones2;
	public function init(){
	$qry                  = Yii::$app->db->createCommand(Queries::qry($this->id))->queryAll();
	$qry2                 = Yii::$app->db->createCommand(Queries::qry2($this->id))->queryAll();
	foreach ($qry as $qry) {
            $this->labels[]=date('Y-m-d', strtotime($qry['start_date']));

            $this->data[] =$qry['num'];
            $this->milestones[] = array(
                                 'id'=>$qry['num'],
                                 'milestone'=>$qry['milestone']
                                 );
        }
    foreach ($qry2 as $qry) {
            $this->labels2[]=date('Y-m-d', strtotime($qry['start_date']));

            $this->data2[] =$qry['num'];
            $this->milestones2[] = array(
                                 'id'=>$qry['num'],
                                 'milestone'=>$qry['milestone']
                                 );
        }
    }


	public function run(){
	return $this->render('/widget/chartjstimeline',
						[
						'labels' 			=> json_encode($this->labels),
						'data' 				=> json_encode($this->data),
						'milestones' 		=> json_encode($this->milestones),
						'labels2' 			=> json_encode($this->labels2),
						'data2' 			=> json_encode($this->data2),
						'milestones2' 		=> json_encode($this->milestones2),
						]);
	}
}
?>