<?php
namespace common\components;
use Yii;
use yii\base\Widget;
use backend\models\Queries;
/*
How to run the widget:

use backend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class CummulativeScore2 extends Widget{
	public $dataProvider;
	public $dataset_cummulative;
	public function init(){
  $pipeline  = isset(Yii::$app->session['pipeline']) && !empty(Yii::$app->session['pipeline']) ? Yii::$app->session['pipeline']
            : VProject::findOne(['id'=>$project])->pipeline_id;

	$qry_cummulative = Yii::$app->db->createCommand("SELECT DISTINCT project_id,project_name FROM `v_indicator_milestone_submission` WHERE pipeline_id=30")->queryAll();
	//echo Queries::qryCummulativeAll($pipeline);
	foreach ($qry_cummulative as $qry) {
            if(isset($qry['project_id'])){
            $this->dataset_cummulative .='{
                        name: \''.$qry['project_name'].'\',
                        data: ['.$this->getCummulativeScores($qry['project_id'],'data').'],
                        tooltip:{
                                    headerFormat: \'<b>'.$qry['project_name'].'</b><br/>\',
                                  }
                    },';
            }
        }
	}

	private function getCummulativeScores($project_id){
        $dataset_cummulative ='';
        $sql ='SELECT * FROM `v_milestone_submission`
                 WHERE project_id='.$project_id.' 
                ORDER BY id ASC';

    	$qry_cummulative = Yii::$app->db->createCommand($sql)->queryAll();
        $score = 0;
        $c=0;
        foreach ($qry_cummulative as $k=>$qry) {
            $score +=$qry['grade']; 
            $c++; 
            $dataset_cummulative .= '{x:'.$c.',
                                      y:'.$score.',
                                      milestone:\''.$qry['milestone'].'\', 
                                      project:\''.$qry['project'].'\', 
                                      score:'.$qry['grade'].', 
                                      },';
            //$dataset_data2 .='{ x: '.$qry2['created_at'].'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $qry2['milestone']).'<br/>\' },';
            
        }
        
        $dataset_cummulative = rtrim($dataset_cummulative);
        return $dataset_cummulative;
    }
	public function run(){
	return $this->render('/widget/cummulativescore',
						['dataset_cummulative' 			=> $this->dataset_cummulative,
						]);
	}
}
?>