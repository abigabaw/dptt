<div class='graphs' id="<?=$elementid?>"></div>
<div id="ggg"></div>
<?= $this->registerJs(
      "$(function() {
      	var milestone_data = ".json_encode($data_indicator).";
      	var indicator_data	= ".json_encode($data_milestone).";

        // set the allowed units for data grouping

        // create the chart
        chart = new Highcharts.StockChart({
            chart: {
                renderTo: ".$elementid.",
                alignTicks: true,
                type: 'spline',
            	inverted: false
            },

            rangeSelector: {
                selected: 1
            },

            title: {
                text: 'Cummulative Scores'
            },
                    xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: 'Milestones'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },  credits: {
      enabled: false
  },

            yAxis: [{
                title: {
                    text: 'Milestones'
                },
                lineWidth: 2,
                height: 200
            }, {
                title: {
                    text: 'Indicators'
                },
                opposite: true,
                lineWidth: 2,
                height: 200
            }, {
                title: {
                    text: 'Difference'
                },
                top: 300,
                height: 100,
                offset: 0,
                lineWidth: 2
            }],

            
            navigator: {
                enabled: true
            },
            
            series: [{
                type: 'line',
                name: 'MILESTONES',
                data: milestone_data,
                yAxis: 1,
            }, {
                type: 'area',
                name: 'INDICATORS',
                data: indicator_data,
                yAxis: 2,
            }]
        });
});")?>

 <?= $this->registerJs(
      ' 
    $(function () {
    var milestone_data = '.json_encode($data_indicator).';
    var indicator_data	= '.json_encode($data_milestone).';
    $(\'#ggg\').highcharts({
        chart: {
            type: \'spline\',
            alignTicks: true,
            inverted: false
        },
        title: {
            text: \'Cummulative  Scores\'
        },
        xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: \'Milestones\'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
                   yAxis: [{
                title: {
                    text: \'Milestones\'
                },
                lineWidth: 2,
                height: 200
            }, {
                title: {
                    text: \'Indicators\'
                },
                opposite: true,
                lineWidth: 2,
                height: 200
            }, {
                title: {
                    text: \'Difference\'
                },
                top: 300,
                height: 200,
                offset: 0,
                lineWidth: 2
            }],credits: {
      enabled: false
  },
        legend: {
            enabled: false
        },
        plotOptions: {
            spline: {
                marker: {
                    enable: true
                }
            }
        },
        series: [{
                name: \'MILESTONES\',
                data: milestone_data,
                yAxis: 0,
            }, {
                name: \'INDICATORS\',
                data: indicator_data,
                yAxis: 1,
            }]
    });

});')?>