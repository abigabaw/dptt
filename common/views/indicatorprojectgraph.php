<div class='graphs' id="<?=$elementid?>"></div>
<!-- Page specific script -->
    <?= $this->registerJs(
      ' 
      $(function () {
    $(\'#'.$elementid.'\').highcharts({
        chart: {
            type: \'spline\',
            inverted: false
        },
        title: {
            text: \'Project Indicator Scores\'
        },
        xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: \'Indicators\'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: \'Cummulative Scores\'
            },
            lineWidth: 2
        },credits: {
      enabled: false
  },
        legend: {
            enabled: false
        },
        tooltip: {
            formatter: function() {
                return \'<b>Indicator:</b> \'+findIndicator(this.x,"name")+\'<br/><b>Cummulative Score</b>: \'+findIndicator(this.x,\'cummulative_score\')+\'<br/><b>Actual Score</b>: \'+findIndicator(this.x,\'score\')+\'\'
            }
        },
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },
        series: [{
            name: \'Cummulative Indicator Score\',
            data: '.$dataset.'
        }]
    });

    function findIndicator(x,data){
        var scale = '.json_encode($scale).';
        if(data==\'name\')
        return scale[x][\'name\'];
        else if(data==\'score\'){
            if(scale[x][\'score\'])
        return scale[x][\'score\']
        else return \'Not yet graded\';
        }else if(data==\'cummulative_score\')
        return scale[x][\'cummulative_score\']
        else 
            return null;
    }
});')?>