    <?php
    	
//	print_r($report[0]['proxy']);

    ?>
	      <style>
			</style>
			 <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Milestone</th>
                        <th>Date Started</th>
			<th>Date Planned End</th>
			<th>Date Achieved</th>
                        <th>Delay</th>
                        <th>Percentage Delay</th>
				<?php if($report[0]['proxy']) {?>
			<th>Proxy Target</th>	
			<th>Proxy Value</th>	
					
					<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      foreach ($report as $data) {
			// $class=$data['delay'] ? 'danger' : '';
			  $class=($data['delay'] || (floatval($data['proxy']['target_value']) > floatval($data['proxy']['value']))) ? 'danger' : '';
                        echo '<tr class='.$class.'>
                              <td>'.$data['milestone'].'</td>
			     <td>'.$data['start_date'].'</td>
				 <td>'.$data['expected_finish_date'].'</td>
                              <td>'.$data['date_achieved'].'</td>
                              <td>'.$data['delay'].'</td>
                              <td>'.$data['percentage_delay'].'</td>';
										
			if($report[0]['proxy']) {
				echo "<td>".$data['proxy']['target_value']."</td><td>".$data['proxy']['value']."</td>";
			}
                           echo ' </tr>';
                      }
                     ?>

                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Milestone</th>
                        <th>Date Started</th>
			<th>Date Planned End</th>			
                        <th>Date Achieved</th>
                        <th>Delay</th>
                        <th>Percentage Delay</th>
				<?php if($report[0]['proxy']) {?>
			<th>Proxy Target</th>	
			<th>Proxy Value</th>	
					
					<?php } ?>
			
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
  <?= $this->registerJs('
      $(function () {
        $("#example1").DataTable();
        $(\'#example2\').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
')?>