  <?php
   $series=json_encode($series,JSON_NUMERIC_CHECK); 
  ?>
     <div class='graphs' id="<?=$elementid?>"></div>

     <?= $this->registerJs(
      ' 
      $(function () {
  	  $(\'#'.$elementid.'\').highcharts(\'StockChart\', {

            rangeSelector: {
                selected: 1
            },
            colors: [\'#562F1E\', \'#AF7F24\', \'#263249\', \'#5F7F90\', \'#D9CDB6\'],

            title: {
                text: \'Actual Timeline Vs Cummulative Milestone Score (Date)\'
            },
        		yAxis: [{
                title: {
                    text: \'Milestone Submission\'
                },
                lineWidth: 2,
                height: 200
            }, {
                title: {
                    text: \'Cummulative Miletone Score\'
                },
                opposite: true,
                lineWidth: 2,
                height: 200
            }],
          tooltip: {
    formatter: function() {
        var s = "";
        console.log(this.points[0].point.text); // ["name1", "name2"] 
        $.each(this.points, function(i, point) {
            s += point.point.text;
        });
     	if(this.points[2].point)
        return this.points[0].point.text+\'<br/>\'+this.points[2].point.text;
    	else
    	return this.points[0].point.text; 
    },
    shared: true
}, credits: {
      enabled: false
  },
plotOptions: {
                series: {
                    events: {
                        mouseOver: function() {                      
                            this.graph.attr(\'stroke\', \'#0000FF\');
                        },
                        mouseOut: function() {
                            this.graph.attr(\'stroke\', this.points[0].color);
                        }
                    }
                }
            },   

            series: '.$series.'
        });

     function getInfo(x){
      var scale = '.json_encode($tooltip).';
      return 

     }

     });')?>