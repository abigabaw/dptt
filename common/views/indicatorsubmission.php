  <?php
   $series=json_encode($series,JSON_NUMERIC_CHECK); 
   $scales=json_encode($scales,JSON_NUMERIC_CHECK);
  ?>
     <div class="graphs <?php if (Yii::$app->request->isAjax) echo "xlarge"?>" id="<?=$elementid?>"></div>

     <?= $this->registerJs(
      ' 
      $(function () {
  	  $(\'#'.$elementid.'\').highcharts(\'StockChart\', {
        
		  

            rangeSelector: {
                selected: 1
            },
            colors: [\'red\', \'blue\', \'green\', \'black\', \'#D9CDB6\'],

            title: {
                text: \'Cumulative Indicator Values for '.$project.'\'
            },yAxis:'.$scales.',
          tooltip: {
    formatter: function() {
        var s = "";
        console.log(this.points[0].point.text); // ["name1", "name2"] 
        $.each(this.points, function(i, point) {
            s += point.point.text;
        });
        return s;
    },
    shared: true
}, credits: {
      enabled: false
  },
plotOptions: {
                series: {
                    events: {
                        mouseOver: function() {                      
                            this.graph.attr(\'stroke\', \'#0000FF\');
                        },
                        mouseOut: function() {
                            this.graph.attr(\'stroke\', this.points[0].color);
                        }
                    }
                }
            },   
            series: '.$series.'
        });

     });')?>