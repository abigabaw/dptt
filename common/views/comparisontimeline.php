  <?php
   $series=json_encode($series,JSON_NUMERIC_CHECK); 
  ?>
     <div class='graphs  <?php if (Yii::$app->request->isAjax) echo "xlarge"?>' id="<?=$elementid?>"></div>

     <?= $this->registerJs(
      ' 
      $(function () {
            Highcharts.setOptions({
        global: {
            timezoneOffset: -3 * 60
        }
    });
      $(\'#'.$elementid.'\').highcharts(\'StockChart\', {

            rangeSelector: {
                selected: 1
            },
            colors: [\'#562F1E\', \'#AF7F24\', \'#263249\', \'#5F7F90\', \'#D9CDB6\'],

            title: {
                text: \'Comparision of Timelines\'
            },
          tooltip: {
    formatter: function() {
        var s = "";
        console.log(this.points[0].point.text); // ["name1", "name2"] 
        $.each(this.points, function(i, point) {
            s += point.point.text;
        });
        return "<div style=\'width: 400px!important; white-space:normal;\'>"+s+"</div>";
    },
    shared: true
},
            plotOptions: {
                series: {
                    events: {
                        mouseOver: function() {                      
                            this.graph.attr(\'stroke\', \'#0000FF\');
                        },
                        mouseOut: function() {
                            this.graph.attr(\'stroke\', this.points[0].color);
                        }
                    },
                }
            },credits: {
      enabled: false
  },

            series: '.$series.'
        });

     });')?>