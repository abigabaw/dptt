 <div class='graphs' id="<?=$elementid?>"></div>
<!-- Page specific script -->
    <?= $this->registerJs(
      ' 
      $(function () {
    $(\'#'.$elementid.'\').highcharts({
        chart: {
            type: \'spline\',
            inverted: false
        },
        title: {
            text: \'Cummulative '.$label.' Scores\'
        },
        xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: \'Milestones\'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: \'Cummulative Scores\'
            },
            lineWidth: 2
        },
        legend: {
            enabled: false
        },
        tooltip: {
            formatter: function() {
                return \'<b>Miletsone:</b> \'+findMilestone(this.x,"name")+\'<br/><b>Cummulative Score</b>: \'+findMilestone(this.x,\'cummulative_score\')+\'<br/><b>Actual Score</b>: \'+findMilestone(this.x,\'score\')+\'\'
            }
        },
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },credits: {
      enabled: false
  },
        series: [{
            name: \'Cummulative Milestone Score\',
            data: '.$dataset.'
        }]
    });

    function findMilestone(x,data){
        var scale = '.json_encode($scale).';
        if(data==\'name\')
        return scale[x][\'milestone\'];
        else if(data==\'score\'){
            if(scale[x][\'score\'])
        return scale[x][\'score\']
        else return \'Not yet graded\';
        }else if(data==\'cummulative_score\')
        return scale[x][\'cummulative_score\']
        else 
            return null;
    }
});')?>