<?php

namespace common\services;

class Time
{
    /**
     * @param string $time any valid representation of time
     */
    private static $tokens = [
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    ];

    public static function momentAgo($time){
        if(!is_numeric($time))
            $time = strtotime($time);

        $duration = time() - $time;
        $duration = ($duration < 1) ? 1 : $duration;

        foreach (self::$tokens as $period => $unit){
            if($duration < $period) continue;
            $no_units = floor($duration/$period);
            return $no_units . ' ' . $unit . ( $no_units > 1 ? 's' : '') . ' ago';
        }

    }
}