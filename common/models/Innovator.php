<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "innovator".
 *
 * @property integer $id
 * @property integer $instituition
 * @property string $email
 * @property string $names
 * @property string $description
 * @property string $gender
 * @property integer $country
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property LoginInnovator[] $loginInnovators
 */
class Innovator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'innovator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instituition', 'status', 'created_at', 'updated_at'], 'required'],
            [['instituition', 'country', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description', 'gender'], 'string'],
            [['email', 'names'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instituition' => 'Instituition',
            'email' => 'Email',
            'names' => 'Names',
            'description' => 'Description',
            'gender' => 'Gender',
            'country' => 'Country',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoginInnovators()
    {
        return $this->hasMany(LoginInnovator::className(), ['innovator' => 'id']);
    }
}
