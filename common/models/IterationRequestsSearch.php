<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VIterationRequests;

/**
 * IterationRequestsSearch represents the model behind the search form about `common\models\IterationRequests`.
 */
class IterationRequestsSearch extends IterationRequests
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'timeline', 'status', 'supervisor_id', 'created_by', 'updated_by'], 'integer'],
            [['reason', 'supervisor_comment', 'approval_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VIterationRequests::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'timeline' => $this->timeline,
            'status' => $this->status,
            'supervisor_id' => $this->supervisor_id,
            'approval_date' => $this->approval_date,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'supervisor_comment', $this->supervisor_comment]);

        return $dataProvider;
    }

    public function searchFront($params,$projects)
    {
        $query = VIterationRequests::find()->where('project_id in (\''.$projects.'\')');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'timeline' => $this->timeline,
            'status' => $this->status,
            'supervisor_id' => $this->supervisor_id,
            'approval_date' => $this->approval_date,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'reason', $this->reason]);

        return $dataProvider;
    }
}
