<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "institution".
 *
 * @property integer $id
 * @property string $email
 * @property string $names
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Grading[] $gradings
 * @property Grading[] $gradings0
 * @property LoginInstitution[] $loginInstitutions
 * @property Project[] $projects
 */
class Institution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['email', 'names'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'names' => 'Names',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings()
    {
        return $this->hasMany(Grading::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings0()
    {
        return $this->hasMany(Grading::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoginInstitutions()
    {
        return $this->hasMany(LoginInstitution::className(), ['institution' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['institution' => 'id']);
    }
}
