<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "v_iteration_requests".
 *
 * @property integer $id
 * @property integer $timeline
 * @property integer $project_id
 * @property string $project_name
 * @property integer $status
 * @property integer $supervisor_id
 * @property string $supervisor_name
 * @property string $approval_date
 * @property string $created_at
 */
class VIterationRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_iteration_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'timeline', 'project_id', 'status', 'supervisor_id'], 'integer'],
            [['timeline', 'supervisor_id', 'created_at'], 'required'],
            [['approval_date', 'created_at'], 'safe'],
            [['project_name', 'supervisor_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timeline' => 'Timeline',
            'project_id' => 'Project ID',
            'project_name' => 'Project Name',
            'status' => 'Status',
            'supervisor_id' => 'Supervisor ID',
            'supervisor_name' => 'Supervisor Name',
            'approval_date' => 'Approval Date',
            'created_at' => 'Created At',
        ];
    }
}
