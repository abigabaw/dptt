<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $currency
 * @property string $fips_code
 * @property string $iso_numeric
 * @property string $capital
 * @property string $continent_name
 * @property string $continent
 * @property string $languages
 * @property string $iso_alpha3
 *
 * @property Project[] $projects
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'fips_code', 'continent'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 45],
            [['currency', 'iso_alpha3'], 'string', 'max' => 3],
            [['iso_numeric'], 'string', 'max' => 4],
            [['capital', 'languages'], 'string', 'max' => 30],
            [['continent_name'], 'string', 'max' => 15],
            [['name'], 'unique'],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'currency' => 'Currency',
            'fips_code' => 'Fips Code',
            'iso_numeric' => 'Iso Numeric',
            'capital' => 'Capital',
            'continent_name' => 'Continent Name',
            'continent' => 'Continent',
            'languages' => 'Languages',
            'iso_alpha3' => 'Iso Alpha3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['country' => 'id']);
    }
}
