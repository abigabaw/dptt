<?php

namespace common\models;

use Yii;
use frontend\models\Timeline;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "iteration_requests".
 *
 * @property integer $id
 * @property integer $iteration
 * @property string $reason
 * @property integer $status
 * @property string $supervisor_comment
 * @property integer $supervisor_id
 * @property string $approval_date
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Iteration $iteration0
 */
class IterationRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iteration_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timeline', 'reason'], 'required'],
            [['timeline', 'status', 'supervisor_id'], 'integer'],
            [['approval_date', 'created_at', 'updated_at'], 'safe'],
            [['reason', 'supervisor_comment'], 'string', 'max' => 255],
            [['timeline'], 'exist', 'skipOnError' => true, 'targetClass' => Timeline::className(), 'targetAttribute' => ['timeline' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
             public function behaviors()
             {
                 return [
                    [ 'class'=>TimestampBehavior::className(),
                    'value'=>new Expression('NOW()')
                ],
                     BlameableBehavior::className(),
                 ];
             }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timeline' => 'Timeline',
            'reason' => 'Reason',
            'status' => 'Status',
            'supervisor_comment' => 'Supervisor Comment',
            'supervisor_id' => 'Supervisor ID',
            'approval_date' => 'Approval Date',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeline0()
    {
        return $this->hasOne(Timeline::className(), ['id' => 'timeline']);
    }
}
