<?php

namespace common\models;

use Yii;
use backend\models\Milestone;
use backend\models\Indicator;
use backend\models\Project;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "project_indicatorweight".
 *
 * @property integer $id
 * @property integer $project
 * @property integer $indicator
 * @property integer $milestone
 * @property double $weight
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Milestone $milestone0
 * @property Project $project0
 * @property Indicator $indicator0
 */
class ProjectIndicatorweight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_indicatorweight';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	[['project', 'indicator', 'milestone'], 'required'],
	[['project', 'indicator', 'milestone', 'created_by', 'updated_by'], 'integer'],
	[['weight'], 'number'],
	[['created_at', 'updated_at'], 'safe'],
	[['milestone'], 'exist', 'skipOnError' => true, 'targetClass' => Milestone::className(), 'targetAttribute' => ['milestone' => 'id']],
	[['project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project' => 'id']],
	[['indicator'], 'exist', 'skipOnError' => true, 'targetClass' => Indicator::className(), 'targetAttribute' => ['indicator' => 'id']],
        ];
    }

        /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
            'value'=>new Expression('NOW()')
        ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project' => Yii::t('app', 'Project'),
            'indicator' => Yii::t('app', 'Indicator'),
            'milestone' => Yii::t('app', 'Milestone'),
            'weight' => Yii::t('app', 'Weight'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator0()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator']);
    }
}
