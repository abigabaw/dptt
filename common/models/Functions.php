<?php 
namespace common\models;
use Yii;
use common\models\ProjectIndicatorweight;
use backend\models\PipelineIndicatorweight;
use backend\models\IndicatorRequests;
use backend\models\Indicator;
use backend\models\Milestone;
use backend\models\Timeline;
use backend\models\MilestoneStage;
use backend\models\VMilestoneStage;
use backend\models\PipelineStage;
use backend\models\Queries;
use backend\models\VProject;
class Functions{

    public static function humanTiming ($time)
    {

       // $mtime=$time;
        $time = time() - $time + (3600*1); // to get the time since that moment
        //return $mtime.'   '.time().' '.$time;
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

    }

    public static function setIndicatorWeight1($project,$milestone,$milestone_weight,$indicators)
    {
        //select records from project_indicatorweight;
        $indicator_weights = ProjectIndicatorweight::find()->where(['milestone'=>$milestone,'project'=>$project])->all();
        
        if($indicator_weights){
            foreach ($indicator_weights as $k=>$indicator) {
                $indicators_w[$k]['weight']=[
                                    'indicator'=>$indicator->indicator,
                                    'weight'=>$indicator->weight,
                                ];
                $indicators_w[$k]=$indicator->weight;
                $indicators_l[$k]=$indicator->indicator;
            }
          $indicators_w = array_merge_recursive($indicators['weight'], $indicators_w);
          $indicators_l = array_merge_recursive($indicators['indicator'], $indicators_l);
          $indicators = ['indicator'=>$indicators_l,'weight'=>$indicators_w];

        }
        //return $indicators;
        $indicator_weights_sum = array_sum($indicators['weight']);
        //return $indicators['indicator'];
        $time = date('Y-m-d : h:i:s');
        foreach ($indicators['indicator'] as $k=>$indicator) {
                $record[]           = [
                                        'project'        =>$project,
                                        'milestone'      =>$milestone,
                                        'indicator'      =>$indicator,
                                        'weight'         =>round(($indicators['weight'][$k]/$indicator_weights_sum)*$milestone_weight,2),
                                        'created_at'     => $time,
                                        'updated_at'    =>$time,
                                        'updated_by'=>Yii::$app->user->identity->id,
                                        'created_by'=>  Yii::$app->user->identity->id
                                      ];

                if(count($record)>0){
                            //delete all existing records for this milestone
                            Yii::$app->db->createCommand('
                                    DELETE FROM project_indicatorweight 
                                    WHERE milestone = '.$milestone.' 
                                    AND project = '.$project.'
                                ')->execute();
                            //add new records
                            $columnNameArray=['project','milestone','indicator','weight','created_by','updated_by','created_at','updated_at'];
                            // below line insert all your record and return number of rows inserted
                            $insertCount = Yii::$app->db->createCommand()
                                           ->batchInsert(
                                                 'project_indicatorweight', $columnNameArray, $record
                                             )
                                           ->execute();
                }
        }
    }
    public static function setIndicatorWeight($project,$milestone,$milestone_weight,$indicators)
    {

        //select records from project_indicatorweight;
        $indicator_weights = ProjectIndicatorweight::find()->where(['milestone'=>$milestone,'project'=>$project])->all();

        if($indicator_weights){
            foreach ($indicator_weights as $k=>$indicator) {
                $indicators_w[$k]['weight']=[
                                    'indicator'=>$indicator->indicator,
                                    'weight'=>$indicator->weight,
                                    'target_value'=>$indicator->target_value
                                ];

                $indicators_w[$k]=$indicator->weight;
                $indicators_l[$k]=$indicator->indicator;
                $indicators_t[$k]=$indicator->target_value;
            }
          $indicators_w = array_merge_recursive($indicators['weight'], $indicators_w);
          $indicators_l = array_merge_recursive($indicators['indicator'], $indicators_l);
          $indicators_t = array_merge_recursive($indicators['target_value'], $indicators_t);
          
          $indicators = ['indicator'=>$indicators_l,'weight'=>$indicators_w,'target_value'=>$indicators_t];

        }

        echo "<pre>";print_r($indicators);
        //return $indicators;
        $indicator_weights_sum = array_sum($indicators['weight']);
        //return $indicators['indicator'];
        $time = date('Y-m-d : h:i:s');
      //  echo "<pre>";
       // echo "indicators list weights";
       // print_r($indicators);
      // echo "indicators list";
       // echo "sum".$indicator_weights_sum;
        print_r($indicators['indicator']);
        foreach ($indicators['indicator'] as $k=>$indicator) {
                $rec = ProjectIndicatorweight::findOne(['milestone'=>$milestone,'project'=>$project,'indicator'=>$indicator]);
                if($rec)
                    $record = $rec;
                else
                    $record = new ProjectIndicatorweight(); 

                $record->project        = $project;
                $record->milestone      = $milestone;
                $record->indicator      = $indicator;
                $record->weight         = round(($indicators['weight'][$k]/$indicator_weights_sum)*$milestone_weight,2);
                $record->target_value   = $indicators['target_value'][$k];
  //              echo "Saving record for $indicator<br>";
                $record->save();
        }
     //   exit;
    }

    public static function setPipelineIndicatorWeight($pipeline,$milestone,$milestone_weight,$indicators)
    {
        //select records from project_indicatorweight;
        $indicator_weights = PipelineIndicatorweight::find()->where(['milestone'=>$milestone,'pipeline'=>$pipeline])->all();
        
        if($indicator_weights){
            foreach ($indicator_weights as $k=>$indicator) {
                $indicators_w[$k]['weight']=[
                                    'indicator'=>$indicator->indicator,
                                    'weight'=>$indicator->weight,
                                    'target_value'=>$indicator->target_value
                                ];
                $indicators_w[$k]=$indicator->weight;
                $indicators_l[$k]=$indicator->indicator;
                $indicators_t[$k]=$indicator->target_value;
            }
          $indicators_w = array_merge_recursive($indicators['weight'], $indicators_w);
          $indicators_l = array_merge_recursive($indicators['indicator'], $indicators_l);
          $indicators_t = array_merge_recursive($indicators['target_value'], $indicators_t);
          $indicators = ['indicator'=>$indicators_l,'weight'=>$indicators_w,'target_value'=>$indicators_t];
        }
        //return $indicators;
        $indicator_weights_sum = array_sum($indicators['weight']);
        //return $indicators['indicator'];
        $time = date('Y-m-d : h:i:s');
      //  echo "<pre>";
       // echo "indicators list weights";
       // print_r($indicators);
      // echo "indicators list";
       // echo "sum".$indicator_weights_sum;
        foreach ($indicators['indicator'] as $k=>$indicator) {
                $rec = PipelineIndicatorweight::findOne(['milestone'=>$milestone,'pipeline'=>$pipeline,'indicator'=>$indicator]);
                if($rec)
                    $record = $rec;
                else
                $record = new PipelineIndicatorweight(); 

                $record->pipeline       = $pipeline;
                $record->milestone      = $milestone;
                $record->indicator      = $indicator;
                $record->target_value   = $indicators['target_value'][$k];
                $record->weight         = round(($indicators['weight'][$k]/$indicator_weights_sum)*$milestone_weight,2);
                $record->save();
        }
        return;
    }

    public static function getIndicatorWeights($project,$milestone){
        return ProjectIndicatorweight::find()->where(['project'=>$project,'milestone'=>$milestone])->all();
    }

    public static function getWeight($project,$milestone){
        $timeline = Timeline::findOne(['project'=>$project,'milestone'=>$milestone]);
        if($timeline){
            return $timeline->weight;
        }else{
        $pipeline  = isset(Yii::$app->session['pipeline']) && !empty(Yii::$app->session['pipeline']) ? Yii::$app->session['pipeline']
            : VProject::findOne(['id'=>$project])->pipeline_id;
        $stage=VMilestoneStage::find(['milestone'=>$milestone,'pipeline_id'=>$pipeline])->one()->stage;
        $timeline = MilestoneStage::findOne(['stage'=>$stage,'milestone'=>$milestone]);
        return $timeline ? $timeline->weight : null;
        }
     //   return null;
    }

    public static function getIndicatorWeight($indicator,$project){
        $data = ProjectIndicatorweight::findOne(['indicator'=>$indicator,'project'=>$project]);
        if($data){
            return $data->weight;
        }else{
            $pipeline  = isset(Yii::$app->session['pipeline']) && !empty(Yii::$app->session['pipeline']) ? Yii::$app->session['pipeline']
            : VProject::findOne(['id'=>$project])->pipeline_id;
            $data = PipelineIndicatorweight::findOne(['indicator'=>$indicator,'pipeline'=>$pipeline]);
            return $data ? $data->weight : null;
        }
    }

    public static function calculateWeight($percentagevalue,$weight){
        return ($percentagevalue/100)*$weight;
    }

    public static function getPipelineMiletstoneWeight($milestonestage){
        $milestonestage = VMilestoneStage::findOne($milestonestage);
        if($milestonestage)
            return $milestonestage->weight;
    }

    public static function calculateWeights($pipeline,$milestone,$milestone_weight,$indicators){
        //select records from project_indicatorweight;
        $indicator_weights = PipelineIndicatorweight::find()->where(['milestone'=>$milestone,'pipeline'=>$pipeline])->all();
        
        if($indicator_weights){
            foreach ($indicator_weights as $k=>$indicator) {
                $indicators_w[$k]['weight']=[
                                    'indicator'=>$indicator->indicator,
                                    'weight'=>$indicator->weight,
                                ];
                $indicators_w[$k]=$indicator->weight;
                $indicators_l[$k]=$indicator->indicator;
            }
          $indicators_w = array_merge_recursive($indicators['weight'], $indicators_w);
          $indicators_l = array_merge_recursive($indicators['indicator'], $indicators_l);
          $indicators = ['indicator'=>$indicators_l,'weight'=>$indicators_w];

        }
        //return $indicators;
        $indicator_weights_sum = array_sum($indicators['weight']);
        foreach ($indicators['indicator'] as $k=>$indicator) {
             $record[]           = [
                                        'id'      =>$indicator,
                                        'name'=>Functions::getIndicator($indicator),
                                        'old_weight'     =>$indicators['weight'][$k],
                                        'weight'         =>round(($indicators['weight'][$k]/$indicator_weights_sum)*$milestone_weight,2),
                                      ];
        }
        return $record;
    }

    public static function calculateIndicatorWeights($req_id,$milestone_weight,$indicators){
                    //select records from project_indicatorweight;
        $request = IndicatorRequests::findOne($req_id);
        $indicator_weights = ProjectIndicatorweight::find()->where(['milestone'=>$request->milestone,'project'=>$request->project])->all();
        
        if($indicator_weights){
            foreach ($indicator_weights as $k=>$indicator) {
                $indicators_w[$k]['weight']=[
                                    'indicator'=>$indicator->indicator,
                                    'weight'=>$indicator->weight,
                                ];
                $indicators_w[$k]=$indicator->weight;
                $indicators_l[$k]=$indicator->indicator;
            }
          $indicators_w = array_merge_recursive($indicators['weight'], $indicators_w);
          $indicators_l = array_merge_recursive($indicators['indicator'], $indicators_l);
          $indicators = ['indicator'=>$indicators_l,'weight'=>$indicators_w];
        }
        //return $indicators;
        $indicator_weights_sum = array_sum($indicators['weight']);
        foreach ($indicators['indicator'] as $k=>$indicator) {
             $record[]           = [
                                        'id'      =>$indicator,
                                        'name'=>Functions::getProjectIndicator($indicator,$req_id),
                                        'old_weight'     =>$indicators['weight'][$k],
                                        'weight'         =>round(($indicators['weight'][$k]/$indicator_weights_sum)*$milestone_weight,2),
                                      ];
        }
        return $record;
    }

    private static function getIndicator($indicator){
        if(Indicator::findOne($indicator)) return Indicator::findOne($indicator)->name; else return $indicator;
    }

    private static function getProjectIndicator($indicator,$req_id){
        if(Indicator::findOne($indicator)){ 
        return Indicator::findOne($indicator)->name;
        }
        else{ 
        $indicator = IndicatorRequests::findOne($req_id);
        if($indicator)
        return $indicator->indicator_name;
        else return null;
        }
    }


    public static function getScaleMilestone($project,$type){
        $pipeline  = isset(Yii::$app->session['pipeline']) && !empty(Yii::$app->session['pipeline']) ? Yii::$app->session['pipeline']
            : VProject::findOne(['id'=>$project])->pipeline_id;
        $scale =[];
        $ct = 0;
        switch($type){
        case 'milestones_achieved':
        //Scale 1: Distinct Milestones Achieved
        $m=Yii::$app->db->createCommand("SELECT distinct milestone_id from v_milestone_submission where project_id=".$project." order by start_date asc")->queryAll();
        foreach($m as $ms)
        $scale[$ms['milestone_id']]=++$ct;
        return $scale;
        
        case 'pipeline':
        //Scale 2: Milestones of pipeline
        $st=PipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->All();
        foreach($st as $stage) {
            $ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
            foreach ($ms as $mst) {
                $scale[$mst['milestone']]=++$ct;
            }
        }
        return $scale;
        }
    }

    public static function getScaleIndicator($project,$type){
        $pipeline  = isset(Yii::$app->session['pipeline']) && !empty(Yii::$app->session['pipeline']) ? Yii::$app->session['pipeline']
            : VProject::findOne(['id'=>$project])->pipeline_id;
        $scale = [];
        $ct = 0;
        switch($type){
        case 'milestones_achieved':
        //Scale 1: Distinct Milestones Achieved
        $m=Yii::$app->db->createCommand("SELECT distinct indicator_id from  v_indicator_milestone where project_id=".$project."")->queryAll();
        foreach($m as $ms)
        $scale[$ms['indicator_id']]=++$ct;
        return $scale;
        
        case 'pipeline':
        //Scale 2: Milestones of pipeline
        $st=PipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->All();
        foreach($st as $stage) {
            $ms=Yii::$app->db->createCommand("select milestone from milestone_stage where stage=".$stage->stage." order by ordering asc")->queryAll();
            foreach ($ms as $mst) {
                $im = Yii::$app->db->createCommand("select indicator from static_indicator_milestone where milestone=".$mst['milestone']."")->queryAll();
                foreach ($im as $ims) {
                    $scale[$ims['indicator']]=++$ct;
                }
            }
        }
        return $scale;
        }
    }

    public static function getScale($xscale,$project){
        $score = 0;
        $weight = 0;
        $data_points=[];
        foreach ($xscale as $key => $indicator) {
            $indicator_weight = Functions::getIndicatorWeight($key,$project);
            $indicator_score = Functions::getIndicatorScore($key,$project);
            $weight+=$indicator_weight;
            $score+=$indicator_score;
            $data_points[] =[
                            'indicator_no'                  =>$indicator,
                            'indicator_id'                  =>$key,
                            'name'                          =>Indicator::findOne($key)->name,
                            'cummulative_indicator_weight'  =>$weight,
                            'score'                         =>$indicator_score,
                            'cummulative_score'             =>$score
                            ];
        }
        return $data_points;
    }

    public static function getScaleM($xscale,$project){
        $score = 0;
        $weight = 0;
        $data_points = [];
        foreach ($xscale as $key => $milestone) {
            $milestone_weight               = Functions::getWeight($project,$key);
            $milestone_score                = Functions::getMilestoneScore($key,$project);
            $weight+=$milestone_weight;
            $score+=$milestone_score;
            $data_points[] =[
                            'milestone_no'                  =>$milestone,
                            'milestone_id'                  =>$key,
                            'milestone'                     =>Milestone::findOne($key)->milestone,
                            'milestone_weight'              =>$milestone_weight,
                            'cummulative_milestone_weight'  =>$weight,
                            'score'                         =>$milestone_score,
                            'cummulative_score'             =>$score
                            ]; 
        }
        return $data_points;
    }

    public static function getIndicatorScore($indicator,$project){
        $score = Yii::$app->db->createCommand(Queries::getIndicatorScore($project,$indicator))->queryOne();
        return $score ? $score['value'] : null;
    }

    public static function getMilestoneScore($milestone,$project){
        $score = Yii::$app->db->createCommand(Queries::getMilestoneScore($project,$milestone))->queryOne();
        return $score ? $score['grade'] : null;
    }
}