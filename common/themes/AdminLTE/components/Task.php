<?php

namespace common\themes\AdminLTE\components;

/**
 * @property string $name
 * @property integer $min_measure
 * @property integer $max_measure
 * @property integer $current_measure
 * @property string $status
 *
 * *** virtual properties ***
 * @property string $progress
 * @property string $statusCode
 */
class Task
{
    const STATUS_EXCELLENT = 'excellent';
    const STATUS_OK = 'ok';
    const STATUS_HIGH_RISK = 'risky';
    const STATUS_FAILED = 'failed';

    public function __construct($attributes=[])
    {
        foreach ($attributes as $property => $value)
            $this->{$property} = $value;

        $this->progress = $this->getProgress();
        $this->statusCode = $this->getStatusCode();
    }

    private static function statuses(){
        return [
            self::STATUS_EXCELLENT,
            self::STATUS_OK,
            self::STATUS_HIGH_RISK,
            self::STATUS_FAILED,
        ];
    }

    private static function statusCode($status){
        switch ($status){
            case self::STATUS_EXCELLENT:
                return 'green';
            case self::STATUS_OK:
                return 'aqua';
            case self::STATUS_HIGH_RISK:
                return 'yellow';
            case self::STATUS_FAILED:
                return 'red';
        }
    }
    
    public function getProgress(){
        return (int) (($this->current_measure * 100)/$this->max_measure);
    }

    public function getStatus(){
        $statuses = self::statuses();
        $index = ((int) ($this->getProgress() * (count($statuses)/100))) - 1;
        return $statuses[$index];
    }

    public function getStatusCode(){
        return self::statusCode($this->getStatus());
    }




    public static function showUserTasks()
    {
        $data = [];//self::generateSampleTasks();
        return \Yii::$app->controller->renderPartial(
            '@common/themes/AdminLTE/views/components/task/user-tasks.php', [
                'tasks'=>$data
            ]
        );
    }

    private static function generateSampleTasks($taskNames=[]){
        $tasks = [];

        if(empty($taskNames))
            $taskNames = [
                'Design some buttons','Create a nice theme','Some task I need to do',
                'Make beautiful transitions',
            ];

        foreach ($taskNames as $name){
            $min = rand(0, 1000);
            $max = rand($min, $min*4);
            $current = rand($min, $max);

            $tasks[] = new self([
                'name' =>$name,
                'min_measure' => $min,
                'max_measure' => $max,
                'current_measure' => $current,
            ]);
        }

        return $tasks;
    }
}