<?php

namespace common\themes\AdminLTE\components;

use yii\helpers\Url;

/**
 * @property string $image_url
 * @property string $title
 * @property string $body
 * @property string $sent_at
 */
class Message
{

    public function __construct($attributes=[])
    {
        foreach ($attributes as $property => $value)
            $this->{$property} = $value;
    }

    public static function showUserMessages($params=[]){
        $data = [];//self::generateSampleMessages($params);
        return \Yii::$app->controller->renderPartial(
            '@common/themes/AdminLTE/views/components/message/user-messages.php', [
                'messages'=>$data
            ]
        );
    }

    private static function generateSampleMessages($params=[]){
        $dir = isset($params['directoryAsset']) ? $params['directoryAsset'].'/img/' : Url::base().'/images/';

        return [
            new self([
                'title' =>'Support Team',
                'body' =>'Why not buy a new awesome theme?',
                'image_url' =>$dir.'user2-160x160.jpg',
                'sent_at' => (time() - rand(1, 50000)),
            ]),
            new self([
                'title' =>'AdminLTE Design Team',
                'body' =>'Why not buy a new awesome theme?',
                'image_url' =>$dir.'user3-128x128.jpg',
                'sent_at' => (time() - rand(1, 50000)),
            ]),
            new self([
                'title' =>'Developers',
                'body' =>'Why not buy a new awesome theme?',
                'image_url' =>$dir.'user4-128x128.jpg',
                'sent_at' => (time() - rand(1, 50000)),
            ]),
            new self([
                'title' =>'Sales Department',
                'body' =>'Why not buy a new awesome theme?',
                'image_url' =>$dir.'user2-160x160.jpg',
                'sent_at' => (time() - rand(1, 50000)),
            ]),
            new self([
                'title' =>'Reviewers',
                'body' =>'Why not buy a new awesome theme?',
                'image_url' =>$dir.'user3-128x128.jpg',
                'sent_at' => (time() - rand(1, 50000)),
            ]),
        ];
    }
}