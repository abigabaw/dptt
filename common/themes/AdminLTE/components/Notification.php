<?php

namespace common\themes\AdminLTE\components;

/**
 * @property string class e.g. 'fa fa-warning text-yellow', 'fa fa-user text-red'
 *                             'fa fa-users text-aqua', 'fa fa-users text-red',
 *                             'fa fa-shopping-cart text-green'
 * @property string link e.g. Url::['/notification/', 'id'=>3]
 * @property string text e.g. You changed your username
 */
class Notification
{
    public function __construct($attributes = [])
    {
        $this->class = isset($attributes['class']) ? $attributes['class'] : '';
        $this->link = isset($attributes['link']) ? $attributes['link'] :'#';
        $this->text = isset($attributes['text']) ? $attributes['text'] :'No Message';
    }

    public static function showUserNotifications()
    {
        return \Yii::$app->controller->renderPartial(
            '@common/themes/AdminLTE/views/components/notification/user-notifications.php',
            ['notifications'=>self::getUserNotifications()]
        );
    }

    /**
     * @return self[]
     */
    public static function getUserNotifications(){
        if(\Yii::$app->user->isGuest)
            return [];
        
        //todo: get logged in user's notifications
        return [];
    }
}