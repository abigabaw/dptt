<?php

namespace common\themes\AdminLTE\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @property string $name
 * @property string $imageUrl
 * @property string $department
 */
class User
{
    public function __construct()
    {
        $this->name = Yii::$app->user->identity->name;
        $this->imageUrl = Url::base() . '/images/user.png';
        $this->department = 'Institution - User';
    }

    public static function showMenu($params = [])
    {
        return \Yii::$app->controller->renderPartial(
            '@common/themes/AdminLTE/views/components/user/menu.php',
            ArrayHelper::merge($params, ['user'=>new User()])
        );
    }
}