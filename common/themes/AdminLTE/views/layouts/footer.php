<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> <?=Yii::$app->params['dptt-version']?>
    </div>
    <strong>
        Copyright &copy; 2014-2017 <a href="http://www.ranlab.org">MAK SPH Resilient Africa Network</a>.
    </strong> 
    All rights reserved.
</footer>