<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $directoryAsset string  */
?>

<header class="main-header">
    <?php $logoUrl = Url::to(['/images/logo1.png'])?>
    <?= Html::a("<span class=\"logo-mini\"><b>D</b>PTT</span><img src=\"$logoUrl\" style=\"width:70%\">",
        ['/site/index'],
        ['class'=>'logo']);
    ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
		  <a style="float:left; margin-left:5px; margin-top:5px; font-size:25px; color:#fff">DYNAMIC PROJECT TRAJECTORY TRACKER</a>
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                <?=\common\themes\AdminLTE\components\Message::showUserMessages(['directoryAsset'=>$directoryAsset])?>
                <?=\common\themes\AdminLTE\components\Notification::showUserNotifications()?>

                <!-- Tasks: style can be found in dropdown.less -->
                <?=\common\themes\AdminLTE\components\Task::showUserTasks()?>

                <!-- User Account: style can be found in dropdown.less -->
                <?=\common\themes\AdminLTE\components\User::showMenu(['baseUrl'=>$directoryAsset])?>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>