<?php
use common\assets\LoginAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

$asset = LoginAsset::register($this);
$this->registerCssFile(Url::to(['/css/style.css']))?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">

        <div class="row" style="margin-bottom:0px; background:#fff; border-bottom:2px solid #00a65a;">
            <img src="<?=Url::base()?>/images/logo.png" style="float:left; width:250px; margin-top:10px; margin-bottom:10px; margin-left:20px;"/>
        </div>
        <div class="container">
            <?= $content ?>
        </div>

    </div>


    <footer style="position:fixed; width:100%; bottom:0px; background:#fff; padding:20px; border-top:2px solid #00a65a;">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2017 <a href="http://www.ranlab.org">MAK SPH Resilient Africa Network</a>.</strong> All rights reserved.
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>