<?php
    /* @var array $menuConfig*/
    /* @var string $sidebarTopView*/
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <?php if(isset($sidebarTopView))
            echo Yii::$app->controller->renderPartial($sidebarTopView)?>

        <!-- search form -->
        <?php //Yii::$app->controller->renderPartial('@common/themes/AdminLTE/views/site/_search.php')?>
        <!-- /.search form -->

        <?php if(isset($menuConfig) && is_array($menuConfig))
            echo dmstr\widgets\Menu::widget($menuConfig) ?>

    </section>

</aside>
