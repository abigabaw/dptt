<?php
/**
 * @var \common\themes\AdminLTE\components\Message $message
 */?>
<!-- start message -->
<li>
    <a href="#">
        <div class="pull-left">
            <img src="<?= $message->image_url ?>" class="img-circle" alt="User Image"/>
        </div>
        <h4>
            <?=$message->title?>
            <small>
                <i class="fa fa-clock-o"></i> 
                <span sent-time="<?=$message->sent_at?>">
                    <?=\common\services\Time::momentAgo($message->sent_at)?>
                </span>
            </small>
        </h4>
        <p><?=$message->body?></p>
    </a>
</li>
<!-- end message -->
