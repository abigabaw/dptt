<?php
/**
 * @var \common\themes\AdminLTE\components\Message[] $messages
 * @var \yii\web\View $this
 */
$count = count($messages)?>

<!-- Messages: style can be found in dropdown.less-->
<li class="dropdown messages-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-envelope-o"></i>
        <span class="label label-default"><?=$count?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <?=$count>0 ? $count : 'no'?> messages</li>
        <?php if($count > 0):?>
            <li>
                <ul class="menu" id="user-messages">
                    <?php foreach ($messages as $message):?>
                        <?= $this->render('@common/themes/AdminLTE/views/components/message/_message',[
                            'message'=>$message
                        ]) ?>
                    <?php endforeach;?>
                </ul>
            </li>
            <li class="footer"><a href="#">See All Messages</a></li>
        <?php endif; ?>
    </ul>
</li>
