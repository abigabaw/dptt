<?php
/**
 * @var \common\themes\AdminLTE\components\User $user
 */
use yii\helpers\Html; ?>
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<?= $user->imageUrl ?>" class="user-image bg-success" alt="<?= $user->name ?>"/>
        <span class="hidden-xs"><?= $user->name ?></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <img src="<?= $user->imageUrl ?>" class="img-circle bg-success" alt="User Image"/>

            <p>
                <?= $user->name ?>
                <small><?= $user->department ?></small>
            </p>
        </li>
        <!-- Menu Body -->
        <!--<li class="user-body">
            <div class="col-xs-4 text-center">
                <a href="#">Followers</a>
            </div>
            <div class="col-xs-4 text-center">
                <a href="#">Sales</a>
            </div>
            <div class="col-xs-4 text-center">
                <a href="#">Friends</a>
            </div>
        </li>-->
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <?= Html::a('Sign out',
                    ['/site/logout'],
                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                ) ?>
            </div>
        </li>
    </ul>
</li>
