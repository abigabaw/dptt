<?php
use common\themes\AdminLTE\components\User;

/**
 * @var User $user
 */

$user = isset($user) ? $user : new User()?>

<div class="user-panel">
    <div class="pull-left image">
        <img src="<?= $user->imageUrl ?>" class="img-circle bg-success" alt="User Image">
    </div>
    <div class="pull-left info">
        <p><?= $user->name ?></p>
    </div>
</div>

