<?php
/**
 * @var \common\themes\AdminLTE\components\Notification[] $notifications
 */?>
<!-- inner menu: contains the actual data -->
<ul class="menu" id="notifications">
    <?php if(!empty($notifications)):?>
        <?php foreach ($notifications as $notification):?>
            <?=Yii::$app->controller->renderPartial(
                '@common/themes/AdminLTE/views/components/notification/_notification.php',
                ['notification'=>$notification]
            )?>
        <?php endforeach;?>
    <?php endif;?>
</ul>
