<?php
/**
 * @var \common\themes\AdminLTE\components\Notification $notification
 */?>

<li>
    <a href="<?=$notification->link?>">
        <i class="<?=$notification->class?>"></i> <?=$notification->text?>
    </a>
</li>
