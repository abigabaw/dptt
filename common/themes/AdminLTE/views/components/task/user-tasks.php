<?php
/**
 * @var \common\themes\AdminLTE\components\Task[] $tasks
 */
$no_tasks = count($tasks)?>

<li class="dropdown tasks-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-danger"><?=$no_tasks?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <?=$no_tasks>0 ? $no_tasks : 'no'?> tasks</li>
        <?php if($no_tasks > 0):?>
            <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu" id="user-tasks">
                    <?php foreach ($tasks as $task):?>
                        <?= $this->render('@common/themes/AdminLTE/views/components/task/_task',[
                            'task'=>$task
                        ])?>
                    <?php endforeach;?>
                </ul>
            </li>
            <li class="footer">
                <a href="#">View all tasks</a>
            </li>
        <?php endif?>
    </ul>
</li>
