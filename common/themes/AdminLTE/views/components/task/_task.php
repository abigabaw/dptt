<?php
/**
 * @var \common\themes\AdminLTE\components\Task $task
 */?>

<!-- Task item -->
<li>
    <a href="#">
        <h3>
            <?=$task->name?>
            <small class="pull-right"><?=$task->progress?>%</small>
        </h3>
        <div class="progress xs">
            <div class="progress-bar progress-bar-<?=$task->statusCode?>" style="width: <?=$task->progress?>%"
                 role="progressbar" aria-valuenow="<?=$task->current_measure?>" aria-valuemin="<?=$task->min_measure?>"
                 aria-valuemax="<?=$task->max_measure?>">
                <span class="sr-only"><?=$task->progress?>% Complete</span>
            </div>
        </div>
    </a>
</li>
<!-- end task item -->
