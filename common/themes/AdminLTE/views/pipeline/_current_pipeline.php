<?php
use backend\models\Pipeline;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * 
 */?>
<div class="user-panel">
    <div class="pull-left image">
        <img src="<?= Url::base() ?>/images/pipeline2a.jpg" class="bg-success" alt="Pipelines"/>
    </div>
    <div class="pull-left info">
        <p>
            <?php if(isset(Yii::$app->session['pipeline'])){?>
                <?=Pipeline::findOne(Yii::$app->session['pipeline'])->name?> <br>
                <?=Html::a('[Change Pipeline]', ['/site/index', 'pipeline'=>0])?>                
            <?php }else{?>
                <?=Html::a("Choose Pipeline", ['/site/index', 'pipeline'=>0])?>
            <?php }?>
        </p>
        
        <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
    </div>
</div>

