<?php

/* @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 * @var $form_title string */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
Use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="login-main">
    <div class="login">
        <?php $form = ActiveForm::begin([
            'id'                     => 'login-form',
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false,
            'validateOnBlur'         => false,
            'validateOnType'         => false,
            'validateOnChange'       => false,
        ]); ?>

        <div class="social">
            <ul>
                <li class="last-mar"><a class="loginicon" href="#"><?=$form_title?></a></li>
            </ul>
        </div>

        <?= $form->field($model, 'login', [
                'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1'],
                'options'=>[
                    'tag'=>'div',
                    'class'=>'form-group field-loginform-usernasme has-feedback required'
                ],
                'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback" style="color:#00a65a"></span>{error}{hint}'
            ])->textInput(['placeholder'=>'Email',]) ?>

        <?= $form->field($model, 'password',[
                'inputOptions' => ['class' => 'form-control', 'tabindex' => '2'],
                'options'=>[
                    'tag'=>'div',
                    'class'=>'form-group field-loginform-password has-feedback required'
                ],
                'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback" style="color:#00a65a"></span>{error}{hint}'
        ])->passwordInput(['placeholder'=>'Password'])
          ->label(Yii::t('user', 'Password') . (
              $module->enablePasswordRecovery
                  ? ' (' . Html::a(Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']) . ')'
                  : '')
          ) ?>

        <?= $form->field($model, 'rememberMe',['options'=>[
            'tag'=>'div',
            'class'=>'form-group field-loginform-rememberme'],
        ])->checkbox(['tabindex' => '4']) ?>

        <ul class="button">
            <li><input type="submit" value="Submit" ></li>
            <li><input class="clear" type="Reset" value="Clear"></li>
        </ul>
        <!--
		<div class="forgot">


            <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['/user/registration/register']) ?>
            </p>
        <?php endif ?>
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>

	   	  <div class="forgot-para">
                <a href="#"> <p>Forgot Your Password?</p></a>
              </div>
            <div class="clear"> </div>
           </div>-->
    </div>
    <?php ActiveForm::end(); ?>
    <div class="login-password">
        <p>Dynamic Project Trajectory Tracking Toolkit</p>
    </div>
</div>
<div class="copyright">
    <p>&copy; 2016 <a href="#" target="_blank">DPTTT</a></p>
</div>