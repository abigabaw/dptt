<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Innovation;
use frontend\models\Milestone;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovation */

$this->title = 'Update Timeline: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Innovations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="innovation-update">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
     <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
         <?= $form->field($model, 'innovation',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Innovation::find()->all(),'id','innovation'),
	['prompt'=>'Select Innovation'] )?>
	<?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
         <?= $form->field($model, 'milestone',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Milestone::find()->all(),'id','milestone'),
	['prompt'=>'Select Innovation'] )?>
		<div class="form-group">
      <?= Html::submitButton('Update Timeline', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>

</div>
</div>
</div>