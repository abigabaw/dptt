<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use unclead\widgets\TabularInput;
use frontend\models\Innovation;
use frontend\models\Item;
use frontend\models\Milestone;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Timeline */

$this->title = 'Create Timeline';
$this->params['breadcrumbs'][] = ['label' => 'Innovations', 'url' => ['/innovation/index']];
$this->params['breadcrumbs'][] = ['label' => 'Timelines', 'url' => ['/timelines/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeline-update">
<div class="box box-success">
<div class="box-header bg-success with-border">
     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box-body">
  <?php $form = \yii\bootstrap\ActiveForm::begin([
    'id'                        => 'tabular-form',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnChange'          => false,
    'validateOnSubmit'          => true,
    'validateOnBlur'            => false,
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]) ?>

<?= TabularInput::widget([
    'models' => $models,
    'attributeOptions' => [
        'enableAjaxValidation'      => true,
        'enableClientValidation'    => false,
        'validateOnChange'          => false,
        'validateOnSubmit'          => true,
        'validateOnBlur'            => false,
    ],
    'columns' => [
        [
            'name' => 'id',
            'type' => \unclead\widgets\TabularColumn::TYPE_HIDDEN_INPUT
        ],
        [
            'name'  => 'milestone',
            'title' => 'Milestone',
            'type'  => 'dropDownList',
            'defaultValue' => 1,
            'items' => ArrayHelper::map(Milestone::find()->all(),'id','milestone')
        ],
        [
            'name'  => 'start_date',
            'type'  => DatePicker::className(),
            'title' => 'Start Day',
            'options' => [
                'pluginOptions' => [
                    'format' => 'yyyy-mm-d',
                    'todayHighlight' => true
                ]
            ],
            'headerOptions' => [
                'style' => 'width: 250px;',
                'class' => 'day-css-class'
            ]
        ],
    ],
]) ?>


<?= Html::submitButton('Add', ['class' => 'btn btn-success']);?>
<?php ActiveForm::end();?>
</div>
</div>
</div>
