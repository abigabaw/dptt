<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
	<?php print_r($_SERVER);?>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Please select a pipeline</p>
    <p></p>
</div>
