<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$asset          = common\assets\HighStockAsset::register($this);
$baseUrl        = $asset->baseUrl;

$this->title = 'DPTT Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?=$team_count?></h3>
                  <p>No. of Team Members</p>
                </div>
                <div class="icon">
                  <i class="fa fa-group "></i>
                </div>
                <a href="<?= Url::to(['/program']);?>" class="small-box-footer">View Portfolios <span class="fa fa-arrow-circle-right"></span></a>
                      </div>
            </div>
                        <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?=$delayed_milestone_count?></h3>
                  <p>No. of Overdue Milestones</p>
                </div>
                <div class="icon">
                  <i class="fa fa-file-text-o"></i>
                </div>
                <a href="#overdue_projects" class="small-box-footer">Milestones not submitted by finish date </a>
                      </div>
            </div>

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
              <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?=$rejected_milestone_count?></h3>
                  <p>No. of Rejected Milestones</p>
                </div>
                <div class="icon">
                <i class="fa fa-tasks"></i>
                </div>
                <a href="#milestone_status" class="small-box-footer">View Rejected Milestones <span class="fa fa-arrow-circle-right"></span></a>
                      </div>
            </div>
                          <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?=$approved_milestone_count?></h3>
                  <p>No. of Approved Milestones</p>
                </div>
                <div class="icon">
                  <i class="fa fa-tasks"></i>
                </div>
                <a href="#milestone_status" class="small-box-footer">View Approved Milestones <span class="fa fa-arrow-circle-right"></span></a>
                </div>
            </div>
          </div><!-- /.row -->

          <div class="row">
                       <?= \common\components\ChartJSTimeline::widget(['id'=>$project]) ?> 
                <div class="col-md-12">
                                          <?= \common\components\CummulativeScoreSingle::widget([
                                                                        'id'=>$project,
                                                                        'elementid'=>'container_cumulative',
                                                                        'scale_type'=>'milestone',
                                                                        'y_scale'=>'milestones_achieved'
                                                                        ]) ?>  </div> 
          <div class="col-md-12">
                  <div class="box-body ">
                       <?= \common\components\ActualTimeline::widget(['id'=>$project,'elementid'=>'container3']) ?> 
                      <br/>
                      <?= \common\components\ComparisonTimeline::widget(['id'=>$project,'elementid'=>'container']) ?>
                </div>
          </div>
          <div class="row col-md-12" id="milestone_status">
            <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Rejected Milestones</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list ui-sortable">
                  <?php 
                   /* foreach ($rejected_milestones as $rejected) {
                      echo '<li class="dropdown notifications-menu">
                                <!-- drag handle -->
                                <span class="handle ui-sortable-handle">
                                  <i class="fa fa-ellipsis-v"></i>
                                  <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <!-- todo text -->
                                <span class="text"><th>'.$rejected['milestone'].'</th></span>
                              <a href="#"  aria-expanded="false">
                                <span class="label label-danger"> score '.$rejected['grade'].'</span>
                              </a>
                            </li>';
                    }*/
                    ?>
                  </ul>
                </div><!-- /.box-body -->
              </div>
            </div>
            <div class="col-md-6">
                          <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title" >Approved Milestones</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                                       <ul class="todo-list ui-sortable">
                  <?php 
                   /* foreach ($approved_milestones as $approved) {
                      echo '<li class="dropdown notifications-menu">
                                <!-- drag handle -->
                                <span class="handle ui-sortable-handle">
                                  <i class="fa fa-ellipsis-v"></i>
                                  <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <!-- todo text -->
                                <span class="text"><th>'.$approved['milestone'].'</th></span>
                              <a href="#"  aria-expanded="false">
                                <span class="label label-warning"> score '.$approved['grade'].'</span>
                              </a>
                            </li>';
                    }*/
                    ?>
                  </ul>

                       </div>
</div>
                    </div>
            </div>
</div>
