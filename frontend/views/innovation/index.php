<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InnovationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Innovations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovation-index">

    <div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Innovation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'innovation',
            [
             'attribute' => 'Innovator',
             'value' => 'innovator0.names'
             ],
            'amount',
            [
             'attribute' => 'Institution',
             'value' => 'institution0.names'
             ],
            // 'project',
            // 'description:ntext',
            // 'country',
            // 'teamleader',
            // 'award_date',
            // 'signing_date',
            // 'startstage',
            // 'startmilestone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
</div>
