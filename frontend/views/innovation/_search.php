<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InnovationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="innovation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'innovation') ?>

    <?= $form->field($model, 'innovator') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'institution') ?>

    <?php // echo $form->field($model, 'project') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'teamleader') ?>

    <?php // echo $form->field($model, 'award_date') ?>

    <?php // echo $form->field($model, 'signing_date') ?>

    <?php // echo $form->field($model, 'startstage') ?>

    <?php // echo $form->field($model, 'startmilestone') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
