<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->stage;
$this->params['breadcrumbs'][] = ['label' => 'Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-create">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">Rearrange milestones of the <?=$model->stage?> stage</h3>
            </div>
            <div class="box-body">
          <?php
          if( is_array( $dataProvider ) && count( $dataProvider ) > 0 ) {
              $count = 1;
                foreach($dataProvider as $v)
                {
                    $items[$v['milestone_id']]['content'] = $count.' - '.$v['milestone'];
                    
                    ++$count;
                }
            $form = ActiveForm::begin();
             
            // Kartiks widget
                echo SortableInput::widget([
                'name'=> 'sort_list',
                'items' => $items,
                'hideInput' => true,
                ]);
            echo Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']);

            ActiveForm::end();
          }else{
            echo 'There are no milestone in this stage. You can add milestones by clicking  
                <a href="'.Url::to(['/milestone-stage']).'">
                <i class="fa fa fa-map-o"></i> <span>Here</span>
              </a>';
          }
?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#stage_nav').addClass('active');")
  ?>