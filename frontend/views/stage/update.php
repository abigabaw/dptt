<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Stage */

$this->title = 'Update Stage: ' . ' ' . $model->stage;
$this->params['breadcrumbs'][] = ['label' => 'Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stage-update">
<div class="box box-success">
            <div class="box-body">
             <?php $form = ActiveForm::begin(['id' => 'stage-form']); ?>
					 <?= $form->field($model, 'stage',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Stage Name',]) ?>
					<?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'Brief description of stage',]) ?>
					<div class="form-group">
				      <?= Html::submitButton('Update Stage', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#stage_nav').addClass('active');")
  ?>