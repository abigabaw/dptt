<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\StageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stage-index">
<div class="box box-success">
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add a  Stage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
            'attribute'=>'Stage',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider['stage'], ['view', 'id' => $dataProvider['id']]);
            }
            ],
            ['attribute' => 'Description',
                 'label' =>'Filename',
                 'value'=>function($dataProvider){
                return $dataProvider['description'];
                  
            },
                 'contentOptions' => ['style' => 'width:70%  '],
              ],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div></div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#stage_nav').addClass('active');")
  ?>
