<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Stage */

$this->title = $model->stage;
$this->params['breadcrumbs'][] = ['label' => 'Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stage-view">
<div class="box box-success">
            <div class="box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>   
        <?= Html::a('View Milestones', ['milestones', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Assign Milestones', ['milestone-stage/milestones','id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stage',
            'description:ntext',
        ],
    ]) ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#stage_nav').addClass('active');")
  ?>