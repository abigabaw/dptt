<?php

use yii\helpers\Html;
Use yii\helpers\Url;
use frontend\models\Timeline;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">
<div class="box box-success">
            <div class="box-body">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                        [
            'attribute'=>'Name',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider['name'], ['view', 'id' => $dataProvider['id']]);
                  
            }
            ],
            [
            'attribute'=>'Portfolio',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider->program0->name, ['/program/view', 'id' => $dataProvider['portfolio_id']]);
                  
            }
            ],
            [
            'attribute'=>'Project Team',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider->innovator0->names, ['/innovator/view', 'id' => $dataProvider['innovator']],['style' => 'color:#000']);
                  
            }
            ],
            [
             'attribute' => 'Team Leader',
             'value' => 'teamleader0.name'
             ],
                       [
            'attribute'=>'Institution',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider->institution0->names, ['/institution/view', 'id' => $dataProvider['institution']],['style' => 'color:#000']);
                  
            }
            ],
            [
            'attribute'=>'Amount',
            'value'=>function($dataProvider){
                return number_format($dataProvider->amount,2);
            }
            ],
            // 'institution',
            // 'project_group',
            // 'description:ntext',
            // 'country',
            // 'teamleader',
            // 'award_date',
            // 'signing_date',
            // 'startstage',
            // 'startmilestone',
            // 'created_by',
            // 'updated_by',
            // 'created_at',
            // 'updated_at',

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}{new_action2}{new_action3}',
            'buttons' => [
                'new_action1' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span> ', $url, [
                                'title' => Yii::t('app', 'View'),
                    ]);
                }
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'new_action1') {
                    $url = Url::to(['project/view/'.$model['id'].'']);
                    return $url;
                }
              }
            ],
        ],
    ]); ?>

</div></div></div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>