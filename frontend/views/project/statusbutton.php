<?php
use yii\helpers\Html;
use frontend\models\Timeline;
use backend\models\Iteration;
        $timeline = Timeline::findOne(['milestone'=>$milestone,'project'=>$project]);
         if($timeline){
            $iteration = Iteration::findOne(['timeline'=>$timeline->id]);
            if($iteration)
            echo Html::a('View Iterations <span class="fa  fa-hand-o-right"></span>', ['review', 'id' =>$milestone,'project'=>$project], ['class' => 'btn btn-success']);
            else
            echo Html::a('start  <span class="fa  fa-hand-o-right"></span>', ['start', 'id' =>$timeline->id], ['class' => 'btn btn-primary']);
         }else{
               echo ' <a href="#" class="btn btn-warning"><span class="fa fa-check-square-o"></span> Timeline not assigned to milestone</a>';
         }
?>