<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\GradingOutcome;
use frontend\models\Milestone;
/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */
$grading_title = Milestone::findOne($model->milestone)->milestone;
$this->title = 'Update Grading';
$this->params['breadcrumbs'][] = ['label' => 'Cohorts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $grading_title
?>
<div class="cohort-create">
<div class="box box-success">
			<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($grading_title) ?></h3>
            </div>
            <div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
    <?= $form->field($model, 'grade',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter the grade',]) ?>

    <?= $form->field($model, 'comment',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a comment',])?>
    
    <?= $form->field($model, 'status',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(GradingOutcome::find()->all(),'id','name'),
                    ['prompt'=>'Please Select the Outcome of this Grading'] )?>

	<div class="form-group">

      <?= Html::submitButton('Submit Grading', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>;