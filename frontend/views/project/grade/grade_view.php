<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */

$this->title = 'View Grading';
$this->params['breadcrumbs'][] = ['label' => 'Program', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->milestone0->milestone;
?>
<div class="grading-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
    <p>
        <?= Html::a('Update', ['update-grading', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete-grading', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this grading?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [   'label' =>'Milestone',
                'value' =>$model->milestone0->milestone,
            ],
            [   'label' =>'Project',
                'value' =>$model->project0->name,
            ],
            [   'label' =>'Grade',
                'value' =>$model->grade.'%',
            ],
            [   'label' =>'Comment',
                'value' =>$model->comment,
            ],
            [   'label' =>'Status',
                'value' =>$model->status0->name,
            ],
        ],
    ]) ?>

    </div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")
  ?>;