<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProjectSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'innovator') ?>

    <?= $form->field($model, 'portfolio') ?>

    <?= $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'institution') ?>

    <?php // echo $form->field($model, 'project_group') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'teamleader') ?>

    <?php // echo $form->field($model, 'award_date') ?>

    <?php // echo $form->field($model, 'signing_date') ?>

    <?php // echo $form->field($model, 'startstage') ?>

    <?php // echo $form->field($model, 'startmilestone') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
