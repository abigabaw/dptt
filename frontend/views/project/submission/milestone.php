<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\Project;
use frontend\models\MilestoneSubmission;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MilestoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Milestones for '. Project::findOne($project)->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
                $stagelist ='';
                $milestonelist ='';
                $count = 0;
                foreach ($data as $stage) {
                  if($count==0){
                  $stagelist.='<li class="active">
                            <a href="#tab_'.$stage['stages']['id'].'" data-toggle="tab">'.ucwords($stage['stages']['stage']).'</a>
                                <div class="progress progress-sm active">
                                        <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar"  aria-valuemin="0" aria-valuemax="100" style="width: '.$stage['stages']['percentage'].'%">
                                          <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                </div>
                        </li>';
                 $milestonelist.='<div class="tab-pane active" id="tab_'.$stage['stages']['id'].'">
                                        <div class="box-group" id="accordion">
                                            <h4><b>Milestones in '.$stage['stages']['stage'].' Stage</b></h4>
                           <table class="table table-striped">
                                  <tbody>
                                  <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Milestone</th>
                                    <th>Action</th>
                                  </tr>
                                  <tr>
                                  ';
                                 foreach ($stage['stages']['milestones'] as $milestone) {
                                   $milestonelist.='
                                    <td>'.$milestone['num'].'</td>
                                    <td>'.$milestone['milestone'].'</td>
                                    <td>'.$milestone['status'].'</td>
                                  </tr>';
                                 }
       
                                        
                 $milestonelist.='</tbody>
                                </table></div>
                      </div><!-- /.tab-pane -->';
        
                  }else{
                    $stagelist.='<li>
                            <a href="#tab_'.$stage['stages']['id'].'" data-toggle="tab">'.ucwords($stage['stages']['stage']).'</a>
                                <div class="progress progress-sm active">
                                        <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar"  aria-valuemin="0" aria-valuemax="100" style="width: '.$stage['stages']['percentage'].'%">
                                          <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                </div>
                        </li>';
                 $milestonelist.='<div class="tab-pane" id="tab_'.$stage['stages']['id'].'">
                                        <div class="box-group" id="accordion">
                                            <h4><b>Milestones in '.$stage['stages']['stage'].' Stage</b></h4>
                          <table class="table table-striped">
                                  <tbody>
                                  <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Milestone</th>
                                    <th>Action</th>
                                  </tr>
                                  <tr>
                                  ';
                                 foreach ($stage['stages']['milestones'] as $milestone) {
                                   $milestonelist.='
                                    <td>'.$milestone['num'].'</td>
                                    <td>'.$milestone['milestone'].'</td>
                                    <td>'.$milestone['status'].'</td>
                                  </tr>';
                                 }
       
                                        
                 $milestonelist.='</tbody>
                                </table></div>
                      </div><!-- /.tab-pane -->';
        
                  }
                $count++;
                }
                 ?>

<div class="milestone-index">


<div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
              <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                <?=$stagelist?>
                  
                  <li class="pull-right header"> Stages <i class="fa fa-dedent "></i></li>
                </ul>
                <div class="tab-content">
                 <?= $milestonelist ?>
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
</div></div>
</div>    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>;