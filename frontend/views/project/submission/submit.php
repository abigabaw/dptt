<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Milestone;
use yii\bootstrap\Modal;
use frontend\models\LoginInnovator;
use frontend\models\IndicatorType;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use frontend\models\VIndicator;
use frontend\models\Project;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = 'Submit Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$institution = Project::findOne($project)->institution;
?>
<div class="project-create">
<div class="box box-success">
            <div class="box-body">
                        <div class="btn-group">
                                <?php 
              if(!$request){
                   Modal::begin([
                        'id' => 'createCompany',
                        // ... any other yii2 bootstrap modal option you need
                        'header' => '<h4 class="modal-title">Add Indicator</h4>',
                        'toggleButton' => ['label' => '<i class="fa  fa-plus"></i> Add a new indicator to Milestone','class'=>'btn btn-primary btn-sm','style'=>'margin-top:3px;'],
                    ]);


                   $form = ActiveForm::begin(['id' => 'cohort-form','action'=>'/project/request-indicator']);
                        echo $form->field($indicatorrequestmodel, 'indicator_id')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VIndicator::find()->where(['institution_id'=>$institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Indicator (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true
                                                ],
                                        ]);

                     echo $form->field($indicatorrequestmodel, 'indicator_type',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(IndicatorType::find()->all(),'id','name'),
    ['prompt'=>'Select Indicator Value'] );
                         echo '<div class="modal-footer">
                         <input type="hidden" name="iteration" value="'.$iteration.'"/>
                         <input type="hidden" name="milestone" value="'.$milestone.'"/>
                         <input type="hidden" name="project" value="'.$project.'"/>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send Request for Addition</button>
                  </div>';
                        ActiveForm::end();
                    Modal::end(); 
}else{

              echo '<button class="btn btn-warning">A request for an indicator has already been sent</button>';
              

}

     ?>              </div>

            <?php $form = ActiveForm::begin(['options' => ['id' => 'indicator-form','enctype' => 'multipart/form-data']]) ?>

                  <?= $form->field($model, 'narration',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Please enter the description of this submission',]) ?>
                   <div class="row col-md-12">
                   <b>Indicators</b><br/><br/>
                   <?=$indicators?>
                   </div>
                  <?= $form->field($model, 'file[]')->widget(FileInput::classname(), [
                          'options' => ['multiple' => true],
                  ]);?>

        <div class="form-group">
      <?= Html::submitButton('Submit Milestone', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
</div>

<?php ActiveForm::end(); ?>


</div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#project_nav').addClass('active');

$(\"#indicatorrequests-indicator_id\").select2({
    placeholder: 'Select or type a new stage if not in list',
    width: '100%' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

    ")
  ?>