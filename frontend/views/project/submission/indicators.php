    <?php
    use frontend\models\VIndicatorMilestone;
    use frontend\models\VStaticIndicatorMilestone;

    $static_indicators      = VStaticIndicatorMilestone::find()->where(['milestone_id'=>$milestone])->all();
    $dynamic_indicators     = VIndicatorMilestone::find()->where(['project_id'=>$project,'milestone_id'=>$milestone])->all();
    
    foreach ($static_indicators as $indicator) {
          echo'<div class="form-group">
                      <div class="col-sm-10">
                        <label for="indicator" class=" control-label" style="text-align:left">'.$indicator['indicator'].'</label>
                      </div>
                      <div class="col-sm-2">
                        <input type="text" class="form-control" name="static_indicator[]" >
                        <input type="hidden" class="form-control" name="static_indicator_ids[]" value="'.$indicator['indicator_id'].'">
                      </div>
              </div>';
              }
    foreach ($dynamic_indicators as $indicator) {
          echo'<div class="form-group"><div class="col-sm-10">
                      <label for="indicator" class="col-sm-10 control-label" style="text-align:left">'.$indicator['indicator'].'</label>
                      </div><div class="col-sm-2">
                        <input type="text" class="form-control" name="dynamic_indicator[]" >
                        <input type="hidden" class="form-control" name="dynamic_indicator_ids[]" value="'.$indicator['indicator_id'].'">
                      </div>
                      </div>';
    }
?>