<?php
use frontend\models\IterationType;
use yii\helpers\Html;
use yii\Grid\GridView;
use backend\models\VMilestoneSubmission;
use backend\models\ExtensionRequests;
use backend\models\Iteration;
function getIterationType($type){
return IterationType::findOne($type)->name;
}

function getSubmitButton($end_date,$id){
  if(!$end_date){
    $iteration= Iteration::findOne($id);
    //check if today is before end_date
     if(date('Y-m-d', strtotime($iteration->start_date. ' + '.$iteration->duration.' days')) < date('Y-m-d : h:i:s')){
        //today is after proposed end_date
        return getExtensionRequest($id,'after');
     }else{
        //today is  before proposed end date so you can request for extension or make submission
        return getExtensionRequest($id,'before');
     }
  }else{
    $extension = ExtensionRequests::findOne(['iteration'=>$id,'status'=>1]);
    if($extension)
    return 'Extended';
    else
    return 'Submitted';
  }
}

function getExtensionRequest($id,$condition){
    $extension = ExtensionRequests::findOne(['iteration'=>$id,'status'=>0]);
    if($extension){
        return 'Extension Request Sent';
    }else{
        if($extension['status']==2)
        return 'Extension Request Rejected<br/></br/>'.Html::a('Request for another Date Extension <span class="fa  fa-hand-o-right"></span>', ['request', 'id' =>$id], ['class' => 'btn btn-warning btn-xs']);    
        
        switch($condition){
        case 'after':
        return Html::a('Request for Date Extension <span class="fa  fa-hand-o-right"></span>', ['request', 'id' =>$id], ['class' => 'btn btn-warning btn-xs']);
        case 'before':
        return Html::a('<i class="fa fa-dot-circle-o"></i>  SUBMIT', ['submit', 'id'=>$id ], ['class' => 'btn btn-primary btn-xs']).'<br/><br/>'.Html::a('Request for Date Extension <span class="fa  fa-hand-o-right"></span>', ['request', 'id' =>$id], ['class' => 'btn btn-warning btn-xs']); 
        }
    }
}
?>
<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Iterations of this Milestone</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'start_date',
                            'duration',
                            'end_date',
                            [
                            'attribute'=>'Type',
                            'format'=>'raw',
                            'value'=>function($dataProvider){
                                if(getIterationType($dataProvider['iteration_type']))
                                return getIterationType($dataProvider['iteration_type']);
                                else
                                return '';
                                  
                            }
                            ],
                            [
                            'attribute'=>'Actions',
                            'format'=>'raw',
                            'value'=>function($dataProvider){
                                return getSubmitButton($dataProvider['end_date'],$dataProvider['id']);    
                            }
                            ],
                        ],
                    ]); ?>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>