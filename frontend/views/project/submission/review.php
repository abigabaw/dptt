<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Milestone;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use common\models\Functions;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */
$milestone = Milestone::findOne($milestone_id);
$this->title = 'Review Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => substr($milestone->milestone,0,80).'...', 'url' => ['milestones','id'=>$milestone->id]];

?>
<div class="project-view">

<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> : <?= Html::encode($milestone->milestone)?></h3>
            </div>
            

        <?php 
        if($milestone_status){
          echo '
   <div class="box-body">

    <p><div class="btn-group">';
        if(!($iterate)){
                   Modal::begin([
                        'id' => 'requestIterate',
                        // ... any other yii2 bootstrap modal option you need
                        'header' => '<h4 class="modal-title">REQUEST TO ITERATE</h4>',
                        'toggleButton' => ['label' => '<i class="fa  fa-plus"></i> Request to iterate milestone','class'=>'btn btn-primary btn-sm','style'=>'margin-top:3px;'],
                    ]);


                   $form = ActiveForm::begin(['id' => 'cohort-form','action'=>'/project/request-iterate']);
                    echo $form->field($iteratemodel, 'reason',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Reason for iterating miletone',]);

                         echo '<div class="modal-footer">
                         <input type="hidden" name="milestone" value="'.$milestone_id.'"/>
                         <input type="hidden" name="project" value="'.$project_id.'"/>
                         <input type="hidden" name="timeline" value="'.$timeline.'"/>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send Request</button>
                  </div>';
                        ActiveForm::end();
                    Modal::end(); 
        }else{

            if($iterate->status==1&&$iterate->approval_date==NULL)
            echo '';
            else if($iterate->status==0)
            echo '<a href="#" class="btn btn-warning"><span class="fa fa-check-square-o"></span> A request to iterate this milestone has been sent</a>';
               
            else if($iterate->status==1&&$iterate->approval_date!=NULL){
                  Modal::begin([
                        'id' => 'requestIterate',
                        // ... any other yii2 bootstrap modal option you need
                        'header' => '<h4 class="modal-title">REQUEST TO ITERATE</h4>',
                        'toggleButton' => ['label' => '<i class="fa  fa-plus"></i> Request to iterate milestone','class'=>'btn btn-primary btn-sm','style'=>'margin-top:3px;'],
                    ]);


                   $form = ActiveForm::begin(['id' => 'cohort-form','action'=>'/project/request-iterate']);
                    echo $form->field($iteratemodel, 'reason',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Reason for iterating miletone',]);

                         echo '<div class="modal-footer">
                         <input type="hidden" name="milestone" value="'.$milestone_id.'"/>
                         <input type="hidden" name="project" value="'.$project_id.'"/>
                         <input type="hidden" name="timeline" value="'.$timeline.'"/>
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send Request</button>
                  </div>';
                        ActiveForm::end();
                    Modal::end(); 
            }
              else{
                echo '';
              }                   
        }
        echo '    </div>
        
    </p>
</div>';
      }

?>
                

</div>
<?=$this->render('iterations',['dataProvider'=>$dataProvider,'searchModel'=>$searchModel]);?>
<ul class="timeline">
  <?php
foreach ($submissions as $submission) {
  echo '<!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-green">
                    '.date('d-M-Y',strtotime($submission['submissions']['submission_date'])).'
                  </span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                  <i class="fa fa-dot-circle-o bg-green"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> About '.Functions::humanTiming(strtotime($submission['submissions']['submission_date'])).' ago ('.date_format(date_create(date($submission['submissions']['submission_date'])),"l jS F Y g:ia").')</span>
                    
                    <div class="timeline-body">
                        <h5><b>Description</b></h5>
                    <p>';


                    foreach ($submission['submissions']['attachments'] as $attachment) {
                       echo '<a href="'.Yii::$app->urlManagerFrontend->createUrl('').''.$attachment['attachment'].'" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download Attachment</a>';
                     }

                      echo '</p> <h5><b>Indicators</b></h5>
                         <div class="box box-success" >
                            <div class="box-body no-padding">
                              <table id="layout-skins-list" class="table table-striped bring-up nth-2-center">
                                <tbody>';

                                               $counter = 1;
                        foreach ($submission['submissions']['indicators'] as $indicator) {
                         echo '<tr>
                                    <td><code>'.$counter.'</code> <a href="#" data-skin="skin-blue" class="btn btn-primary btn-xs" style="margin-left:5px;">'.$indicator['indicator'].'</a></td>
                                  </tr>';
                          $counter++;
                        }

                      echo'</tbody>
                              </table>
                            </div><!-- /.box-body -->
                          </div><!-- /.box -->
                    </div>

                  </div>
                </li>

                <!-- END timeline item -->
                    <!-- timeline item -->

    <!-- END timeline item -->
 '.$this->render('indicator_submissions',['milestone_submission'=>$submission['submissions']['id']]).' 
    ';
}
?>
</ul>

</div>
  <?= $this->registerJs(
    " 
    $('#project_nav').addClass('active');")
  ?>
