<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
        <style type="text/css">
            table th:first-child {
                width: 150px;
            }
            /* Bootstrap 3.x re-reset */
            .fn-gantt *,
            .fn-gantt *:after,
            .fn-gantt *:before {
              -webkit-box-sizing: content-box;
                 -moz-box-sizing: content-box;
                      box-sizing: content-box;
            }
        </style>
<div class="project-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> - GRAPH</h3>
            </div>
            <div class="box-body ">
            <h3 class="box-title">Timeline - GRAPH</h3>
            <div class="chart">
                    <canvas id="lineChart" style="height:250px"></canvas>
    
                  </div>
                  <h3 class="box-title">Actual Submission - GRAPH</h3>
            <div class="chart">
                    <canvas id="lineChart2" style="height:250px"></canvas>
    
                  </div>

    

</div>
</div></div>

<!-- Page specific script -->
    <?= $this->registerJs(
      ' $(function () {
        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //--------------
        //- AREA CHART -
        //--------------

        // Get context with jQuery - using jQuery\'s .get() method.

        Chart.types.Line.extend({
          name: "LineAlt",
          draw: function () {
              Chart.types.Line.prototype.draw.apply(this, arguments);
              
              var ctx = this.chart.ctx;
              ctx.save();
              // text alignment and color
              ctx.textAlign = "center";
              ctx.textBaseline = "bottom";
              ctx.fillStyle = this.options.scaleFontColor;
              // position
              var x = this.scale.xScalePaddingLeft * 0.4;
              var y = this.chart.height / 2;
              // change origin
              ctx.translate(x, y)
              // rotate text
              ctx.rotate(-90 * Math.PI / 180);
              ctx.fillText(this.datasets[0].label, 0, 0);
              ctx.restore();
          }
      });

        var areaChartData = {
          labels: '.$labels.',
          datasets: [
            {
              label: "MILESTONES",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: '.$data.'
            },
          ]
        };
        var areaChartData2 = {
          labels: '.$labels2.',
          datasets: [
            {
              label: "MILESTONES",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: '.$data2.'
            },
          ]
        };

        var areaChartOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };


        //-------------
        //- LINE CHART -
        //--------------


        var ctx = document.getElementById("lineChart").getContext("2d");
        var myLineChart = new Chart(ctx).LineAlt(areaChartData, {
            // make enough space on the right side of the graph
            scaleLabel: "          <%=value%>"
        });
        var ctx2 = document.getElementById("lineChart2").getContext("2d");
        var myLineChart = new Chart(ctx2).LineAlt(areaChartData2, {
            // make enough space on the right side of the graph
            scaleLabel: "          <%=value%>"
        });
      });')
  ?>