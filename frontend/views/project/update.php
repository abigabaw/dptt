<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Innovator;
use frontend\models\Timeline;
use frontend\models\Portfolio;
use common\models\Institution;
use frontend\models\Country;
use frontend\models\Stage;
use frontend\models\Milestone;
use frontend\models\LoginInnovator;
use frontend\models\Program;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = 'Update Project: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-update">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
            <?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
    <?= $form->field($model, 'name',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Project Name',]) ?>
                <?= $form->field($model, 'program',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Program::find()->all(),'id','name'),
                    ['prompt'=>'Please Select the progam under which this project belongs'] )?>

                <?= $form->field($model, 'amount',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'Project Amount',]) ?>
                <?= $form->field($model, 'institution',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Institution::find()->where(['id'=>Yii::$app->user->identity->institution])->all(),'id','names') )?>
                <?= $form->field($model, 'innovator',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Innovator::find()->all(),'id','names'),
                    ['prompt'=>'Please Select the project Team'] )?>
                <?= $form->field($model, 'teamleader',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(LoginInnovator::find()->all(),'id','name'),
                    ['prompt'=>'Please Select the project\'s Team Leader'] )?>
                <?= $form->field($model, 'description',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Project Description',]) ?>

                <?= $form->field($model, 'country',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Country::find()->all(),'id','name'),
                    ['prompt'=>'Please Select Country'] )?>
                    <?= $form->field($model, 'award_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter Award Date'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-d',
                            ]
                        ]);?>
                    <?= $form->field($model, 'signing_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter Signing Date'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-d',
                            ]
                        ]);?>
                    <?php 
                    $timelines = count(Timeline::find()->where(['project'=>$model->id])->all());
                    if(!$timelines>0){
                        echo $form->field($model, 'startstage',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Stage::find()->all(),'id','stage'),
                    ['prompt'=>'Please Select Start stage'] );

                        echo $form->field($model, 'startmilestone',
                                ['options'=>[
                                'tag'=>'div',
                                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                                'template'=>'{label}{input}{error}{hint}'
                                ])->dropDownList(
                    ArrayHelper::map(Milestone::find()->orderBy('milestone')->all(),'id','milestone'),
                    ['prompt'=>'Please Select Starting Milestone'] );
                    }
                    ?>


				        <div class="form-group">

				      <?= Html::submitButton('Update Project', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>

				<?php ActiveForm::end(); ?>

</div>
</div></div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>