<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use frontend\models\Project;
use frontend\models\MilestoneSubmission;
/* @var $this yii\web\View */
/* @var $model backend\models\Timeline */

$this->title = 'Timeline';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeline-view">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> Details for <?=Project::findOne($model->project)->name?></h3>
            </div>
            <div class="box-body">
                <?php 
$submission = MilestoneSubmission::findOne(['project'=>$model->project,'milestone'=>$model->milestone]);
if($submission)
echo '<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>    <i class="icon fa fa-check"></i> Milestone Status</h4>
                   This milestone was submitted on '.date('Y-m-d', strtotime($timeline_detail['start_date']). ' + '.$timeline_detail['sum'].' days').'
                  </div>';

else
echo '<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Milestone Status</h4>
                    This milestone is supposed to be submitted on '.date('Y-m-d', strtotime($timeline_detail['start_date']). ' + '.$timeline_detail['sum'].' days').' but has not yet been submitted
                  </div>';
  ?>
    <p>
        <?= Html::a('Update', ['project/edit-timeline-entry', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [   'label' =>'Project',
                 'value' =>$model->project0->name,
             ],
            [   'label' =>'Milestone',
                 'value' =>$model->milestone0->milestone,
             ],
             [   'label' =>'Start Date',
                 'value' =>date('d-M-Y',strtotime($timeline_detail['start_date'])),
             ],
             [   'label' =>'Proposed Initial Duration',
                 'value' =>$timeline_detail['initial_duration'].' day(s)',
             ],
             [   'label' =>'Actual Duration',
                 'value' =>$timeline_detail['sum']. ' day(s)',
             ]
        ],
        
    ]) ?>   
</div>
</div>

<div class="box box-success">
  <div class="box-header bg-success with-border">
    <h3 class="box-title">Requests For Date Extension</h3>
            </div>
            <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProviderExtension,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'reason',
            'duration',
                       [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}',
            'buttons' => [
                'new_action1' => function ($url, $dataProviderExtension) use ($project) {
                    if($dataProviderExtension['status']==0)
                    return Html::a('Approve <span class="fa  fa-hand-o-right"></span>', ['approve', 'id' =>$project,'ext'=> $dataProviderExtension['id']], ['class' => 'btn btn-primary']);
                    else
                    return 'Approved';
                },
            ],
            ],

        ],
    ]);
    ?>

</div>
</div>

<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title">Iterations (Date Extensions)</h3>
            </div>
            <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'comment',
            'start_date',
            'duration'
        ],
    ]); ?>

</div>
</div>
</div>