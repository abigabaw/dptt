<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Project;
use frontend\models\Milestone;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovation */

$this->title = 'Request Extension';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Timelines', 'url' => ['project/timeline/']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="innovation-update">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> for <?= $milestone?></h3>
            </div>
            <div class="box-body">
     <?php $form = ActiveForm::begin(['id' => 'project-form']); ?>
     <?= $form->field($model, 'reason',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textArea(['placeholder'=>'Reason for Extension Request',]) ?>
     <?= $form->field($model, 'duration',
                                                    ['options'=>[
                                                                'tag'           =>'div',
                                                                'class'         =>'form-group fieldcohort-form-name has-feedback required'],
                                                                'template'      =>'{label}{input}{error}{hint}'
                                                    ])->textInput(['placeholder'=>'No of Days to extend ',]) ?>

		<div class="form-group">
      <?= Html::submitButton('Send Request', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
<?php ActiveForm::end();?>
</div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#project_nav').addClass('active');")
  ?>