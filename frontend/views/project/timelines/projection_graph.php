<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Milestone;
use yii\widgets\DetailView;

$asset          = common\assets\HighStockAsset::register($this);
$baseUrl        = $asset->baseUrl;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
        <style type="text/css">
            table th:first-child {
                width: 150px;
            }
            /* Bootstrap 3.x re-reset */
            .fn-gantt *,
            .fn-gantt *:after,
            .fn-gantt *:before {
              -webkit-box-sizing: content-box;
                 -moz-box-sizing: content-box;
                      box-sizing: content-box;
            }
        </style>

<div class="project-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">
                        <div class="box-tools pull-left">
                        <span class="label label-danger timeline_display" style="padding:5px; cursor:pointer;">Show Line of best fit graphs <i class="fa fa-angle-left" id="caret"></i></span>
                      </div>
                  </h3>
              
            </div>
            <div class="box-body" style="display:none" id="timeline_content">
            <h3 class="box-title">Expected Timeline</h3>
            <?= \common\components\ChartJSTimeline::widget(['id'=>$model->id]) ?> 

</div>
</div>

</div>
<div class="project-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?> - GRAPH</h3>
            </div>
            <div class="box-body ">
            <h3 class="box-title">Graphs</h3>
            <div class="chart">
              
                  <?= \common\components\ActualTimeline::widget(['id'=>$model->id,'elementid'=>'container3']) ?> 
              <br/>
                 <?= \common\components\ComparisonTimeline::widget(['id'=>$model->id,'elementid'=>'container']) ?>
              <br/>
                                   <?= \common\components\CummulativeScoreSingle::widget([
                                                                        'id'=>$model->id,
                                                                        'elementid'=>'container_cumulative',
                                                                        'scale_type'=>'milestone',
                                                                        'y_scale'=>'milestones_achieved'
                                                                        ]) ?>
                                                                        
                <?= \common\components\CummulativeScoreSingleMulti::widget([
                                                                        'id'=>$model->id,
                                                                        'elementid'=>'container_cumulative_multi',
                                                                        'scale_type'=>'milestone',
                                                                        'y_scale'=>'milestones_achieved'
                                                                        ]) ?>
    

</div>
</div>

</div>
