<?php

use yii\helpers\Html;
use yii\grid\GridView;
Use yii\helpers\Url;
use frontend\models\Grading;
use frontend\models\MilestoneSubmission;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TimelineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Timelines for '.$model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeline-index">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title">Below are the Project <?= Html::encode($this->title) ?>, agreed with the supervisor</h3>
            </div>
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Calendar View', ['/timeline/calendar','id' => $project], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
             'attribute' => 'Milestone',
             'value' => 'milestone'
             ],
             [
                'label'=>'Start Date',
                'format'=>'raw',
                'value' => function($dataProvider){
                    return date('d-M-Y', strtotime($dataProvider['start_date'])); 
                }
            ],
            [
                'label'=>'Duration (Days)',
                'format'=>'raw',
                'value' => function($dataProvider){
                    return $dataProvider['duration']; 
                }
            ],
            'comment:ntext',
            [
                'label'=>'Status',
                'format'=>'raw',
                'value' => function($dataProvider){
                    $submission = MilestoneSubmission::findOne(['milestone'=>$dataProvider['milestone_id'],'project'=>$dataProvider['project_id']]);
                    if($submission){
                       $grading = Grading::findOne(['milestone'=>$dataProvider['milestone_id'],'project'=>$dataProvider['project_id']]); 
                       if($grading){
                        $grading = Grading::findOne(['milestone'=>$dataProvider['milestone_id'],'status'=>1,'project'=>$dataProvider['project_id']]); 
                        if($grading)
                            return '<i class="fa  fa-check-circle-o"></i> Submission Approved';
                        else
                            return '<i class="fa fa-times-circle"></i> Submission Rejected';
                       }else{
                        return '<i class="fa fa-circle-o"></i> Milestone Submitted but not yet assessed';
                       }

                    }else{
                        return '<i class="fa fa-circle-o"></i> Milestone Not Submitted';
                    }
                }
            ],
            
            // 'start_date',
            // 'created_by',
            // 'created_at',
            // 'updated_at',

            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}{new_action2}{new_action3}',
            'buttons' => [
                'new_action1' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'View'),
                    ]);
                }
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'new_action1') {
                    $url = Url::to(['project/timeline-entry/'.$model['id'].'']);
                    return $url;
                }
              }
            ],   
            ],
    ]); ?>

</div>
</div>
</div>