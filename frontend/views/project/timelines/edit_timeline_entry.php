<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Project;
use frontend\models\Milestone;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovation */

$this->title = 'Update Timeline';
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Timelines', 'url' => ['project/timeline/']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="innovation-update">
<div class="box box-success">
  <div class="box-header bg-success with-border">
                  <h3 class="box-title">Update Timeline for <?=Project::findOne($model->project)->name?></h3>
            </div>
            <div class="box-body">
     <?php $form = ActiveForm::begin(['id' => 'project-form']); ?>
         <?= $form->field($model, 'project',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required '],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Project::find()->where(['id'=>$model->project])->all(),'id','name'),
	['prompt'=>'Select Project'] )?>
  <?= $form->field($model, 'milestone',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Milestone::find()->all(),'id','milestone'),
	['prompt'=>'Select Milestone'] )?>
		<div class="form-group">
      <?= Html::submitButton('Update Timeline Entry', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
<?php ActiveForm::end();?>
</div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');")

  ?>