<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use unclead\widgets\TabularInput;
use frontend\models\Innovation;
use frontend\models\Item;
use frontend\models\Milestone;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Timeline */

$this->title = 'Create Project Timeline';
$this->params['breadcrumbs'][] = ['label' => 'Innovations', 'url' => ['/innovation/index']];
$this->params['breadcrumbs'][] = ['label' => 'Timelines', 'url' => ['/timelines/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeline-create">
<div class="box box-success">
<div class="box-header bg-success with-border">
     <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
</div>
<div class="box-body">

  <?php $form = \yii\bootstrap\ActiveForm::begin([
    'id'                        => 'tabular-form',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnChange'          => false,
    'validateOnSubmit'          => true,
    'validateOnBlur'            => false,
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]) ?>
<?= TabularInput::widget([
    'models' => $models,
    'attributeOptions' => [
        'enableAjaxValidation'      => true,
        'enableClientValidation'    => false,
        'validateOnChange'          => false,
        'validateOnSubmit'          => true,
        'validateOnBlur'            => false,
    ],
    'columns' => [
          [
            'name'  => 'num',
            'title' => 'No.',
            'type'  => 'static',
        ],
        [
            'name'  => 'milestone',
            'title' => 'Milestone',
            'type'  => 'dropDownList',
            'defaultValue' => 5,
            'items' => ArrayHelper::map(Milestone::find()->all(),'id','milestone'),
            'options'=>['class'=>'milestone_values']
        ],
        [
            'name'  => 'milestone_hidden',
            'title' => 'Milestone',
            'type' => \unclead\widgets\TabularColumn::TYPE_HIDDEN_INPUT,
            'defaultValue' => 3,
            'items' => ArrayHelper::map(Milestone::find()->all(),'id','milestone'),
            'options'=>['class'=>'milestone_values']
        ],
        [
            'name'  => 'start_date',
            'type'  => DatePicker::className(),
            'title' => 'Start Day',
            'options' => [
                'pluginOptions' => [
                    'format' => 'yyyy-mm-d',
                    'todayHighlight' => true
                ],
                'options'=>['class'=>'milestone_start_date']
            ],
            'headerOptions' => [
                'style' => 'width: 300px;',
                'class' => 'day-css-class'
            ]
        ],
        [
            'name'  => 'duration',
            'title' => 'Duration',
            'options'=>['class'=>'milestone_duration']
        ],
        [
            'name'  => 'comment',
            'title' => 'Comment',
            'type'  => 'textarea',
            'headerOptions' => [
                'style' => 'width: 300px;',
                'class' => 'day-css-class'
            ]

        ],
    ],
]) ?>

<div class="form-group">
				      <?= Html::submitButton('Add Timelines', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
<?php ActiveForm::end();?>
</div>
</div>
</div>
<?= $this->registerJS("
    $('.milestone_values').prop('disabled', true);
    $('#project_tree').addClass('active');
    $('#project_nav').addClass('active');


(function($, window, document, undefined){
  $('.milestone_duration').change(function() {
    var date = new $(this).closest('.multiple-input-list__item').find('.milestone_start_date').val();
    var days = parseInt($(this).val());
  date = parseDate(date);
    if(!isNaN(date.getTime())){
            date.setDate(date.getDate() + days);
            $(\"#end_date\").val(date.toInputFormat());
            $(this).closest('.multiple-input-list__item').next().find('.milestone_start_date').val(date.toInputFormat());
    } else {
            alert(\"Invalid Date\");  
    }
  });

  // parse a date in yyyy-mm-dd format
  function parseDate(input) {
    var parts = input.match(/(\d+)/g);
    // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
  }

  //From: http://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
  Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return yyyy + \"-\" + (mm[1]?mm:\"0\"+mm[0]) + \"-\" + (dd[1]?dd:\"0\"+dd[0]); // padding
  };
})(jQuery, this, document);


    "); 
?>