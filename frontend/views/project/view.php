<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">
<div class="box box-success">
            <div class="box-body">

    <p><div class="btn-group">
        
        <?= Html::a('<i class="fa fa-dot-circle-o"></i>  VIEW TIMELINES', ['timelines','id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-list"></i>  VIEW MILESTONES', ['milestones','id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php // Html::a('<i class="fa fa-line-chart"></i>  GANTT GRAPH', ['graph','id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('<i class="fa fa-line-chart"></i>  TIMELINE GRAPH', ['timeline-graph','id' => $model->id], ['class' => 'btn btn-warning']) ?>
        
    </div>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [   'label' =>'Program',
                'value' =>$model->program0->name,
            ],
            [   'label' =>'Preject Team',
                'value' =>$model->innovator0->names,
            ],
            [   'label' =>'Team Leader',
                'value' =>$model->teamleader0->name,
            ],
            [   'label' =>'Amount',
                'value' =>number_format($model->amount,2),
            ],
            [   'label' =>'Project Manager',
                'value' =>$model->institution0->names,
            ],
            'description:ntext',
            [   'label' =>'Country',
                'value' =>$model->country0->name,
            ],
            [   'label' =>'Award Date',
                'value' =>date('F d, Y',strtotime($model->award_date)),
            ],
             [   'label' =>'Signing Date',
                'value' =>date('F d, Y',strtotime($model->signing_date)),
            ],
            [   'label' =>'Start Stage',
                'value' =>$model->startstage0->stage,
            ],
            [   'label' =>'Start Milestone',
                'value' =>$model->startmilestone0->milestone,
            ],
        ],
    ]) ?>

</div>
</div></div>
  <?= $this->registerJs(
    " 
    $('#project_nav').addClass('active');")
  ?>