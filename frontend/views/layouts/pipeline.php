<?php
use yii\helpers\Url;
use yii\helpers\Html;
$this->title = "Welcome to DPTT";
//print_r(Yii::$app->session['pipeline_a']);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title">No Projects</h3>
            </div>
            <div class="box-body">
            <div style="text-align:center ">
                    Sorry You have no projects assigned to you yet. Please wait until you are assigned a project
             </div>
            </div>
 </div>
 </div>