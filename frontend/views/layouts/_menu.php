<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Program;
use frontend\models\Pipeline;
use frontend\models\Project;
use frontend\models\ExtensionRequests;
use frontend\models\IndicatorRequests;
use common\models\VIterationRequests;

$projects = Project::find()->where(['innovator'=>Yii::$app->user->identity->rel_table_id])->all();
$project_ids='';
foreach ($projects as $key => $project) {
           $project_ids.=''.$project['id'].',';
}

$project_ids = rtrim($project_ids, ',');
$project_ids=array_map('intval', explode(',', $project_ids));
$project_ids = implode("','",$project_ids);

$extension_requests  = count(ExtensionRequests::find()->where('status=0 AND project in (\''.$project_ids.'\')')->all());
$indicator_requests  = count(IndicatorRequests::find()->where('status=0 AND project in (\''.$project_ids.'\')')->all());
$repeat_requests     = count(VIterationRequests::find()->where('status=0 AND project_id in (\''.$project_ids.'\')')->all());
?>

<?php
    return [
        'options' => ['class' => 'sidebar-menu'],
	'encodeLabels' => false,
        'items' => [
            ['label' => 'MAIN NAVIGATION', 'options' => ['class' => 'header', 'style'=>'color:#fff']],
            ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/']],
            ['label' => 'MODULES', 'options' => ['class' => 'header', 'style'=>'color:#fff']],
            ['label' => 'My Projects', 'icon' => 'fa fa-file-text-o', 'url' => ['/project']],
            ['label' => 'Team Members', 'icon' => 'fa fa-users', 'url' => ['/innovator']],
            [
                'label' => 'Requests',
                'icon' => 'fa fa-lightbulb-o',
                'url' => '#',
                'options' => ['class' => 'treeview', 'id'=>'request_tree'],
                'items' => [
                    [
                        'label' => 'Iteration Requests <small class="label pull-right bg-red">'.$repeat_requests.'</small>','icon' => 'fa fa-circle-o',
                        'url' => ['/milestone/requests'], 'options' => ['id'=>'repeat_requests','encodeLabels'=>false],
                    ],
                    [
                        'label' => 'Indicator Requests <small class="label pull-right bg-red">'.$indicator_requests.'</small>', 'icon' => 'fa fa-circle-o',
                        'url' => ['/indicator/requests'], 'options' => ['id'=>'indicator_requests'],
                    ],
                    [
                        'label' => 'Extension Requests <small class="label pull-right bg-red">'.$extension_requests.'</small>', 'icon' => 'fa fa-circle-o',
                        'url' => ['/project/extension-requests'], 'options' => ['id'=>'extension_requests'],
                    ],
                ],
            ],
            ['label' => 'ACCOUNT', 'options' => ['class' => 'header', 'style'=>'color:#fff']],
            ['label' => 'Settings', 'icon' => 'fa fa-cog', 'url' => ['/account']],
            ['label' => 'Logout', 'icon' => 'fa fa-sign-out',
                'url' => ['/site/logout'], 'options'=>['data-method'=>'post']],
        ],
    ];


