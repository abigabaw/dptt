<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\IndicatorMilestoneSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Indicator Milestones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-milestone-index">
<div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Indicator Milestone', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
             'attribute' => 'Indicator',
             'value' => 'indicator0.name'
             ],
             [
             'attribute' => 'Milestone',
             'value' => 'milestone0.milestone'
             ],
             [
             'attribute' => 'Project',
             'value' => 'project0.name'
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
    </div>
    </div>
</div>

