<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Indicator;
use frontend\models\Milestone;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\IndicatorMilestone */

$this->title = 'Add or Create Indicators for  Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Indicator Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-milestone-create">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title">
                   <?php 
                   	if(!$milestone){
	                  echo '';
	                  }else{
	                    echo 'Add an indicator to '.Milestone::findOne(['id'=>$milestone])->milestone;
	                  }
                    ?>

                  </h3>
            </div>
            <div class="box-body">
			<?php $form = ActiveForm::begin(['id' => 'indicator-milestone-form']); ?>
			<?php
    		if(!$milestone) 
    			echo $form->field($model, 'milestone',
						['options'=>[
		                'tag'=>'div',
		                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
		                'template'=>'{input}{error}{hint}'
		                ])->dropDownList(
			ArrayHelper::map(Milestone::find()->all(),'id','milestone'),
			['prompt'=>'Select Milestone'] )?>
			<?= $form->field($model, 'indicator')->widget(Select2::classname(), [
					    'data' => ArrayHelper::map(Indicator::find()->orderBy('name ASC')->all(),'id','name'),
					    'options' => ['placeholder' => 'Type new or select indicator (s) from list'],
					    'pluginOptions' => [
					        'allowClear' => true,
					        'multiple' => true
					    ],
				]);
			 ?>

			<div class="form-group">
		      <?= Html::submitButton('Add indicator to milestone', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
		    </div>
</div>
</div>
</div>
</div>
<?= $this->registerJs(
      ' 
$("#staticindicatormilestone-indicator").select2({
    placeholder: \'Select or type a new indicator if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

')?>
