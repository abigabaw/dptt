<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PipelineStage */

$this->title = 'Update Pipeline Stage: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pipeline Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pipeline-stage-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
