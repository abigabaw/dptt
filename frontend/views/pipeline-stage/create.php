<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Pipeline;
use frontend\models\Stage;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\PipelineStage */

$this->title = 'Create Pipeline Stage';
$this->params['breadcrumbs'][] = ['label' => 'Pipeline Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pipeline-stage-create">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
	<?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
                    <?= $form->field($model, 'pipeline',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                            ])->dropDownList(
                ArrayHelper::map(Pipeline::find()->all(),'id','name'),
                ['prompt'=>'Select Pipeline'] )?>

    <?= $form->field($model, 'stage')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Stage::find()->orderBy('stage ASC')->all(),'id','stage'),
                        'options' => ['placeholder' => 'Select Stage (s)'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => true
                        ],
                ]);
             ?>

<div class="form-group">
      <?= Html::submitButton('Create Pipeline Stage', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    </div>
</div>
</div>
<?= $this->registerJs(
      ' 
$("#pipelinestage-stage").select2({
    placeholder: \'Select or type a new stage if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

')?>