<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\StaticIndicatorMilestone */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-indicator-milestone-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'indicator')->textInput() ?>

    <?= $form->field($model, 'milestone')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
