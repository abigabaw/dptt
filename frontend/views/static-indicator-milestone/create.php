<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Indicator;
use frontend\models\Milestone;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\IndicatorMilestone */

$this->title = 'Create Indicator Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Indicator Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-milestone-create">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
			<?php $form = ActiveForm::begin(['id' => 'indicator-milestone-form']); ?>
			<?= $form->field($model, 'milestone',
						['options'=>[
		                'tag'=>'div',
		                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
		                'template'=>'{input}{error}{hint}'
		                ])->dropDownList(
			ArrayHelper::map(Milestone::find()->orderBy('milestone ASC')->all(),'id','milestone'),
			['prompt'=>'Select Milestone'] )?>
			<?= $form->field($model, 'indicator')->widget(Select2::classname(), [
					    'data' => ArrayHelper::map(Indicator::find()->all(),'id','name'),
					    'options' => ['placeholder' => 'Select Indicator (s)'],
					    'pluginOptions' => [
					        'allowClear' => true,
					        'multiple' => true
					    ],
				]);
			 ?>

			<div class="form-group">
		      <?= Html::submitButton('Create Indicator Milestone Map', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
		    </div>
</div>
</div>
</div>
</div>