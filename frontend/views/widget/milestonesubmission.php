<div id="container"></div>
<!-- Page specific script -->
<?= $this->registerJs(
    " 
/**
 * Fires on chart load, called from the chart.events.load option.
 */
function onChartLoad() {

    var centerX = 140,
        centerY = 110,
        path = [],
        angle,
        radius,
        badgeColor = Highcharts.Color(Highcharts.getOptions().colors[0]).brighten(-0.2).get(),
        spike,
        empImage,
        big5,
        label,
        left,
        right,
        years,
        renderer;

    if (this.chartWidth < 530) {
        return;
    }



    // Big 5
    big5 = '';

}

$(function () {
    var options = {
        chart: {
            panning: true
        },

        rangeSelector: {
            selected: 1
        },

        xAxis: {
            type: 'datetime',
            minTickInterval: 30,
            labels: {
                align: 'left'
            },
            plotBands: []

        },

        title: {
            text: 'Actual Timeline Graph of Milestones achieved over time by project'
        },

        tooltip: {
            style: {
                width: '250px'
            }
        },

        yAxis: [{
            max: ".$max_y.",
            labels: {
                enabled: false
            },
            title: {
                text: 'Milestones'
            },
            lineWidth:1,
            gridLineColor: 'rgba(0, 0, 0, 0.07)'
        }, {
            allowDecimals: false,
            max: 15,
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            opposite: true,
            gridLineWidth: 0
        }],

        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2
                },
                fillOpacity: 0.5
            },
            flags: {
                tooltip: {
                    xDateFormat: '%B %e, %Y'
                }
            }
        },

        series: [".$data."]
    };
// Add flags for important milestones. This requires Highstock.
    if (Highcharts.seriesTypes.flags) {
        options.series.push(".$flags.");
    }


    $('#container').highcharts(options);
});")
?>