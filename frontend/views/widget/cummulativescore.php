<div id="container_cumulative"></div>

    <?= $this->registerJs(
    " 
      $(function () {
      $('#container_cumulative').highcharts({
        chart: {
            type: 'spline',
            inverted: false
        },
        title: {
            text: 'Cummulative Indicator Scores'
        },
        xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: 'Indicators'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: 'Cummulative Scores'
            },
            lineWidth: 2
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },
        series: [".$dataset_cummulative."]
    });
});
    ")
?>
