  <div class="row" id="overdue_projects">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Overdue Projects</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ul class="todo-list ui-sortable">
                        <?php
                        foreach ($overdue_milestones as $delayed) {
                            echo '<li class="dropdown notifications-menu">
                                                                <!-- drag handle -->
                                <span class="handle ui-sortable-handle">
                                  <i class="fa fa-ellipsis-v"></i>
                                  <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <!-- todo text -->
                                <span class="text"><th>'.$delayed['project'].'</th></span>
                                <a href="#" class="dropdown-toggle load_milestone" id="'.$delayed['project_id'].'" data-toggle="dropdown" aria-expanded="false">
                                  <span class="label label-warning">'.$delayed['number_of_milestones'].' Overdue milestones</span>
                                </a>
                                <ul class="dropdown-menu col-md-12">
                                  <li class="header col-md-12" id="milestone_header"></li>
                                   <li class="overlay">
                                      <i class="fa fa-refresh fa-spin"></i>
                                  </li>
                                  <li class="col-md-12">
                                    <!-- inner menu: contains the actual data -->
                                    <table class="table table-bordered">
                                    <tbody class="menu menu-milestone">
                                  </tbody>
                                  </table>
                                  </li>
                                </ul>
                              </li>';
                        }
                        ?>
                    </ul>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title" >Project Progress</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- Progress bars -->
                    <?php
                    foreach ($overdue_milestones as $delayed) {
                        $percentage = round(($delayed['number_of_milestones']/$delayed['number_of_assigned_milestones'])*100,1);
            
                        if($percentage>80){
                            $style='progress-bar-green';
                        }else if($percentage>50){
                            $style='progress-bar-yellow';
                        }else{
                            $style='progress-bar-red';
                        }
                        echo '
                      <div class="clearfix">
                        <span class="pull-left">'.$delayed['project'].'</span>
                        <small class="pull-right">'.$percentage.'%</small>
                      </div>
                      <div class="progress xs">
                        <div class="progress-bar '.$style.'" style="width: '.$percentage.'%;"></div>
                      </div>';

                    }
                    ?></div>
            </div>
        </div>
    </div>