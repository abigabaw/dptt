  <?php
   $series=json_encode($series,JSON_NUMERIC_CHECK); 
  ?>
     <div id="<?=$elementid?>"></div>

     <?= $this->registerJs(
      ' 
      $(function () {
      $(\'#'.$elementid.'\').highcharts(\'StockChart\', {

            rangeSelector: {
                selected: 1
            },
            colors: [\'#562F1E\', \'#AF7F24\', \'#263249\', \'#5F7F90\', \'#D9CDB6\'],

            title: {
                text: \'Comparision of Timelines\'
            },

            series: '.$series.'
        });

     });')?>

