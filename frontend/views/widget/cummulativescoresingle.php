 <div id="<?=$elementid?>"></div>
<!-- Page specific script -->
    <?= $this->registerJs(
      ' 
      $(function () {
    $(\'#'.$elementid.'\').highcharts({
        chart: {
            type: \'spline\',
            inverted: false
        },
        title: {
            text: \'Cummulative Indicator Scores\'
        },
        xAxis: {
            reversed: false,
            title: {
                enabled: true,
                text: \'Indicators\'
            },
            maxPadding: 0.05,
            showLastLabel: true
        },
        yAxis: {
            title: {
                text: \'Cummulative Scores\'
            },
            lineWidth: 2
        },
        legend: {
            enabled: false
        },
        '.$tooltip.'
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },
        series: [{
            name: \'Cummulative Indicator Score\',
            data: ['.$dataset_cummulative.']
        }]
    });
});')?>