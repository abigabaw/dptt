<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Indicator */

$this->title = 'Indicator Data Type';
$this->params['breadcrumbs'][] = ['label' => 'Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->indicator0->name;
?>
<div class="indicator-view">
<div class="box box-success">

            <div class="box-body">

    <p>
        <?= Html::a('Update', ['edit-value', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete-value', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
             [   'label' =>'Indicator',
                 'value' =>$model->indicator0->name,
             ],
             [   'label' =>'Data Type',
                 'value' =>$model->indicatorType->name,
             ],
        ],
    ]) ?>
</div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#adminitrator_tree').addClass('active');
    $('#indicator_tree').addClass('active');
    $('#indicator_nav').addClass('active');")
  ?>
