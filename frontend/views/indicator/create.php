<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $model backend\models\Indicator */

$this->title = 'Create Indicator';
$this->params['breadcrumbs'][] = ['label' => 'Indicators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grant-create">
<div class="box box-success">
            <div class="box-body">
			<?php $form = ActiveForm::begin(['id' => 'indicator-form']); ?>
					 <?= $form->field($model, 'name',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Indicator Name',]) ?>
					  <?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'Indicator Description',]) ?>
					<div class="form-group">
				      <?= Html::submitButton('Create Indicator', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#adminitrator_tree').addClass('active');
    $('#indicator_tree').addClass('active');
    $('#indicator_nav').addClass('active');")
  ?>