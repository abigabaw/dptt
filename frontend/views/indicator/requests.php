<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IndicatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Indicator Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicator-index">
<div class="box box-success">

            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'milestone0.milestone',
            [
            'attribute'=>'Project Name',
            'value'=>'project0.name'
            ],
            'indicator:ntext',

           [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{new_action1}',
            'buttons' => [
                'new_action1' => function ($url, $dataProvider) {
                    if($dataProvider['status']==0)
                    return 'Pending Review';//return Html::a('Approve <span class="fa  fa-hand-o-right"></span>', ['approve', 'id' =>$dataProvider['id']], ['class' => 'btn btn-primary btn-sm']).'<br/> <br/> '.Html::a('Reject <span class="fa  fa-hand-o-right"></span>', ['reject', 'id' =>$dataProvider['id']], ['class' => 'btn btn-danger btn-sm']);
                    else if($dataProvider['status']==1)
                    return 'Approved';
                    else if($dataProvider['status']==2)
                    return 'Rejected';
                },
            ],
            ],
        ],
    ]); ?>
</div>
    </div>
</div>
</div>
   <?= $this->registerJs(
      " 
    $('#request_tree').addClass('active');
    $('#indicator_requests').addClass('active');")
  ?>