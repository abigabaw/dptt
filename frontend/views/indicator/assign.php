<?php
use yii\bootstrap\modal;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\IndicatorType;
use frontend\models\Indicator;
use frontend\models\Stage;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\MilestoneStage */

$this->title = 'Assign Data Type';
$this->params['breadcrumbs'][] = ['label' => 'Indicator Type List', 'url' => ['types']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-stage-create">
<div class="box box-success">
            <div class="box-body">
	<?php $form = ActiveForm::begin(['id' => 'indicator-type-map-form']); ?>
    <?= $form->field($model, 'indicator_type',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(IndicatorType::find()->all(),'id','name'),
    ['prompt'=>'Select Indicator Value'] )?>

    <?= $form->field($model, 'indicator')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Indicator::find()->all(),'id','name'),
                        'options' => ['placeholder' => 'Select Indicator (s)'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => true
                        ],
                ]);
    ?>
<div class="form-group">
      <?= Html::submitButton('Assign Data Type', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    
    </div>
</div>
