<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Milestone */

$this->title = 'Update Milestone';
$this->params['breadcrumbs'][] = ['label' => 'Milestones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->milestone, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="milestone-update">
<div class="box box-success">
            <div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'milestone-form']); ?>
					 <?= $form->field($model, 'milestone',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Milestone Name',]) ?>
					<?= $form->field($model, 'num',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Milestone Num',]) ?>
					<?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'=>'div',
					                			'class'=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'Brief description of Milestone',]) ?>
					<div class="form-group">
				      <?= Html::submitButton('Create Milestone', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#stage_nav').addClass('active');")
  ?>