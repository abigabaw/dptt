<?php
	
use yii\widgets\Pjax;
use yii\web\JsExpression;
use execut\widget\TreeView;
use yii\helpers\Url;
use yii\helpers\Html;


$this->title = 'Viewing Pipeline: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-success">

            <div class="box-body">

    <p>
        <?= Html::a('Update Name', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Rearrange Stages', ['stages', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Table View', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete Pipeline', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div></div>
<?php
Pjax::begin([
    'id' => 'pjax-container',
]);

echo \yii::$app->request->get('page');

Pjax::end();

$onSelect = new JsExpression(<<<JS
function (undefined, item) {
    if (item.href !== location.pathname) {
        $.pjax({
            container: '#pjax-container',
            url: item.href,
            timeout: null
        });
    }

    var otherTreeWidgetEl = $('.treeview.small').not($(this)),
        otherTreeWidget = otherTreeWidgetEl.data('treeview'),
        selectedEl = otherTreeWidgetEl.find('.node-selected');
    if (selectedEl.length) {
        otherTreeWidget.unselectNode(Number(selectedEl.attr('data-nodeid')));
    }
}
JS
);

echo TreeView::widget([
    'data' => $data,//$items,
    'size' => TreeView::SIZE_NORMAL,
	 'header' => '',
	    'searchOptions' => [
	        'inputOptions' => [
	            'placeholder' => 'Search Stages/Milestones/Indicators'
	        ],
		  ],
    'clientOptions' => [
        'onNodeSelected' => $onSelect,
		  
    ],
]);

?>