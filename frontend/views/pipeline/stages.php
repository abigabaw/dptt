<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;
use yii\helpers\ArrayHelper;
use frontend\models\Pipeline;
use frontend\models\PipelineStage;


$this->title = $pipeline_model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pipeline-stages">
<div class="box box-success">
<div class="box-header bg-success with-border">
            <h3 class="box-title">Rearrange Stages</h3>
            </div>
            <div class="box-body">
    <?php
            $items = array();
              if( is_array( $dataProvider ) && count( $dataProvider ) > 0 ) {
              $count = 1;
                foreach($dataProvider as $v)
                {
                    $items[$v['stage_id']]['content'] = $count.' - '.$v['stage'];
                    
                    ++$count;
                }
            $form = ActiveForm::begin();
             
            // Kartiks widget
                echo SortableInput::widget([
                'name'=> 'sort_list',
                'items' => $items,
                'hideInput' => true,
                ]);
            echo Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']);

            ActiveForm::end();
          }else{
            echo 'There are no stages in this pipeline.';
          }
?>

</div>
</div></div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>