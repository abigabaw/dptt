<?php
use kartik\growl\Growl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use frontend\models\VStage;
use frontend\models\VMilestone;
use frontend\models\VIndicator;
use frontend\models\Project;
use kartik\select2\Select2;
use kartik\alert\Alert;
/* @var $this yii\web\View */
/* @var $model backend\models\Pipeline */

$this->title = $model->name. ' Pipeline';
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  .select2-selection__rendered li{
    width:100%;
  }
  .select2-selection__rendered li input{
    width:100%!important;
  }
</style>
<div class="pipeline-view">
<div class="box box-success">

            <div class="box-body">

    <p>
        <?= Html::a('Update Name', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Rearrange Stages', ['stages', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		  <?= Html::a('Tree View', ['viewtree', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete Pipeline', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
</div></div>
          <div class="row">
            <div class="col-md-12">
            <?php 

	  foreach (Yii::$app->session->getAllFlashes() as $key => $flash) {	
		  if($key=='success')
					echo Alert::widget([
					    'type' => Alert::TYPE_SUCCESS,
					    'title' => $flash['title'],
					    'icon' => 'glyphicon glyphicon-ok-sign',
					    'body' => $flash['message'],
					    'showSeparator' => true,
					    'delay' => 22000
					]);
            }
            ?>
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <?= $pipleinetablist?>
                   <li>
                   <?php 
                   Modal::begin([
                        'id' => 'createCompany',
                        // ... any other yii2 bootstrap modal option you need
                        'header' => '<h4 class="modal-title">Add Stage</h4>',
                        'toggleButton' => ['label' => '<i class="fa  fa-plus"></i> Add Stage','class'=>'btn btn-primary btn-sm','style'=>'margin-top:3px;'],
                    ]);


                   $form = ActiveForm::begin(['id' => 'cohort-form']);
                         echo $form->field($pipelinestage, 'stage')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VStage::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','stage'),
                                                'options' => ['placeholder' => 'Select Stage (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true
                                                ],
                                        ]);
                         echo '<div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>';
                        ActiveForm::end();
                    Modal::end(); ?>
                   </li>
                  <li class="pull-right header"> Stages <i class="fa fa-dedent "></i></li>
                </ul>
                <div class="tab-content">
                  <?= $pipleinetabcontentlist ?>
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
          </div> <!-- /.row -->
<!-- Large modal -->


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add New Milestone</h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'milestone-form','action' =>['pipeline/milestones']]);
                         echo $form->field($milestonestage, 'milestone')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VMilestone::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','milestone'),
                                                'options' => ['placeholder' => 'Select Milestones (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true
                                                ],
                                        ]);
                                   echo '<div class="modal-footer">
                      <input type ="hidden" name="stage" value="" id="stage_value"/>
                      <input  type="hidden" name="pipeline" value="'.$model->id.'"/>
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Add New Milestone</button>
                    </div>';
                        ActiveForm::end();
                                        ?>



    </div>
  </div>
</div></div>

<div class="modal fade bs-indicator-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Add New Indicator </h4>
      </div>
      <div class="modal-body">
      <?php 
                         $form = ActiveForm::begin(['id' => 'indicator-form','action' =>['pipeline/indicators']]);
                          echo $form->field($indicatormilestone, 'indicator')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(VIndicator::find()->where(['institution_id'=>Yii::$app->user->identity->institution])->all(),'id','name'),
                                                'options' => ['placeholder' => 'Select Indicators (s)'],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'multiple' => true,
                                                ],
                                        ]);

              echo '<div class="modal-footer">
                        <input  type="hidden" name="milestone" value="" id="milestone_value"/>
                        <input  type="hidden" name="pipeline" value="'.$model->id.'"/>
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add New Indicator</button>
                    </div>';
                        ActiveForm::end();
		  
                                        ?>


    </div>
  </div>
</div></div>
  <?= $this->registerJs(
      " 
    $('.alert').fadeIn('slow').delay(3000).hide(0);
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>
  <?= $this->registerJs(
      ' 
$("#pipelinestage-stage").select2({
    placeholder: \'Select or type a new stage if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

  $("#milestonestage-milestone").select2({
    placeholder: \'Select or type a new milestone if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});

$("#staticindicatormilestone-indicator").select2({
    placeholder: \'Select or type a new indicator if not in list\',
    width: \'100%\' ,
    tags: true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true
    }
  }
});
$(".milestone_button").click(function(){
    $("#stage_value").val($(this).attr("id"));
});
$(".indicator_button").click(function(){
    $("#milestone_value").val($(this).attr("id"));
    //alert($("#milestone_value").val());
});

');
?>
