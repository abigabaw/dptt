<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Pipeline */

$this->title = 'Update Pipeline: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pipelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pipeline-update">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['id' => 'pipeline-form']); ?>
    <?= $form->field($model, 'name',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter the name of the portfolio',]) ?>

    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a description of this portfolio',]) ?>

	<div class="form-group">

      <?= Html::submitButton('Update Pipeline', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#pipeline_nav').addClass('active');")
  ?>