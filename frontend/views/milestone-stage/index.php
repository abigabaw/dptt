<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\controllers\MilestoneStageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Milestone Stages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-stage-index">
<div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Milestone Stage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             [
             'attribute' => 'Milestone',
             'value' => 'milestone0.milestone'
             ],
             [
             'attribute' => 'Stage',
             'value' => 'stage0.stage'
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
    </div>
</div>
