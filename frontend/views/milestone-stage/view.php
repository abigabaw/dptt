<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MilestoneStage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Milestone Stages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="milestone-stage-view">
 <div class="box box-success">
    <div class="box-header bg-success with-border">
                      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                </div>
                <div class="box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
             [   'label' =>'Milestone',
                'value' =>$model->milestone0->milestone,
             ],
            [   'label' =>'Grant',
                'value' =>$model->stage0->stage,
            ],
        ],
    ]) ?>

</div>
</div>
</div>
</div>
