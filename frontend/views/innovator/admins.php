<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InnovatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Admins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-index">
<div class="box box-success">
            <div class="box-body">
                       
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'email:email',
            'name',
            'gender',
            // 'gender',
            // 'country',
            // 'status',
            // 'created_at',
            // 'updated_at',
        ],
    ]); ?>
</div>
</div>
</div>
 <?= $this->registerJs(
      " 
      $('#project_tree').addClass('active');
    $('#project_admins').addClass('active');")

  ?>