<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovator */

$this->title = $model->names;
$this->params['breadcrumbs'][] = ['label' => 'Innovators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add User', ['users','id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
                ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                   [   'label' =>'Institution',
                 'value' =>$model->instituition0->names,
             ],
            'email:email',
            'names',
            'description:ntext',
            [   'label' =>'Country',
                 'value' =>$model->country0->name,
             ],
        ],
    ]) ?>
</div>
</div>
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Users in this Project Team</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <?= $users?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_teams').addClass('active');")

  ?>
