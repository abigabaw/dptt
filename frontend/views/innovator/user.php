<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovator */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Innovators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-view">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">

    <p>
        <?= Html::a('Update', ['update-user', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
                ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
             'attributes' => [
            'name',
            'email:email',
            'gender',
        		],])
    		 ?>
</div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_teams').addClass('active');")

  ?>