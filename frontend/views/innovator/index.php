<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InnovatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Teams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="innovator-index">
<div class="box box-success">
<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Project Team', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                         [
            'attribute'=>'Name',
            'format'=>'raw',
            'value'=>function($dataProvider){
                return Html::a($dataProvider['names'], ['view', 'id' => $dataProvider['id']],['style' => 'color:#000']);
                  
            }
            ],
            'instituition0.names',
            'email:email',
            'description:ntext',
            // 'gender',
            // 'country',
            // 'status',
            // 'created_at',
            // 'updated_at',
        ],
    ]); ?>
</div>
</div></div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_teams').addClass('active');")

  ?>