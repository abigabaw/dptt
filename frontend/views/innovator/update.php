<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Institution;
use frontend\models\Country;

/* @var $this yii\web\View */
/* @var $model backend\models\Innovator */

$this->title = 'Update Project Team: ' . ' ' . $model->names;
$this->params['breadcrumbs'][] = ['label' => 'Innovators', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->names, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="innovator-update">
<div class="box box-success">
			<div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'innovator-form']); ?>
	<?= $form->field($model, 'names',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Full Name of the Project Team',]) ?>
	<?= $form->field($model, 'instituition',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Institution::find()->all(),'id','names'),
	['prompt'=>'Select Insititution'] )?>

    <?= $form->field($model, 'email',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Email Address of the Project Team',]) ?>
    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'A brief description of the Project Team','rows'=>6,]) ?>

     <?= $form->field($model, 'country',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Country::find()->all(),'id','name'),

	['prompt'=>'Select Country'] )?>
	<div class="form-group">
      <?= Html::submitButton('Update Project Team', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
	<?php ActiveForm::end(); ?>

</div>
</div>
</div>
    <?= $this->registerJs(
      " 
    $('#project_tree').addClass('active');
    $('#project_teams').addClass('active');")

  ?>
