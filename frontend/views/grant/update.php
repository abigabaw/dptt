<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Agency;

/* @var $this yii\web\View */
/* @var $model backend\models\Grant */

$this->title = 'Update Grant: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Grants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grant-update">
<div class="box box-success">
            <div class="box-body">
			<?php $form = ActiveForm::begin(['id' => 'grant-form']); ?>
					 <?= $form->field($model, 'name',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textInput(['placeholder'=>'Grant Name',]) ?>
					 <?= $form->field($model, 'agency',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map(Agency::find()->all(),'id','name'),
	['prompt'=>'Please the agency that provided this grant'] )?>
                <?= $form->field($model, 'description',
					                ['options'=>[
					                			'tag'			=>'div',
					                			'class'			=>'form-group fieldcohort-form-name has-feedback required'],
					                			'template'		=>'{input}{error}{hint}'
					                ])->textArea(['placeholder'=>'A brief description of the grant',]) ?>
					<div class="form-group">
				      <?= Html::submitButton('Update Grant', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
				    </div>
			<?php ActiveForm::end(); ?>

</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#grant_nav').addClass('active');")
  ?>