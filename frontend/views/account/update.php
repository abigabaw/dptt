<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LoginInstitution */

$this->title = 'Update Details: ';
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="login-institution-update">
<div class="box box-success">
            <div class="box-header bg-success with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin(['id' => 'milestone-form']); ?>
                <?= $form->field($model, 'name',
                    ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group fieldcohort-form-name has-feedback required'],
                        'template'=>'{input}{error}{hint}'
                    ])->textInput(['placeholder'=>'Enter your Name Here',]) ?>
                <?= $form->field($model, 'email',
                    ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group fieldcohort-form-name has-feedback required'],
                        'template'=>'{input}{error}{hint}'
                    ])->textInput(['placeholder'=>'Enter your email address here',]) ?>
				<div class="form-group">
			      <?= Html::submitButton('Update Details', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
			    </div>
                <?php ActiveForm::end(); ?>

            </div>
</div>
</div>
  <?= $this->registerJs(
    " 
    $('#account_nav').addClass('active');")
  ?>;