<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use frontend\models\Pipeline;
use frontend\models\Grant;
/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */

$this->title = 'Update Portfolio: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cohort-update">
<div class="box box-success">
	<div class="box-header bg-success with-border">
	 	<h3 class="box-title"><?= Html::encode($this->title) ?></h3>
	</div>
    <div class="box-body">
	        <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
    <?= $form->field($model, 'name',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textInput(['placeholder'=>'Please enter the name of the portfolio',]) ?>

    <?= $form->field($model, 'pipeline',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(Pipeline::find()->all(),'id','name'),
    ['prompt'=>'Please Select A Pipeline'] )?>
    
                <?= $form->field($model, 'grant',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
    ArrayHelper::map(Grant::find()->all(),'id','name'),
    ['prompt'=>'Select the Agency for this portfolio '] )?>
    <?= $form->field($model, 'description',
                ['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-name has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->textArea(['placeholder'=>'Please enter a description of this portfolio',]) ?>
	<div class="form-group">
      <?= Html::submitButton('Update Portfolio', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
	</div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#program_nav').addClass('active');")
  ?>