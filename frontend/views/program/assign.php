<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Cohort */

$this->title = 'Add Project to pipeline';
$this->params['breadcrumbs'][] = ['label' => 'Cohorts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cohort-create">
<div class="box box-success">
			<div class="box-header with-border">
                  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="box-body">
    <?php $form = ActiveForm::begin(['id' => 'cohort-form']); ?>
    <?= $form->field($model, 'project',
				['options'=>[
                'tag'=>'div',
                'class'=>'form-group fieldcohort-form-pipeline has-feedback required'],
                'template'=>'{label}{input}{error}{hint}'
                ])->dropDownList(
	ArrayHelper::map($projects,'id','name'),
	['prompt'=>'Please Select A project'] )?>

	<div class="form-group">

      <?= Html::submitButton('Assign Project to pipeline', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
  <?= $this->registerJs(
      " 
    $('#adminitrator_tree').addClass('active');
    $('#program_nav').addClass('active');")
  ?>