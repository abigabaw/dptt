<?php

namespace frontend\controllers;

use Yii;
use frontend\models\StaticIndicatorMilestone;
use frontend\models\Indicator;
use frontend\controllers\IndicatorMilestoneSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * IndicatorMilestoneController implements the CRUD actions for IndicatorMilestone model.
 */
class IndicatorMilestoneController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IndicatorMilestone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndicatorMilestoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IndicatorMilestone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IndicatorMilestone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StaticIndicatorMilestone();

        if(Yii::$app->request->post()){
                $data = Yii::$app->request->post();
                $indicators = $data['StaticIndicatorMilestone']['indicator'];
                $record = array();
                foreach($indicators as $indicator) {
                    if( filter_var($indicator, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'milestone'     =>$data['StaticIndicatorMilestone']['milestone'],
                                'indicator'     =>$indicator
                            ];
                    }else{
                            $indicator_model = new Indicator ();
                            $indicator_model->name = $indicator;
                            $indicator_model->save();
                            $record[] = [
                                'milestone'     =>$data['StaticIndicatorMilestone']['milestone'],
                                'indicator'     =>$indicator_model->id
                            ];
                    }
                }
                if(count($record)>0){
                        $columnNameArray=['milestone','indicator'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'static_indicator_milestone', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['milestone']);
                }
        }else {
            return $this->render('create', [
                'model' => $model,
                'milestone'=>false
            ]);
        }
    }
    /**
     * Creates a new IndicatorMilestone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIndicators($id)
    {
        $model = new StaticIndicatorMilestone();

        if(Yii::$app->request->post()){
                $data = Yii::$app->request->post();
                $indicators = $data['StaticIndicatorMilestone']['indicator'];
                $record = array();
                foreach($indicators as $indicator) {
                    if( filter_var($indicator, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'milestone'     =>$id,
                                'indicator'     =>$indicator
                            ];
                    }else{
                            $indicator_model = new Indicator ();
                            $indicator_model->name = $indicator;
                            $indicator_model->save();
                            $record[] = [
                                'milestone'     =>$id,
                                'indicator'     =>$indicator_model->id
                            ];
                    }
                }
                if(count($record)>0){
                        $columnNameArray=['milestone','indicator'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'static_indicator_milestone', $columnNameArray, $record
                                         )
                                       ->execute();
                         return $this->redirect(['/milestone/view', 'id' => $id]);
                }
        }else {
            return $this->render('create', [
                'model'         => $model,
                'milestone'     => $id
            ]);
        }
    }
    /**
     * Updates an existing IndicatorMilestone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IndicatorMilestone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IndicatorMilestone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IndicatorMilestone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IndicatorMilestone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
