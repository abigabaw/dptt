<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\Pipeline;
use frontend\models\PipelineSearch;
use frontend\models\PipelineStage;
use app\models\VPipelineStage;
use frontend\models\VMilestoneStage;
use frontend\models\MilestoneStage;
use frontend\models\Indicator;
use frontend\models\Milestone;
use frontend\models\Stage;
use frontend\models\IndicatorMilestone;
use frontend\models\StaticIndicatorMilestone;
use frontend\models\VPipelineStageMilestoneStaticindicator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;

/**
 * PipelineController implements the CRUD actions for Pipeline model.
 */
class PipelineController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
	   'access' => [
              'class' => \yii\filters\AccessControl::className(),
              'rules' => [
                  // allow authenticated users
                  [
				//'actions'=>['index','view','viewtree'],
                      'allow' => true,
                      'roles' => ['@'],
                  ],
                  // everything else is denied
              ],
          ],            

        ];
    }

    /**
     * Lists all Pipeline models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PipelineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pipeline model.
     * @param integer $id
     * @return mixed
     */
	 public function actionViewtree($id)
	 {
		 $model = $this->findModel($id);
		 $stages=VPipelineStage::find()->where(['pipeline'=>$id])->orderBy(['ordering'=>SORT_ASC])->all();
//		 print_r($stages);
		$items=[];
		foreach($stages as $k=>$stage) {
			$milestones=VMilestoneStage::find()->where(['stage_id'=>$stage['stage'],'pipeline_id'=>$id])->distinct()->orderBy(['milestone_ordering'=>SORT_ASC])->all();
			$nodes=[];
			foreach($milestones as $km=>$milestone) {
				$nodes[$km]=[
					'text'=>($km+1).'. '.$milestone['milestone']
				];
			}
			$items[$k]=[
					'text'=>($k+1).'. '.$stage['stage_name'],
					'href'=>"javascript: $('#tree').treeview('toggleNodeExpanded', [ nodeId, { silent: true } ]);",
				   	'state'=>['expanded'=>false],
					'nodes'=>$nodes
			];
		}
		return $this->render('viewtree',['model'=>$model,'data'=>$items]);
	 }
    public function actionView($id)
    {
//      $this->layout = 'pipeline';
        $model = new Pipeline();
        $pmodel = $this->findModel($id); 
        $pipelinestage = new PipelineStage;
        $milestonestage = new MilestoneStage;
        $indicatormilestone = new StaticIndicatorMilestone;
        $pipelines = Yii::$app->db->createCommand('SELECT a.id, a.stage as stage_id, b.stage as stage, a.ordering FROM pipeline_stage a LEFT JOIN stage b on a.stage=b.id WHERE a.pipeline='.$id.' order by ordering')->queryAll();
        // $pipelines = VPipelineStageMilestoneStaticindicator::find()
        //                                                            ->where(['pipeline_id'=>$id])
        //                                                            ->groupBy('stage_id')->all();

        $pipleinetablist = '';
        $pipleinetabcontentlist = '';
        $count = 0;
        foreach ($pipelines as $pipeline) {
        if($count==0){
           $pipleinetablist.='<li class="active"><a href="#tab_'.$pipeline['stage_id'].'" data-toggle="tab">'.ucwords($pipeline['stage']).'</a></li>';
           $pipleinetabcontentlist .= '<div class="tab-pane active" id="tab_'.$pipeline['stage_id'].'">
                                        <div class="box-group" id="accordion">
                                            <h4>Milestones</h4>
                                            <button class="btn btn-primary btn-sm milestone_button" id="'.$pipeline['stage_id'].'" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right;margin-top:-35px;"><i class="fa  fa-plus"></i>  Add Milestone</button>

                                        '.$this->getMilestones($id,$pipeline['stage_id']).'
                                        </div>
                                      </div><!-- /.tab-pane -->';
        }else{
           $pipleinetablist.='<li ><a href="#tab_'.$pipeline['stage_id'].'" data-toggle="tab">'.ucwords($pipeline['stage']).'</a></li>';
           $pipleinetabcontentlist .= '<div class="tab-pane " id="tab_'.$pipeline['stage_id'].'">
                                            <h4>Milestones</h4>
                                            <button class="btn btn-primary btn-sm milestone_button" id="'.$pipeline['stage_id'].'" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right;margin-top:-35px;"><i class="fa  fa-plus"></i>  Add Milestone</button>

                                        <div class="box-group" id="accordion">'.$this->getMilestones($id,$pipeline['stage_id']).'

                                        </div>
                                      </div><!-- /.tab-pane -->';
        }
        $count++;
        }

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $stages = $data['PipelineStage']['stage'];
                $record = array();
                foreach($stages as $stage) {
                   if( filter_var($stage, FILTER_VALIDATE_INT) !== false ){
                          $record[] = [
                                'pipeline'      =>$pmodel->id,
                                'stage'         =>$stage
                            ];
                    }else{
                    $stage_model = new Stage();
                    $stage_model->stage = $stage;
                    $stage_model->description = $stage;
                    $stage_model->save();
                    $record[] = [
                                'pipeline'      =>$pmodel->id,
                                'stage'         =>$stage_model->id
                            ];
                    }
                }
                if(count($record)>0){
                        $columnNameArray=['pipeline','stage'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'pipeline_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        Yii::$app->getSession()->setFlash('success', ['key'=>'Changes Saved','message'=>'Changes have been made. You can link a project to this pipeline clicking on the porfolio link']);           
                        return $this->redirect(['view','id' => $pmodel->id]);
                }
        }else{

        return $this->render('view', [
            'model' => $pmodel,
            'pipelinestage' => $pipelinestage,
            'milestonestage' => $milestonestage,
            'indicatormilestone' => $indicatormilestone,
            'pipleinetablist'=>$pipleinetablist,
            'pipleinetabcontentlist'=>$pipleinetabcontentlist
        ]);
      }
    }
    private function getMilestones($pipeline,$stage){
       $milestonelist='';
       //$milestones = Yii::$app->db->createCommand('SELECT id, milestone as milestone_id, (SELECT milestone FROM milestone WHERE milestone_stage.milestone=milestone.id) as milestone, ordering FROM milestone_stage WHERE stage='.$id.' group by ordering order by ordering')->queryAll();
        $milestones = VPipelineStageMilestoneStaticindicator::find()
                                                                   ->where(['pipeline_id'=>$pipeline])
                                                                   ->where(['stage_id'=>$stage])
                                                                   ->groupBy(['milestone_id'])
                                                                   ->all();
        foreach ($milestones as $milestone) {
           $milestonelist.= '<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                              <div class="box-header with-border">
                                <h4 class="box-title">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$milestone['milestone_id'].'">
                                     '.$milestone['milestone'].'
                                  </a>
                                </h4>
                              </div>
                              <div id="collapse'.$milestone['milestone_id'].'" class="panel-collapse collapse">
                                <div class="box-body">
                                <h4>Indicators</h4>
                                <ol>
                                '.$this->getIndicators($pipeline,$stage,$milestone['milestone_id']).'
                                </ol>
                                <button class="btn btn-primary btn-sm indicator_button" id="'.$milestone['milestone_id'].'" data-toggle="modal" data-target=".bs-indicator-modal-lg" style="float:right;margin-top:-4px;"><i class="fa  fa-plus"></i>  Add Indicator</button>

                                    </div>
                              </div>
                            </div>';
        }
        if($milestonelist)
       return $milestonelist;
       else
       return 'This stage has no milestones added yet.';
    }

    private function getIndicators($pipeline,$stage,$milestone){
      //return $stage.' '.$milestone;
       $indicatorlist='';
       $indicators = VPipelineStageMilestoneStaticindicator::find()
                                                                   ->where(['stage_id'=>$stage,'milestone_id'=>$milestone])
                                                                   ->groupBy('indicator_id')
                                                                   ->all();
      foreach ($indicators as $indicator) {
          $indicatorlist .='<li>'.$indicator['indicator'].'</li>';
       }
       if($indicatorlist)
       return $indicatorlist;
       else
       return 'This Milestone has no indicators added yet.'; 
    }

    public function actionSort($id)
    {
             $model = new Pipeline();  
             $dataProvider = Yii::$app->db->createCommand('SELECT id, stage as stage_id, (SELECT stage FROM stage WHERE pipeline_stage.stage=stage.id) as stage, ordering FROM pipeline_stage WHERE pipeline='.$id.' order by ordering')->queryAll();

            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE pipeline_stage SET ordering = :ordering WHERE stage = :stage";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':stage', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 return $this->redirect(['sort','id' => $id]);
            } else {
                 return $this->render('stages', ['dataProvider' => $dataProvider, 'model' => $model]);
            }
    }

    public function actionStages($id)
    {
             $pipeline_model = $this->findModel($id);
             $model = new Pipeline();  
             $dataProvider = Yii::$app->db->createCommand('SELECT id, stage as stage_id, (SELECT stage FROM stage WHERE pipeline_stage.stage=stage.id) as stage, ordering FROM pipeline_stage WHERE pipeline='.$id.' order by ordering')->queryAll();

            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE pipeline_stage SET ordering = :ordering WHERE stage = :stage";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':stage', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 Yii::$app->getSession()->setFlash('success', ['title'=>'Stages Saved','message'=>'Stages have been rearranged']);
                 return $this->redirect(['view','id' => $id]);
            } else {
                 return $this->render('stages', ['dataProvider' => $dataProvider, 'model' => $model,'pipeline_model'=>$pipeline_model]);
            }
    }

    /**
     * Creates a new IndicatorMilestone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIndicators()
    {
        $model = new StaticIndicatorMilestone();

        if(Yii::$app->request->post()){
                $data = Yii::$app->request->post();
                $indicators = $data['StaticIndicatorMilestone']['indicator'];
                $record = array();
                foreach($indicators as $indicator) {
                    if( filter_var($indicator, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'milestone'     =>$data['milestone'],
                                'indicator'     =>$indicator
                            ];
                    }else{
                            $indicator_model = new Indicator ();
                            $indicator_model->name = $indicator;
                            $indicator_model->save();
                            $record[] = [
                                'milestone'     =>$data['milestone'],
                                'indicator'     =>$indicator_model->id
                            ];
                    }
                }
                if(count($record)>0){
                        $columnNameArray=['milestone','indicator'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'static_indicator_milestone', $columnNameArray, $record
                                         )
                                       ->execute();
//                        Yii::$app->getSession()->setFlash('changes_success', '');
				Yii::$app->getSession()->setFlash('success', ['title'=>'Changes Saved','message'=>'Changes have been made. You can link a project to this pipeline clicking on the porfolio link']);
           
                        return $this->redirect(['view','id' => $data['pipeline']]);
                }
        }else {
            return $this->redirect(['index']);
        }
    }
/**
     * Creates a new MilestoneStage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionMilestones()
    {
        $model = new MilestoneStage();

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $milestones = $data['MilestoneStage']['milestone'];
                $request = Yii::$app->getRequest();
                if ($request->isPost && $request->post('ajax') !== null) {
                        $data = Yii::$app->request->post('Item', []);
                        foreach (array_keys($data) as $index) {
                            $models[$index] = new MilestoneStage();
                        }
                        Model::loadMultiple($models, Yii::$app->request->post());
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = ActiveForm::validateMultiple($models);
                        return $result;
                }
                $record = array();
                foreach($milestones as $milestone) {
                    if( filter_var($milestone, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'stage'         =>$data['stage'],
                                'milestone'     =>$milestone
                            ];
                    }else {
                        //first add the milestone to the db
                        $milestone_model = new Milestone;
                        $milestone_model->milestone = $milestone;
                        $milestone_model->num = 000;
                        $milestone_model->save();
                        //then add the mapping
                        $record[] = [
                                'stage'         =>$data['stage'],
                                'milestone'     =>$milestone_model->id
                        ];

                    }

                }

                if(count($record)>0){
                        $columnNameArray=['stage','milestone'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'milestone_stage', $columnNameArray, $record
                                         )
                                       ->execute();
                        Yii::$app->getSession()->setFlash('success', ['title'=>'Milestone Saved','message'=>'Your Milestones have been successfully saved. You may continue editing or add a <a href="">portfolio</a> to this pipeline']);
                        return $this->redirect(['view','id' => $data['pipeline']]);
                }
        } else {
            return $this->redirect(['index']);
        }
    }
    /**
     * Creates a new Pipeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pipeline();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['key'=>'Pipeline Created','message'=>'Pipeline Created, you can now complete editing it by adding stages milestones and indicators']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pipeline model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pipeline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
            try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this pipeline has values dependant on it. Please contact your administrator');
            }
        
    }

    /**
     * Finds the Pipeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pipeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pipeline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
