<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Project;
use frontend\models\Indicator;
use frontend\models\ProjectSearch;
use frontend\models\Timeline;
use frontend\models\TimelineSearch;
use frontend\models\TimelineForm;
use frontend\models\Iteration;
use frontend\models\IterationSearch;
use frontend\models\Milestone;
use frontend\models\MilestoneSubmission;
use frontend\models\Attachments;
use frontend\models\ExtensionRequests;
use frontend\models\ExtensionRequestsSearch;
use common\models\IterationRequests;
use common\models\Functions;
use frontend\models\MilestoneSub;
use frontend\models\Pipeline;
use frontend\models\Program;
use frontend\models\VMilestoneStage;
use frontend\models\VPipelineStage;
use frontend\models\VMilestoneSubmission;
use frontend\models\VIndicatorSubmission;
use frontend\models\IndicatorMilestone;
use frontend\models\VStaticIndicatorMilestone;
use frontend\models\IndicatorRequests;
use frontend\models\Innovator;
use frontend\models\Queries;
use app\models\Notification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Action;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\web\Response;
use yii\grid\GridView;
use yii\helpers\Html;
use frontend\models\IndicatorMilestoneSubmission;
use frontend\models\IndicatorMilestoneSubmissionSearch;
use frontend\models\IndicatorMilestoneSubSearch;
use frontend\models\Grading;
use frontend\models\LoginInnovator;
use yii\web\ForbiddenHttpException;
use frontend\models\UserForm;
use yii\web\UploadedFile;
/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //$timeline = Timeline::findOne(['project'=>$id]);
        $timeline = $this->getDifference($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'timeline'=>$timeline,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionGraph($id)
    {
        $sql ='SELECT a.milestone as milestone_id, b.timeline, b.comment, b.start_date, b.duration, c.milestone, c.num FROM timeline a INNER JOIN iteration b ON b.timeline=a.id INNER JOIN milestone c ON a.milestone=c.id WHERE b.project='.$id.' ORDER BY b.timeline ASC';
        $qry = Yii::$app->db->createCommand($sql)->queryAll();
        $timeline = '';
        foreach ($qry as $qry) {
            $timeline.='{desc   :"'.$qry['num'].'",
                         values :[{ 
                                   from:"'.$qry['start_date'].'",
                                   to: "'.date('Y-m-d', strtotime($qry['start_date']. ' + '.$qry['duration'].' days')).'",
                                   label:"'.$qry['num'].'",
                                   customClass:"ganttRed"
                                 }]},'; 
        }

        return $this->render('timelines/graph_stage', [
            'model' => $this->findModel($id),
            'timeline'=>'['.rtrim($timeline,',').']'
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionTimelineGraph($id)
    {
        $model = $this->findModel($id);
        return $this->render('timelines/projection_graph', [
            'model' =>  $model,
        ]);
        //return json_encode($labels).'<br/>'.json_encode($data);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        if(Yii::$app->request->post()){
            //return json_encode(Yii::$app->request->post());
            $data = Yii::$app->request->post();
            if( filter_var($data['Project']['teamleader'], FILTER_VALIDATE_INT) !== false ){
            $model->load($data);
            $model->innovator = LoginInnovator::findOne($data['Project']['teamleader'])->innovator;
            if ($model->save()) {
                Notification::notify(Notification::PROJECT_CREATION,$model->innovator, $model->id);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            }else{
                //create new team
                $innovatormodel                 = new Innovator();
                $innovatormodel->names          = $data['Project']['teamleader'];
                $innovatormodel->instituition   = Yii::$app->user->identity->institution;
                $innovatormodel->save();

                $usermodel = new LoginInnovator();
                $usermodel->name = $data['Project']['teamleader'];
                $email = md5(time());
                $usermodel->email = substr($email,6).'@example.com';
                $usermodel->status = 10;
                $usermodel->innovator = $innovatormodel->id;
                $usermodel->setPassword('123456789xyz');
                $usermodel->generateAuthKey();
                $usermodel->save();
                
                $model->load($data);
                $model->innovator = $innovatormodel->id;
                $model->teamleader = $usermodel->id;
                if ($model->save()) {
                    Notification::notify(Notification::PROJECT_CREATION,$model->innovator, $model->id);
                    return $this->redirect(['view', 'id' => $model->id]);
                }else{
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

            } 

        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this project has values dependant on it. Please contact your administrator');
            }
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


   /**
     * Lists all Timeline models.
     * @return mixed
     */
    public function actionTimelines($id)
    {
        $searchModel = new TimelineSearch();
        $dataProvider = $searchModel->searchTimeline(Yii::$app->request->queryParams,$id);
	  $model=$this->findModel($id);
        return $this->render('timelines/project_timeline_view', [
            	'searchModel' => $searchModel,
            	'dataProvider' => $dataProvider,
            	'project'=>$id,
			'model'=>$model,
        ]);
    }

        /**
     * Creates a new Timeline model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionTimeline($id)
    {
        $models = $this->getItems($id);
        if(!$models){
         return $this->redirect(['view', 'id' => $id]);
        }
        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $data = Yii::$app->request->post('Item', []);

            foreach (array_keys($data) as $index) {
                $models[$index] = new TimelineForm();
            }
            Model::loadMultiple($models, Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validateMultiple($models);
            return $result;
        }

        if (Model::loadMultiple($models, Yii::$app->request->post())) {
                $model = new TimelineForm();
                if($model->addTimeline($id,$models,Yii::$app->user->identity->id))
                return $this->redirect(['view', 'id' => $id]);
                
        }
        return $this->render('timelines/project_timeline', [
                'models' => $models,
                'project'=>$id
            ]);
    }
    private function getDifference($id){
        $pipeline = Program::findOne(Project::findOne($id)->program)->pipeline;
        $data = VMilestoneStage::find()
                ->where(['pipeline_id'=>$pipeline])
                ->groupBy('milestone_id')
                ->orderBy('milestone_id')
                ->all();

        $data2 = Timeline::find()->where(['project'=>$id])->orderBy('milestone')->all();
        $record1=array();
        $record2=array();
        foreach ($data as $data) {
            $record2[] = $data['milestone_id'];
        }
        foreach ($data2 as $data2) {
            $record1[] = $data2['milestone'];
        }
        $milestones = array_diff($record2, $record1);
        return count($milestones);
    }

    private function getItems($id)
    {
        $pipeline = Program::findOne(Project::findOne($id)->program)->pipeline;
        $data = VMilestoneStage::find()
                ->where(['pipeline_id'=>$pipeline])
                ->groupBy('milestone_id')
                ->orderBy('milestone_id')
                ->all();

        $data2 = Timeline::find()->where(['project'=>$id])->orderBy('milestone')->all();
        $record1=array();
        $record2=array();
        foreach ($data as $data) {
            $record2[] = $data['milestone_id'];
        }
        foreach ($data2 as $data2) {
            $record1[] = $data2['milestone'];
        }
        $milestones = array_diff($record2, $record1);

        $record="";
        $count = 1;
        foreach ($milestones as $d=>$val) {
            $record[] = array(
                'num'=>$count,
                'milestone'=>$val,
                'milestone_hidden'=>$val,
                'duration'=>'',
                'iteration'=>'',
                'start_date'=>''
                );
            $count ++;
        }

        $items = [];
        foreach ($milestones as $row=>$val) {
            $item = new TimelineForm();
            $item->setAttributes($row);
            $item->milestone = $val;
            $items[] = $item;
        }
        return $items;
    }

    private function getTimelineValue($project,$milestone,$case){
        $timeline = Timeline::findOne(['project'=>$project,'milestone'=>$milestone]);
        if($timeline){

        }else{
            return '';
        }
    }

     /**
     * Displays a single Timeline model.
     * @param integer $id
     * @return mixed
     */
    public function actionTimelineEntry($id)
    {
        $model = $this->findModelTimeline($id);

        $searchModel = new IterationSearch();
        $searchModelExt = new ExtensionRequestsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $dataProviderExtension = $searchModelExt->search($model->milestone,$model->project);
        
        return $this->render('timelines/timeline_entry_view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderExtension'=>$dataProviderExtension,
            'project'=>$id,
            'timeline_detail'=>$this->findSearchTimelineDetails($id)
        ]);
    }
    public function actionRequestIndicator(){
       $model = new IndicatorRequests();
       $time  = time();
       $data = Yii::$app->request->post();
       if ($data) {
       $indicators = $data['IndicatorRequests']['indicator_id'];
       $record = array();
        foreach($indicators as $indicator) {
                    if( filter_var($indicator, FILTER_VALIDATE_INT) !== false ){
                           $record[] = [
                                'indicator_id'    => $indicator,
                                'indicator_name'  => NULL,
                                'indicator_type'  => $data['IndicatorRequests']['indicator_type'],
                                'milestone'       => $data['milestone'],
                                'project'         => $data['project'],
                            ];
                    }else {
                        //first add the milestone to the db
                               $record[] = [
                                'indicator_id'    => NULL,
                                'indicator_name'  => $indicator,
                                'indicator_type'  => $data['IndicatorRequests']['indicator_type'],
                                'milestone'       => $data['milestone'],
                                'project'         => $data['project'],
                            ];
                    }
        }
        if(count($record)>0){
                        $columnNameArray=['indicator_id','indicator_name','indicator_type','milestone','project'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'indicator_requests', $columnNameArray, $record
                                         )
                                       ->execute();

                        $institution = Project::findOne($data['project'])->institution;
                        Notification::notify(Notification::INDICATOR_REQUEST,$institution,$model->id);      
                        
                                                  
                }
                return $this->redirect(['submit', 'id'        => $data['iteration'], ]);

        } 
        return $this->redirect(['index']);
    }

    public function actionRequestIterate(){
       $model = new IterationRequests();
       $data = Yii::$app->request->post();
       if ($data) {
       $model->reason = $data['IterationRequests']['reason'];
       $model->timeline = $data['timeline'];
       $model->save();
       $institution = Project::findOne($data['project'])->institution;
       Notification::notify(Notification::ITERATION_REQUEST,$institution, $model->id);
       return $this->redirect(['review', 'id'        => $data['milestone'],
                                         'project'   => $data['project']
                                   ]);
        }
        return $this->redirect(['index']);
    }

    public function actionSubmit($id){
        $iteration = Iteration::findOne($id);
        $timeline = Timeline::findOne($iteration->timeline); 
        $milestone = $timeline->milestone;
        $project=$timeline->project;
        $model = new MilestoneSubmission();
        $indicatormodel = new IndicatorMilestoneSubmission();
        $indicatorrequestmodel = new IndicatorRequests();
        $data = Yii::$app->request->post();
        $request = IndicatorRequests::findOne(['milestone'=>$milestone,'project'=>$project,'status'=>0]);

        if ($model->load( $data)) { 
            $model->iteration = $id;
            
            //return $model->attachment;
            if($model->save()){
                //Save attachments if any;    
                $model->file = UploadedFile::getInstances($model, 'file');
                if ($model->upload($project,$milestone,$model->id)) {
                  $model->file = null;
                }

                //save indicators if any
                if(isset($data['static_indicator']))
                $this->saveIndicators($data,$data['static_indicator'],$model->id,'static');
                if(isset($data['dynamic_indicator']))
                $this->saveIndicators($data,$data['dynamic_indicator'],$model->id,'dynamic');
                //check if this is a repeat of a milestone. If so update the submit status to 1
                if($iteration->iteration_type==3||$iteration->iteration_type==4){
                $iteration_request = IterationRequests::findOne($iteration->timeline);
                if($iteration_request){
                $iteration_request->submit_status =1;
                $iteration_request->save();
                }
                }
                $iteration->end_date = $model->created_at;
                $iteration->save();
                $instituition = Project::findOne($project)->institution;
                Notification::notify(Notification::MILESTONE_SUBMISSION,$instituition, $model->id);
                return $this->redirect(['review', 'id' =>$milestone,'project'=>$project]);
            }
            return json_encode($model->errors);
            return $this->redirect(['review', 'id' =>$milestone,'project'=>$project]);
        } else {
            return $this->render('submission/submit', [
                'model' => $model,
                'indicatormodel'=>$indicatormodel,
                'milestone'=>$milestone,
                'project'=>$project,
                'request'=>$request,
                'iteration'=>$id,
                'indicatorrequestmodel'=>$indicatorrequestmodel,
                'indicators'=>$this->renderPartial('submission/indicators',['milestone'=>$milestone,'project'=>$project])
            ]);
        }

    }

    private function saveIndicators($data,$indicators,$id,$type){
        foreach ($indicators as $key => $indicator) {
            if($type == 'static')
                $indicator_ids = $data['static_indicator_ids'];
            else
                $indicator_ids = $data['dynamic_indicator_ids'];

                 $record[]  = [
                                            'milestone_submission'         =>$id,
                                            'indicator'                    =>$indicator_ids[$key],
                                            'value'                        =>$indicator,
                                            'created_at'                   =>time(),
                                            'created_by'                   =>Yii::$app->user->identity->id
                              ];
                                # code...
                    }
                    if(count($record)>0){
                        $columnNameArray    =['milestone_submission','indicator','value','created_at','created_by'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount        = Yii::$app->db->createCommand()
                                            ->batchInsert(
                                             'indicator_milestone_submission', $columnNameArray, $record
                                         )
                                       ->execute();
                    }
    }

        /**
     * Performs submisson of a milestone
     * If deletion is successful, the browser will be redirected to the 'submit' page.
     * @param integer $id (milestone id)
     * @return mixed
     */

    public function actionReview($id,$project){
        $iteratemodel = new IterationRequests();
        $timeline = Timeline::findOne(['project'=>$project,'milestone'=>$id])->id;
        $searchModel = new IterationSearch();
        $dataProvider = $searchModel->searchReview(Yii::$app->request->queryParams,$timeline);

        $iterate = IterationRequests::findOne(['timeline'=>$timeline]);
        //$model = $this->findModelIteration($id,$project);
        $milestone_status = VMilestoneSubmission::find()
                                            ->where(['milestone_id'=>$id,'project_id'=>$project,'grading_status'=>1])->one();
        $submissons = VMilestoneSubmission::find()
                                            ->where(['milestone_id'=>$id,'project_id'=>$project])
                                            ->orderBy('id DESC')
                                            ->all();

        $milestones=[];
        foreach ($submissons as $km => $submisson) {
            $attachments    = Attachments::find()->where(['milestone_submission'=>$submisson['id']])->all();
            $indicators     = IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$submisson['id']])->all();
            $nodes_a=[];
            $nodes_i=[];
            foreach ($attachments as $ka => $attachment) {
                $nodes_a[$ka]=[
                                    'id'        =>$attachment['id'],
                                    'attachment'=>$attachment['attachment']
                                 ];

            }
            foreach ($indicators as $ki => $indicator) {
                   $nodes_i[$ki]=[
                                    'id'        =>$indicator['indicator'],
                                    'indicator' =>Indicator::findOne($indicator['indicator'])->name
                                 ];
            }

            $milestones[$km]=[
                                'submissions'=>[
                                                'id'=>$submisson['id'],
                                                'milestone_id'=>$submisson['milestone_id'],
                                                'milestone'=>$submisson['milestone'],
                                                'project'=>$submisson['project_id'],
                                                'comment'=>$submisson['comment'],
                                                'submission_date'=>$submisson['created_at'],
                                                'attachments'=>$nodes_a,
                                                'indicators'=>$nodes_i,
                                             ]
                            ];
        }
        return $this->render('submission/review', [
            //'model' => $model,
            'submissions'       =>$milestones,
            'milestone_id'      =>$id,
            'project_id'        =>$project,
            'timeline'          =>$timeline,
            'iteratemodel'      =>$iteratemodel,
            'dataProvider'      =>$dataProvider,
            'searchModel'       =>$searchModel,
            'milestone_status'  =>$milestone_status,
            //'status'=>$this->getStatus($model->status),
            'iterate'           =>$iterate
        ]);
    }
    public function actionScale(){
        $scale = Functions::getScaleMilestone(42,'pipeline');
        return json_encode(Functions::getScaleM($scale,40));
    }

    public function actionRequest($id){
        $iteration = Iteration::findOne($id);
        $timeline   = Timeline::findOne(['id'=>$iteration->timeline]);
        $institution = Project::findOne($timeline->project)->institution;
        $model      = new ExtensionRequests();
        if(Yii::$app->request->post()){
        $data = Yii::$app->request->post();
        if ($data) {
            $model->load($data);
            $model->iteration       = $id;
            $model->project         = $timeline->project;
            $model->milestone       = $timeline->milestone;
            $model->duration        = $data['ExtensionRequests']['duration'];
            $model->status          = 0;
            if($model->save()){
            Notification::notify(Notification::DATE_EXTENSION,$institution, $model->id);  
            return $this->redirect(['review', 'id' =>$timeline->milestone,'project'=>$timeline->project]);
            }
        } 
      }else {
            return $this->render('timelines/request', [
                'model' => $model,
                'milestone'=>Milestone::findOne($timeline->milestone)->milestone
            ]);
        }
    }
    private function findSearchTimelineDetails($id){
        $iteration = Iteration::find()->where(['timeline'=>$id])->all();
        $initial_duration = '';
        $start_date = '';
        $sum = '';
        if($iteration){
            $iteration              = Iteration::findOne(['timeline'=>$id]);
            $initial_duration       = $iteration->duration;
            $start_date             = $iteration->start_date;
            $command = Yii::$app->db->createCommand("SELECT sum(duration) FROM iteration WHERE timeline=$id");
            $sum = $command->queryScalar();
        }
        return array(
                        'initial_duration'=>$initial_duration,
                        'start_date'=>$start_date,
                        'sum'=>(int) $sum
                    );

    }

     /**
     * Finds the Timeline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelTimeline($id)
    {
        if (($model = Timeline::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIteration($id,$milestone){
        $model = $this->findModelIteration($id);
        $start_date = $model->start_date;
        $project = $model->project;
        if ($model->load(Yii::$app->request->post())) {
            $sql = 'INSERT INTO iteration (`timeline`,`project`,`comment`,`start_date`,`duration`,`created_by`)
                    VALUES ('.$id.','.$project.',"'.$model->comment.'","'.$start_date.'",'.$model->duration.','.Yii::$app->user->identity->id.')';
            if(Yii::$app->db->createCommand($sql)->execute())
            return $this->redirect(['timeline-entry', 'id' => $id]);
            else
            return $this->render('/iteration/update', [
                'model' => $model,
                'milestone'=>$milestone
            ]);
        } else {
            return $this->render('/iteration/update', [
                'model' => $model,
                'milestone'=>$milestone
            ]);
        }
    }

    /**
     * Updates an existing Timeline model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditTimelineEntry($id)
    {
        $model = $this->findModelTimeline($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('timelines/edit_timeline_entry', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Timeline model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTimelineEntry($id)
    {
        $this->findModelTimeline($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Iteration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timeline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelIteration($id)
    {
        if (($model = Iteration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //FUNCTIONS FOR VIEWING SUBMISSION
    /**
     * Lists all Project milestones.
     * @return mixed
     */
    public function actionMilestones($id)
    {
        //return Project::findOne($id)->program;

        $pipeline = Program::findOne(Project::findOne($id)->program)->pipeline;


        $stages=VPipelineStage::find()->where(['pipeline'=>$pipeline])->orderBy(['ordering'=>SORT_ASC])->all();
//       print_r($stages);
        //     print_r($stages);
        $items=[];
        foreach($stages as $k=>$stage) {
        $milestones=VMilestoneStage::find()->where(['stage_id'=>$stage['stage'],'pipeline_id'=>$pipeline])->distinct()->orderBy(['milestone_ordering'=>SORT_ASC])->all();
            $nodes=[];
            foreach($milestones as $km=>$milestone) {
                $indicators = VStaticIndicatorMilestone::find()->where(['milestone_id'=>$milestone['milestone_id']])->distinct()->all();
                $secondarynodes=[];
                foreach ($indicators as $ki => $indicator) {
                  $secondarynodes[$ki]=[
                      'id'          =>$indicator['indicator_id'],
                      'indicator'   =>$indicator['indicator']
                  ];
                }
                $nodes[$km]=[
                                            'id'            =>$milestone['milestone_id'],
                                            'num'           =>$milestone['milestone_ordering'],
                                            'milestone'     =>$milestone['milestone'],
                                            'status'        =>$this->renderPartial('statusbutton',['milestone'=>$milestone['milestone_id'],'project'=>$id]),
                                            'indicators'    =>$secondarynodes
                            ];
              }
                $items[$k]=[ 'stages'=>[
                  'id'          =>$stage['stage'],
                  'stage'       =>$stage['stage_name'],
                  'percentage'  =>$this->calculatePercentage($stage['stage'],$id),
                  'milestones'  =>$nodes
                  ]
              ];
            }

        return $this->render('submission/milestone', [
            'data' => $items,
            'project'=>$id,
        ]);
    }

    public function calculatePercentage($stage,$project){
        $stages_sql        = 'SELECT COUNT(*) FROM `v_milestone_stage` WHERE stage_id ='.$stage.'';
        $stages            =  Yii::$app->db->createCommand($stages_sql)->queryScalar();

        $submissions_sql        = 'SELECT COUNT(a.milestone_id) FROM v_milestone_submission a LEFT JOIN milestone_stage b
                                  ON a.milestone_id = b.milestone WHERE a.project_id='.$project.' AND b.stage='.$stage.'';
        return $submissions      =  Yii::$app->db->createCommand($submissions_sql)->queryScalar();
        return ($submissions/$stages)*100;
    }

    public function actionStart($id){
        $timeline                   = Timeline::findOne($id);
        $iteration                  = new Iteration();
        $iteration->timeline        = $id;
        $iteration->start_date      = date('Y-m-d : h:i:s');
        $iteration->duration        = $timeline->duration;
        $iteration->iteration_type  = 1;
        $iteration->save();
        return $this->redirect(['review', 'id' => $timeline->milestone,'project'=>$timeline->project]);
    }

    public function actionStartExtension($id){
        $request = ExtensionRequests::findOne([$id]);
        $request->start_status=1;
        $request->save();
        //determine the type of extension
        $type = $request->type;
        $timeline = Timeline::findOne(['project'=>$request->project,'milestone'=>$request->milestone]); 
        if($type==2){
        //add first new iteration value of the timeline with iteration_type of normal.
        //this iteration takes the values of the timeline
        $iteration                              = new Iteration();
        $iteration->timeline                    = $timeline->id;
        $iteration->start_date                  = $timeline->start_date;
        $iteration->duration                    = $timeline->duration;
        $iteration->iteration_type              =  $type;
        $iteration->end_date                    = date('Y-m-d : h:i:s', strtotime($iteration->start_date. ' + '.$iteration['duration'].' days'));
        $iteration->save();
        }else{
        $iteration = Iteration::findOne(['timeline'=>$timeline->id,'end_date'=>'is NULL','iteration_type'=>$type]);
        $iteration->end_date = date('Y-m-d : h:i:s', strtotime($iteration->start_date. ' + '.$iteration['duration'].' days'));
        $iteration->save();
        }


        $iteration_extension                    = new Iteration();
        $iteration_extension->timeline          = $timeline->id;
        $iteration_extension->start_date        = $iteration->end_date;
        $iteration_extension->duration          = $request->duration;
        $iteration_extension->iteration_type    =  $type;
        $iteration_extension->end_date          = NULL;
        $iteration_extension->save();

        return $this->redirect(['review', 'id' => $request->milestone,'project'=>$request->project]);
    }

    /**
     * Performs submisson of a milestone
     * If deletion is successful, the browser will be redirected to the 'submit' page.
     * @param integer $id (milestone id)
     * @return mixed
     */

    public function actionAssess($id,$project){
        $model = $this->findModelSub($id,$project);
        $submissons = VMilestoneSubmission::find()
                                            ->where(['milestone_id'=>$id,'project_id'=>$project])
                                            ->orderBy('id DESC')
                                            ->all();
        $milestones=[];
        foreach ($submissons as $km => $submisson) {
            $attachments    = Attachments::find()->where(['milestone_submission'=>$submisson['id']])->all();
            $indicators     = IndicatorMilestoneSubmission::find()->where(['milestone_submission'=>$submisson['id']])->all();
            $nodes_a=[];
            $nodes_i=[];
            foreach ($attachments as $ka => $attachment) {
                $nodes_a[$ka]=[
                                    'id'        =>$attachment['id'],
                                    'attachment'=>$attachment['attachment']
                                 ];

            }
            foreach ($indicators as $ki => $indicator) {
                   $nodes_i[$ki]=[
                                    'id'        =>$indicator['indicator'],
                                    'indicator' =>Indicator::findOne($indicator['indicator'])->name
                                 ];
            }

            $milestones[$km]=[
                                'submissions'=>[
                                                'id'=>$submisson['id'],
                                                'milestone_id'=>$submisson['milestone_id'],
                                                'milestone'=>$submisson['milestone'],
                                                'project'=>$submisson['project_id'],
                                                'comment'=>$submisson['comment'],
                                                'submission_date'=>$submisson['created_at'],
                                                'attachments'=>$nodes_a,
                                                'indicators'=>$nodes_i,
                                             ]
                            ];
        }
        return $this->render('submission/assess', [
            'model' => $model,
            'submissions'=>$milestones,
            'status'=>$this->getStatus($model->status)
        ]);
    }

    private function getAttacthment($attachment){
        if($attachment)
        return '<a href="http://trajectory.ranlab.org/projects/'.$attachment.'" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download Attachment</a>';
                   
        else 
        return '<a class="btn btn-warning btn-xs"> No attachment Uploaded</a>';
    }

    public function actionGrade($id,$project,$iteration){
           $model = $this->findModelSub($id,$project);
           $indicators = VIndicatorSubmission::find()->where(['milestone_submission'=>$id])->all();
           $attachments = Attachments::find()->where(['milestone_submission'=>$id])->all();
           //return $innovator->innovator;
           $gradingmodel= new Grading();
           $data = Yii::$app->request->post();
           if($data){
            $indicators=$data['indicator'];
            $gradingmodel->save();
            foreach ($indicators as $key => $indicator) {
                        $record[] = [
                                            'indicator_milestone_submission'            =>$data['indicator_ids'][$key],
                                            'value'                                     =>$data['indicator'][$key],
                                            'created_at'                                =>time(),
                                            'created_by'                                =>Yii::$app->user->identity->id,
                                            'updated_at'                                =>time(),
                                            'updated_by'                                =>Yii::$app->user->identity->id,
                                    ];
                                # code...
            }
            if(count($record)>0){
                $columnNameArray=['indicator_milestone_submission','value','grade','created_at','created_by','updated_at','updated_by'];
                        // below line insert all your record and return number of rows inserted
                $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'indicator_grades', $columnNameArray, $record
                                         )
                                       ->execute();
                $grading_id = Yii::$app->db->getLastInsertID();
                $innovator = $this->findModel($project)->innovator;
                Notification::notify(Notification::PROJECT_ASSESMENT,$innovator,$grading_id);
                return $this->redirect(['grade-view', 'id' => $grading_id]);
            }else {
                return $this->render('grade/grade', [
                    'model' => $model,
                    'gradingmodel'=>$gradingmodel,
                    'milestone'=>$id,
                    'indicators'=>$indicators,
                    'project'=>$project,
                    'iteration'=>$iteration,
                ]);
            }

           }else{
            return $this->render('grade/grade', [
                    'model' => $model,
                    'gradingmodel'=>$gradingmodel,
                    'milestone'=>$id,
                    'indicators'=>$indicators,
                    'project'=>$project,
                    'iteration'=>$iteration,
                    'attachments'=>$attachments
                ]);
           }
    }

    public function actionGradeView($id){
        return $this->render('grade/grade_view', [
            'model' => $this->findModelGrade($id),
        ]);
    }

    public function actionUpdateGrading($id){
        $model = $this->findModelGrade($id);

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['grade-view', 'id' => $model->id]);
        } else {
            return $this->render('grade/grade_update', [
                'model' => $model,
            ]);
        }
    }
    public function actionApprove($id,$ext){
        $milestone = ExtensionRequests::findOne([$ext]);
        $sql = 'UPDATE extension_requests SET status = 1 WHERE id='.$ext.'';
        $iteration = Iteration::findOne(['timeline'=>$id]);
        Yii::$app->db->createCommand($sql)->execute();
        $sql = 'INSERT INTO iteration (`timeline`,`project`,`comment`,`start_date`,`duration`,`created_by`)
                VALUES ('.$id.','.$milestone->project.',"'.$milestone->reason.'","'.$iteration->start_date.'",'.$milestone->duration.','.Yii::$app->user->identity->id.')';
        if(Yii::$app->db->createCommand($sql)->execute()){
        Notification::notify(Notification::EXTENSION_APPROVAL,$milestone->innovator,$milestone->id);
        }  
        return $this->redirect(['timeline-entry', 'id' =>$id]);   
    }

    public function actionExtensionRequests(){
        $projects = Project::find()->where(['innovator'=>Yii::$app->user->identity->rel_table_id])->all();
        $project_ids='';
        foreach ($projects as $key => $project) {
           $project_ids.=''.$project['id'].',';
        }

        $project_ids = rtrim($project_ids, ',');
        $project_ids=array_map('intval', explode(',', $project_ids));
        $project_ids = implode("','",$project_ids);

        $searchModel = new ExtensionRequestsSearch();
        $dataProvider = $searchModel->searchMain(Yii::$app->request->queryParams,$project_ids);
        return $this->render('requests', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionApproveExtension($id){
        $request = ExtensionRequests::findOne([$id]);
        $sql = 'UPDATE extension_requests SET status = 1 WHERE id='.$id.'';
        $timeline = Timeline::findOne(['project'=>$request->project,'milestone'=>$request->milestone]);
        $iteration = Iteration::findOne(['timeline'=>$timeline->id]);
        Yii::$app->db->createCommand($sql)->execute();
        $sql = 'INSERT INTO iteration (`timeline`,`project`,`comment`,`start_date`,`duration`,`created_by`)
                VALUES ('.$timeline->id.','.$request->project.',"'.$request->reason.'","'.date('Y-m-d', strtotime($iteration->start_date. ' + '.$iteration['duration'].' days')).'",'.$request->duration.','.Yii::$app->user->identity->id.')';
        if(Yii::$app->db->createCommand($sql)->execute()){
        $innovators = LoginInnovator::find()->where(['innovator'=>$request->innovator])->all();
        Notification::notify(Notification::EXTENSION_APPROVAL,$innovators,$this->generateUrl(Notification::EXTENSION_APPROVAL,$id,$request->project), $request->id);
        }  
        return $this->redirect(['extension-requests']);   
    }
     public function actionRejectExtension($id){
        $request = ExtensionRequests::findOne([$id]);
        $sql = 'UPDATE extension_requests SET status = 2 WHERE id='.$id.'';
        Yii::$app->db->createCommand($sql)->execute();
        if(Yii::$app->db->createCommand($sql)->execute()){
        $innovators = LoginInnovator::find()->where(['innovator'=>$request->innovator])->all();
        Notification::notify(Notification::EXTENSION_REJECTION,$innovators,$this->generateUrl(Notification::EXTENSION_REJECTION,$id,$request->project), $request->id);
        }  
        return $this->redirect(['extension-requests']);   
    }

    private function findIndicatorSubmissions($id){
        $sql = 'SELECT a.id, a.indicator as indicator_id, a.value, b.name
                FROM indicator_milestone_submission a LEFT JOIN indicator b ON a.indicator=b.id
                WHERE a.milestone_submission ='.$id.'';
        $indicators = Yii::$app->db->createCommand($sql)->queryAll();
        $indicator_list ='';
        foreach ($indicators as $indicator) {
            $indicator_list.='<tr>
                                    <td class="col-md-10"><code>'.$indicator['name'].'</code></td>
                                    <td class="col-md-2"><a href="#" data-skin="skin-blue" class="btn btn-primary btn-xs">'.$indicator['value'].'</a></td>
                                  </tr>';
        }
        return $indicator_list;
    }

    private function getStatus($status){
        switch ($status) {
                    case 0:
                        return 'Not Reviewed';
                    case 5:
                        return 'Rejected';
                    case 10:
                        return 'Approved';
                    default:
                    return null;
                }
    }

     /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelSub($milestone,$project)
    {
        if (($model = VMilestoneSubmission::findOne(['milestone_id'=>$milestone,'project_id'=>$project])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelGrade($id)
    {

        if (($model = Grading::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
