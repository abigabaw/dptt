<?php

namespace frontend\controllers\user;

use dektrium\user\models\LoginForm;
use Yii;

class SecurityController extends \dektrium\user\controllers\SecurityController
{
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        $this->layout = '@common/themes/AdminLTE/views/layouts/login.php';

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goHome();
        }

        return $this->render('@common/themes/AdminLTE/views/user/security/login', [
            'model'  => $model,
            'module' => $this->module,
            'form_title' => 'Project Login'
        ]);
    }
}