<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Milestone;
use frontend\models\Project;
use frontend\models\LoginInnovator;
use app\models\Notification;
use frontend\models\Iteration;
use frontend\models\Timeline;
use frontend\models\MilestoneSearch;
use frontend\models\MilestoneStage;
use common\models\IterationRequests;
use common\models\IterationRequestsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * MilestoneController implements the CRUD actions for Milestone model.
 */
class MilestoneController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Milestone models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MilestoneSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionMilestones(){
        $post = Yii::$app->request->post();
        if($post){
            $sql           = 'SELECT a.milestone as id, b.milestone FROM `milestone_stage` a left join
                              milestone b on a.milestone=b.id WHERE a.stage='.$post['stage_id'].'';
            $miletsones    = Yii::$app->db->createCommand($sql )->queryAll();
            return json_encode($miletsones);
        }else{
            return null;
        }
        
    }
        /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionRequests()
    {
        $projects = Project::find()->where(['innovator'=>Yii::$app->user->identity->rel_table_id])->all();
        $project_ids='';
        foreach ($projects as $key => $project) {
           $project_ids.=''.$project['id'].',';
        }

        $project_ids = rtrim($project_ids, ',');
        $project_ids=array_map('intval', explode(',', $project_ids));
        $project_ids = implode("','",$project_ids);

        $searchModel = new IterationRequestsSearch();
        $dataProvider = $searchModel->searchFront(Yii::$app->request->queryParams,$project_ids);

        return $this->render('requests', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Milestone model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionIndicators($id)
    {
             $model = new Milestone();  
             $dataProvider = Yii::$app->db->createCommand('SELECT a.id, a.indicator as indicator_id, b.name as indicator, a.ordering FROM static_indicator_milestone a INNER JOIN indicator b on a.indicator=b.id WHERE a.milestone='.$id.' order by ordering')->queryAll();

            $post = Yii::$app->request->post();
            if($post && Yii::$app->request->validateCsrfToken() == $post['_csrf'])
            {   
                 $sort = trim($post['sort_list']);      
                 $sortArray = explode(',', $sort) ;
                 $i = 1;
                 $sql = "UPDATE static_indicator_milestone SET ordering = :ordering WHERE milestone = :milestone";
                 foreach($sortArray as $v)
                 {      
                      $v = intval($v);
                      if($v > 0)
                      {             
                           $cmd = \Yii::$app->db->createCommand($sql);
                           $cmd->bindValue(':ordering', $i);
                           $cmd->bindValue(':indicator', $v);
                           $cmd->execute();
                      } else {
                           // whatever  
                      }
                      $i++;
                 }  
                 return $this->redirect(['indicators','id' => $id]);
            } else {
                 return $this->render('indicator', ['dataProvider' => $dataProvider, 'model' => $this->findModel($id)]);
            }
    }


    public function actionStart($id){
        $modelapprove = $this->findModelRequests($id);
        $timeline = Timeline::findOne($modelapprove->timeline);
        
        $iterationnew = new Iteration();
        $iterationnew->timeline = $timeline->id;
        $iterationnew->start_date = date('Y-m-d : h:i:s');
        $iterationnew->iteration_type = 5;
        $iterationnew->duration = $timeline->duration;
        $iterationnew->save();

        return $this->redirect(['/project/review', 'id' =>$timeline->milestone,'project'=>$timeline->project]);  
    }
    
    /**
     * Creates a new Milestone model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Milestone();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Milestone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Milestone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this milestone has values dependant on it. Please contact your administrator');
        }
    }

    /**
     * Finds the Milestone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Milestone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Milestone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelRequests($id)
    {
        if (($model = IterationRequests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
