<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Indicator;
use frontend\models\IndicatorSearch;
use frontend\models\Project;
use frontend\models\LoginInnovator;
use frontend\models\IndicatorTypeMap;
use frontend\models\IndicatorTypeMapSearch;
use frontend\models\IndicatorMilestone;
use frontend\models\IndicatorRequests;
use frontend\models\IndicatorRequestsSearch;
use app\models\Notification;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * IndicatorController implements the CRUD actions for Indicator model.
 */
class IndicatorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionRequests()
    {
        $projects = Project::find()->where(['innovator'=>Yii::$app->user->identity->rel_table_id])->all();
        $project_ids='';
        foreach ($projects as $key => $project) {
           $project_ids.=''.$project['id'].',';
        }

        $project_ids = rtrim($project_ids, ',');
        $project_ids=array_map('intval', explode(',', $project_ids));
        $project_ids = implode("','",$project_ids);
        $searchModel = new IndicatorRequestsSearch();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$project_ids);

        return $this->render('requests', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndicatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Indicator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    

    /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Indicator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

        /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelRequests($id)
    {
        if (($model = IndicatorRequests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    //CONTROLLER FUNCTIONS FOR DATA TYPE OF INNOVATOR
    public function actionValues(){

        $model = new IndicatorTypeMap();

        if (Yii::$app->request->post()) {
                $data = Yii::$app->request->post();
                $indicators = $data['IndicatorTypeMap']['indicator'];
                $request = Yii::$app->getRequest();
                if ($request->isPost && $request->post('ajax') !== null) {
                        $data = Yii::$app->request->post('Item', []);
                        foreach (array_keys($data) as $index) {
                            $models[$index] = new MilestoneStage();
                        }
                        Model::loadMultiple($models, Yii::$app->request->post());
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        $result = ActiveForm::validateMultiple($models);
                        return $result;
                }
                $record = array();
                foreach($indicators as $indicator) {
                    $record[] = [
                                'indicator'             =>$indicator,
                                'indicator_type'        =>$data['IndicatorTypeMap']['indicator_type'],
                            ];
                }

                if(count($record)>0){
                        $columnNameArray=['indicator','indicator_type'];
                        // below line insert all your record and return number of rows inserted
                        $insertCount = Yii::$app->db->createCommand()
                                       ->batchInsert(
                                             'indicator_type_map', $columnNameArray, $record
                                         )
                                       ->execute();
                        return $this->redirect(['types']);
                }
        } else {
            return $this->render('assign', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Lists all Indicator models.
     * @return mixed
     */
    public function actionTypes()
    {
        $searchModel = new IndicatorTypeMapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('typelist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     /**
         * Displays a single Indicator model.
         * @param integer $id
         * @return mixed
         */
    public function actionValue($id)
        {
            return $this->render('value', [
                'model' => $this->findModelType($id),
            ]);
        }

        /**
     * Finds the Indicator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelType($id)
    {
        if (($model = IndicatorTypeMap::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Updates an existing IndicatorType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditValue($id)
    {
        $model = $this->findModelType($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('edit_value', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Indicator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteValue($id)
    {
        try {
              $this->findModelType($id)->delete();
              return $this->redirect(['values']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Please contact your administrator');
            }
    }
}
