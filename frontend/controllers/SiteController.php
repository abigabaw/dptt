<?php
namespace frontend\controllers;

use Yii;
use frontend\models\VProgram;
use frontend\models\Project;
use frontend\models\LoginInnovator;
use frontend\models\Pipeline;
use frontend\models\VProject;
use frontend\models\Queries;
use frontend\models\DashboardSearch;
use frontend\models\Innovator;
use frontend\models\VDelayedMilestone;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','milestones','choosepipeline'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'milestones' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionMilestones(){
        $data = Yii::$app->request->post();
        $sql_delayed ='SELECT a.timeline,a.milestone,a.expected_finish_date
                    FROM v_delayed_milestone a
                    WHERE finish_date is null
                    AND a.project_id='.$data['project_id'].'
				    AND date(a.expected_finish_date)	<= date\''.date('Y-m-d').'\'	  
						  ';
        $delayed_milestones = Yii::$app->db->createCommand($sql_delayed)->queryAll();
        return json_encode($delayed_milestones);
    }

    public function actionIndex()
    { 
        //$delayed_milestones = VDelayedMilestone::find()->where(['submission_date'=>0])->all();
        $project                        = Project::findOne(['innovator'=>Yii::$app->user->identity->innovator])->id;
        if(!$project){
            return $this->render('welcome');
        }

        $team_members                   = LoginInnovator::find()->where(['innovator'=>Yii::$app->user->identity->innovator])->all();
        $team_count                     = count($team_members);

        $delayed_milestones             = Yii::$app->db->createCommand(Queries::qryDelayed($project))->queryAll();
        $approved_milestones            = Yii::$app->db->createCommand(Queries::SQLApproved($project))->queryAll();
        $rejected_milestones            = Yii::$app->db->createCommand(Queries::SQLRejected($project))->queryAll();
        $qry_iteration                  = Yii::$app->db->createCommand(Queries::qryIteration($project))->queryAll();
        $delayed_milestone_count        = count($delayed_milestones);
        $approved_milestone_count       = count($approved_milestones);
        $rejected_milestone_count       = count($rejected_milestones);

        
        return $this->render('index', [
            'innovators'                => $this->getInnovators(),
            'projects'                  => $this->getProjects(),
            'pipelines'                 => $this->getPipelines(),
            'delayed_milestones'        => $delayed_milestones,
            'team_count'                => $team_count,
            'project'                   => $project,
            'delayed_milestone_count'   => $delayed_milestone_count,
            'rejected_milestones'       => $rejected_milestones,
            'approved_milestones'       => $approved_milestones,
            'rejected_milestone_count'  => $rejected_milestone_count,
            'approved_milestone_count'  => $approved_milestone_count
        ]);
    }    
   
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = '@common/themes/AdminLTE/views/layouts/login.php';
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('@common/themes/AdminLTE/views/site/login.php', [
                'model' => $model,
                'form_title'=>'Project Login'
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout(); 
        return $this->goHome();
    }

     //HELPER FUNCTIONS FOR DASHBOARD

    private function getProjects(){
        return Project::find()->where(['innovator'=>Yii::$app->user->identity->innovator])->count();
    }
    private function getPipelines(){
        return Pipeline::find()->count();
    }
    private function getInnovators(){
        return Innovator::find()->count();
    }
}
