<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Innovator;
use frontend\models\InnovatorSearch;
use frontend\models\LoginInnovatorSearch;
use frontend\models\LoginInnovator;
use frontend\models\UserForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
/**
* InnovatorController implements the CRUD actions for Innovator model.
**/
class InnovatorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Innovator models.
     * @return mixed
     */
    public function actionUsers($id)
    {
        $model = new UserForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup($id)) {
                    Yii::$app->session->setFlash('success', 'User has been created. Login Details Sent to User');
                    return $this->redirect(['user', 'id' => $user->id]);
            }
            else {
                        Yii::$app->session->setFlash('error', 'Sorry, unable to create user.');
                        return $this->render('users', [
                        'model' => $model,
            ]);
                }
        }

        return $this->render('users', [
                'model' => $model,
            ]);
    }

    public function actionUser($id){
        return $this->render('user', [
            'model' => $this->findUser($id),
        ]);
    }
    /**
     * Lists all Login Innovator models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new LoginInnovatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admins', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Innovator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoginInnovatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admins', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Innovator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),'users'=>$this->getLoginInnovators($id),
        ]);
    }

    /**
     * Creates a new Innovator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Innovator();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Innovator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            //return var_dump($model->errors);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Innovator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	 public function actionUpdateUser($id) {
	$model = $this->findUser($id);
	//print_r($_POST);exit;
	
       if (Yii::$app->request->post()){
			 $data=Yii::$app->request->post();
			 $model->name=$data['LoginInnovator']['name'];
			  $model->email=$data['LoginInnovator']['email'];
			   $model->gender=$data['LoginInnovator']['gender'];
				$model->save();
		return $this->redirect(['user', 'id' => $model->id]);
	}
	return $this->render('updateuser', ['model' => $model]);
	 }
    public function actionDelete($id)
    {
        try {
              $this->findModel($id)->delete();
              return $this->redirect(['index']);
            } catch (\yii\db\Exception $e) {
                throw new ForbiddenHttpException('The action could not be completed. Because this innovator has values dependant on it. Please contact your administrator');
            }
    }

    /**
     * Finds the Innovator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Innovator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Innovator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Innovator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Innovator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id)
    {
        if (($model = LoginInnovator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getLoginInnovators($id){
        $users = LoginInnovator::find()
        ->where(['innovator' => $id])
        ->orderBy('id')
        ->all();
        $userlist = 'There are no users in this innovator';
        if($users){
            $userlist = '';
            foreach ($users as $user) {
                $userlist.='<tr>
                              <td><a href="'.Url::to(['/innovator/user/'.$user['id'].'']).'">'.$user['name'].'</a></td>
                           </tr>';
            }
        }
        return $userlist;
    }
}
