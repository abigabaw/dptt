<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VPipelineStage]].
 *
 * @see VPipelineStage
 */
class VPipelineStageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return VPipelineStage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VPipelineStage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
