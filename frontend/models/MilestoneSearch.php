<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Milestone;

/**
 * MilestoneSearch represents the model behind the search form about `backend\models\Milestone`.
 */
class MilestoneSearch extends Milestone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'num'], 'integer'],
            [['milestone','description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VMilestone::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
           'institution_id' => Yii::$app->user->identity->institution,
        ]);

        $query->andFilterWhere([
            'id' => $this->id,
            'num' => $this->num,
        ]);

        $query->andFilterWhere(['like', 'milestone', $this->milestone]);
        $query->andFilterWhere(['like', 'description', $this->milestone]);

        return $dataProvider;
    }
}
