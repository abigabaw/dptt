<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_pipeline".
 *
 * @property integer $pipeline_id
 * @property string $pipeline_name
 * @property string $pipeline_description
 * @property integer $created_by
 * @property string $institution_name
 * @property integer $institution_id
 */
class VPipeline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_pipeline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'institution_id'], 'integer'],
            [['name', 'description', 'created_by'], 'required'],
            [['name', 'description', 'institution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Pipeline ID'),
            'name' => Yii::t('app', 'Pipeline Name'),
            'description' => Yii::t('app', 'Pipeline Description'),
            'created_by' => Yii::t('app', 'Created By'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return VPipelineQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VPipelineQuery(get_called_class());
    }
}
