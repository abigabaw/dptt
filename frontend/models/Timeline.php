<?php

namespace frontend\models;

use Yii;
use common\models\LoginInstitution;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "timeline".
 *
 * @property integer $id
 * @property integer $project
 * @property integer $milestone
 * @property integer $duration
 * @property integer $iteration
 * @property string $start_date
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Iteration $iteration0
 * @property Milestone $milestone0
 */
class Timeline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timeline';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project', 'milestone','created_by', 'created_at', 'updated_at'], 'required'],
            [['project', 'milestone',  'created_by', 'created_at', 'updated_at'], 'integer'],
            [['start_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project' => 'Project',
            'milestone' => 'Milestone',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIteration0()
    {
        return $this->hasOne(Iteration::className(), ['id' => 'iteration']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(LoginInstitution::className(), ['id' => 'created_by']);
    }
}
