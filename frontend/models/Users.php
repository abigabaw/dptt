<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $rel_table
 * @property integer $rel_table_id
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 *
 * @property Grading[] $gradings
 * @property Grading[] $gradings0
 * @property Pipeline[] $pipelines
 * @property Pipeline[] $pipelines0
 * @property Profile $profile
 * @property Program[] $programs
 * @property SocialAccount[] $socialAccounts
 * @property Token[] $tokens
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['rel_table_id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags'], 'integer'],
            [['username', 'email', 'rel_table', 'unconfirmed_email'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 45],
            [['email'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'username' => Yii::t('backend', 'Username'),
            'email' => Yii::t('backend', 'Email'),
            'password_hash' => Yii::t('backend', 'Password Hash'),
            'auth_key' => Yii::t('backend', 'Auth Key'),
            'rel_table' => Yii::t('backend', 'Rel Table'),
            'rel_table_id' => Yii::t('backend', 'Rel Table ID'),
            'confirmed_at' => Yii::t('backend', 'Confirmed At'),
            'unconfirmed_email' => Yii::t('backend', 'Unconfirmed Email'),
            'blocked_at' => Yii::t('backend', 'Blocked At'),
            'registration_ip' => Yii::t('backend', 'Registration Ip'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'flags' => Yii::t('backend', 'Flags'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings()
    {
        return $this->hasMany(Grading::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings0()
    {
        return $this->hasMany(Grading::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelines()
    {
        return $this->hasMany(Pipeline::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelines0()
    {
        return $this->hasMany(Pipeline::className(), ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrograms()
    {
        return $this->hasMany(Program::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(SocialAccount::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }
}
