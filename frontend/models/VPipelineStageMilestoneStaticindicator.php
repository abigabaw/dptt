<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_pipeline_stage_milestone_staticindicator".
 *
 * @property integer $pipeline_id
 * @property string $pipeline
 * @property integer $stage_id
 * @property string $stage
 * @property integer $milestone_id
 * @property string $milestone
 * @property integer $indicator_id
 * @property string $indicator
 */
class VPipelineStageMilestoneStaticindicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_pipeline_stage_milestone_staticindicator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pipeline_id', 'stage_id'], 'required'],
            [['pipeline_id', 'stage_id', 'milestone_id', 'indicator_id'], 'integer'],
            [['pipeline', 'stage', 'milestone', 'indicator'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pipeline_id' => Yii::t('app', 'Pipeline ID'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'stage_id' => Yii::t('app', 'Stage ID'),
            'stage' => Yii::t('app', 'Stage'),
            'milestone_id' => Yii::t('app', 'Milestone ID'),
            'milestone' => Yii::t('app', 'Milestone'),
            'indicator_id' => Yii::t('app', 'Indicator ID'),
            'indicator' => Yii::t('app', 'Indicator'),
        ];
    }
}
