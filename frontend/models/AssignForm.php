<?php
namespace frontend\models;

use frontend\models\IndicatorMilestone;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class AssignForm extends Model
{
    public $project;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['project', 'required'],
                 ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function assign($innovator,$project)
    {
    	if ($this->validate()) {
        	$record = array();
			foreach($indicators as $indicator) {
			                    $record[] = [
			                                'milestone'     =>$indicator->milestone,
			                                'indicator'     =>$indicator->indicator,
			                                'project'       =>$project
			                            ];
			}
			if(count($record)>0){
			$columnNameArray=['milestone','indicator','project'];
			                        // below line insert all your record and return number of rows inserted
			$insertCount = Yii::$app->db->createCommand()
			                ->batchInsert(
			                               'indicator_milestone', $columnNameArray, $record
			                             )
			                ->execute();
        	}
        }
        return null;
    }
}