<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VIndicator]].
 *
 * @see VIndicator
 */
class VIndicatorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VIndicator[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VIndicator|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}