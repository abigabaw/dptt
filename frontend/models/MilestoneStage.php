<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "milestone_stage".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $stage
 * @property integer $ordering
 *
 * @property Stage $stage0
 * @property Milestone $milestone0
 */
class MilestoneStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'milestone_stage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone', 'stage', 'ordering'], 'required'],
            [['milestone', 'stage', 'ordering'], 'integer'],
            [['milestone'],'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'milestone' => 'Milestone',
            'stage' => 'Stage',
            'ordering' => 'Ordering',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStage0()
    {
        return $this->hasOne(Stage::className(), ['id' => 'stage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
}
