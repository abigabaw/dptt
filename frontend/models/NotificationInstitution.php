<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "notification_institution".
 *
 * @property integer $id
 * @property string $key
 * @property integer $key_id
 * @property string $type
 * @property integer $user_id
 * @property integer $seen
 * @property string $created_at
 */
class NotificationInstitution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_institution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'type', 'user_id', 'seen', 'created_at'], 'required'],
            [['key_id', 'user_id', 'seen'], 'integer'],
            [['created_at'], 'safe'],
            [['key', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'key' => Yii::t('backend', 'Key'),
            'key_id' => Yii::t('backend', 'Key ID'),
            'type' => Yii::t('backend', 'Type'),
            'user_id' => Yii::t('backend', 'User ID'),
            'seen' => Yii::t('backend', 'Seen'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
