<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "static_indicator_milestone".
 *
 * @property integer $id
 * @property integer $indicator
 * @property integer $milestone
 *
 * @property Milestone $milestone0
 * @property Indicator $indicator0
 */
class StaticIndicatorMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_indicator_milestone';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator', 'milestone'], 'required'],
            [['indicator', 'milestone'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'indicator' => Yii::t('app', 'Indicator'),
            'milestone' => Yii::t('app', 'Milestone'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator0()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator']);
    }
}
