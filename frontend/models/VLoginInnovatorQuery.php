<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VLoginInnovator]].
 *
 * @see VLoginInnovator
 */
class VLoginInnovatorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VLoginInnovator[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VLoginInnovator|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}