<?php

/**
 * @link https://github.com/unclead/yii2-multiple-input
 * @copyright Copyright (c) 2014 unclead
 * @license https://github.com/unclead/yii2-multiple-input/blob/master/LICENSE.md
 */

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Class Item
 * @package unclead\widgets\examples\models
 */
class TimelineForm extends Model
{

	public $id;
    public $num;
    public $project;

    public $milestone;
    public $milestone_hidden;

    public $duration;

    public $comment;

    public $start_date;

    public $created_by;
	public $created_at;
	public $updated_at;

    public function behaviors()
    {
        return [
            // you have to install https://github.com/vova07/yii2-fileapi-widget
            /*
            'uploadBehavior' => [
                'class' => \vova07\fileapi\behaviors\UploadBehavior::className(),
                'attributes' => [
                    'file' => [
                        'path' => Yii::getAlias('@webroot') . '/images/',
                        'tempPath' => Yii::getAlias('@webroot') . '/images/tmp/',
                        'url' => '/images/'
                    ],
                ]
            ]*/
        ];
    }

    public function rules()
    {
        return [
            [['project', 'milestone',], 'required'],
            [['project', 'milestone', 'duration','num','created_by', 'created_at', 'updated_at'], 'integer'],
            [['comment'], 'string', 'max' => 255],
            [['start_date'], 'safe']
        ];
    }

    public function addTimeline($project,$models,$userid){
    			$record = array();
                foreach($models as $model) {
                if($model['comment']&&$model['duration']&&$model['start_date']){
                 $sql = 'INSERT INTO timeline (`project`, `milestone`,`stage`, `created_by`,`created_at`,`updated_at`) 
                         VALUES ('.$project.','.$model['milestone'].','.MilestoneStage::findOne(['milestone'=>$model['milestone']])->stage.','.$userid.','.time().','.time().')';
                
                if(Yii::$app->db->createCommand($sql)->execute())
                    $this->addIteration(Yii::$app->db->getLastInsertID(),$project,$model['duration'],$model['comment'],$model['start_date'],$userid );
                }
			}
                return true;

    }
    private function addIteration($timeline,$project, $duration,$comment,$start_date,$userid){
        $sql = 'INSERT INTO iteration (`timeline`,`project`, `comment`, `start_date`, `duration`, `created_by`,`created_at`,`updated_at`) 
                VALUES ('.$timeline.','.$project.', "'.preg_replace("/[^a-zA-Z0-9]+/", " ", html_entity_decode($comment)).'","'.$start_date.'", '.$duration.', '.$userid.','.time().','.time().')';
        if(Yii::$app->db->createCommand($sql)->execute())
        return;
    }
}