<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VProject]].
 *
 * @see VProject
 */
class VProjectQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VProject[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VProject|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}