<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_milestone".
 *
 * @property integer $id
 * @property integer $num
 * @property string $milestone
 * @property string $description
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $institution_name
 * @property integer $institution_id
 */
class VMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'num', 'created_by', 'updated_by', 'institution_id'], 'integer'],
            [['num', 'milestone', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['milestone', 'description', 'institution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'num' => Yii::t('app', 'Num'),
            'milestone' => Yii::t('app', 'Milestone'),
            'description' => Yii::t('app', 'Description'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return VMilestoneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VMilestoneQuery(get_called_class());
    }
}
