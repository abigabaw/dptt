<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use frontend\models\VMilestoneSubmission;
use frontend\models\Grading;
use frontend\models\Project;
use frontend\models\NotificationInstitution;
use machour\yii2\notifications\NotificationsModule;
use frontend\models\ExtensionRequests;
use frontend\models\IndicatorRequests;
use common\models\VIterationRequests;
use frontend\models\Milestone;
use frontend\models\Iteration;
use frontend\models\Timeline;
use machour\yii2\notifications\models\Notification as BaseNotification;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "notification".
 *
 * @property integer $idf
 * @property integer $key_id
 * @property string $key
 * @property string $type
 * @property boolean $seen
 * @property string $created_at
 * @property integer $user_id
 */
class Notification extends BaseNotification
{
	/**
     * new creation notification
     */
    const MILESTONE_SUBMISSION          = 'milestone_submission';
    const ITERATION_REQUEST             = 'iteration_request';
    const ITERATION_REQUEST_APPROVAL    = 'iteration_request_approval';
    const ITERATION_REQUEST_REJECT      = 'iteration_request_reject';
    const PROJECT_CREATION              = 'project_creation';
    const EXTENSION_APPROVAL            = 'extension_approval';
    const INDICATOR_REQUEST             = 'indicator_request';
    const INDICATOR_REQUEST_APPROVAL    = 'indicator_request_approval';
    const INDICATOR_REQUEST_REJECT      = 'indicator_request_reject';
    const DATE_EXTENSION                = 'date_extension';
    const DATE_EXTENSION_REJECT         = 'date_extension_reject';
    const PROJECT_ASSESMENT             = 'project_assement';
    const EXTENSION_REQUEST_REJECT      = 'extension_request_reject';



/**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        self::MILESTONE_SUBMISSION,
        self::ITERATION_REQUEST,
        self::PROJECT_CREATION,
        self::EXTENSION_APPROVAL,
        self::ITERATION_REQUEST_APPROVAL,
        self::INDICATOR_REQUEST,
        self::INDICATOR_REQUEST_APPROVAL,
        self::INDICATOR_REQUEST_REJECT,
        self::DATE_EXTENSION,
        self::PROJECT_ASSESMENT,
        self::EXTENSION_REQUEST_REJECT
    ];


    /**
     * @var array List of all enabled notification types
     */
    public static $types = [
        self::TYPE_WARNING,
        self::TYPE_DEFAULT,
        self::TYPE_ERROR,
        self::TYPE_SUCCESS,
    ];


    /**
     * Gets the notification title
     *
     * @return string
     */
    public function getTitle(){
   
        switch ($this->key) {

            case 'project_creation':
                return Yii::t('app', 'A new project has been created');
            case 'extension_approval':
                return Yii::t('app', 'Approval of date extension request');
            case 'extension_request_reject':
                return Yii::t('app', 'Rejection of date extension request');
            case 'iteration_request_approval':
                return Yii::t('app', 'Approval of iteration request');
            case 'iteration_request_reject':
                return Yii::t('app', 'Rejection of iteration request');
            case 'indicator_request_reject':
                return Yii::t('app', 'Rejection of indicator request');
            case 'project_assement':
                return Yii::t('app', 'Milestone has been assessed');
            case 'milestone_repeat':
                return Yii::t('app', 'Milestone Repeat');
        }
    }

    /**
     * Gets the notification description
     *
     * @return string
     */
    public function getDescription(){
        switch ($this->key) {
        case 'project_creation':
                $request = Project::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'Project Name:<br/> {milestone}', [
                    'milestone' => $request->name
                ]);
                }
                return;
        case 'extension_approval':
                $request = ExtensionRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone0->milestone
                ]);
                }
                return;
        case 'extension_request_reject':
                $request = ExtensionRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone0->milestone
                ]);
                }
                return;
        case 'iteration_request_approval':
                $request = VIterationRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone_name
                ]);
                }
                return;
         case 'iteration_request_reject':
                $request = VIterationRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone_name
                ]);
                }
                return;
        case 'indicator_request_approval':
                $request = IndicatorRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone0->milestone
                ]);
                }
                return;
        case 'indicator_request_reject':
                $request = IndicatorRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $request->milestone0->milestone
                ]);
                }
                return;
        case 'project_assement':
                $request = Grading::findOne(['id'=>$this->key_id]);
                if($request){
                $milestone_submission = VMilestoneSubmission::findOne(['id'=>$request->milestone_submission]);
                return Yii::t('app', 'For milestone:<br/> {milestone}', [
                    'milestone' => $milestone_submission->milestone
                ]);
                }
                return;
        case 'milestone_repeat':
                $request = Iteration::findOne(['id'=>$this->key_id]);
                if($request){
                return Yii::t('app', 'Of milestone:<br/> {milestone}', [
                    'milestone' => Milestone::findOne(Timeline::findOne($request->timeline)->milestone)->milestone
                ]);
                }
                return;
        }

    }
    
    /**
     * Gets the notification route
     *
     * @return string
     */
    public function getRoute(){
		switch ($this->key) {
            case 'milestone_submission':
                $request = VMilestoneSubmission::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone_id.'?project='.$request->project_id];
                }else{
                return ['/project'];
                }
            case 'project_creation':
                $request = Project::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/'.$request->id];
                }else{
                return ['/project'];
                }
            case 'extension_approval':
                $request = ExtensionRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone.'?project='.$request->project];
                }else{
                return ['/project'];
                }
            case 'extension_request_reject':
                $request = ExtensionRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone.'?project='.$request->project];
                }else{
                return ['/project'];
                }
            case 'iteration_request_approval':
                $request = VIterationRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone_id.'?project='.$request->project_id];
                }else{
                return ['/project'];
                }
            case 'iteration_request_reject':
                $request = VIterationRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone_id.'?project='.$request->project_id];
                }else{
                return ['/project'];
                }
            case 'indicator_request_approval':
                $request = IndicatorRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone_id.'?project='.$request->project_id];
                }else{
                return ['/project'];
                }
            case 'indicator_request_reject':
                $request = IndicatorRequests::findOne(['id'=>$this->key_id]);
                if($request){
                return ['/project/review/'.$request->milestone_id.'?project='.$request->project_id];
                }else{
                return ['/project'];
                }
            case 'project_assement':
                $request = Grading::findOne(['id'=>$this->key_id]);
                if($request){
                $iteration =MilestoneSubmission::findOne($request->milestone_submission)->iteration;
                $submission = VMilestoneSubmission::findOne($iteration);
                return ['/project/review/'.$submission->milestone_id.'?project='.$submission->project_id];
                }else{
                return ['/project'];
                }
            case 'milestone_repeat':
                $request = Iteration::findOne(['id'=>$this->key_id]);
                if($request){
                $timeline =Timeline::findOne($request->timeline);
                $submission = VMilestoneSubmission::findOne($iteration);
                return ['/project/review/'.$timeline->milestone.'?project='.$timeline->project];
                }else{
                return ['/project'];
                }
        };
    }

    /**
     * @inheritdoc
     */
    static function tableName()
    {
        return '{{%notification_innovator}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'user_id', 'key', 'created_at'], 'required'],
            [['id', 'key_id', 'created_at'], 'safe'],
            [['key_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * Creates a notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The foreign instance id
     * @param string $type
     * @return bool Returns TRUE on success, FALSE on failure
     * @throws \Exception
     */
    public static function notify($key, $user_id, $key_id = null, $type = self::TYPE_DEFAULT)
    {
        $notification = NotificationInstitution::className();
        //@overide the vendor class function
        //return NotificationsModule::notify(new $class(), $key, $user_id, $key_id, $type);
        /** @var Notification $instance */

        $instance = $notification::findOne(['user_id' => $user_id, 'key' => $key, 'key_id' => $key_id]);
        if (!$instance) {
            $instance = new $notification([
                'key' => $key,
                'type' => $type,
                'seen' => 0,
                'user_id' => $user_id,
                'key_id' => $key_id,
                'created_at' => new Expression('NOW()'),
            ]);
            return $instance->save();
        }

    }

    /**
     * Creates a warning notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function warning($key, $user_id, $key_id = null)
    {
        return static::notify($key, $user_id, $key_id, self::TYPE_WARNING);
    }


    /**
     * Creates an error notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function error($key, $user_id, $key_id = null)
    {
        return static::notify($key, $user_id, $key_id, self::TYPE_ERROR);
    }


    /**
     * Creates a success notification
     *
     * @param string $key
     * @param integer $user_id The user id that will get the notification
     * @param integer $key_id The notification key id
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function success($key, $user_id, $key_id = null)
    {
        return static::notify($key, $user_id, $key_id, self::TYPE_SUCCESS);
    }

}