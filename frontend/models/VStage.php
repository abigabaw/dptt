<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_stage".
 *
 * @property integer $id
 * @property string $stage
 * @property string $description
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $institution_name
 * @property integer $institution_id
 */
class VStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_stage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'institution_id'], 'integer'],
            [['stage', 'description', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['stage', 'description', 'institution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stage' => Yii::t('app', 'Stage'),
            'description' => Yii::t('app', 'Description'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return VStageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VStageQuery(get_called_class());
    }
}
