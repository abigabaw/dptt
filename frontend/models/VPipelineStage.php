<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_pipeline_stage".
 *
 * @property integer $pipeline
 * @property integer $stage
 * @property integer $ordering
 * @property string $pipeline_name
 * @property string $pipeline_description
 * @property string $stage_name
 * @property string $stage_description
 */
class VPipelineStage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_pipeline_stage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pipeline', 'stage', 'pipeline_name', 'pipeline_description', 'stage_name', 'stage_description'], 'required'],
            [['pipeline', 'stage', 'ordering'], 'integer'],
            [['pipeline_name', 'pipeline_description', 'stage_name', 'stage_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pipeline' => Yii::t('app', 'Pipeline'),
            'stage' => Yii::t('app', 'Stage'),
            'ordering' => Yii::t('app', 'Ordering'),
            'pipeline_name' => Yii::t('app', 'Pipeline Name'),
            'pipeline_description' => Yii::t('app', 'Pipeline Description'),
            'stage_name' => Yii::t('app', 'Stage Name'),
            'stage_description' => Yii::t('app', 'Stage Description'),
        ];
    }

    /**
     * @inheritdoc
     * @return VPipelineStageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VPipelineStageQuery(get_called_class());
    }
}
