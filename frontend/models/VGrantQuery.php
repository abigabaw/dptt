<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VGrant]].
 *
 * @see VGrant
 */
class VGrantQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VGrant[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VGrant|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}