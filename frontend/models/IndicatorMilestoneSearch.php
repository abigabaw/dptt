<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\IndicatorMilestone;

/**
 * IndicatorMilestoneSearch represents the model behind the search form about `backend\models\IndicatorMilestone`.
 */
class IndicatorMilestoneSearch extends IndicatorMilestone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'indicator', 'milestone', 'project'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IndicatorMilestone::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'indicator' => $this->indicator,
            'milestone' => $this->milestone,
            'project' => $this->project,
        ]);

        return $dataProvider;
    }
}
