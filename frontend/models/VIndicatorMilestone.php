<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_indicator_milestone".
 *
 * @property integer $indicator_id
 * @property string $indicator
 * @property integer $indicator_type
 * @property string $indicator_type_name
 * @property integer $milestone_id
 * @property string $milestone
 * @property integer $project_id
 * @property string $project
 */
class VIndicatorMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_indicator_milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_id', 'milestone_id', 'project_id'], 'required'],
            [['indicator_id', 'indicator_type', 'milestone_id', 'project_id'], 'integer'],
            [['indicator', 'indicator_type_name', 'milestone', 'project'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'indicator_id' => Yii::t('backend', 'Indicator ID'),
            'indicator' => Yii::t('backend', 'Indicator'),
            'indicator_type' => Yii::t('backend', 'Indicator Type'),
            'indicator_type_name' => Yii::t('backend', 'Indicator Type Name'),
            'milestone_id' => Yii::t('backend', 'Milestone ID'),
            'milestone' => Yii::t('backend', 'Milestone'),
            'project_id' => Yii::t('backend', 'Project ID'),
            'project' => Yii::t('backend', 'Project'),
        ];
    }
}
