<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use common\models\LoginInstitution;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "cohort".
 *
 * @property integer $id
 * @property string $name
 * @property integer $pipeline
 * @property integer $grant
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property LoginInstitution $createdBy
 * @property Pipeline $pipeline0
 * @property Grant $grant0
 * @property Project[] $projects
 */
class Program extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',  'grant', 'description'], 'required'],
		['pipeline','default','value'=>Yii::$app->session['pipeline']],
            [['grant', 'created_by','updated_by','created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

       /**
     * @inheritdoc
     */
	    public function behaviors()
	    {
	        return [
	           [ 'class'=>TimestampBehavior::className(),
			    'value'=>new Expression('NOW()')
			],
	            BlameableBehavior::className(),
	        ];
	    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'pipeline' => 'Pipeline',
            'grant' => 'Grant',
            'description' => 'Program Description',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(LoginInstitution::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipeline0()
    {
        return $this->hasOne(Pipeline::className(), ['id' => 'pipeline']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrant0()
    {
        return $this->hasOne(Grant::className(), ['id' => 'grant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['cohort' => 'id']);
    }

}