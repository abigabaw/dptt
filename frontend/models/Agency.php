<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "agency".
 *
 * @property integer $id
 * @property integer $admin
 * @property string $name
 * @property string $email
 * @property string $contact
 * @property string $description
 * @property string $auth_key
 * @property string $password
 * @property string $token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Admin $admin0
 * @property Grant[] $grants
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'contact', 'description'], 'required'],
            [[ 'created_at', 'updated_at'], 'integer'],
            [['name', 'email', 'contact', 'description'], 'string', 'max' => 255],
            [['email'], 'unique']
        ];
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin' => 'Admin',
            'name' => 'Name',
            'email' => 'Email',
            'contact' => 'Contact',
            'description' => 'Description',
            'auth_key' => 'Auth Key',
            'password' => 'Password',
            'token' => 'Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin0()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrants()
    {
        return $this->hasMany(Grant::className(), ['agency' => 'id']);
    }
}
