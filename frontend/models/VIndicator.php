<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_indicator".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $update_at
 * @property string $institution_name
 * @property integer $institution_id
 */
class VIndicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_indicator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'institution_id'], 'integer'],
            [['created_at', 'created_by', 'updated_by', 'update_at'], 'required'],
            [['created_at', 'update_at'], 'safe'],
            [['name', 'description', 'institution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'update_at' => Yii::t('app', 'Update At'),
            'institution_name' => Yii::t('app', 'Institution Name'),
            'institution_id' => Yii::t('app', 'Institution ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return VIndicatorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VIndicatorQuery(get_called_class());
    }
}
