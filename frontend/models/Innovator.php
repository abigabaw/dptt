<?php

namespace frontend\models;

use Yii;
use common\models\Institution;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "innovator".
 *
 * @property integer $id
 * @property integer $instituition
 * @property string $email
 * @property string $names
 * @property string $description
 * @property integer $country
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property LoginInstitution $instituition0
 * @property Country $country0
 * @property LoginInnovator[] $loginInnovators
 */
class Innovator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'innovator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instituition'], 'required'],
            [['instituition', 'country', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['email', 'names'], 'string', 'max' => 255]
        ];
    }

              /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instituition' => 'Instituition',
            'email' => 'Email',
            'names' => 'Names',
            'description' => 'Description',
            'country' => 'Country',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstituition0()
    {
        return $this->hasOne(Institution::className(), ['id' => 'instituition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry0()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoginInnovators()
    {
        return $this->hasMany(LoginInnovator::className(), ['innovator' => 'id']);
    }
}
