<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\IndicatorMilestone;
use yii\data\SqlDataProvider;

/**
 * IndicatorMilestoneSearch represents the model behind the search form about `backend\models\IndicatorMilestone`.
 */
class IndicatorMilestoneSubSearch extends IndicatorMilestone
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'indicator', 'milestone', 'project'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$project,$stage,$pipeline)
    {

        $sql        = 'SELECT a.pipeline AS pipeline_id, d.name AS pipeline, a.stage AS stage_id, e.stage, b.milestone AS milestone_id, f.milestone, c.indicator AS indicator_id, g.name AS indicator
FROM pipeline_stage a
LEFT JOIN milestone_stage b ON a.stage = b.stage
LEFT JOIN static_indicator_milestone c ON b.milestone = c.milestone
LEFT JOIN pipeline d ON a.pipeline = d.id
LEFT JOIN stage e ON b.stage = e.id
LEFT JOIN milestone f ON b.milestone = f.id
LEFT JOIN indicator g ON c.indicator = g.id
WHERE a.stage='.$stage.' AND a.pipeline ='.$pipeline.' GROUP BY b.milestone 
UNION 
SELECT a.pipeline AS pipeline_id, d.name AS pipeline, a.stage AS stage_id, e.stage, b.milestone AS milestone_id,
f.milestone, c.indicator AS indicator_id, g.name AS indicator
FROM pipeline_stage a
LEFT JOIN milestone_stage b ON a.stage = b.stage
LEFT JOIN indicator_milestone c ON b.milestone = c.milestone
LEFT JOIN pipeline d ON a.pipeline = d.id
LEFT JOIN stage e ON b.stage = e.id
LEFT JOIN milestone f ON b.milestone = f.id
LEFT JOIN indicator g ON c.indicator = g.id
WHERE c.project='.$project.' AND a.stage='.$stage.' AND a.pipeline ='.$pipeline.' GROUP BY b.milestone';
        $count      =   Yii::$app->db->createCommand($sql)->queryScalar();
        //$query = IndicatorMilestone::findBySql($sql)->all();

        $dataProvider = new SqlDataProvider([
                    'sql' => $sql,
                    'totalCount' => $count,
                    ]);
        return $dataProvider;
    }
}