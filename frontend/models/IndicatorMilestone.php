<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "indicator_milestone".
 *
 * @property integer $id
 * @property integer $indicator
 * @property integer $milestone
 * @property integer $project
 *
 * @property Project $project0
 * @property Indicator $indicator0
 * @property Milestone $milestone0
 */
class IndicatorMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator', 'milestone', 'project'], 'required'],
            [['indicator', 'milestone', 'project'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator' => 'Indicator',
            'milestone' => 'Milestone',
            'project' => 'Project',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator0()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
}
