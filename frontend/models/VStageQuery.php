<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VStage]].
 *
 * @see VStage
 */
class VStageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VStage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VStage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}