<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VMilestoneStage]].
 *
 * @see VMilestoneStage
 */
class VMilestoneStageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VMilestoneStage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VMilestoneStage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}