<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "pipeline".
 *
 * @property integer $id
 * @property integer $name
 *
 * @property Cohort[] $cohorts
 * @property PipelineStage[] $pipelineStages
 */
class Pipeline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pipeline';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    public function rules()
    {
        return [
            [['name','description'], 'required'],
            [['name','description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCohorts()
    {
        return $this->hasMany(Cohort::className(), ['pipeline' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPipelineStages()
    {
        return $this->hasMany(PipelineStage::className(), ['pipeline' => 'id']);
    }
}
