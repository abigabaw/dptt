<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

/**
 * TimelineSearch represents the model behind the search form about `backend\models\Timeline`.
 */
class DashboardSearch extends Timeline
{

    public function search($params)
    {
         $sql        = 'SELECT * FROM `v_delayed_milestone` WHERE  finish_date IS NOT NULL
                          AND expected_finish_date <= date\''.date('Y-m-d').'\' AND pipeline='.Yii::$app->session['pipeline'].' GROUP BY timeline
                        
                        ';

        $count      =   count(Yii::$app->db->createCommand($sql)->queryAll());

        $dataProvider = new SqlDataProvider([
                    'sql' => $sql,
                    'totalCount' => $count,
                    ]);

 

        return $dataProvider;
    }
}