<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "indicator".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 *
 * @property IndicatorMilestone[] $indicatorMilestones
 * @property IndicatorMilestoneSubmission[] $indicatorMilestoneSubmissions
 */
class Indicator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator';
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at'], 'safe'],
            [['name','description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestones()
    {
        return $this->hasMany(IndicatorMilestone::className(), ['indicator' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestoneSubmissions()
    {
        return $this->hasMany(IndicatorMilestoneSubmission::className(), ['indicator' => 'id']);
    }
}
