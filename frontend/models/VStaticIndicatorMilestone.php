<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_static_indicator_milestone".
 *
 * @property integer $indicator_id
 * @property string $indicator
 * @property integer $indicator_type
 * @property string $indicator_type_name
 * @property integer $milestone_id
 * @property string $milestone
 */
class VStaticIndicatorMilestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_static_indicator_milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator_id', 'milestone_id'], 'required'],
            [['indicator_id', 'indicator_type', 'milestone_id'], 'integer'],
            [['indicator', 'indicator_type_name', 'milestone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'indicator_id' => Yii::t('app', 'Indicator ID'),
            'indicator' => Yii::t('app', 'Indicator'),
            'indicator_type' => Yii::t('app', 'Indicator Type'),
            'indicator_type_name' => Yii::t('app', 'Indicator Type Name'),
            'milestone_id' => Yii::t('app', 'Milestone ID'),
            'milestone' => Yii::t('app', 'Milestone'),
        ];
    }
}
