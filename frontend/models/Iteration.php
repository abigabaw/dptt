<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "iteration".
 *
 * @property integer $id
 * @property integer $timeline
 * @property integer $project
 * @property string $comment
 * @property string $start_date
 * @property integer $duration
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Iteration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iteration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timeline', 'start_date', 'duration'], 'required'],
            [['timeline',  'created_by'], 'integer'],
            [['start_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'timeline' => Yii::t('app', 'Timeline'),
            'start_date' => Yii::t('app', 'Start Date'),
            'duration' => Yii::t('app', 'Duration'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
