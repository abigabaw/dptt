<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "milestone".
 *
 * @property integer $id
 * @property integer $num
 * @property string $milestone
 *
 * @property DateIteration[] $dateIterations
 * @property Grading[] $gradings
 * @property IndicatorMilestone[] $indicatorMilestones
 * @property IndicatorMilestoneSubmission[] $indicatorMilestoneSubmissions
 * @property Iteration[] $iterations
 * @property MilestoneStage[] $milestoneStages
 */
class Milestone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'milestone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num', 'milestone'], 'required'],
            [['num'], 'integer'],
            [['milestone','description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'num' => 'Num',
            'milestone' => 'Milestone',
            'description'=>'Description',
        ];
    }
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
		    'value'=>new Expression('NOW()')
		],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDateIterations()
    {
        return $this->hasMany(DateIteration::className(), ['milestone' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradings()
    {
        return $this->hasMany(Grading::className(), ['milestone' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestones()
    {
        return $this->hasMany(IndicatorMilestone::className(), ['milestone' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestoneSubmissions()
    {
        return $this->hasMany(IndicatorMilestoneSubmission::className(), ['milestone' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIterations()
    {
        return $this->hasMany(Iteration::className(), ['milestone' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestoneStages()
    {
        return $this->hasMany(MilestoneStage::className(), ['milestone' => 'id']);
    }
}
