<?php 
namespace frontend\models;
use Yii;

class Queries{
  public static function ReportQuery($project){
  	return 'SELECT a.milestone as milestone_id, b.timeline, 
  			(SELECT SUM(duration) FROM iteration WHERE timeline=b.timeline) AS total_duration,
  			 b.comment, b.start_date, b.duration, e.created_at, c.milestone, c.num, d.stage as stage_id, f.stage, e.attachment FROM timeline
  			 a LEFT JOIN iteration b ON b.timeline=a.id LEFT JOIN milestone c ON a.milestone=c.id LEFT JOIN
  			 milestone_stage d on a.milestone=d.milestone LEFT JOIN milestone_submission e ON a.milestone=e.milestone
  			  AND e.project='.$project.' LEFT JOIN stage f ON d.stage = f.id WHERE b.project='.$project.' GROUP BY a.milestone ORDER BY b.timeline ASC';
  }

  public static function qry($project){
  	return 'SELECT a.id as timeline, a.milestone as milestone_id,a.comment, a.start_date, a.duration, b.milestone, 
        b.num FROM timeline a LEFT JOIN milestone b 
        ON a.milestone=b.id
         WHERE a.project='.$project.' ORDER BY a.start_date ASC';


  }
  public static function qry2($project){
  	return  'SELECT * from v_milestone_submission
        where  project='.$project.'
                GROUP BY project_id
                ORDER BY created_at ASC';
  }
    public static function qryMilestoneSubmission($project){
    return  'SELECT * from v_milestone_submission
        where  project_id='.$project.'
                ORDER BY start_date ASC';
  }
  public static function qry3($project){
  	return 'SELECT reason, duration, created_at FROM `extension_requests` WHERE project = '.$project.'';
  }
  public static function qryIteration($project){
  	return 'SELECT * from v_milestone_submission WHERE iteration_type_name=\'Iteration\'';
  }
  public static function qryRepeat($repeat){
    return 'SELECT * from v_milestone_submission WHERE iteration_type_name=\'Repeat\'';
  }
  public static function qryExtension($repeat){
    return 'SELECT * from v_milestone_submission WHERE iteration_type_name=\'Extension\'';
  }
  public static function qryDelayed($project){
  	return 'SELECT a.project_id, a.project, iteration_type,( SELECT COUNT( DISTINCT milestone_id ) 
          FROM v_delayed_milestone WHERE project_id = '.$project.' AND finish_date is null AND
           expected_finish_date is not null
          AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' ) AS 
          number_of_milestones FROM v_delayed_milestone a
          WHERE finish_date is null AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' AND
          a.project = '.$project.'';
  }

  public static function qryOverDue(){
  return 'SELECT a.project_id, a.project, iteration_type,( SELECT COUNT( DISTINCT milestone_id ) 
          FROM v_delayed_milestone WHERE project_id = a.project_id AND finish_date is null AND expected_finish_date is not null
          AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' ) AS 
          number_of_milestones, ( SELECT COUNT( DISTINCT milestone ) 
          FROM timeline WHERE project = a.project_id) AS 
          number_of_assigned_milestones FROM v_delayed_milestone a
          WHERE finish_date is null AND date(a.expected_finish_date)  <= date\''.date('Y-m-d').'\' and pipeline =\''.Yii::$app->session['pipeline'].'\'
                
                    GROUP BY project_id';
}


  public static function qryLateSubmission($project){
    return 'SELECT * FROM `v_delayed_milestone` WHERE  finish_date IS NOT NULL
            AND  project = '.$project.' AND expected_finish_date <= \''.date('Y-m-d').'\'';

  }

  public static function qryCummulative($project){
  	return 'SELECT * FROM v_cummulative_scores_indicator WHERE project_id='.$project.'
  	ORDER BY id ASC';
  }
  public static function qryCummulativeAll(){
    return 'SELECT * FROM v_cummulative_scores_indicator
            GROUP BY project_id ORDER BY id ASC';
  }

  public static function MilestoneSubmissionsAll(){
    return 'SELECT * from v_milestone_submission
        where pipeline=\''.Yii::$app->session['pipeline'].'\'
                GROUP BY project_id
                ORDER BY created_at ASC';
  }


  public static function getIndicatorRequests($institution){
    return 'SELECT a.id,a.project as project_id, b.name as project_name, a.milestone FROM `indicator_requests`
            a LEFT JOIN project b on a.project=b.id left join innovator c on b.innovator = c.id where
            c.instituition = '.$institution.' and status=0';
  }

  public static function getRepeatRequests($institution){
    return 'SELECT a.id,a.project as project_id, b.name as project_name, a.milestone FROM `repeat_requests`
           a LEFT JOIN project b on a.project=b.id left join innovator c on b.innovator = c.id where c.instituition ='.$institution.' 
           and status=0';
  }

  public static function getExtensionRequests($institution){
    return 'SELECT a.id,a.project as project_id, b.name as project_name, a.milestone FROM `extension_requests` a 
            LEFT JOIN project b on a.project=b.id left join innovator c on b.innovator = c.id where c.instituition ='.$institution.'
            and status=0';
  }

  public static function SQLApproved($project){
    return 'SELECT a.grade, a.comment, d.milestone as milestone_id, e.milestone from grading a 
            left join milestone_submission b on a.milestone_submission=b.id left join iteration c
            on b.iteration=c.id left join timeline d on c.timeline=d.id left join milestone e
            on d.milestone=e.id WHERE a.status=1';

  }
  public static function SQLRejected($project){
    return 'SELECT a.grade, a.comment, d.milestone as milestone_id, e.milestone from grading a 
            left join milestone_submission b on a.milestone_submission=b.id left join iteration c
            on b.iteration=c.id left join timeline d on c.timeline=d.id left join milestone e
            on d.milestone=e.id WHERE a.status=2';
  }
}
?>