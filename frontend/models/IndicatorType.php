<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "indicator_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property IndicatorTypeMap[] $indicatorTypeMaps
 */
class IndicatorType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorTypeMaps()
    {
        return $this->hasMany(IndicatorTypeMap::className(), ['indicator_type' => 'id']);
    }
}
