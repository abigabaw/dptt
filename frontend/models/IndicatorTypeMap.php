<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "indicator_type_map".
 *
 * @property integer $id
 * @property integer $indicator
 * @property integer $indicator_type
 *
 * @property IndicatorType $indicatorType
 * @property Indicator $indicator0
 */
class IndicatorTypeMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indicator_type_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['indicator', 'indicator_type'], 'required'],
            [['indicator', 'indicator_type'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indicator' => 'Indicator',
            'indicator_type' => 'Indicator Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorType()
    {
        return $this->hasOne(IndicatorType::className(), ['id' => 'indicator_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicator0()
    {
        return $this->hasOne(Indicator::className(), ['id' => 'indicator']);
    }
}
