<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[VPipeline]].
 *
 * @see VPipeline
 */
class VPipelineQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VPipeline[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VPipeline|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}