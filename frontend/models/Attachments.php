<?php

namespace frontend\models;


use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "attachments".
 *
 * @property integer $id
 * @property integer $milestone_submission
 * @property string $attachment
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property MilestoneSubmission $milestoneSubmission
 */
class Attachments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['milestone_submission', 'attachment'], 'required'],
            [['milestone_submission', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['attachment'], 'string', 'max' => 255],
            [['milestone_submission'], 'exist', 'skipOnError' => true, 'targetClass' => MilestoneSubmission::className(), 'targetAttribute' => ['milestone_submission' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
            'value'=>new Expression('NOW()')
        ],
            BlameableBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'milestone_submission' =>'Milestone Submission',
            'attachment' => 'Attachment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestoneSubmission()
    {
        return $this->hasOne(MilestoneSubmission::className(), ['id' => 'milestone_submission']);
    }
}
