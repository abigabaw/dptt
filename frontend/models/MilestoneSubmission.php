<?php

namespace frontend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "milestone_submission".
 *
 * @property integer $id
 * @property integer $milestone
 * @property integer $project
 * @property string $narration
 * @property string $attachment
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property IndicatorMilestoneSubmission[] $indicatorMilestoneSubmissions
 * @property Project $project0
 * @property Milestone $milestone0
 */
class MilestoneSubmission extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'milestone_submission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iteration','narration', ], 'required'],
            [['file'], 'file','extensions' => 'pdf, xls, doc, docx','maxFiles' => 4],
            [['iteration', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['narration'], 'string', 'max' => 500]
        ];
    }
          /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           [ 'class'=>TimestampBehavior::className(),
            'value'=>new Expression('NOW()')
        ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'milestone_submission' => 'Project',
            'narration' => 'Narration',
            'status' => 'Status',
            'file' => 'Attachment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function upload($project,$milestone,$id)
    { 
            foreach ($this->file as $file) {
                if($this->file){
                $base_name = $file->baseName;
                $time = time();
                $file_name = $time.'_'.$milestone.'_'.$project.'_'.$base_name;
                $file->saveAs('uploads/'.$file_name.'.'.$file->extension);
                $attachment                         = new Attachments();
                $attachment->milestone_submission   = $id;
                $attachment->attachment             = 'uploads/'.$file_name.'.'.$file->extension;
                $attachment->save();
                }
            }
            return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicatorMilestoneSubmissions()
    {
        return $this->hasMany(IndicatorMilestoneSubmission::className(), ['milestone_submission' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMilestone0()
    {
        return $this->hasOne(Milestone::className(), ['id' => 'milestone']);
    }
}