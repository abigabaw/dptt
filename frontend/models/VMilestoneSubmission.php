<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "v_milestone_submission".
 *
 * @property integer $id
 * @property integer $iteration
 * @property string $narration
 * @property integer $status
 * @property string $start_date
 * @property integer $duration
 * @property string $end_date
 * @property string $iteration_type
 * @property string $comment
 * @property double $grade
 * @property string $supervisor_comment
 * @property integer $grading_status
 * @property integer $milestone_id
 * @property integer $milestone_no
 * @property string $milestone
 * @property integer $project_id
 * @property string $project
 * @property integer $pipeline
 * @property string $created_at
 */
class VMilestoneSubmission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_milestone_submission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iteration', 'status', 'duration', 'grading_status', 'milestone_id', 'milestone_no', 'project_id', 'pipeline'], 'integer'],
            [['iteration', 'narration'], 'required'],
            [['start_date', 'end_date', 'created_at'], 'safe'],
            [['iteration_type', 'supervisor_comment'], 'string'],
            [['grade'], 'number'],
            [['narration'], 'string', 'max' => 500],
            [['comment', 'milestone', 'project'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'iteration' => Yii::t('app', 'Iteration'),
            'narration' => Yii::t('app', 'Narration'),
            'status' => Yii::t('app', 'Status'),
            'start_date' => Yii::t('app', 'Start Date'),
            'duration' => Yii::t('app', 'Duration'),
            'end_date' => Yii::t('app', 'End Date'),
            'iteration_type' => Yii::t('app', 'Iteration Type'),
            'comment' => Yii::t('app', 'Comment'),
            'grade' => Yii::t('app', 'Grade'),
            'supervisor_comment' => Yii::t('app', 'Supervisor Comment'),
            'grading_status' => Yii::t('app', 'Grading Status'),
            'milestone_id' => Yii::t('app', 'Milestone ID'),
            'milestone_no' => Yii::t('app', 'Milestone No'),
            'milestone' => Yii::t('app', 'Milestone'),
            'project_id' => Yii::t('app', 'Project ID'),
            'project' => Yii::t('app', 'Project'),
            'pipeline' => Yii::t('app', 'Pipeline'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
