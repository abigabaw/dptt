<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Queries;
/*
How to run the widget:

use frontend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class OverdueProject extends Widget{
	public $overdue_milestones;
	public $overdue_milestone_count;
	public function init(){
    $this->overdue_milestones = Yii::$app->db->createCommand(Queries::qryOverDue())->queryAll();
    $this->overdue_milestone_count = count($this->overdue_milestones);
	}
	public function run(){
	return $this->render('/widget/overdueprojects',
						['overdue_milestones' 			=> $this->overdue_milestones,
						 'overdue_milestone_count'		=>$this->overdue_milestone_count
						]);
	}
}
?>