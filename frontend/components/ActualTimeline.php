<?php
namespace frontend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Queries;
use frontend\models\IterationType;
/*
How to run the widget:

use frontend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class ActualTimeline extends Widget{
	public $id;
	public $elementid;
  public $normal_color;
  public $repeat_color;
  public $iteration_color;

	//Data for normal progress
  public $dataset_normal;
	public $dataset_data_normal;
  
  //Data for extension of normal progress
  public $dataset_normal_extension;
  public $dataset_data_normal_extension;
	
  //Data for repeat of milestone
  public $dataset_repeat;
	public $dataset_data_repeat;
  //Data for extension of repeat of milestone
  public $dataset_repeat_extension;
  public $dataset_data_repeat_extension;

  
  //Data for iteration of milestone
	public $dataset_iteration;
	public $dataset_data_iteration;
  //Data for extension of iteration of milestone
  public $dataset_iteration_extension;
  public $dataset_data_iteration_extension;


	public function init(){
	$qry                 = Yii::$app->db->createCommand(Queries::qryMilestoneSubmission($this->id))->queryAll();
  $this->normal_color     = IterationType::findOne(1)->color_code;
  $this->repeat_color     = IterationType::findOne(3)->color_code;
  $this->iteration_color  = IterationType::findOne(5)->color_code;
    foreach($qry as $key => $ar){
    	switch($ar['iteration_type']){
    	case 1:
            $this->dataset_normal .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Started milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_normal .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
        	//set other values to null
        break;
      case 2:
            $this->dataset_normal_extension .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal_extension .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Extended milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
            
            $this->dataset_normal_extension .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_normal_extension .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
          //set other values to null
            $this->dataset_normal .= '{ x: '.strtotime($ar['start_date']).'000, y: null },';
            $this->dataset_normal .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
    


        break;
      case 3:
        	  $this->dataset_repeat .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_repeat .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Started repeat of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_repeat .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_repeat .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted repeat of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
      	break;
      case 4:
            $this->dataset_repeat_extension .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_repeat_extension .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Extended milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
            
            $this->dataset_repeat_extension .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_repeat_extension .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted  of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
            
            $this->dataset_repeat .= '{ x: '.strtotime($ar['start_date']).'000, y: null },';
            $this->dataset_repeat .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
    

        break;
      case 5:
        	  $this->dataset_iteration .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_iteration .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Started iteration of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
           	$this->dataset_iteration .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
            $this->dataset_data_iteration .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted iteration of milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
      	   

        break;
      	case 6:
        	$this->dataset_iteration_extension .= '{ x: '.strtotime($ar['start_date']).'000, y: '.$ar['milestone_id'].' },';
          $this->dataset_data_iteration_extension .='{ x: '.strtotime($ar['start_date']).'000, text: \'<b>Extended milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
           	
          $this->dataset_iteration_extension .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
          $this->dataset_data_iteration_extension .='{ x: '.strtotime($ar['end_date']).'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $ar['milestone']).'<br/>\' },';
      	
        	$this->dataset_iteration .= '{ x: '.strtotime($ar['start_date']).'000, y: null },';
          $this->dataset_iteration .= '{ x: '.strtotime($ar['end_date']).'000, y: '.$ar['milestone_id'].' },';
      	break;
        }	
        }
        $this->dataset_normal 			                 = rtrim($this->dataset_normal);
        $this->dataset_data_normal 		               = rtrim($this->dataset_data_normal);

        $this->dataset_normal_extension              = rtrim($this->dataset_normal_extension);
        $this->dataset_data_normal_extension         = rtrim($this->dataset_data_normal_extension);

        $this->dataset_repeat 			                 = rtrim($this->dataset_repeat);
        $this->dataset_data_repeat 		               = rtrim($this->dataset_data_repeat);

        $this->dataset_repeat_extension              = rtrim($this->dataset_repeat_extension);
        $this->dataset_data_repeat_extension         = rtrim($this->dataset_data_repeat_extension);

        $this->dataset_iteration 		                 = rtrim($this->dataset_iteration);
        $this->dataset_data_iteration 	             = rtrim($this->dataset_data_iteration);

        $this->dataset_iteration_extension           = rtrim($this->dataset_iteration_extension);
        $this->dataset_data_iteration_extension      = rtrim($this->dataset_data_iteration_extension);

    }

	public function run(){
	return $this->render('/widget/actualtimeline',
						[
            'normal_color'                      => $this->normal_color,
            'repeat_color'                      => $this->repeat_color,
            'iteration_color'                   => $this->iteration_color,
						'dataset_normal' 			              => $this->dataset_normal,
						'dataset_data_normal' 		          => $this->dataset_data_normal,
            'dataset_normal_extension'          => $this->dataset_normal_extension,
            'dataset_data_normal_extension'     => $this->dataset_data_normal_extension,
						'dataset_repeat' 			              => $this->dataset_repeat,
						'dataset_data_repeat' 		          => $this->dataset_data_repeat,
            'dataset_repeat_extension'          => $this->dataset_repeat_extension,
            'dataset_data_repeat_extension'     => $this->dataset_data_repeat_extension,
						'dataset_iteration' 		            => $this->dataset_iteration,
						'dataset_data_iteration' 	          => $this->dataset_data_iteration,
            'dataset_iteration_extension'       => $this->dataset_iteration,
            'dataset_data_iteration_extension'  => $this->dataset_data_iteration,
						'elementid'					                => $this->elementid,
						]);
	}
}
?>