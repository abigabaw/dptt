<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Queries;
/*
How to run the widget:

use frontend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class MilestoneSubmission extends Widget{
	public $dataProvider;
	public $dataset_cummulative;
	public $data;
    public $flags;
    public $max;
	public function init(){
        $qry = Yii::$app->db->createCommand(Queries::MilestoneSubmissionsAll())->queryAll();
        foreach ($qry as $qry) {
            $this->data .='{
                        type: \'spline\',
                        id: \'project-'.$qry['project_id'].'\',
                        dashStyle: \'line\',
                        name: \''.$qry['project'].'</em>\',
                        step:true,
                        data: ['.$this->getSubmissions($qry['project_id'],'data').'],
                        tooltip: {
                            xDateFormat: \'%B %Y\',
                            valueSuffix: \' '.$qry['project'].'\'
                        }
                    },';
            $this->max[]= $this->getMaxYaxis($qry['project_id']);
            // $flags .='{
            //             type: \'flags\',
            //             name: \'Proposed Timeline\',
            //             color: \'#333333\',
            //             shape: \'circlepin\',
            //             data: [
            //                 '.$this->getSubmissions($qry['project_id'],'flags').'
            //             ],
            //             showInLegend: false
            //          },';

        }  
            
        $this->data = rtrim($this->data, ',');
        $this->flags = rtrim($this->flags, ',');
        //$this->max = max($this->max);
	}

	private function getSubmissions($project,$scenario){
        $dataset ='';
        $dataset_data = '';
        $sql = 'SELECT * FROM `v_milestone_submission`
                    WHERE project_id ='.$project.' order by created_at asc';  
        $qry =  Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($qry as $qry) {
        $dataset .= '{ x: '.$qry['created_at'].'000, y: '.$qry['milestone_id'].' } ,';
        $dataset_data .='{ x: '.$qry['created_at'].'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $qry['milestone']).'<br/>\' } ,';
        
        }

        $dataset = rtrim($dataset, ',');
        $dataset_data = rtrim($dataset_data, ',');
        switch($scenario){
            case 'data':
            return $dataset;
            case 'flags':
            return $dataset_data;
        }

    }
	private function getMaxYaxis($project){
        $sql = 'SELECT max(milestone_no) as milestone_no FROM  v_milestone_submission
                    WHERE project_id ='.$project.'';
        $qry =  Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($qry as $qry) {
        $max =  $qry['milestone_no'];       
        }
        return $max; 

    }
	public function run(){
	return $this->render('/widget/milestonesubmission',
						['data' 			=> $this->data,
						'flags'=>$this->flags,
						'max_y'=>$this->max
						]);
	}
}
?>