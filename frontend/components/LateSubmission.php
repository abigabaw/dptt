<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use frontend\models\DashboardSearch;
/*
How to run the widget:

use frontend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class LateSubmission extends Widget{
	public $dataProvider;
	public function init(){
	$searchModel = new DashboardSearch();
    $this->dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	}
	public function run(){
	return $this->render('/widget/latesubmission',
						['dataProvider' 			=> $this->dataProvider,
						]);
	}
}
?>