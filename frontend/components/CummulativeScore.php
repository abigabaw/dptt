<?php
namespace backend\components;
use Yii;
use yii\base\Widget;
use frontend\models\Queries;
/*
How to run the widget:

use frontend\components\DPTTInfo;
<?=DPTTInfo::widget(['message' => ' Yii2.0']) ?>

*/
class CummulativeScore extends Widget{
	public $dataProvider;
	public $dataset_cummulative;
	public function init(){
	$qry_cummulative = Yii::$app->db->createCommand(Queries::qryCummulativeAll())->queryAll();
	foreach ($qry_cummulative as $qry) {
            if(isset($qry['project_id'])){
            $this->dataset_cummulative .='{
                        name: \'Cummulative Indicator Score\',
                        data: ['.$this->getCummulativeScores($qry['project_id'],'data').'],
                        tooltip:{
                                    headerFormat: \'<b>'.$qry['project_name'].'</b><br/>\',
                                  }
                    },';
            }
        }
	}

	private function getCummulativeScores($project_id,$scenario){
        $dataset_cummulative ='';
        $sql ='SELECT * FROM `v_cummulative_scores_indicator`
                 WHERE project_id='.$project_id.'
                ORDER BY id ASC';

    	$qry_cummulative = Yii::$app->db->createCommand($sql)->queryAll();
        $score = 0;
        foreach ($qry_cummulative as $qry) {
            $score +=$qry['grade']; 
            $dataset_cummulative .= '['.$qry['id'].','.$score.' ],';
            $tootlip = '{
                                    headerFormat: \'<b>'.$qry['project_id'].'</b><br/>\',
                                    pointFormat: \'<b>Indicator:</b>'.$qry['indicator'].' <br/> <b>Cummulative Score:</b> '.$score.' <br/><b>Actual Score:</b>'.$qry['grade'].'\'
                                  },';
            //$dataset_data2 .='{ x: '.$qry2['created_at'].'000, text: \'<b>Submitted milestone:</b> <br/>'.str_replace('\'', '', $qry2['milestone']).'<br/>\' },';
            
        }
        $dataset_cummulative = rtrim($dataset_cummulative);
        $tootlip = rtrim($tootlip);
        switch($scenario){
            case 'data':
            return $dataset_cummulative;
            case 'tootlip':
            return $tootlip;
        }
    }
	public function run(){
	return $this->render('/widget/cummulativescore',
						['dataset_cummulative' 			=> $this->dataset_cummulative,
						]);
	}
}
?>